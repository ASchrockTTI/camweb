import kamApp from '../app/kam.js';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularComponent } from './components/angularComponent.component';
import { UpgradeModule } from '@angular/upgrade/static';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
  BrowserModule,
    AppRoutingModule,
    UpgradeModule
  ],
  //bootstrap: [AppComponent]
})
export class AppModule { 

  constructor(private upgrade: UpgradeModule) { }
  ngDoBootstrap() {
    this.upgrade.bootstrap(document.documentElement, [kamApp.name]);
  }

}
