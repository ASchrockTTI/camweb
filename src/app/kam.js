import headerComponent from './components/header/header.component';
import {app, server, environment, roles, entity, session, keys, actionItemTypes} from './shared/app.constants';
import utilService from './shared/services/utilities/utilities.svc';
import templateService from './shared/services/utilities/template.svc';
import UserProfile from './shared/services/userProfile.svc';
import MockSessionService from './shared/services/mockSession.svc';
import userInfoService from './shared/services/userInfo.svc';
import userInfoResource from './shared/resources/userInfoResource.fact';
import sidebarComponent from './components/sidebar/sidebar.component';
import PermissionsFactory from './shared/factories/permissions.fact';
import { totalFilter, mainPageComponent } from './components/management/management.component';
import planService from './shared/services/plan.svc';
import planResource from './shared/resources/planResource.fact';
import NewsFeedService from './shared/services/newsfeed.svc';
import NewsFeedResource from './shared/resources/newsfeedResource.fact';
import OmsProjectFilter from './shared/filters/omsProjectFilter.filter';
import AltInfoFilter from './shared/filters/altInfoFilter.filter';
import BranchFilter from './shared/filters/branchFilter.filter';
import BusinessPartnerFilter from './shared/filters/businessPartnerFilter.filter';
import BusinessPartnerNameFilter from './shared/filters/businessPartnerNameFilter.filter';
import CommentFilter from './shared/filters/commentFilter.filter';
import EmptyToEnd from './shared/filters/emptyToEnd.filter';
import Initials from './shared/filters/initials.filter';
import Percentage from './shared/filters/percentage.filter';
import Truncate from './shared/filters/truncate.filter';
import TopTierFilter from './shared/filters/topTierFilter.filter';
import Titlecase from './shared/filters/titlecase.filter';
import StartFrom from './shared/filters/startFrom.filter';
import AuditFactory from './shared/factories/audit.fact';
import AccountResource from './shared/resources/accountResource.fact';
import ActionItemResource from './shared/resources/actionItemResource.fact';
import AdminResource from './shared/resources/adminResource.fact';
import ApprovalResource from './shared/resources/approvalResource.fact';
import businessPlanResource from './shared/resources/businessPlanResource.fact';
import CallReportResource from './shared/resources/callReportResource.fact';
import CommentResource from './shared/resources/commentResource.fact';
import commodityResource from './shared/resources/commodityResource.fact';
import ContactResource from './shared/resources/contactResource.fact';
import EmailResource from './shared/resources/emailResource.fact';
import entityResource from './shared/resources/entityResource.fact';
import FamilyResource from './shared/resources/familyResource.fact';
import FeedbackResource from './shared/resources/feedbackResource.fact';
import FinancialResource from './shared/resources/financialResource.fact';
import locationResource from './shared/resources/locationResource.fact';
import mfrResource from './shared/resources/mfrResource.fact';
import notificationResource from './shared/resources/notificationResource.fact';
import NuggetsResource from './shared/resources/nuggetsResource.fact';
import OmsResource from './shared/resources/omsResource.fact';
import OverviewResource from './shared/resources/overviewResource.fact';
import PendingCustomerResource from './shared/resources/pendingCustomerResource.fact';
import PreferencesResource from './shared/resources/preferencesResource.fact';
import SalesIQResource from './shared/resources/salesIQResource.fact';
import StrategyResource from './shared/resources/strategyResource.fact';
import TacticDelegateResource from './shared/resources/tacticDelegateResource.fact';
import TacticResource from './shared/resources/tacticResource.fact';
import TmwResource from './shared/resources/tmwResource.fact';
import VbsResource from './shared/resources/vbsResource.fact';
import AdminService from './shared/services/admin.svc';
import AccountService from './shared/services/account.svc';
import ApprovalService from './shared/services/approval.svc';
import CommentService from './shared/services/comment.svc';
import ContactService from './shared/services/contact.svc';
import FamilyService from './shared/services/family.svc';
import FeedbackService from './shared/services/feedback.svc';
import FinancialService from './shared/services/financial.svc';
import LocationService from './shared/services/location.svc';
import NotificationService from './shared/services/notification.svc';
import NuggetsService from './shared/services/nuggets.svc';
import OmsService from './shared/services/oms.svc';
import OverviewService from './shared/services/overview.svc';
import PendingCustomerService from './shared/services/pendingCustomer.svc';
import PermissionService from './shared/services/permissions.svc';
import PreferencesService from './shared/services/preferences.svc';
import salesIQService from './shared/services/salesIQ.svc';
import StrategyService from './shared/services/strategy.svc';
import TacticService from './shared/services/tactic.svc';
import TacticDelegateService from './shared/services/tacticDelegate.svc';
import TmwService from './shared/services/tmw.svc';
import authService from './shared/services/utilities/auth.svc';
import CharLimit from './shared/directives/charLimit.dir';
import ngUploadChange from './shared/directives/ngUploadChange.dir';
import selectNgFiles from './shared/directives/selectNgFiles.dir';
import ShowTooltipOnTextOverflow from './shared/directives/showTooltipOnTextOverflow.dir';
import NumbersOnly from './shared/directives/numbersOnly.dir';
import CurrencyFormatter from './shared/directives/currencyFormatter.dir';
import ConfirmOnExit from './shared/directives/confirmOnExit.dir';
import CommaSeparatedNumbersOnly from './shared/directives/commaSeparatedNumbersOnly.dir';
import CharLimitLabel from './shared/directives/charLimitLabel.dir';
import SetClassWhenAtTop from './shared/directives/scrollDiv.dir';
import admin from './components/admin/admin.component';
import account from './components/admin/account/account.component';
import deletedAccountsModal from './components/admin/account/deletedAccounts/deletedAccountsModal.component';
import exceptions from './components/admin/exceptions/exceptions.component';
import general from './components/admin/general/general.component';
import pendingCustomers from './components/admin/pendingCustomers/pendingCustomers.component';
import discardPendingCustomerModal from './components/admin/pendingCustomers/discard/discardPendingCustomerModal.component';
import qualifyPendingCustomerModal from './components/admin/pendingCustomers/qualify/qualifyPendingCustomerModal.component';
import { plan, planModal} from './components/admin/plan/plan.component';
import releaseNotesUploadModal from './components/admin/releaseNotes/releaseNotesUploadModal/releaseNotesUploadModal.component';
import title from './components/admin/title/title.component';
import user from './components/admin/user/user.component';
import businessPlan from './components/businessPlan/businessPlan.component';
import archiveModal from './components/businessPlan/archiveModal/archiveModal.component';
import region from './components/businessPlan/region/region.component';
import callReport from './components/callReport/callReport.component';
import completeActionItemModal from './components/callReport/completeActionItemModal/completeActionItemModal.component';
import callReportConfirmationModal from './components/callReport/confirmationModal/callReportConfirmationModal.component';
import callReportDraftsModal from './components/callReport/draftsModal/callReportDraftsModal.component';
import callReportModal from './components/callReport/modal/callReportModal.component';
import participant from './components/callReport/participant/participant.component';
import callReportBranchItemLayout from './components/callReportList/callReportBranchItemLayout.component';
import callReportList from './components/callReportList/callReportList.component';
import topLevelCallReports from './components/callReportList/topLevelCallReports.component';
import callReportSearchModal from './components/callReportList/modal/callReportSearchModal.component';
import contacts from './components/contacts/contacts.component';
import customers from './components/contacts/customers/customers.component';
import contactModal from './components/contacts/modal/contactModal.component';
import participants from './components/contacts/participants/participants.component';
import suppliers from './components/contacts/suppliers/suppliers.component';
import createPlan from './components/createPlan/createPlan.component';
import dashboard from './components/dashboard/dashboard.component';
import actionItemsWidget from './components/dashboard/widgets/actionItems/actionItemsWidget.component';
import callReportsWidget from './components/dashboard/widgets/callReports/callReportsWidget.component';
import dRegSummary from './components/dashboard/widgets/dReg/dRegSummary.component';
import dRegWidget from './components/dashboard/widgets/dReg/dRegWidget.component';
import leadsWidget from './components/dashboard/widgets/leads/leadsWidget.component';
import leadsSummaryWidget from './components/dashboard/widgets/leadsSummary/leadsSummaryWidget.component';
import noneRoleWidget from './components/dashboard/widgets/noneRole/noneRoleWidget.component';
import nuggetsWidget from './components/dashboard/widgets/nuggets/nuggetsWidget.component';
import d3FunnelChart from './components/dashboard/widgets/oms/d3FunnelChart.component';
import omsWidget from './components/dashboard/widgets/oms/omsWidget.component';
import plansWidget from './components/dashboard/widgets/plans/plansWidget.component';
import planSummaryWidget from './components/dashboard/widgets/planSummary/planSummaryWidget.component';
import salesIQWidget from './components/dashboard/widgets/salesIQ/salesIQWidget.component';
import deleteConfirmation from './components/deleteConfirmation/deleteConfirmation.component';
import modalError from './components/error/modal/modalError.component';
import pageError from './components/error/page/pageError.component';
import suggestions from './components/feedback/feedback.component';
import { feedbackItem, feedbackDetailsModal} from './components/feedback/feedbackItem/feedbackItem.component';
import featurePopover from './components/feedback/feedbackItem/featurePopover/featurePopover.component';
import feedbackReply from './components/feedback/feedbackItem/feedbackReply/feedbackReply.component';
import replyModal from './components/feedback/replyModal/replyModal.component';
import financial from './components/financial/financial.component';
import commodity from './components/financial/commodity/commodity.component';
import mfr from './components/financial/mfr/mfr.component';
import site from './components/financial/site/site.component';
import leads from './components/leads/leads.component';
import lookupUser from './components/lookupUser/lookupUser.component';
import managementHeader from './components/management/managementHeader/managementHeader.component';
import {newsfeed, searchContent} from './components/newsfeed/newsfeed.component';
import callReports from './components/newsfeed/callReports/callReports.component';
import documents from './components/newsfeed/documents/documents.component';
import {dragDrop, dropZone} from './components/newsfeed/dragDrop/dragDrop.component';
import feed from './components/newsfeed/feed/feed.component';
import newComment from './components/newsfeed/newComment/newComment.component';
import {uploadFile, fileModel} from './components/newsfeed/uploadFile/uploadFile.component';
import notifications from './components/notifications/notifications.component';
import nuggets from './components/nuggets/nuggets.component';
import deleteNuggetModal from './components/nuggets/deleteNugget/deleteNuggetModal.component';
import nuggetComment from './components/nuggets/nuggetComments/nuggetComment.component';
import nuggetCommentList from './components/nuggets/nuggetComments/nuggetCommentList.component';
import nuggetList from './components/nuggets/nuggetList/nuggetList.component';
import nuggetPartModal from './components/nuggets/nuggetPart/nuggetPartModal.component';
import submitNewNuggetModal from './components/nuggets/submitNewNugget/submitNewNuggetModal.component';
import nuggetSummary from './components/nuggetSummary/nuggetSummary.component';
import nuggetSearchModal from './components/nuggetSummary/searchModal/nuggetSearchModal.component';
import oms from './components/oms/oms.component';
import actionItemsModal from './components/oms/actionItems/actionItemsModal.component';
import initialsBadge from './components/oms/initialsBadge/initialsBadge.component';
import omsModal from './components/oms/modal/omsModal.component';
import projectDetails from './components/oms/omsProjects/projectDetails/projectDetails.component';
import businessPartnerModal from './components/oms/omsProjects/projectDetails/businessPartners/businessPartnerModal.component';
import lineItemsModal from './components/oms/omsProjects/projectDetails/lineItems/lineItemsModal.component';
import omsList from './components/omsList/omsList.component';
import omsSearchModal from './components/omsList/searchModal/omsSearchModal.component';
import { pageHeader, pageHeaderBtn, pageHeaderStatus } from './components/pageHeader/pageHeader.component';
import customerClassificationBanner from './components/pageHeader/customerClassification/customerClassificationBanner.component';
import customerClassificationModal from './components/pageHeader/customerClassification/customerClassificationModal.component';
import planValidationBanner from './components/pageHeader/planValidation/planValidationBanner.component';
import planValidationModal from './components/pageHeader/planValidation/planValidationModal.component';
import purgedParentBanner from './components/pageHeader/purgedParentAccount/purgedParentBanner.component';
import planContainer from './components/planContainer/planContainer.component';
import salesIQ from './components/salesIQ/salesIQ.component';
import salesIQActionModal from './components/salesIQ/salesIQAction/salesIQActionModal.component';
import salesIQSummary from './components/salesIQSummary/salesIQSummary.component';
import salesIQSearchModal from './components/salesIQSummary/searchModal/salesIQSearchModal.component';
import helpMenu from './components/sidebar/helpMenu/helpMenu.component';
import strategy from './components/strategy/strategy.ctrl';
import submitNewCustomer from './components/submitNewCustomer/submitNewCustomer.component';
import automation from './components/testResults/automation.component';
import tmw from './components/tmw/tmw.component';
import vbs from './components/vbs/vbs.component';
import { cmMatrixGroup, cmMatrix } from './components/vbs/cmMatrix/cmMatrix.component';
import {designSummaryGroup, designSummary} from './components/vbs/designSummary/designSummary.component';
import vbsProjects from './components/vbs/vbsProjects/vbsProjects.component';
import vbsProjectModal from './components/vbs/vbsProjects/vbsProjectModal/vbsProjectModal.component';
import financialRegion from './components/financial/region/region.component';
import { influenceGroup, influenceMap, influenceMapData, influenceMapModal } from './components/vbs/influenceMap/influenceMap.component';
import { productionSummary, productionSummaryGroup } from './components/vbs/productionSummary/productionSummary.component';
import commaDelimitedFilter from './shared/filters/commaDelimitedFilter.filter';
import CallReportService from './shared/services/callReport.svc';
import ActionItemService from './shared/services/actionItem.svc';
import actionItems from './components/actionItems/actionItems.component';
import preferencesModal from './modals/preferences/preferences.component';
import { VbsService, vbsForm } from './shared/services/vbs.svc';
import strategyTemplate from './components/strategy/strategy.html';
import omsProjects from './components/oms/omsProjects/omsProjects.component';
import lookupPlan from './components/lookupPlan/lookupPlan.component';
import { AngularComponent } from './../ng-app/components/angularComponent.component';
import { downgradeComponent } from '@angular/upgrade/static';
import actionItemList from './components/actionItems/actionItemList/actionItemList.component';
import actionItemsSearchModal from './components/actionItems/actionItemsSearchModal/actionItemsSearchModal.component';
import releaseNotesModal from './components/releaseNotes/releaseNotes.component';
import releaseNotesContent from './components/admin/releaseNotes/releaseNotes.component';
import lookupNuggetMfr from './components/lookupNuggetMfr/lookupNuggetMfr.component';
import lookupCompetitor from './components/lookupCompetitor/lookupCompetitor.component';


	var kamApp = angular.module('scmApp', [
		'ui.bootstrap',
		'ngResource',
		'ngSanitize',
		'focus-if',
		'infinite-scroll',
		'ngTagsInput',
		'as.sortable',
		'ui.router',
		'ui.router.state.events',
		'checklist-model',
		'ngFileSaver',
		'sly',
		'rzModule',
		'angularjs-dropdown-multiselect',
		'ngAnimate',
		'gridster',
		'chart.js'
	])
	//filters
	.filter('totalFilter', totalFilter)
	.filter('omsProjectFilter', OmsProjectFilter)
	.filter('altInfoFilter', AltInfoFilter)
	.filter('branchFilter', BranchFilter)
	.filter('businessPartnerFilter', BusinessPartnerFilter)
	.filter('businessPartnerNameFilter', BusinessPartnerNameFilter)
	.filter('commentFilter', CommentFilter)
	.filter('emptyToEnd', EmptyToEnd)
	.filter('initials', Initials)
	.filter('percentage',Percentage)
	.filter('startFrom', StartFrom)
	.filter('titlecase', Titlecase)
	.filter('topTierFilter', TopTierFilter)
	.filter('truncate', Truncate)
	.filter('searchContent', searchContent)
	.filter('commaDelimitedFilter', commaDelimitedFilter)

	//constants
	.constant('app', app)
	.constant('server', server)
	.constant('environment', environment)
	.constant('roles', roles)
	.constant('entity', entity)
	.constant('session', session)
	.constant('keys', keys)
	.constant('actionItemTypes', actionItemTypes)
	.constant('vbsForm', vbsForm)

	//factories
	.factory('userInfoResource', userInfoResource)
	.factory('permissionsFactory', PermissionsFactory)
	.factory('planResource', planResource)
	.factory('auditFactory', AuditFactory)
	.factory('accountResource', AccountResource)
	.factory('actionItemResource', ActionItemResource)
	.factory('adminResource', AdminResource)
	.factory('approvalResource', ApprovalResource)
	.factory('businessPlanResource', businessPlanResource)
	.factory('callReportResource', CallReportResource)
	.factory('commentResource', CommentResource)
	.factory('commodityResource', commodityResource)
	.factory('contactResource', ContactResource)
	.factory('emailResource', EmailResource)
	.factory('entityResource',entityResource)
	.factory('familyResource', FamilyResource)
	.factory('feedbackResource', FeedbackResource)
	.factory('financialResource', FinancialResource)
	.factory('locationResource',locationResource)
	.factory('mfrResource',mfrResource)
	.factory('newsFeedResource', NewsFeedResource)
	.factory('notificationResource', notificationResource)
	.factory('nuggetsResource', NuggetsResource)
	.factory('omsResource', OmsResource)
	.factory('overviewResource', OverviewResource)
	.factory('pendingCustomerResource', PendingCustomerResource)
	.factory('preferencesResource', PreferencesResource)
	.factory('salesIQResource', SalesIQResource)
	.factory('strategyResource', StrategyResource)
	.factory('tacticDelegateResource', TacticDelegateResource)
	.factory('tacticResource', TacticResource)
	.factory('tmwResource', TmwResource)
	.factory('userInfoResource',userInfoResource)
	.factory('vbsResource', VbsResource)
	.factory('adminService', AdminService)
	.factory('callReportResource', CallReportResource)
	.factory('actionItemResource', ActionItemResource)

	//services
	.service('utilService', utilService)
	.service('userProfile', UserProfile)
	.service('templateService', templateService)
	.service('mockSessionService', MockSessionService)
	.service('userInfoService', userInfoService)
	.service('planService', planService)
	.service('newsfeedService', NewsFeedService)
	.service('accountService', AccountService)
	.service('approvalService', ApprovalService)
	.service('commentService',CommentService)
	.service('contactService', ContactService)
	.service('familyService', FamilyService)
	.service('feedbackService', FeedbackService)
	.service('financialService', FinancialService)
	.service('locationService', LocationService)
	.service('notificationService', NotificationService)
	.service('nuggetsService', NuggetsService)
	.service('omsService', OmsService)
	.service('overviewService', OverviewService)
	.service('pendingCustomerService', PendingCustomerService)
	.service('permissionService', PermissionService)
	.service('planService', planService)
	.service('preferencesService', PreferencesService)
	.service('salesIQService', salesIQService)
	.service('strategyService', StrategyService)
	.service('tacticService', TacticService)
	.service('tacticDelegateService', TacticDelegateService)
	.service('tmwService', TmwService)
	.service('userInfoService', userInfoService)
	.service('authService', authService)
	.service("callReportService", CallReportService)
	.service("actionItemService", ActionItemService)
	.service("vbsService", VbsService)

	//directives
	.directive('charLimit', CharLimit)
	.directive('charLimitLabel', CharLimitLabel)
	.directive('commaSeparatedNumbersOnly', CommaSeparatedNumbersOnly)
	.directive('confirmOnExit', ConfirmOnExit)
	.directive('currencyFormatter', CurrencyFormatter)
	.directive('numbersOnly', NumbersOnly)
	.directive('setClassWhenAtTop', SetClassWhenAtTop)
	.directive('showTooltipOnTextOverflow', ShowTooltipOnTextOverflow)
	.directive("ngUploadChange", ngUploadChange)
	.directive("selectNgFiles", selectNgFiles)
	.directive('fileModel', fileModel)
	.directive('dropZone', dropZone)

	//components
	.component('headerContent', headerComponent)
	.component('sidebarContent', sidebarComponent)
	.component('mainPage', mainPageComponent)
	.component('admin', admin)
	.component('accountContent', account)
	.component('deletedAccountsModal', deletedAccountsModal)
	.component('exceptionsContent', exceptions)
	.component('generalContent', general)
	.component('pendingCustomers', pendingCustomers)
	.component('discardPendingCustomerModal', discardPendingCustomerModal)
	.component('qualifyPendingCustomerModal', qualifyPendingCustomerModal)
	.component('planContent', plan)
	.component('planModal', planModal)
	.component('releaseNotesModal', releaseNotesModal)
	.component('releaseNotesUploadModal', releaseNotesUploadModal)
	.component('titleContent', title)
	.component('userContent', user)
	.component('businessPlan', businessPlan)
	.component('archiveModal', archiveModal)
	.component('region', region)
	.component('callReport', callReport)
	.component('completeActionItemModal', completeActionItemModal)
	.component('callReportConfirmationModal', callReportConfirmationModal)
	.component('callReportDraftsModal', callReportDraftsModal)
	.component('callReportModal', callReportModal)
	.component('participant', participant)
	.component('callReportBranchItemLayout', callReportBranchItemLayout)
	.component('callReportList', callReportList)
	.component('topLevelCallReports', topLevelCallReports)
	.component('callReportSearchModal', callReportSearchModal)
	.component('contacts', contacts)
	.component('customers', customers)
	.component('contactModal', contactModal)
	.component('participants', participants)
	.component('suppliers', suppliers)
	.component('createPlanModal', createPlan)
	.component('dashboard', dashboard)
	.component('actionItemsWidget', actionItemsWidget)
	.component('callReportsWidget', callReportsWidget)
	.component('dregSummary', dRegSummary)
	.component('dregWidget', dRegWidget)
	.component('leadsWidget', leadsWidget)
	.component('leadsSummaryWidget', leadsSummaryWidget)
	.component('noneRoleWidget', noneRoleWidget)
	.component('nuggetsWidget', nuggetsWidget)
	.component('d3FunnelChart', d3FunnelChart)
	.component('omsWidget', omsWidget)
	.component('plansWidget', plansWidget)
	.component('planSummaryWidget', planSummaryWidget)
	.component('salesIqWidget', salesIQWidget)
	.component('deleteConfirmModal', deleteConfirmation)
	.component('errorModal', modalError)
	.component('errorPage', pageError)
	.component('suggestions', suggestions)
	.component('feedbackItem', feedbackItem)
	.component('feedbackDetailsModal', feedbackDetailsModal)
	.component('featurePopover', featurePopover)
	.component('feedbackReply', feedbackReply)
	.component('replyModal', replyModal)
	.component('financial', financial)
	.component('financialCommodity', commodity)
	.component('financialMfr', mfr)
	.component('financialRegion', financialRegion)
	.component('financialSite', site)
	.component('leads', leads)
	.component('lookupUser', lookupUser)
	.component('managementHeader', managementHeader)
	.component('newsfeed', newsfeed)
	.component('callReports', callReports)
	.component('documents', documents)
	.component('dragDrop', dragDrop)
	.component('feed', feed)
	.component('newComment', newComment)
	.component('uploadFile', uploadFile)
	.component('notifications', notifications)
	.component('nuggets', nuggets)
	.component('deleteNuggetModal', deleteNuggetModal)
	.component('nuggetComment', nuggetComment)
	.component('nuggetCommentList', nuggetCommentList)
	.component('nuggetList', nuggetList)
	.component('nuggetPartModal', nuggetPartModal)
	.component('submitNewNuggetModal', submitNewNuggetModal)
	.component('nuggetSummary', nuggetSummary)
	.component('nuggetSearchModal', nuggetSearchModal)
	.component('oms', oms)
	.component('actionItems', actionItems)
	.component('actionItemList', actionItemList)
	.component('actionItemsSearchModal', actionItemsSearchModal)
	.component('actionItemsModal', actionItemsModal)
	.component('initialsBadge', initialsBadge)
	.component('omsModal', omsModal)
	.component('projectDetails', projectDetails)
	.component('businessPartnerModal', businessPartnerModal)
	.component('lineItemsModal', lineItemsModal)
	.component('omsList', omsList)
	.component('omsSearchModal', omsSearchModal)
	.component('pageHeader', pageHeader)
	.component('pageHeaderBtn', pageHeaderBtn)
	.component('pageHeaderStatus', pageHeaderStatus)
	.component('customerClassificationBanner', customerClassificationBanner)
	.component('customerClassificationModal', customerClassificationModal)
	.component('planValidationBanner', planValidationBanner)
	.component('planValidationModal', planValidationModal)
	.component('purgedParentBanner', purgedParentBanner)
	.component('planContainer', planContainer)
	.component('releaseNotesContent', releaseNotesContent)
	.component('salesIQ', salesIQ)
	.component('salesIQActionModal', salesIQActionModal)
	.component('salesIQSummary', salesIQSummary)
	.component('salesIQSearchModal', salesIQSearchModal)
	.component('helpMenu', helpMenu)
	.controller('strategies', strategy)
	.component('submitNewCustomerModal', submitNewCustomer)
	.component('automationTestResults', automation)
	.component('tmw', tmw)
	.component('vbs', vbs)
	.component('cmMatrix', cmMatrix)
	.component('cmMatrixGroup', cmMatrixGroup)
	.component('designSummary', designSummary)
	.component('designSummaryGroup', designSummaryGroup)
	.component('influenceGroup', influenceGroup)
	.component('influenceMap', influenceMap)
	.component('influenceMapData', influenceMapData)
	.component('influenceMapModal', influenceMapModal)
	.component('productionSummaryGroup', productionSummaryGroup)
	.component('productionSummary', productionSummary)
	.component('vbsProjects', vbsProjects)
	.component('vbsProjectModal', vbsProjectModal)
	.component('preferencesModal', preferencesModal)
	.component("omsProjects", omsProjects)
	.component("lookupPlan", lookupPlan)
	.component("lookupNuggetMfr", lookupNuggetMfr)
	.component("lookupCompetitor", lookupCompetitor)

	//Angular Components
	.directive('angularComponent', downgradeComponent({component: AngularComponent}))
	
	kamApp.$inject = [
		'ui.bootstrap',
		'ngResource',
		'ngSanitize',
		'focus-if',
		'infinite-scroll',
		'ngTagsInput',
		'as.sortable',
		'ui.router',
		'ui.router.state.events',
		'checklist-model',
		'ngFileSaver',
		'sly',
		'rzModule',
		'angularjs-dropdown-multiselect',
		'ngAnimate',
		'gridster',
		'chart.js'
  ];

	angular.module('scmApp').config(function ($httpProvider) {
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
		});
	
	
	configFn.$inject = ['$logProvider','$httpProvider','$compileProvider','$stateProvider','$urlRouterProvider','$locationProvider'];
	
	// register work which needs to be performed on module loading.
	kamApp.config(configFn);

	// removes templates from cache
	kamApp.run(function($rootScope, $state) {
		
		$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState) {
			
		});   
	})	
	
	function configFn($logProvider, $httpProvider,$compileProvider,$stateProvider,$urlRouterProvider,$locationProvider) {
		
		var templateUrlPrefix = './components/';		
		
		$logProvider.debugEnabled(false);
		
		//Angular 1.6 Changes
		//$compileProvider.preAssignBindingsEnabled(false); // Remove When possible
		$locationProvider.hashPrefix('');
		
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);		
		
		$httpProvider.defaults.cache = false;
		
		if (!$httpProvider.defaults.headers.get) {
	        $httpProvider.defaults.headers.get = {};    
	    } 

	    // disable IE ajax request caching
	    // See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.Cn4jy3X4.dpuf
	    // http://stackoverflow.com/questions/16098430/angular-ie-caching-issue-for-http
	    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';	    
//	    $httpProvider.defaults.headers.common = {};
//	    $httpProvider.defaults.headers.common['x-api-key'] = keys.KAM;
//	    $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
	    
	    $stateProvider
	    .state('home', {
	    	url: '/',
	    	component: 'dashboard'			
		})
	    .state('customerList', {
	    	url: '/customerList/:type',
	    	params: {type: {value: 'customers', dynamic: true}},
	    	component: 'mainPage'
	    })
	    .state('plan', {
	    	abstract: true,
	    	component: 'planContainer'
	    })
	    .state('businessPlan', {
	    	parent: 'plan',
	    	url: '/plans/:planId',
	    	component: 'businessPlan'
	    })
	    .state('strategies', {
	    	parent: 'plan',
	    	url: '/plans/:planId/strategies',
	    	controller: 'strategies',
			template: strategyTemplate
	    })
	    //This route does not appear to work...
	    .state('strategyTactics', {
	    	parent: 'plan',
	    	url: '/plans/:planId/strategies/:planStrategyId/tactics/:planTacticId', 
            controller: 'strategies',
            template: strategyTemplate
		})
		.state('oms', {
			parent: 'plan',
	    	url: '/plans/:planId/oms/:type/?projectId',
	    	component: 'oms',
	    	params: {type: {value: 'active', dynamic: true}, actionItemId: {value: undefined, dynamic: true}},
	    	reloadOnSearch: false
		})
	    .state('financials', {
	    	parent: 'plan',
	    	url: '/plans/:planId/financials/:entity',
	    	component: 'financial',
	    	params: {
	            entity: null
	        }			
	    })
	    .state('contacts', {	    	
	    	parent: 'plan',
	    	url: '/plans/:planId/contacts/:contactsTab',
	    	component: 'contacts',
	    	params: {
	    		contactsTab: {value: 'customers', dynamic: true}
	        }
			})
		.state('nuggets', {	    	
			parent: 'plan',
	    	url: '/plans/:planId/nuggets?nuggetID',
	    	component: 'nuggets',
	    	reloadOnSearch: false
	    })
	    .state('vbs', {
	    	parent: 'plan',
	    	url: '/plans/:planId/vbs/:type/?projectId',
	    	component: 'vbs',
	    	params: {type: {value: 'active', dynamic: true}},
	    	reloadOnSearch: false
	    })
	    .state('suggestions', {
	    	url: '/suggestions',
	    	component: 'suggestions'			
	    })
	    .state('bug-reports', {
	    	url: '/bug-reports',
	    	component: 'suggestions'			
	    })
	    .state('tmw', {
	    	url: '/tmw',
	    	component: 'tmw'			
	    })
		.state('actionItems', {
	    	url: '/actionItems',
	    	component: 'actionItems'			
	    })
	    .state('leads', {
	    	url: '/leads',
	    	component: 'leads'			
	    })
	    .state('salesIQ', {
	    	parent: 'plan',
	    	url: '/plans/:planId/salesIQ',
	    	component: 'salesIQ',
	    })
	    .state('salesIQSummary', {
	    	url: '/salesIQSummary',
	    	component: 'salesIQSummary'
	    })
	    .state('callReport', {
	    	parent: 'plan',
	    	url: '/plans/:planId/call-report/?callReportId',
	    	component: 'callReport'			
		})
		.state('topLevelCallReports', {
	    	url: '/users/call-reports/topLevel',
	    	component: 'topLevelCallReports'			
	    })
	    .state('admin', {
	    	url: '/admin/:adminTab',
	    	component: 'admin',
	    	params: {
	    		adminTab: {value: 'general', dynamic: true}
	        },
	    	resolve: {
                adminOnly: adminOnly
              }
	    })
		.state('activity', {
			parent: 'plan',
			url: '/plans/:planId/activity/:activityTab',
	    	component: 'newsfeed',
	    	params: {
	    		activityTab: {value: 'newsfeed', dynamic: true}
	        }
	    })
	    .state('omsList', {
			url: '/oms',
	    	component: 'omsList',
	    	params: {
	    		planId: {value: '', dynamic: true},
	    		username: {value: '', dynamic: true},
	    		startDate: {dynamic: true},
	    		endDate: {dynamic: true}
	        }
	    })
	    .state('nuggetSummary', {
			url: '/opb',
	    	component: 'nuggetSummary'
	    })
	    
	    $urlRouterProvider.otherwise('/');	    
	    
	}
	
	function adminOnly($q, $state, $location, authService) {
		var deferred = $q.defer();
		var promise = authService.allowAdminConsole();
		promise.then( function(response) {
		    if (response) {
		        deferred.resolve();
		    } else {
		        $location.path('/');
		        $state.go('home', {}, {reload: true});
		    }
		})
		.catch(function (error) {

		});
		return deferred.promise;
	}
	
export default kamApp;