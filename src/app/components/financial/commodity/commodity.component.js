import commodityTemplate from './commodity.html';

const commodity =  {
		template: commodityTemplate,
		bindings: {
			planId: '<',
			expandFinancials: '&',
			onFinancialRowSelect: '&',
			entity: '<',
			account: '<',
			accountEntity: '<',
			mfr: '<',
			onCrumbClick: '&',
			isExpanded: '=',
			canEdit: '<'
		},
		controller: function($rootScope, financialService, roles, utilService) {
			
			var vm = this;
			var financialEnum = 'COMMODITY';
			vm.callNumber = 0;

			vm.$onInit = function() {
			}
			
			vm.$onChanges = function(bindings) {
				refreshCommodityFinancials();
			}

			
			vm.onFinancialExpandClick = function() {
				vm.expandFinancials({financialToExpand: financialEnum});
			}

			function refreshCommodityFinancials() {
				vm.commodities = undefined;
				vm.commodityRow.clear();
				getCommodityFinancials(vm.planId, (vm.account? vm.accountEntity : vm.entity), vm.account, vm.mfr);
			}
			

			function getCommodityFinancials(planId, entity, account, mfr) {
				vm.callNumber++;
				var callNumber = vm.callNumber;
				financialService.getCommodityFinancials(planId, entity, account, mfr)
				.then(function(response) {
					if(callNumber === vm.callNumber) vm.commodities = response;
				})
				.catch(function(error) {
					//$scope.clientError.message = error;
				})
			}
			
			vm.enableDsamEdit = function(commodity) {
				commodity.editMode = true;
				
				commodity.originalDsam = commodity.dSam;
				
				setTimeout(function(){
					var element = angular.element('#' + commodity.commodity + '-dsam');
					element.focus();
				}, 100);
			}
			
			vm.cancelDsamEdit = function(commodity) {
				commodity.editMode = false;
				commodity.dSam = commodity.originalDsam;
			}
			
			vm.saveDsam = function(commodity) {
				dsamObject = {
					dsam: commodity.dSam,
					planId: parseInt(vm.planId),
					entity: vm.account? vm.accountEntity : vm.entity,
					account: vm.account,
					manufacturer: vm.mfr,
					commodity: commodity.commodity
				};
				
				financialService.updateDsam(dsamObject).then(
				function(response) {
					commodity.dSam = response.response.dsam;
					commodity.originalDsam = commodity.dSam;
					commodity.editMode = false;
				})
				.catch(function(error) {
				})
			}
			
			vm.commodityRow = {
					data: undefined,
					index: undefined,
					select: function(commodity, index) {				
						vm.commodityRow.index = index;
						
						vm.onFinancialRowSelect({financialType: financialEnum, selectedValue: commodity.commodity});
					},
					
					clear: function() {
						vm.commodityRow.data = undefined;
						vm.commodityRow.index = undefined;
					},
				}
			
			vm.sort = {
	          type: "runRate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
		}
	}
export default commodity;