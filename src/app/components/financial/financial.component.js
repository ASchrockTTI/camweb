import financialTemplate from './financial.html';

const financial =  {
		template: financialTemplate,
		controller: function($stateParams,	
				utilService,
				planService,
				permissionsFactory,
				entity,
				roles) {
			
			var vm = this;
			
			vm.isFinancialExpanded      = false;
			
			vm.hideSite                 = false;
			vm.hideRegion               = false;
			vm.hideCommodity            = false;
			vm.hideMfr                  = false;

			vm.expandedFinancial = undefined;

			vm.entity    = undefined;
			vm.account      = undefined;
			vm.mfr       = undefined;
			
			vm.selectedEntityTotals = undefined;

			vm.$onInit = function() {
				vm.params = $stateParams;
				vm.planId = vm.params.planId;
				vm.clientError = utilService.getClientErrorObject();
				vm.plan = undefined;
				
				vm.permissionsValidator = permissionsFactory.newInstance();
				vm.permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.EXEC, roles.RSE, roles.BRANCH_MEMBER]);

				getPlanById();
			}
			
			vm.onCrumbClick = function(callBackObj) {
				console.log("Crumb Clicked: ", callBackObj.financialType);
				hideOrExpandAllFinancials(callBackObj.financialType, true);
			}

			vm.expandFinancials = function(callBackObj) {
				console.log("Expanding Financials: ", callBackObj.financialToExpand);
				
				hideOrExpandAllFinancials(callBackObj.financialToExpand);
			}

			vm.onFinancialRowSelect = function(callBackObj) {
				changeFinancialTable(callBackObj.financialType);
			}

			function changeFinancialTable(financialType) {
				
				//if in summary mode or "shrink" mode: do nothing
				if(vm.isFinancialExpanded == false || vm.isFinancialExpanded == undefined) {
					return;
				}

				switch(financialType) {
					
					case 'REGION': {
						hideOrExpandAllFinancials('SITE', true);
						break;
					}

					case 'SITE': {
						hideOrExpandAllFinancials('MFR', true);
						break;
					}

					case 'MFR': {
						hideOrExpandAllFinancials('COMMODITY', true);
						break;
					}

					case 'COMMODITY': {
						break;
					} 
					
					default: {
						return;
					}

				}
			}

			function hideOrExpandAllFinancials(financialType, forceExpand) {
				if(vm.expandedFinancial !== financialType)	vm.expandedFinancial = financialType;
				else vm.expandedFinancial = undefined;
				
				if(vm.isFinancialExpanded == true && (forceExpand == false || forceExpand == undefined)) {
					
					vm.isFinancialExpanded = false;

					vm.hideSite      = false;
					vm.hideRegion    = false;
					vm.hideCommodity = false;
					vm.hideMfr       = false;
					
					return;
				}

				switch(financialType) {
					
					case 'SITE': {
						vm.hideSite      = false;
						vm.hideRegion    = true;
						vm.hideCommodity = true;
						vm.hideMfr       = true;

						vm.isFinancialExpanded = true;
						break;
					}

					case 'REGION': {
						vm.hideSite      = true;
						vm.hideRegion    = false;
						vm.hideCommodity = true;
						vm.hideMfr       = true;

						vm.isFinancialExpanded = true;
						break;
					}

					case 'COMMODITY': {
						vm.hideSite      = true;
						vm.hideRegion    = true;
						vm.hideCommodity = false;
						vm.hideMfr       = true;

						vm.isFinancialExpanded = true;
						break;
					} 

					case 'MFR': {
						vm.hideSite      = true;
						vm.hideRegion    = true;
						vm.hideCommodity = true;
						vm.hideMfr       = false;

						vm.isFinancialExpanded = true;

						break;
					}

					default: {
						vm.hideSite      = false;
						vm.hideRegion    = false;
						vm.hideCommodity = false;
						vm.hideMfr       = false;

						vm.isFinancialExpanded = true;
					}

				}
			}

			function getPlanById() {
				
				planService.lookupPlan(vm.params.planId)
				.then(function(response) {
					vm.plan = response;
					vm.entity = vm.plan.entity;
					
					vm.canEdit = vm.permissionsValidator.hasPermission('edit', vm.plan);
				})
				.catch(function(error) {				
					vm.clientError.message = error;
				})	
			}
		}
	}
export default financial;