import manufacturerTemplate from './manufacturer.html';

const mfr =  {
		template: manufacturerTemplate,
		bindings: {
			planId: '<',
			expandFinancials: '&',
			onFinancialRowSelect: '&',
			entity: '<',
			account: '<',
			accountEntity: '<',
			mfr: '=',
			onCrumbClick: '&',
			isExpanded: '=',
			canEdit: '<'
		},
		controller: function($rootScope, financialService, utilService, roles) {
			
			var vm = this;
			var financialEnum = 'MFR';
			vm.site = undefined;
			
			vm.callNumber = 0;

			vm.$onInit = function() {
			}

			vm.$onChanges = function(bindings) {
				refreshManufacturerFinancials();
			}
			
			vm.onFinancialExpandClick = function() {
				vm.expandFinancials({financialToExpand: financialEnum});
			}

			vm.mfrRow = {
					data: undefined,
					select: function(mfr) {
						var selectedMfr = mfr.mfr;
						var entity = mfr.entity;
						
						if(vm.mfrs.length === 1) return;
						
						if(selectedMfr === vm.mfr) {
							selectedMfr = 'Total';
							vm.mfr = undefined;
						} else {
							vm.mfr = selectedMfr;
						}

						vm.onFinancialRowSelect({financialType: financialEnum, selectedValue: selectedMfr});
					},
					
					clear: function() {
						vm.mfrRow.data = undefined;
					},
				}
			
			vm.sort = {
	          type: "runRate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
			
			vm.enableDsamEdit = function(mfr, event) {
				mfr.editMode = true;
				
				mfr.originalDsam = mfr.dSam;
				
				event.stopPropagation();
				
				setTimeout(function(){
					var element = angular.element('#' + mfr.mfr + '-dsam');
					element.focus();
				}, 100);
			}
			
			vm.cancelDsamEdit = function(mfr) {
				mfr.editMode = false;
				mfr.dSam = mfr.originalDsam;
			}
			
			vm.saveDsam = function(mfr, event) {
				event.stopPropagation();
				
				dsamObject = {
					dsam: mfr.dSam,
					planId: parseInt(vm.planId),
					entity: vm.account? vm.accountEntity : vm.entity,
					account: vm.account,
					manufacturer: mfr.mfr,
				};
				
				debugger;
				financialService.updateDsam(dsamObject).then(
				function(response) {
					mfr.dSam = response.response.dsam;
					mfr.originalDsam = mfr.dSam;
					mfr.editMode = false;
				})
				.catch(function(error) {
				})
			}
			
			function refreshManufacturerFinancials() {
				vm.mfrs = undefined;
				vm.mfrRow.clear();
				getMfrFinancials(vm.planId, (vm.account? vm.accountEntity : vm.entity), vm.account);
			}
			
			function getMfrFinancials(planId, entity, account) {
				vm.callNumber++;
				var callNumber = vm.callNumber;
				financialService.getMfrFinancials(planId, entity, account)
				.then(function(response) {
					if(callNumber === vm.callNumber) {
						vm.mfrs = response;
						if(vm.mfrs.length === 1) vm.mfrRow.select(vm.mfrs[0]);
					}
				})
				.catch(function(error) {
					//$scope.clientError.message = error;
				})
			}
			
		}
	}
export default mfr;