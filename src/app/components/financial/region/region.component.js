import regionTemplate from './region.html';

const financialRegion =  {
		template: regionTemplate,
		bindings: {
			entity: '=',
			plan: '<',
			expandFinancials: '&',
			onFinancialRowSelect: '&',
			selectedEntityTotals: '=',
			account: '=',
			mfr: '=',
			isExpanded: '<'
		},
		controller: function($rootScope, financialService) {
			
			var vm = this;
			var financialEnum = 'REGION';
			

			vm.$onInit = function() {
				vm.currentDateLastYear = new Date();
				vm.currentDateLastYear.setFullYear( vm.currentDateLastYear.getFullYear() - 1 );
				
				getEntityFinancials(vm.plan.planId);
			}
			
			vm.row = {
				data: undefined,
				entity: 'TOTAL',
				
				select: function(region, index) {
					
					if(vm.entity === region.entity) {
						region = vm.entities[vm.entities.length - 1];
					}
					
					var entity = region.entity === 'TOTAL' ? vm.plan.entity : region.entity;
					vm.onFinancialRowSelect({financialType: financialEnum, selectedValue: entity});
					vm.row.entity = region.entity;
					
					vm.selectedEntityTotals = region;
					vm.entity = entity;
					
					if(vm.entities.length === 2) return;
					vm.account = undefined;
					vm.mfr = undefined;
				},
				
				clearSelection: function() {
					vm.row.data = undefined;
					vm.row.index = undefined;
				}
			}
			
			vm.onFinancialExpandClick = function() {
				vm.expandFinancials({financialToExpand: financialEnum});
			}

			function getEntityFinancials(planId) {
				
				financialService.getEntityFinancials(planId)
				.then(function(response) {
					vm.entities = response;
					
					vm.selectedEntityTotals = vm.entities[vm.entities.length - 1];
				})
				.catch(function(error) {
					//$scope.clientError.message = error;	
				})
			}
			
			vm.sort = {
	          type: "runRate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
		}
	}
export default financialRegion;