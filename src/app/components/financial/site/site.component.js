import siteTemplate from './site.html';

const site =  {
		template: siteTemplate,
		bindings: {
			planId: '<',
			selectedEntityTotals: '=',
			expandFinancials: '&',
			onFinancialRowSelect: '&',
			entity: '<',
			account: '=',
			accountEntity: '=',
			mfr: '=',
			onCrumbClick: '&',
			isExpanded: '='
		},
		controller: function($sce, $rootScope, $window, permissionsFactory, financialService, roles, userProfile, utilService, FileSaver) {
			
			var vm = this;
			var financialEnum = 'SITE';
			vm.callNumber = 0;

			vm.$onInit = function() {
				vm.currentDateLastYear = new Date();
				vm.currentDateLastYear.setFullYear( vm.currentDateLastYear.getFullYear() - 1 );
			}
			
			vm.$onChanges = function(bindings) {
				refreshAccountFinancials();
			}
			
			vm.onFinancialExpandClick = function() {
				vm.expandFinancials({financialToExpand: financialEnum});
			}

			vm.siteRow = {
				data: undefined,
				index: undefined,
				select: function(site, index) {
					var location = site.location;
					
					if(vm.siteRow.data === site && vm.locations.length !== 1) {
						index = undefined;
						location = 'Total';
						vm.siteRow.data = undefined;
						
						vm.account = undefined;
						vm.accountEntity = undefined;
					} else {
						vm.siteRow.data = site;
						
						vm.account = location;
						vm.accountEntity = site.entity;
					}
					vm.mfr = undefined;
					
					vm.siteRow.index = index;
					vm.siteRow.location = location;
					
					vm.onFinancialRowSelect({financialType: financialEnum, selectedValue: location});

				},
				
				clear: function() {
					vm.siteRow.data = undefined;
					vm.siteRow.index = undefined;
				},
				
			}
			
			vm.sort = {
	          type: "runRate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
			
			vm.exportFinancials = function () {
				
				financialService.exportSites(vm.planId)
				.then(function(response) {
					
					var header = response.headers('Content-Disposition');
					var fileName = header.split("=")[1].replace(/\"/gi,'');
					
					var downloadableBlob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, fileName);				
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			function refreshAccountFinancials() {
				vm.locations = undefined;
				vm.siteRow.clear();
				getAccountFinancials(vm.planId, vm.entity);
			}
			
			function getAccountFinancials(planId, selectedEntity) {
				vm.callNumber++;
				var callNumber = vm.callNumber;
				financialService.getAccountFinancials(planId, selectedEntity)
				.then(function(response) {
					if(callNumber === vm.callNumber) {
						vm.locations = response;
						if(vm.locations.length === 1) vm.siteRow.select(vm.locations[0], 0)
					}
				})
				.catch(function(error) {
					vm.clientError.message = error;	
				})
			}
			
		}
	}
export default site;