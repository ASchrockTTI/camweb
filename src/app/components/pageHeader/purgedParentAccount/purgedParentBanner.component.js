import purgedParentBannerTemplate from './purgedParentBanner.html';

const purgedParentBanner =  {
		template: purgedParentBannerTemplate,
		bindings: {
			plan: '<'
		},
		controller: function(
			utilService,
			userInfoService,
			userProfile,
			planService,
			$rootScope,
			$state
		) {
		
			var vm = this;
			
			vm.$onInit = function() {
				
			}			
			
			 vm.discardPlan = function() {
		          utilService.deleteConfirmation(
		              "Discard",
		              "You are about to discard this plan.",
		              "btn-danger"
		            )
		            .result.then(
		            	function(result) {
		            		if(result) {
		            			var container = planService.getNewPlanContainer();
		        				
		        				container.plan = vm.plan;								
		        				container.securityProfile = userProfile.getUserProfile();
		        				
		        				userInfoService.checkUserInfo()
		        				.then(function() {
		        					planService.discardPlan(container)
		        					.then(function() {
		        						sessionStorage.removeItem('sessionPlans');
		        						sessionStorage.removeItem('sessionPlansNonTTI');
		        						$rootScope.$broadcast('businessPlanEdited');
		        					})
		        					.catch(function(error) {
		        						vm.clientError.message = error;
		        					})
		        				})
		        				.catch(function(error) {
		        					vm.clientError.message = error;
		        				})
		            		}
		            	},
		            	function(dismiss) {
		            		
		            	});
		        };
			
		}
	}
export default purgedParentBanner;