import planValidationModalTemplate from './planValidationModal.html';

const planValidationModal =  {
		template: planValidationModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			planService,
			pendingCustomerService,
			userInfoService,
			userProfile,
			$scope
		) {
		
			var vm = this;
			
			vm.selection = 'assign';
			vm.reason = '';
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.plan = vm.data.plan;
				vm.osrName = initOSRName(vm.plan.acctManager);
				vm.placeHolder = initPlaceHolder(vm.osrName);
				getPendingCustomer();
				
				setTimeout(function () {
					var input = angular.element('#lookupUser');
					input.focus();
				}, 500);
			}
			
			$scope.$watch('$ctrl.selection', function() {
				var input;
				if(vm.selection === 'assign') {
					input = angular.element('#lookupUser');
				} else {
					input = angular.element('#' + vm.selection + 'Input');
				}
				input.focus();
			}, true);
			
			function initOSRName(acctManager) {
				
				let osrName = undefined;

				if(acctManager) {
					osrName = acctManager;
					vm.selectOsr(osrName);
				}

				console.log('osrName: ', osrName)

				return osrName;
			}

			function initPlaceHolder(placeHolder) {
				
				let placeHolderToSet = undefined;

				if(placeHolder) {
					placeHolderToSet = placeHolder;
				}
				else {
					placeHolderToSet = 'Search OSR by last name...';
				}
				
				console.log("placeholder: ", placeHolderToSet);
				return placeHolderToSet;
			}

			function getPendingCustomer() {
				pendingCustomerService.getPendingCustomerByPlanId(vm.plan.planId).then(
					function(response) {
						vm.pendingCustomer = response;
					})
					.catch(function(error) {
						vm.pendingCustomers = [];
					})
			}
			
			vm.selectOsr = function(osrName) {
				console.log('selected OSR: ', osrName);
				vm.selectedOsr = osrName;
			}
			
			vm.assign = function() {
				vm.plan.acctManager = vm.osrName;
				
				var container = planService.getNewPlanContainer();
				
				container.plan = vm.plan;								
				container.securityProfile = userProfile.getUserProfile();
				
				
				userInfoService.checkUserInfo()
				.then(function() {
					planService.updatePlan(vm.plan.planId, container)
					.then(function() {
						vm.modalInstance.close('assigned');
					})
					.catch(function(error) {
						vm.modalInstance.close('error');
						vm.clientError.message = error;
					})
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			
			vm.reject = function() {
				pendingCustomerService.rejectPendingCustomer(vm.pendingCustomer.pendingCustomerId, vm.rejectReason).then(
				function(response) {
					vm.modalInstance.close('rejected');
				})
				.catch(function(error) {
					vm.modalInstance.close('error');
				})
			}
			
			vm.notifyWrongBranch = function() {
				pendingCustomerService.notifyWrongBranch(vm.pendingCustomer.pendingCustomerId).then(
						function(response) {
							vm.modalInstance.close('wrongBranch');
						})
						.catch(function(error) {
							vm.modalInstance.close('error');
						})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default planValidationModal;