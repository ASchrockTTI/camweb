import planValidationBannerTemplate from './planValidationBanner.html';

const planValidationBanner =  {
		template: planValidationBannerTemplate,
		bindings: {
			plan: '<'
		},
		controller: function(
			utilService,
			$scope,
			$state
		) {
		
			var vm = this;
			
			vm.$onInit = function() {
				
			}			
			
			vm.planValidationModal = function() {
            	utilService.uibModal().open({
                    component: 'planValidationModal',
                    size: 'md',
                    scope: $scope,
                    resolve: { 
                    	modalData: function() {
                        	return {
                        		plan: vm.plan
                        	}
                    	}
                    }
                }).result.then(
                    function(result) {
                    	$state.go($state.current, {}, {reload: true});
                    },
                    function(dismiss) {
                    	
                    }
                )
            }
			
		}
	}
export default planValidationBanner;