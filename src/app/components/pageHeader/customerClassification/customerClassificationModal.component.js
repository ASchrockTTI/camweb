import customerClassificationModalTemplate from './customerClassificationModal.html';

const customerClassificationModal =  {
		template: customerClassificationModalTemplate,
		bindings: {
			modalInstance: '<',
			plan: '<',
		},
		controller: function(
			$scope,
			$stateParams,
			planService
		) {
		
			var vm = this;
			vm.params = $stateParams;
			
			vm.$onInit = function() {
				
			}
			
			
			vm.classify = function() {
				var classification = {
					manufacturing: vm.manufacturing,
					purchasing: vm.purchasing,
					engineering: vm.engineering,
					administrative: vm.administrative,
					closed: vm.closed
				}
				
				planService.classifyCustomer(vm.params.planId, classification).then(
					function(response) {
						vm.modalInstance.close('classified');
					})
					.catch(function(error) {
						vm.modalInstance.close('error');
					})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
		}
	}
export default customerClassificationModal;