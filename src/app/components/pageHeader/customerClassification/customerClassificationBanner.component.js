import customerClassificationBannerTemplate from './customerClassificationBanner.html';

const customerClassificationBanner =  {
		template: customerClassificationBannerTemplate,
		bindings: {
			plan: '<',
		},
		controller: function(
			utilService,
			$scope,
			$state
		) {
		
			var vm = this;
			
			vm.$onInit = function() {
				
			}			
			
			vm.customerClassificationModal = function() {
            	utilService.uibModal().open({
                    component: 'customerClassificationModal',
                    size: 'sm',
                    scope: $scope,
                    resolve: { 
                    	modalData: function() {
                        	return {
                        		plan: vm.plan
                        	}
                    	}
                    }
                }).result.then(
                    function(result) {
                    	$state.reload();
                    },
                    function(dismiss) {
                        
                    }
                )
            }
			
		}
	}
export default customerClassificationBanner;