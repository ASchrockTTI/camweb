import pageHeaderTemplate from './pageHeader.html';

const pageHeader =  {
		template: pageHeaderTemplate,
		bindings: {
			activePage: '<',
			plan: '<',
			relatedPlans: '<',
			preventNav: '<'
		},
		controller: function($rootScope, $scope, utilService, familyService, $window, userProfile, mockSessionService, roles, permissionsFactory) {
			
			var vm = this;
			vm.vbsText = 'Prod. Summary';
			vm.displaySalesIQ = true;

			var permissionsValidator;
			var callReportRoles = [roles.ADMIN, roles.BRANCH_MEMBER, roles.OWNER];
						
			vm.$onInit = function() {
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setCustomPermission('callReport', callReportRoles, null, false);
				
				if(userProfile.getFullName() !== 'undefined undefined') {
					checkEntity();
				}
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				checkEntity();
			});
			
			function checkEntity() {
				switch(userProfile.getEntity()) {
					case 'NDC':
						vm.showSalesIQ = true;
						break;
					default:
						vm.salesIQ = false;
				}
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				update();
			});
			
			function update() {
				if(!vm.plan) return;
				if(vm.plan.marketSegment) {
					if (vm.plan.marketSegment.marketSegmentCode.substring(0,1) === "4") {
						vm.vbsText = 'CM Matrix';
					} else {
						switch(vm.plan.marketSegment.marketSegmentCode) {
						case 'A-4':
						case '6-A':
						case '9-3':
						case '9-5':
						case '9-6':
							vm.vbsText = 'CM Matrix';
							break;
						default: 
							vm.vbsText = 'Prod. Summary';
						}
					}
				} else {
					vm.vbsText = 'Prod. Summary';
				}
				
				if(utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), vm.plan.acctManager)) {
					var threshold = new Date('February 11, 2019 00:00:00');
					//only display banner for plans created after Feb 11.
					if(threshold < vm.plan.dateCreated && !vm.plan.planClassification) vm.showClassificationBanner = true;
				}
				
				if(vm.plan.planStatus.planStatus === 'Pending')
					// if(vm.plan.acctManager === "Unassigned Owner")
					if(utilService.equalsIgnoreCaseSpace(userProfile.getUserType(),'GM') ||utilService.equalsIgnoreCaseSpace(userProfile.getUserType(),'FSM'))
					if(userProfile.getBranchList().indexOf(vm.plan.branch) !== -1 || userProfile.getBranchLocation() === (vm.plan.branch)) {
					vm.showValidationBanner = true;
				} else {
					vm.showValidationBanner = false;
				}
				
				if(userProfile.getFullName() !== 'undefined undefined') {
					var entity = userProfile.getUserProfile().entity;
					
					if(userProfile && userProfile.getUserProfile() && userProfile.getUserProfile().entity && userProfile.getUserProfile().entity.toUpperCase() !== 'NDC') {
						vm.displaySalesIQ = false;					
					}
				}
			}
			
			vm.hasPermission = function(action) {
				return permissionsValidator.hasPermission(action, vm.plan);
			}
			
			vm.$onDestroy = function() {
				vm.plan = {};
				$rootScope.$broadcast('closePlan');
			}
			
			vm.$onChanges = function(changes) {				
				if (changes.plan) {
					if (changes.plan.currentValue) {
						vm.plan = changes.plan.currentValue;
						$rootScope.$broadcast('openPlan', {planId: vm.plan.planId});
						
						update();
					}
				}				
			}
			
			vm.print = function() {
				$window.print();
			}
			
			vm.isAdmin = function() {
				if(userProfile.isAdmin() || mockSessionService.isMocking()) {
					return true;
				}
				return false;
			}
			
			vm.isBeta = function() {
				return userProfile.isBeta();
			}
			
			
			vm.showConstruction = function() {
				utilService.deleteConfirmation('Under Construction', 'The page you are attempting to access is not available at this time.', 'btn-default', true).result					
				.then(
					function(result) {
						
					},
					function(dismiss) {
						
					}
				)
			}
			
			
			vm.loadPlan = function(planId) {
				utilService.location.path('plans/' + planId);
			}

		}
	}
	const pageHeaderBtn = {
		template: '<a id="page-{{$ctrl.buttonId}}" ng-class="{\'btn-primary\': $ctrl.isActive() == true}" class="btn btn-default page-{{$ctrl.buttonId}}" ui-sref="{{$ctrl.functionName}}" ng-click="$ctrl.checkNav($event)"> ' +
		          '<i class="{{$ctrl.icon}}"></i>{{$ctrl.text}} ' +
		          '</a>',
		require: {
			pageHeader: '^^pageHeader'
		},
		bindings: {
			planId: '<',
			buttonId: '<',
			text: '<',
			icon: '<'
		},
		controller: function($rootScope, utilService, $location, $state) {
			
			var vm = this;
			var activeSet = false;
			
			
			vm.$onInit = function() {
				vm.functionName = vm.buttonId + '({planId: $ctrl.planId})';
				vm.preventNav = false;
			}

			vm.$onDestroy = function() {
				preventNavListener();
			}
			
			vm.$postLink = function() {
			}
			
			var preventNavListener = $rootScope.$on('preventnav', function(event, value) {
				vm.preventNav = value.isDirty;
				utilService.getLogger().info(vm.preventNav);
			})
			
			vm.isActive = function() {				
				if (vm.pageHeader.activePage == vm.buttonId) {
					return true;
				}
				
				return false;				
				
			}
			
			vm.checkNav = function($event) {
							
				//$event.preventDefault();	
				
				$rootScope.$broadcast('lookupPlan-clear-text',undefined);
				
//				if (vm.preventNav || vm.pageHeader.preventNav) {
//					
//					utilService.deleteConfirmation('Prevent Navigation', 'Changes have been made to this page.', 'btn-warning').result
//					.then(
//						function(result) {
//							if (result == true) {
//								$state.go(vm.buttonId, {planId: vm.planId});
//							}
//							
//						},
//						function(dismiss) {
//							
//						}
//					)
//				} else {
//					$state.go(vm.buttonId, {planId: vm.planId});
//				}
				$state.go(vm.buttonId, {planId: vm.planId});
			}
			
		}
	}
	const pageHeaderStatus = {
		template: '<div ng-if="$ctrl.isValidPlanStatusId()" class="pull-right bg-{{$ctrl.background}}" style="border-radius: 0px 13px 13px 0; display:flex; flex:1"> ' +
            	  '<div class="page-nav-divider hidden-print"></div> ' +
            	  '<div class="page-nav-btn-group hidden-print"> ' +
            	  '<span class="label label-{{$ctrl.background}}"> ' +
        		  '{{$ctrl.displayMessage}} ' +
        		  '</span> ' +                      
        		  '</div> ' +
        		  '</div>',
		bindings: {
			planStatus: '<',
			planType: '<'
		},
		require: {
			pageHeader : '^^pageHeader'
		},
		controller: function(utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.displayMessage = '';
			}
			
			vm.$onChanges = function(changes) {
				vm.planStatus = changes.planStatus.currentValue;
				vm.planType = changes.planType.currentValue;
				
				
				if (vm.planStatus) {
					switch(vm.planStatus.planStatusId) {
					case 1:
						vm.background = 'warning';						
						vm.displayMessage = vm.planStatus.planStatus;
						break;
					case 4:
						vm.background = 'gray';						
						vm.displayMessage = vm.planStatus.planStatus;
						break;
					case 3:
					case 5:
						vm.background = 'danger';
						vm.displayMessage = vm.planStatus.planStatus;
						break;
					default:
						if (vm.planType) {
							switch(vm.planType.planTypeId) {
							case 6:
								vm.background = 'warning';
								vm.displayMessage = vm.planType.planType;
								break;
							default:
								vm.background = 'default';
							}					
						}
					}					
				}
				
				
			}
			
			vm.$doCheck = function() {				
				if (vm.pageHeader.plan !== undefined) {
					vm.plan = vm.pageHeader.plan;
				}
			}			
			
			vm.isValidPlanStatusId = function() {
				
				var display = false;
				
				if (!vm.planStatus) {
					return;
				}
				
				if (!vm.planType) {
					return;
				}
				
				switch(vm.planStatus.planStatusId) {
				case 1:
				case 4:
				case 5:
					display = true;
				default:					
				}
				
				switch(vm.planType.planTypeId) {
				case 6:
					display = true;
				default:
				}
				
				return display;
				
			}
		}
	}
export { pageHeader, pageHeaderBtn, pageHeaderStatus};