import lookupPlanTemplate from "./lookupPlan.html";

const lookupPlan = {
  template: lookupPlanTemplate,
  bindings: {
    ngModel: "=",
    minLength: "<",
    onSelect: "&",
    lookupPlaceholder: "<",
    branches: "<",
    inputId: "<",
    topTierOnly: "<",
    businessPartners: "<",
    tooltipPosition: "<",
    tooltipText: "<",
    hideInfo: "<",
  },
  controller: function (
    $attrs,
    $rootScope,
    accountService,
    utilService,
    $filter,
    $stateParams
  ) {
    var vm = this;

    vm.$onInit = function () {
      vm.minLength = vm.minLength === undefined ? 2 : vm.minLength;
      vm.plan = undefined;
      vm.planText = undefined;
      vm.results = [];
      if (!vm.lookupPlaceholder) {
        vm.lookupPlaceholder = "Go to Plan...";
      }
    };

    vm.$onDestroy = function () {
      lookupPlanClearTextListener();
    };

    vm.$postLink = function () {
      var elem = angular.element("#lookup-plan");

      if ($attrs.style) {
        elem[0].setAttribute("style", $attrs.style);
      }
    };

    vm.$doCheck = function () {
      if (!vm.ngModel) {
        vm.noResults = undefined;
      }
    };

    vm.findPlan = function (value) {
      vm.noResults = undefined;

      return accountService
        .findPlans(value, false)
        .then(function (response) {
          var orderedList = utilService.orderBy(
            response,
            ["planType.planTypeId", "branch"],
            false
          );

          if (orderedList.length == 0) {
            vm.noResults = true;
          }
          if (vm.branches) {
            orderedList = $filter("branchFilter")(orderedList, vm.branches);
          }
          if (vm.topTierOnly) {
            //custom filter that omits all but first plan for each familyID
            orderedList = $filter("topTierFilter")(orderedList);
          } else if (vm.businessPartners) {
            orderedList = $filter("businessPartnerFilter")(
              orderedList,
              $stateParams.planId
            );
          }

          return orderedList;
        })
        .catch(function (error) {
          utilService.getLogger().error(error);
        });
    };

    var lookupPlanClearTextListener = $rootScope.$on(
      "lookupPlan-clear-text",
      function (event, value) {
        vm.ngModel = undefined;
      }
    );
  },
};
export default lookupPlan;
