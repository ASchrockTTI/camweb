import createPlanTemplate from './createPlan.html';

const createPlan =  {
		template: createPlanTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(planService, utilService, userProfile) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				
				vm.creatingPlan = false;
				vm.pageIncludes = utilService.getTemplates().getAll();
				vm.clientError = utilService.getClientErrorObject();
				
				vm.formFields = {
					customerName: undefined,
					branch: undefined,
					marketSegment: undefined,
					acctManager: vm.data.acctManager
				};
				
				if(vm.data.customerName) {
					vm.formFields.customerName = vm.data.customerName;
				}

				getMarketSegments();
				getBranches();

				setTimeout(function () {
					var input = angular.element(document.querySelector('#planCustomerNameInput'));
					input.focus();
				}, 500);
			}
			
			vm.add = function() {
				
				vm.creatingPlan = true;
				
				var mainPageContainer = {
					plan: {},
					securityProfile: {},
					mockingProfile: {}
				};
				
				if (vm.owner == undefined) {
					vm.owner = {
						fullName: userProfile.getFullName()					
					}
				}
				
				mainPageContainer.plan = {
					acctManager: vm.formFields.acctManager,				
					customerName: vm.formFields.customerName,
					branch: vm.formFields.branch.branch,
					marketSegment: vm.formFields.marketSegment,
					familyID: 0
				}
				mainPageContainer.securityProfile = {
					fullName: userProfile.getFullName(),
					title: userProfile.getTitle(),
					branchLocation: userProfile.getBranchLocation(), 
					physicalDeliveryOfficeName: userProfile.getBranchLocation(),
					mail: userProfile.getMail(),
					selectedEntity: userProfile.getEntity()
				}
				
				if (userProfile.getIsMock()) {
					mainPageContainer.mockingProfile = {
						fullName: userProfile.getMockUser().fullName,
						title: userProfile.getMockUser().title,
						branchLocation: userProfile.getMockUser().branchLocation,
						physicalDeliveryOfficeName: userProfile.getBranchLocation(),
						mail: userProfile.getMockUser().mail
					}
				}
				
				planService.addPlan(mainPageContainer)
				.then(function(response) {												
					vm.creatingPlan = false;
					sessionStorage.removeItem('sessionPlans');
					sessionStorage.removeItem('sessionPlansNonTTI');
					vm.modalInstance.close(response);
				})
				.catch(function(error) {
					vm.creatingPlan = false;
					vm.clientError.message = error;
				})
				
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			vm.setSelected = function(item) {
				selectedAccount = item;
			}
			
			function getMarketSegments() {
				
				planService.getMarketSegments()
				.then(function(response) {
					vm.marketSegments = [];
					angular.forEach(response, function(segment) {
						vm.marketSegments.push.apply(vm.marketSegments, segment.subCategoryList)
					})
				})
				.catch(function(error) {
					
				})
			}
						
			function branchAdded(branches, branchName) {
				return branches.some(function(branch) {
					return branch.fullParentBranchName === branchName;
				}); 
			}
			
			function getBranches() {
				planService.getEntityParentBranches(userProfile.getEntity())
				.then(function(response) {
					vm.branches = response;
					
					angular.forEach(vm.branches, function(branch){
						if(branch.fullParentBranchName === userProfile.getBranch()) {
							vm.formFields.branch = branch;
						}
					})
				})
				.catch(function(error) {
					
				})
			}
		}
	}
export default createPlan;