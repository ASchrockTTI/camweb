import callReportConfirmationModalTemplate from './callReportConfirmationModal.html';

const callReportConfirmationModal =  {
		template: callReportConfirmationModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function() {
			
			var vm = this;
			
			
			vm.$onInit = function() {
				vm.callDate = vm.resolve.modalData.callDate;
			}
			
			vm.validateDate = function() {
				var valid = false;

				if(vm.callDate) {
					var callDate = new Date(vm.callDate);
					callDate.setHours(0,0,0,0);
					var currentDate = new Date();
					currentDate.setHours(0,0,0,0);

					if(currentDate >= callDate) {
						valid = true;
					}
				}

				vm.callReportConfirmationForm.callDate.$setValidity("validDate", valid);
				return valid;
			}
			
			vm.submit = function() {
				vm.modalInstance.close(vm.callDate);
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default callReportConfirmationModal;