import callReportTemplate from './callReport.html';

const callReport =  {
		template: callReportTemplate,
		controller: function(
			$stateParams,
			$state,
			$timeout,
			callReportService,
			planService,
			permissionsFactory,
			roles,
			userProfile,
			utilService,
			contactService,
			$scope,
			$filter,
			newsfeedService,
			$window,
			$sce,
			FileSaver,
		) {
			
			var vm = this;
			
			vm.strategies = [
				{
					strategyId: '1',
					strategy: 'None'
				},
				{
					strategyId: '2',
					strategy: 'WTS'
				},
				{
					strategyId: '3',
					strategy: 'WTO'
				},
				{
					strategyId: '4',
					strategy: 'WTD'
				},
				{
					strategyId: '5',
					strategy: 'WTC'
				}
			];
			
			vm.contactLists = {
				tti: [],
				customer: [],
				suppliers: []
			}
			
			vm.contactList = [];

			var permissionsValidator;
			
			vm.$onInit = function() {
				vm.editMode = undefined;
				vm.params = $stateParams;
				
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setEditRoles([roles.OWNER, roles.GM, roles.FSM, roles.ADMIN, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setCustomPermission('download', [roles.ADMIN, roles.OWNER, roles.BRANCH_MEMBER, roles.RSE], null, null);
				
				retrievePlanById(vm.params.planId);

				loadContacts();
				loadDrafts();
				
				vm.callDate = new Date();
				
				vm.strategy = vm.strategies[0];
				
				vm.contacts = [];
				vm.addContact();

				vm.actionItems = [];
				vm.addActionItem();
				
				vm.fileList = [];
			}
			
			vm.hasPermission = function(action){
				return vm.isReporter() || permissionsValidator.hasPermission(action, vm.plan);
			}
			
			vm.isReporter = function() {
				return utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), vm.reporter)
			}
			
			vm.toggleEditMode = function() {
				vm.editMode = !vm.editMode;
				if(!vm.editMode) {
					loadCallReport();
				}
			}
			
			function retrievePlanById(planId) {
				
				planService.lookupPlan(planId)
				.then(function(response) {
					
					vm.plan = response;
					
					if (vm.plan.insideSalesPerson != null) {
						var isrList = response.insideSalesPerson.substring(0,response.insideSalesPerson.length -1).trim().split(',');
						vm.insideSalesPersonList = isrList;
					}
					
					planService.setPlan(vm.plan);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			$scope.$on('refreshContacts', function() {
				loadContacts();
			});

			function loadContacts() {

				contactService.retrieveContacts(vm.params.planId)
					.then(function (response) {
						vm.contactLists.customer = contactService.getCustomers();
						vm.contactLists.customer = $filter('orderBy')(vm.contactLists.customer, 'delegateLastName');
						
						
						vm.contactLists.tti = contactService.getDelegates();
						vm.contactLists.tti = $filter('orderBy')(vm.contactLists.tti, 'delegateLastName');
						vm.contactLists.tti = contactService.getOwners().concat(vm.contactLists.tti);
						
						vm.contactLists.suppliers = contactService.getSuppliers();
						
						vm.contactList = [];
						vm.contactList.push.apply(vm.contactList, vm.contactLists.customer);
						vm.contactList.push.apply(vm.contactList, vm.contactLists.suppliers);
						vm.contactList.push.apply(vm.contactList, vm.contactLists.tti);
						
						for(var i = 0; i < vm.contacts.length; i++) {
							if(vm.contacts[i].selectedContact) {
								var contact = $filter('filter')(vm.contactList, {delegateId: vm.contacts[i].selectedContact.delegateId}, true)[0];
								vm.contactSelect(vm.contacts[i], contact);
							} else if(vm.contacts[i].name) {
								var contact = $filter('filter')(vm.contactList, {delegateName: vm.contacts[i].name}, true)[0];
								vm.contactSelect(vm.contacts[i], contact);
							}
						}
						
						if(vm.params.callReportId && !vm.editMode) {
							loadCallReport();
						} else {
							vm.editMode = true;
						}
					})
					.catch(function (error) {

					})
			}
			
			function loadCallReport() {
				callReportService.retrieveCallReport(vm.params.planId, vm.params.callReportId)
				.then(function (response) {
					var response = response.response;
					
					var callReport = response.callReport;
					
					if(callReport.submitDateTime) {
						vm.editMode = false;
					} else {
						vm.editMode = true;
					}
					
					vm.submitDateTime = callReport.submitDateTime;
					vm.callReportId = callReport.callReportId;
					vm.createDate = callReport.dateTime;
					vm.modifiedDate = callReport.modifyDateTime;
					vm.modifiedBy = callReport.modifiedBy;
					vm.topic = callReport.topic;
					vm.callDate = callReport.callDate;
					vm.notes = callReport.notes;
					vm.documentList = response.documentList;
					vm.reporter = callReport.reporter;
					
					vm.strategy = callReport.strategy;
					vm.actionItems = response.actionItems;

					angular.forEach(vm.actionItems, function(actionItem, key) {
						actionItem.customerContacts = true;
						actionItem.supplierContacts = true;
						actionItem.ttiContacts = true;
						actionItem.showOther = true;
						vm.whoSelect(actionItem, actionItem.whoContact)
					})
					
					
					vm.contacts = [];
					for(var i = 0; i < response.participants.length; i++) {
						vm.addContact();
						vm.contactSelect(vm.contacts[i], response.participants[i]);
					}
					
					vm.callReportForm.$setPristine();
				})
				.catch(function (error) {
					vm.deleted = true;
				})
			}
			
			function loadDrafts() {
				callReportService.retrieveCallReportDrafts(vm.params.planId)
				.then(function (response) {
					vm.drafts = response.response;
				})
				.catch(function (error) {

				})
			}
			
			$scope.$watch(function() {
				if(!vm.editMode) return;
				if(vm.contacts.length > 0){return vm.contacts[vm.contacts.length - 1].delegateName}
				else {vm.addContact()}} , function(newVal, oldVal){
					if(newVal && newVal.length > 0) {
						vm.addContact();
					}
			});
			
			vm.addContact = function() {
				vm.contacts.push({
					delegateName: '',
					delegateTitle: '',
					delegatePhone: '',
					customerContacts: true,
					supplierContacts: true,
					ttiContacts: true
				});
			}
			
			vm.removeContact = function(index) {
				vm.contacts.splice(index, 1);
				vm.callReportForm.$setDirty();
			}
			
			vm.contactSelect = function(contactField, customer) {
				contactField.delegateName = customer.delegateName;
				contactField.delegateTitle = customer.delegateTitle;
				contactField.delegatePhone = customer.delegatePhone;
				
				contactField.selectedContact = customer;
				
				contactField.createContactEnabled = false;
				
				vm.callReportForm.$setDirty();
			}
			
			vm.newReport = function() {
				$state.go('callReport', {planId: vm.params.planId, callReportId: null}, {reload: true});
			}
			
			vm.viewDrafts = function() {
				utilService.uibModal().open({					
					component: 'callReportDraftsModal',
					size: 'lg',
					resolve: {
						modalData: function () {
							return { 
								drafts: vm.drafts,
							}
						}
					}
				}).result.then(
					function (result) {
						vm.params.callReportId = result;
						loadCallReport();
					},
					function (dismiss) {

					}
				)
			}
			
			

			vm.addActionItem = function () {
				vm.actionItems.push({
					actionItem: '',
					whoType: '',
					subject: '',
					response: '',
					customerContacts: true,
					supplierContacts: true,
					ttiContacts: true
				});
			}
			
			vm.removeActionItem = function(index) {
				vm.actionItems.splice(index, 1);
				vm.callReportForm.$setDirty();
			}
			
			vm.canCompleteAction = function(actionItem) {
				if(actionItem.completeDate !== null) return false;
				if(utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), actionItem.whoName)
				   || utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), actionItem.creator)) {
					return true;
				} else return false;
			}
			
			vm.completeActionItemModal = function(actionItem) {
				actionItem.actionItemSummaryType = 'CALL_REPORT';
				utilService.uibModal().open({					
					component: 'completeActionItemModal',
					size: 'sm',
					resolve: {
						modalData: function () {
							return { 
								actionItem: actionItem
							}
						}
					}
				}).result.then(
					function (actionItemContainer) {
						let result = actionItemContainer.response.actionItem;
						actionItem.comment = result.comment;
						actionItem.completeDate = result.completeDate;
						actionItem.commenter = result.commenter;
					},
					function (dismiss) {

					}
				)
			}
			
			
			vm.whoSelect = function(actionItem, contact) {
				if(!contact) {
					actionItem.whoType = 'other';
					return;
				}
				
				actionItem.whoName = contact.delegateName;
				actionItem.whoNameOriginal = contact.delegateName;
				actionItem.whoContact = contact;
				
				switch(contact.delegateType.delegateType.toLowerCase()) {
					case 'customer':
						actionItem.whoType = 'customer';
						break;
					case 'supplier':
						actionItem.whoType = 'supplier';
						break;
					default:
						actionItem.whoType = 'tti';
				}
				
				vm.callReportForm.$setDirty();
			}
			
			vm.compareWhoNametoOriginal = function(actionItem){
				if(actionItem.whoName !== actionItem.whoNameOriginal) {
					actionItem.whoContact = null;
					if(actionItem.whoName !== '') {
						actionItem.whoType = 'Other';
					} else actionItem.whoType = '';
				}
			}
			
			vm.toggleContactGroup = function(event, field, group) {
				event.stopPropagation();
				field[group] = !field[group];
			}
			
			
			vm.confirmSubmission = function() {
				utilService.uibModal().open({					
					component: 'callReportConfirmationModal',
					size: 'md',
					resolve: {
						modalData: function () {
							return { 
								callDate: vm.callDate,
							}
						}
					}
				}).result.then(
					function (callDate) {
						vm.callDate = callDate;
						vm.save(true);
					},
					function (dismiss) {

					}
				)
			}
			
			
			vm.save = function(submit) {
				var callReportContainer = {
					callReport: {
						callReportId: vm.callReportId,
						planId: vm.params.planId,
						reporter: vm.reporter,
						topic: vm.topic,
						strategy: vm.strategy,
						callDate: vm.callDate,
						notes: vm.notes,
					},
					participantContactIds:  [],
					newContacts: [],
					actionItems: vm.actionItems
				}

				angular.forEach(vm.contacts, function(contact, key) {
					if(contact.selectedContact) {
						callReportContainer.participantContactIds.push(contact.selectedContact.delegateId);
					} else if (contact.delegateName) {
						callReportContainer.newContacts.push(contact);
					}
				})

				

				callReportService.saveCallReport(vm.params.planId, callReportContainer, submit)
				.then(function(response) {
					vm.callReportId = response.response;
					
					//attach files to call report
					vm.uploadCount = 0;
					if(vm.fileList) {
						vm.uploadCount = vm.fileList.length;
						angular.forEach(vm.fileList, function(file) {
							uploadDocument(vm.params.planId, vm.callReportId, file);
						})
					}
					
					vm.callReportForm.$setPristine();
					
					waitForUploads();
					
				})
				.catch(function(error) {
					
				});

			}
			
			vm.addSelectedFiles = function() {
				Array.prototype.push.apply(vm.fileList, vm.selectedFiles);
			}
			
			vm.removeFile = function(file) {
				vm.fileList.splice(vm.fileList.indexOf(file), 1);
			}
			
			function waitForUploads() {
				$timeout(function(){
					if(vm.uploadCount === 0) {
						$state.go('callReport', {planId: vm.params.planId, callReportId: vm.callReportId}, {reload: true});
					} else waitForUploads()
				}, 500)
			}
			
			function uploadDocument(planId, callReportId, newFile) {
				if (utilService.isNotEmpty(newFile.name)) {
					
					var document = callReportService.uploadDocument(vm.params.planId, callReportId, newFile)
					.then(function (response) {
						vm.uploadCount--;
					})
					.catch(function (error) {
	
					})
						
					}
			}
			
			vm.downloadDocument = function(document) {
				if(!vm.hasPermission('download')) return;
				
				newsfeedService.downloadDocument(vm.params.planId, document.documentID)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
			}
			
			
			vm.deleteDocument = function(document) {
				utilService.deleteConfirmation('Delete Confirmation', 'You are about to delete "' + document.documentName + '.' + document.documentExt + '".', 'btn-danger').result
				.then(function(results) {
					newsfeedService.deleteDocument(vm.params.planId, document.documentID);
					for(var i = 0; i < vm.documentList.length; i++) {
						if(vm.documentList[i].documentID === document.documentID) {
							vm.documentList.splice(i, 1);
						}
					}
				},
				function(dismiss) {
					
				})
			}
			
			vm.discard = function() {
				utilService.deleteConfirmation(
			              "Call Report",
			              "You are about to discard the selected call report and any attached files and action items. \nIf this call report is from the current quarter, your TMW will be affected.",
			              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		callReportService.deleteCallReport(vm.params.planId, vm.params.callReportId);
		            		$state.go('callReport', {planId: vm.params.planId, callReportId: null}, {reload: true});
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
			
		}
	}
export default callReport;