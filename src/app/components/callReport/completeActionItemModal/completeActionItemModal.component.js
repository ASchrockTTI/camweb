import completeActionItemModalTemplate from './completeActionItemModal.html';

const completeActionItemModal =  {
		template: completeActionItemModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(actionItemService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				
				vm.actionItem = vm.data.actionItem;
				
				vm.type = vm.actionItem.actionItemSummaryType;
				
				switch(vm.type) {
					case 'CALL_REPORT':
						vm.showCommentBox = true;
						vm.actionItemId = vm.actionItem.actionItemId;
						break;
					case 'PLAN_TACTIC':
						vm.showCommentBox = true;
						vm.actionItemId = vm.actionItem.planTacticDelegateId;
						break;
					case 'OMS':
						vm.actionItemId = vm.actionItem.actionId;
					default:
						vm.showCommentBox = false;
				}
			}
			
			vm.complete = function() {
				
				var actionItemContainer = {
					actionItemId: vm.actionItemId,
					actionItemType: vm.type,
					comment: vm.comment
				}
				
				actionItemService.completeActionItem(actionItemContainer)
				.then(function(response) {
					actionItemContainer.response = response.response;
					vm.modalInstance.close(actionItemContainer);
				})
				.catch(function(error) {
					
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default completeActionItemModal;