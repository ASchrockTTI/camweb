import participantTemplate from './participant.html';

const participant =  {
		template: participantTemplate,
		bindings: {
            contactField: '=',
            contactLists: '=',
            editMode: '=',
            plan: '=',
            contactSelect: '=',
        },
		controller: function(
				$scope,
				contactService,
				utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				var test = $scope.editMode;
			}
			
			
			vm.checkContactName = function(contactField) {
				if(contactField.selectedContact && contactField.delegateName !== contactField.selectedContact.delegateName) {
					contactField.selectedContact = undefined;
				}
			}

			vm.createContact = function(contactField) {
				var contact = {
					delegateName: contactField.delegateName,
					delegateTitle: contactField.delegateTitle,
					delegatePhone: contactField.delegatePhone
				}

				utilService.uibModal().open({
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function () {
							return {
								plan: vm.plan,
								formType: '',
								editContact: contact
							}
						}
					}
				}).result.then(
					function (result) {
						$scope.$emit('refreshContacts');
						vm.contactSelect(contactField, result);
					},
					function (dismiss) {

					}
				)

				setTimeout(function () {
					var input = angular.element(document.querySelector('#customerNameInput'));
					input.focus();
				}, 500);

			}
			
			vm.toggleContactGroup = function(event, field, group) {
				event.stopPropagation();
				field[group] = !field[group];
			}
			
			
			vm.editContact = function(contactField) {

				var contact = contactField.selectedContact;
				
				utilService.uibModal().open({					
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function () {
							contact.selectedRelation = {
									value: contact.spw.spwID.toString(),
									display: contact.spw.spw
								}
								
								contact.selectedVbsRole = {
									value: contact.vbsRole.vbsRoleID.toString(),
									display: contact.vbsRole.vbsRole
								}
							
							return {
								plan: vm.plan,
								formType: 'edit',
								editContact: contact
							}
						}
					}
				}).result.then(
					function (result) {
						$scope.$emit('refreshContacts');
					},
					function (dismiss) {

					}
				)
			}
		}
	}
export default participant;