import callReportModalTemplate from './callReportModal.html';

const callReportModal =  {
		template: callReportModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function($state, userProfile) {
			
			var vm = this;
			
			vm.branches = undefined;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				if (vm.data.selectedPlanId) {
					vm.planId = vm.data.selectedPlanId;
					vm.customerName = vm.data.selectedCustomerName;
				}
				
				vm.branches = getBranches();
				
				setTimeout(function () {
					var input = angular.element(document.querySelector('#callReportLookup'));
					input.focus();
				}, 500);
			}
			
			function getBranches() {
				var role = userProfile.getUserType();
				
				switch(role.toUpperCase()) {
				case 'ADMIN':
				case 'EXECUTIVE':
				case 'SAM_GAM':
					return undefined;
				default:
					if(userProfile.getBranchList().length > 0) {
						return userProfile.getBranchList();
					} else {
						return userProfile.getBranchLocation();
					}
				}
			}
			
			vm.setPlanId = function(selectedPlanId) {
				vm.planId = selectedPlanId;
			}
			
			vm.create = function() {
				$state.go('callReport', {planId: vm.planId});
				vm.modalInstance.dismiss('create');
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default callReportModal;