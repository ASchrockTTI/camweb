import callReportDraftsModalTemplate from './callReportDraftsModal.html';

const callReportDraftsModal =  {
		template: callReportDraftsModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function($state, $stateParams, callReportService, utilService) {
			
			var vm = this;
			
			vm.params = $stateParams;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				
				vm.callReportDrafts = vm.data.drafts;
			}
			
			
			vm.openCallReport = function(callReportId) {
				$state.go('callReport', {planId: $stateParams.planId, callReportId: callReportId}, {reload: true});
				vm.modalInstance.dismiss('draft selected');
			}
			
			vm.deleteCallReport = function(callReportId) {
				var warningText = "You are about to discard the selected call report draft and any attached files."
				if(callReportId.toString() === vm.params.callReportId) {
					warningText = "You are about to discard the current call report draft and any attached files."
				}
				
				utilService.deleteConfirmation(
			              "Call Report",
			              warningText,
			              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		callReportService.deleteCallReport(vm.params.planId, callReportId);
	            			for (var i = 0; i < vm.callReportDrafts.length; i++) {
		            			if (vm.callReportDrafts[i].callReport.callReportId === callReportId) {
		            				vm.callReportDrafts.splice(i, 1);
		            		    }
		            		}
	            			if(callReportId.toString() === vm.params.callReportId) {
	            				$state.go('callReport', {planId: vm.params.planId, callReportId: null});
	            			}
	            			if(vm.callReportDrafts.length === 0) {
	            				vm.modalInstance.dismiss('no drafts');
	            			}
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default callReportDraftsModal;