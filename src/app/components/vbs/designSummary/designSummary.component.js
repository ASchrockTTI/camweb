import designSummaryTemplate from './designSummary.html'
const designSummaryGroup =  {
		template: '<div uib-accordion-group is-open="$ctrl.isOpen" class="panel-info" style="text-decoration: none !important; margin-top: 5px;"> ' +
		'<uib-accordion-heading> ' +
		'<span style="font-weight: bold;">{{$ctrl.formTitle}}</span> ' +		
		'<span class="pull-right" style="font-weight: bold; color: red; text-decoration: none;"></span> ' +	
		'<span id="design-summary-last-update" class="pull-right" style="text-decoration: none;">Last Update: {{$ctrl.maxUpdateDate}}</span> ' +
		'</uib-accordion-heading> ' +
		'<design-summary data="$ctrl.data" org-data="$ctrl.orgData"></design-summary> ' +
		'</div>',
		require: {
			vbs: '^^vbs'
		},
		bindings: {
			formTitle: '<',
			plan: '<'
		},
		controller: function(utilService, vbsService, userProfile) {
			
			var vm = this;
			var dataRetrieved = false;
			
			vm.form;
			
			vm.$onInit = function() {
				vm.isOpen = false;
				vm.data = [];
				vm.orgData = [];
				vm.maxUpdateDate;
				vm.cancelCollapse = false;
				vm.form;
			}
			
			vm.$onChanges = function(changes) {
				vm.plan = changes.plan.currentValue;				
			}
			
			vm.$doCheck = function() {
				
				//This is here to handle an IE bug where it doesn't update the variable after its loaded from the parent
				if (vm.vbs.designSummaryDate) {
					vm.maxUpdateDate = vm.vbs.designSummaryDate;
				};
				
				switch(vm.isOpen) {
				case true:
					
					if (dataRetrieved) {
						return;
					}
					
					dataRetrieved = true;
					
					if (!vm.cancelCollapse) {
						getData();
					} else {
						vm.cancelCollapse = false;
					}					
					
					break;
				case false:
					
					dataRetrieved = false;
					
					if (!vm.form) {
						return;
					}
					
					if (vm.form.$dirty) {
						if (!window.confirm('Changes have been made to this form. You will lose your changes?\n\nDo you wish to continue?') ) {
							vm.cancelCollapse = true;
							vm.isOpen = true;
						} else {
							vm.form.$setPristine();						
						}
					}
					
					break;
				}
			}
			
			function getData() {
				vbsService.designSummaryProject.getList(vm.plan.planId)
				.then(function(response) {								
					vm.data = response ? response : [];
					vm.orgData = utilService.deepCopy(vm.data)
				})
				.catch(function(error) {
					
				});
			}
			
			vm.project = {
				getContainer: function() {
					return vbsService.designSummaryProject.getContainer()
				},
				save: function(data) {					
					
					var container = {							
							impact: data.impact,
							projectName: data.projectName,
							prototypeDate: data.prototypeDate ? new Date(data.prototypeDate).getTime() : undefined,
							productionDate: data.productionDate ? new Date(data.productionDate).getTime() : undefined,
							eau: data.eau,
							cmName: data.cmName,
							cmPlanID: data.cmPlanID,
							locationName: data.locationName,
							locationPlanID: data.locationPlanID,
							vbsID: data.vbsID,
							projectID: data.projectID,
							modifiedBy: userProfile.getFullName(),
							modifiedDateTime: new Date().getTime()
						}						
					
						if (data.selectedProjectType) {
							container.designSummaryProjectType = {
								projectType: data.selectedProjectType.display,
								projectTypeID: data.selectedProjectType.value
							}
						} else {
							container.designSummaryProjectType = {
									projectType: undefined,
									projectTypeID: undefined
								}
						}						
						
						vm.maxUpdateDate = utilService.convertToISODate(new Date());
						
						if (container.vbsID > 0) {
							
							vbsService.designSummaryProject.update(vm.plan.planId, container)
							.then(function(response) {
								data.saved = true;
								vm.form.$setPristine();
							})
							.catch(function(error) {
								
							})
							
						} else {
							vbsService.designSummaryProject.save(vm.plan.planId, container)
							.then(function(response) {						
								data.projectID = response.projectID;
								data.vbsID = response.vbsID
								data.saved = true;
								vm.form.$setPristine();
							})
							.catch(function(error) {
								
							})
						}
				},
				delete: function(data) {

					vbsService.designSummaryProject.delete(data.projectID)
					.then(function(response) {
						var arrayLen = vm.data.length;
						var index = -1;
						for(var lv = 0; lv < arrayLen; lv++) {
							if(vm.data[lv].projectID == data.projectID) {
								index = lv;
								break;
							}
						}
						if(index>-1) {
							vm.data.splice(index, 1);
						}
						
						vm.form.$setPristine();
					})
					.catch(function(error) {
						throw error;
					});					
				},
			}
			
			vm.product = {
				getContainer: function() {
					return vbsService.designSummaryProduct.getContainer()
				},
				save: function(parent, data) {					
					
					var container = {							
							featureBenefitObjectives: data.featureBenefitObjectives,
							technologyConsiderations: data.technologyConsiderations,
							productID: data.productID,
							projectID: data.projectID ? data.projectID : parent.item.projectID,
							modifiedBy: userProfile.getFullName(),
							modifiedDateTime: new Date().getTime()
						}
					
						if (data.selectedCommodity) {
							container.commodity = {
								commodity: data.selectedCommodity.display,
								commodityID: data.selectedCommodity.value
							}
						} else {
							container.commodity = {
								commodity: undefined,
								commodityID: undefined
							}
						}						
						
						vm.maxUpdateDate = utilService.convertToISODate(new Date());
						
						if (container.productID > 0) {
							
							vbsService.designSummaryProduct.update(vm.plan.planId, container)
							.then(function(response) {
								data.saved = true;
								vm.form.$setPristine();
							})
							.catch(function(error) {
								
							})
							
						} else {
							vbsService.designSummaryProduct.save(vm.plan.planId, container)
							.then(function(response) {						
								data.productID = response.productID;								
								data.saved = true;
								vm.form.$setPristine();
							})
							.catch(function(error) {
								
							})
						}
				},
				delete: function(parent, data) {
					
					vbsService.designSummaryProduct.delete(data.productID)
					.then(function(response) {
						var arrayLen = parent.item.designSummaryProductContainerList.length;
						var index = -1;
						for(var lv = 0; lv < arrayLen; lv++) {
							if(parent.item.designSummaryProductContainerList[lv].productID == data.productID) {
								index = lv;
								break;
							}
						}
						if(index>-1) {
							parent.item.designSummaryProductContainerList.splice(index, 1);
						}
						
						vm.form.$setPristine();
					})
					.catch(function(error) {
						throw error;
					});					
				}
			}
		}
	}
	const designSummary = {
		template: designSummaryTemplate,
		require: {
			designSummaryGroup: '^^designSummaryGroup'
		},
		bindings: {
			data: '<',
			orgData: '<'
		},
		controller: function(utilService, userProfile, permissionsFactory, roles) {
			
			var vm = this;
			var permissionsValidator;
			
			vm.$onInit = function() {
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
			}
			
			vm.$onChanges = function(changes) {
				vm.project.data = changes.data.currentValue;
				vm.project.orgData = changes.orgData.currentValue;
			}
			
			vm.$postLink = function() {
				vm.designSummaryGroup.form = vm.designSummaryForm;
			}
			
			vm.permissions = {
				validated: function(action) {
					return permissionsValidator.hasPermission(action, vm.designSummaryGroup.plan);				
				},	
			}
			
			vm.project = {				
				projectTypes: [
					{value: "1", display: "Redesign/Upgrade"},
					{value: "2", display: "New Design"}
				],
				protoTypeCalendarOpen: function(index, e) {			
					vm.project.data[index].protoTypeCalendarOpen = !vm.project.data[index].protoTypeCalendarOpen;				
				},
				productionCalendarOpen: function(index, e) {			
					vm.project.data[index].productionCalendarOpen = !vm.project.data[index].productionCalendarOpen;				
				},
				typeChanged: function(item) {
					switch(item.selectedProjectType.value) {
					case "1":
						item.showImpact = false;
						break;
					case "2":
						item.showImpact = true;
						break;
					}
					item.changedValue = true;
				},
				changed: function(item) {
					
					var changedValue = false;
					
					if (vm.project.orgData[vm.project.index] == undefined) {
						return;
					}
					
					angular.forEach(item, function(value, key) {
							
						if (key != '$$hashKey') {
							if (typeof vm.project.orgData[vm.project.index][key] !== 'object') {
								if (vm.project.orgData[vm.project.index][key] != value) {									
									changedValue = true;
								}
							} 
						}
					})
					
					if (changedValue) {
						item.changedValue = true;
					}					
				},					
				addRow: function() {
					vm.project.data.unshift(vm.designSummaryGroup.project.getContainer());
				},
				removeRow: function(data) {
					var item = data ? data : vm.project.data[0];
					
					if (!item.vbsID) {
						vm.project.data.splice(0, 1);
						return;
					} 
					
					utilService.deleteConfirmation('Design Summary', 'You are about to delete the selected row.', 'btn-danger').result
					.then(
						function(result) {
							if (result == true) {
								vm.designSummaryGroup.project.delete(item);
							}
						},
						function(dismiss) {
							
						}
					)
					
				},
				save: function(item) {
					vm.designSummaryGroup.project.save(item);
				},
				selectRow: function(index) {
					vm.project.index = index;
					vm.project.row = vm.project.data[index];
				},
				setType: function(value, index) {
					vm.project.data[index].type = value == undefined ? '    ' : value;
				},
				loadCmPlan: function(data) {
					utilService.location.path('plans/' + data.cmPlanID);
				},
				loadLocationPlan: function(data) {
					utilService.location.path('plans/' + data.locationPlanID);
				},
				searched: {
					findCM: function(value) {				
						return vbsService.designSummaryProject.findCM(value)
						.then(function(response) {
								return response;
						})
						.catch(function(error) {
							utilService.getLogger().error(error);
						})
					},
					setSelected: function(item) {
						//$scope.searched.plan = item;
						vm.project.data[vm.project.index].cmPlanID = item.planId;
						vm.project.data[vm.project.index].cmName = item.customerName;
					}
					
				}
			},
			vm.product = {
				commodities: [
					{value: "1", display: "Capacitors"},
					{value: "2", display: "Resistors"},
					{value: "3", display: "Discretes"},
					{value: "4", display: "Electro-Magnetic Components"},
					{value: "5", display: "Connectors"},
					{value: "6", display: "Circuit Protection"},
					{value: "7", display: "Miscellaneous"},
					{value: "8", display: "Customers"},
					{value: "9", display: "Power"},
					{value: "10", display: "Sensors"},
					{value: "11", display: "Electro-Mechanical"},
				],
				protoTypeCalendarOpen: function(index, e) {			
					vm.product.data[index].protoTypeCalendarOpen = !vm.product.data[index].protoTypeCalendarOpen;				
				},
				productionCalendarOpen: function(index, e) {			
					vm.product.data[index].productionCalendarOpen = !vm.product.data[index].productionCalendarOpen;				
				},
				typeChanged: function(item) {
					item.changedValue = true;
				},
				changed: function(item) {
					var changedValue = false;
					
					var orgData = vm.project.orgData[vm.project.index].designSummaryProductContainerList[vm.product.index];

					if (!orgData) {
						return;
					}
					angular.forEach(item, function(value, key) {
							
						if (key != '$$hashKey') {
							if (typeof orgData[key] !== 'object') {
								if (orgData[key] != value) {									
									changedValue = true;
								}
							} 
						}
					})
					
					if (changedValue) {
						item.changedValue = true;
					}
				},				
				addRow: function(parent) {					
					parent.designSummaryProductContainerList.unshift(vm.designSummaryGroup.product.getContainer());
				},
				removeRow: function(parent, data) {
					var item = data ? data : parent.designSummaryProductContainerList[0];
					
					if (!item.projectID) {
						parent.designSummaryProductContainerList.splice(0, 1);
					} else {
						utilService.deleteConfirmation('Design Summary', 'You are about to delete the selected row.', 'btn-danger').result
						.then(
							function(result) {
								if (result == true) {
									vm.designSummaryGroup.product.delete(parent, item);
								}
							},
							function(dismiss) {
								
							}
						)
					}
					
										
				},
				selectRow: function(parentIndex, childIndex) {
					
					vm.project.selectRow(parentIndex);
					
					vm.product.index = childIndex;
					vm.product.row = vm.project.data[parentIndex].designSummaryProductContainerList[childIndex];
				},
				save: function(parent, data) {
					vm.designSummaryGroup.product.save(parent, data);
				}
			}				
		}
	}
export {designSummaryGroup, designSummary};