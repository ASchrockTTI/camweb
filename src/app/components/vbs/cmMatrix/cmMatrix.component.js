import cmMatrixTemplate from './cmMatrix.html';

const cmMatrixGroup =  {
		template:  '<div uib-accordion-group is-open="$ctrl.isOpen" ng-if="$ctrl.isMarketSegment()" class="panel-info" style="text-decoration: none !important; margin-top: 5px;"> ' +
		'<uib-accordion-heading> ' +
		'<span style="font-weight: bold;">{{$ctrl.formTitle}}</span> ' +		
		'<span class="pull-right" style="font-weight: bold; color: red; text-decoration: none;"></span> ' +	
		'<span id="cmmatrix-last-update" class="pull-right" style="text-decoration: none;">Last Update: {{$ctrl.maxUpdateDate}}</span> ' +
		'</uib-accordion-heading> ' +
		'<cm-matrix plan="$ctrl.plan" data="$ctrl.data" org-data="$ctrl.orgData"></cm-matrix> ' +	     
		'</div>',
		require: {
			vbs: '^^vbs'
		},
		bindings: {
			formTitle: '<',
			plan: '<'
		},
		controller: function(utilService, vbsService, userProfile) {
			
			var vm = this;
			var dataRetreived = false;
			
			vm.$onInit = function() {
				vm.isOpen = false;
				vm.data = [];
				vm.orgData = [];
				vm.cancelCollapse = false;
				vm.form;
			}
			
			vm.$onChanges = function(changes) {
				vm.plan = changes.plan.currentValue;				
			}
			
			vm.$doCheck = function() {
				
				//This is here to handle an IE bug where it doesn't update the variable after its loaded from the parent
				if (vm.vbs.cmMatrixDate) {
					vm.maxUpdateDate = vm.vbs.cmMatrixDate;
				};
				
				switch(vm.isOpen) {
				case true:
					
					if (dataRetrieved) {
						return;
					}
					
					dataRetrieved = true;
					
					if (!vm.cancelCollapse) {
						getData();
					} else {
						vm.cancelCollapse = false;
					}
					
					break;
				case false:
					dataRetrieved = false;
					
					if (!vm.form) {
						return;
					}
					
					if (vm.form.$dirty) {
						if (!window.confirm('Changes have been made to the CM Matrix form. You will lose your changes?\n\nDo you wish to continue?') ) {
							vm.cancelCollapse = true;
							vm.isOpen = true;
						} else {
							vm.form.$setPristine();
						}
					}
					break;
				}
			}
			
			
			
			
			
			vm.isMarketSegment = function() {
				
				if (!vm.plan) {
					return false;
				}
				
				if (vm.plan.marketSegment.marketSegmentCode.substring(0,1) === "4") {
					return true;
				}
				
				if (vm.plan.marketSegment.marketSegmentCode == "A-1" || vm.plan.marketSegment.marketSegmentCode == "A-3") {
					return true;
				}
				
				return false;
					
			}

			vm.getContainer = function() {
				return vbsService.cmMatrix.getContainer();
			}
			
			vm.save = function(data) {
				var container = {
					customer: data.customer,
					locationName: data.locationName,
					projectName: data.projectName,
					estAnnualRev: data.estAnnualRev,
					customerContact: data.customerContact,
					cmMgr: data.cmMgr,
					cmLocation: data.cmLocation,						
					vbsID: data.vbsContainer ? data.vbsContainer.vbsID : undefined,
					matrixID: data.matrixID,
					modifiedBy: userProfile.getFullName(),
					modifiedDateTime: new Date().getTime()
				}
				
				vm.maxUpdateDate = utilService.convertToISODate(new Date());
				
				if (container.vbsID > 0) {
					
					vbsService.cmMatrix.update(vm.plan.planId, container)
					.then(function(response) {
						data.saved = true;
						vm.form.$setPristine();
					})
					.catch(function(error) {
						
					})
					
				} else {
					vbsService.cmMatrix.save(vm.plan.planId, container)
					.then(function(response) {						
						data.matrixID = response.matrixID;
						data.vbsID = response.vbsID
						data.saved = true;
						vm.form.$setPristine();
					})
					.catch(function(error) {
						
					})
				}
			}
			
			vm.delete = function(data) {
				
				if (data.matrixID == undefined) {
					vm.data.splice(vm.data.length - 1, 1);
					return;
				}
								
				vbsService.cmMatrix.delete(data.matrixID)
				.then(function(response) {
					var arrayLen = vm.data.length;
					var index = -1;
					for(var lv = 0; lv < arrayLen; lv++) {
						if(vm.data[lv].matrixID == data.matrixID) {
							index = lv;
							break;
						}
					}
					if(index>-1) {
						vm.data.splice(index, 1);							
					}
					
					vm.form.$setPristine();
				})
				.catch(function(error) {
					
				});				
			}
			
			function getData() {
				vbsService.cmMatrix.getList(vm.plan.planId)
				.then(function(response) {						
					vm.data = response ? response : [];					
					vm.orgData = utilService.deepCopy(vm.data);
				})
				.catch(function(error) {
					
				});
			}
		}
	};
	
	const cmMatrix = {
		template: cmMatrixTemplate,
		bindings: {
			data: '<',
			orgData: '<',
			plan: '<'
		},
		controller: function(utilService, vbsService, accountService, permissionsFactory, userProfile, roles, $state, $stateParams) {
			
			var vm = this;
			var permissionsValidator;
			
			vm.$onInit = function() {
				//also called in "active" project component (consolidate at root component level)
				getProjects();
				
				
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
			}
			
//			vm.$postLink = function() {
//				vm.cmMatrixGroup.form = vm.cmMatrixForm;
//			}
			
			vm.permissions = {
				hasPermission: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);				
				},	
			}
			
			
			
			function getProjects() {
				vbsService.project.queryByStatus(parseInt($stateParams.planId), 'active')
				.then(function(response) {
					vm.projects = response;
				})
				.catch(function(error) {

				})
			}
			
			
			
			vm.migrate = function(oldProject) {
				utilService.uibModal().open({
					component: "vbsProjectModal",
					size: "lg",
					resolve: {
						modalData: function () {
					        return { 
					            plan: vm.plan,
					            project: {
					            	projectName: oldProject.projectName,
					            	estAnnualRevenue: oldProject.estAnnualRev,
					            	businessPartnerContactName: oldProject.customerContact,
					            	programManagerName: oldProject.cmMgr
					            },
					            oldProject: oldProject,
					            activeProjects: vm.projects
					        }
					    }
					}
				})
				.result.then(
				function(result) {
					var params = {
	                    planId: $stateParams.planId,
	                    type: 'active',
	                    projectId: result.vbsProjectId
	                };
					$state.go('vbs', params, {reload: true});
				},
				function(dismiss) {
					
				})
		    }
			
			vm.deleteProject = function(project) {
				utilService.deleteConfirmation(
	              "VBS",
	              "You are about to delete \"" + project.projectName + "\".",
	              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		vbsService.cmMatrix.delete(project.matrixID)
		            		.then(function() {
		            			vm.data.splice(vm.data.indexOf(project), 1);
            				})
							.catch(function(error) {
								
							})
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
			
			
			
		}
	}
export {cmMatrixGroup, cmMatrix};