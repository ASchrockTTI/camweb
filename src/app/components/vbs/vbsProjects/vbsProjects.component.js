import vbsProjectsTemplate from './vbsProjects.html';

const vbsProjects =  {
		template: vbsProjectsTemplate,
		bindings: {
			status: '<',
			plan: '<'
		},
		controller: function(
			$rootScope,
			$scope,
			$timeout,
			userProfile,
			utilService,
			permissionsFactory,
			accountService,
			roles,
			$state,
			$stateParams,
			omsService,
			contactService,
			vbsService
			) {
			var vm = this;
			var permissionsValidator;
			
			vm.params = $stateParams;
			
			vm.projects = [];
			
			vm.$onInit = function() {
				
				vm.year = {
					current: new Date().getFullYear(),
					next: new Date().getFullYear() + 1
				};
				
				
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);

				init();
			}
			
			
			function init() {
				var name = userProfile.getFullName();
				if(name !== 'undefined undefined') {
					getProjects();
				}
			}

			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(init(), 100);
			});

			vm.isSelected = function(project) {
				return (project.vbsProjectId === parseInt(vm.params.projectId));
			}
			
			vm.permissions = {
				hasPermission: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);				
				}	
			}
			
			function getProjects() {
				vbsService.project.queryByStatus(vm.params.planId, vm.status)
				.then(function(response) {
					vm.projects = response;
					var fullName = userProfile.getFullName();
					if(vm.projects && vm.projects.length > 0) {
						for(var indx = 0; indx < vm.projects.length; indx++) {
							var project = vm.projects[indx];
							project.isOwner = false;
							if(project.ownerName && fullName && project.ownerName.toUpperCase() === fullName.toUpperCase()) {
								project.isOwner = true;
							}
						}
					}
				})
				.catch(function(error) {

				})
			}
			
			vm.openPartnerPlan = function(planId) {
				$state.go("businessPlan", {planId: planId});
			}
			
			vm.deleteProject = function(project) {
				utilService.deleteConfirmation(
	              "VBS",
	              "You are about to delete \"" + project.projectName + "\".",
	              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		vbsService.project.delete(project)
		            		.then(function(projectId) {
		            			vm.projects.splice(vm.projects.indexOf(project), 1);
            				})
							.catch(function(error) {
								
							})
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
			
			vm.projectModal = function(project) {
				utilService.uibModal().open({
					component: "vbsProjectModal",
					size: "lg",
					resolve: {
						modalData: function () {
					        return { 
					            plan: vm.plan,
					            project: project,
					            activeProjects: vm.projects
					        }
					    }
					}
				})
				.result.then(
				function(result) {
					for(var i = 0; i < vm.projects.length; i++) {
						if(vm.projects[i].vbsProjectId === result.vbsProjectId) {
							vm.projects[i] = result;
							return;
						}
					}
					vm.projects.push(result);
				},
				function(dismiss) {
					
				})
		    }
			
			
			vm.sort = {
	          type: "plannedModificationDate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
			
			vm.filterBusinessPartnerName = function (businessPartnerName){
				if(vm.businessPartnerNameFilter) {
					vm.businessPartnerNameFilter = undefined;
					return;
				}

				vm.businessPartnerNameFilter = businessPartnerName;
			};
			
			
		}
	}
export default vbsProjects;