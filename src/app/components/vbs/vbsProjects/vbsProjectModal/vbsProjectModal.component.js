import vbsProjectModalTemplate from './vbsProjectModal.html';

const vbsProjectModal =  {
		template: vbsProjectModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			contactService,
			omsService,
			utilService,
			vbsService,
			$filter,
			$scope,
			$state,
			$stateParams
		) {
			
			var vm = this;
			
			vm.params = $stateParams;
			
			vm.showCustomerProjects = true;
			vm.showPartnerProjects = true;
			
			vm.year = {
				current: new Date().getFullYear(),
				next: new Date().getFullYear() + 1
			};
			
			vm.popovers = [
				{
					title: 'Product Overview',
					templateUrl: 'popoverProductOverview.html'
				},
				{
					title: 'Planned Modifications',
					templateUrl: 'popoverPlanModification.html'
				},
				{
					title: 'EMS Involvement',
					templateUrl: 'popoverEmsInvolvement.html'
				}
			];
			
			
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.plan = vm.data.plan;
				vm.customerVbsProjects = vm.data.activeProjects;
				
				getCustomerProjects();
				getCustomerContacts();
				
                if(vm.data.project) {
                	vm.project = vm.data.project;
                	
                	if(vm.data.oldProject) {
                		vm.oldProject = vm.data.oldProject;
                		vm.project.migrate = true;
                		vm.project.originalProjectId = vm.oldProject.matrixID;
                		vm.modalTitle = 'Migrate Project';
                	} else {
                		vm.modalTitle = 'Edit Project';
                	}
                	
                	vm.originalBusinessPartnerName = vm.project.businessPartnerName;
                	vm.originalBusinessPartnerContactName = vm.businessPartnerContactName;
                	
                	if(vm.project.businessPartnerPlanId) {
                		getPartnerProjects();
        				getPartnerContacts();
                	}
                    vm.confirmLabel = 'Save';
                } else{
                	vm.project = {projectName: ''};
                	vm.modalTitle = 'Add New Project';
                    vm.confirmLabel = 'Create';
                }
                
                vm.original = JSON.parse(JSON.stringify(vm.project));
                    
                setTimeout(function(){
					var input = angular.element( document.querySelector( 'input' ) );
					input.focus();
				}, 500);
			}
			
			
			function getCustomerProjects() {
				vm.loadingCustomerProjects = true;
				omsService.retrieveProjectsByPlan(vm.params.planId, 'active')
				.then(function(response) {
					vm.customerOmsProjects = response;
					vm.loadingCustomerProjects = false;
					vm.verifyUnique();
				})
			}
			
			function getPartnerProjects() {
				if(vm.project.businessPartnerPlanId === parseInt(vm.params.planId)) return;
				
				vm.loadingPartnerProjects = true;
				vm.partnerOmsProjects = [];
				omsService.retrieveProjectsByPlan(vm.project.businessPartnerPlanId, 'active')
				.then(function(response) {
					vm.partnerOmsProjects = response;
					vm.loadingPartnerProjects = false;
					vm.verifyUnique();
				})
				
				vbsService.project.queryByStatus(vm.project.businessPartnerPlanId, 'active')
				.then(function(response) {
					vm.partnerVbsProjects = response;
				})
				.catch(function(error) {
 
				})
			}
			
			function getCustomerContacts() {
				vm.loadingCustomerContacts = true;
				contactService.retrieveContacts(vm.plan.planId)
				.then(function (response) {
					vm.loadingCustomerContacts = false;
					vm.customerContacts = contactService.getCustomers();
					
					if(vm.project.programManagerName && !vm.project.programManagerContactId) {
						findContact(vm.project.programManagerName, vm.customerContacts);
					}
				})
				.catch(function (error) {

				})
			}
			
			function getPartnerContacts() {
				vm.loadingPartnerContacts = true;
				contactService.retrieveContacts(vm.project.businessPartnerPlanId)
				.then(function (response) {
					vm.loadingPartnerContacts = false;
					vm.partnerContacts = contactService.getCustomers();
				})
				.catch(function (error) {

				})
			}
			
			function findContact(name, contactList) {
				angular.forEach(contactList, function(contact) {
					if(contact.delegateName === name) {
						vm.selectProgramManager(contact);
					}
				})
			}
			
			vm.selectPartner = function(partner) {
				vm.originalBusinessPartnerName = partner.customerName;
				vm.project.businessPartnerName = partner.customerName;
				vm.project.businessPartnerPlanId = partner.planId;
				getPartnerProjects();
				getPartnerContacts();
			}
			
			vm.openOmsProject = function(planId, project) {
				var params = {
                    planId: planId,
                    type: 'active',
                    projectId: project.projectId
                };
						
				$state.go('oms', params);
				
				vm.modalInstance.dismiss();
			}
			
			vm.openVbsProject = function(planId, project) {
				var params = {
                    planId: planId,
                    type: 'active',
                    projectId: project.vbsProjectId
                };
				$state.go('vbs', params);
				
				vm.modalInstance.dismiss();
			}
			
			vm.selectContact = function(contact) {
				vm.originalBusinessPartnerContactName = contact.delegateName;
				vm.project.businessPartnerContactName = contact.delegateName;
				vm.project.businessPartnerContactId = contact.delegateId;
			}
			
			vm.selectProgramManager = function(contact) {
				vm.originalProgramManagerName = contact.delegateName;
				vm.project.programManagerName = contact.delegateName;
				vm.project.programManagerContactId = contact.delegateId;
			}
			
			vm.togglePartnerProjects = function(event) {
				event.stopPropagation();
				vm.showPartnerProjects = !vm.showPartnerProjects;
			}
			
			vm.toggleCustomerProjects = function(event) {
				event.stopPropagation();
				vm.showCustomerProjects = !vm.showCustomerProjects;
			}
			
			vm.interceptEvent = function(event) {
				event.stopPropagation();
			}
			
			$scope.customerVbsProjectFilter = function() {
				return function( project ) {
					return project.vbsProjectId !== vm.project.vbsProjectId;
				}
			}
			
			$scope.$watch('$ctrl.project.businessPartnerName', function() {
				checkBusinessPartner();
		    });
			
			$scope.$watch('$ctrl.project.businessPartnerContactName', function() {
				checkBusinessPartnerContact();
		    });
			
			$scope.$watch('$ctrl.project.programManagerName', function() {
				checkProgramManager();
		    });
			
			function checkBusinessPartner() {
				if(!vm.project.businessPartnerName || vm.project.businessPartnerName !== vm.originalBusinessPartnerName) {
					vm.project.businessPartnerPlanId = null;
					vm.project.businessPartnerContactId = null;
					vm.partnerOmsProjects = [];
					vm.partnerVbsProjects = [];
					vm.verifyUnique();
				}
			}
			
			function checkBusinessPartnerContact() {
				if(!vm.project.businessPartnerContactName || vm.project.businessPartnerContactName !== vm.originalBusinessPartnerContactName) {
					vm.project.businessPartnerContactId = null;
				}
			}
			
			function checkProgramManager() {
				if(!vm.project.programManagerName || vm.project.programManagerName !== vm.originalProgramManagerName) {
					vm.project.programManagerContactId = null;
				}
			}
			
			vm.verifyUnique = function() {
				
				if(vm.customerOmsProjects){
					var matchedCustomerOmsProjects = $filter('filter')(vm.customerOmsProjects, function(project) {
						return (project.projectName.toLowerCase() === vm.project.projectName.toLowerCase());
					}, true);
					if(matchedCustomerOmsProjects.length > 0) return setProjectNameValidity(false);
				}
				
				if(vm.customerVbsProjects) {
					var matchedCustomerVbsProjects = $filter('filter')(vm.customerVbsProjects, function(project) {
						return (project.projectName.toLowerCase() == vm.project.projectName.toLowerCase() && project.vbsProjectId !== vm.project.vbsProjectId);
					}, true);
					if(matchedCustomerVbsProjects.length > 0) return setProjectNameValidity(false);
				}
				
				if(vm.partnerOmsProjects) {
					var matchedPartnerOmsProjects = $filter('filter')(vm.partnerOmsProjects, function(project) {
						return project.projectName.toLowerCase() == vm.project.projectName.toLowerCase();
					}, true);
					if(matchedPartnerOmsProjects.length > 0) return setProjectNameValidity(false);
				}
				
				if(vm.partnerVbsProjects) {
					var matchedPartnerVbsProjects = $filter('filter')(vm.partnerVbsProjects, function(project) {
						return project.projectName.toLowerCase() == vm.project.projectName.toLowerCase();
					}, true);
					if(matchedPartnerVbsProjects.length > 0) return setProjectNameValidity(false);
				}
				
				
				setProjectNameValidity(true);
			}
			
			function setProjectNameValidity(val) {
				vm.vbsProjectForm.projectName.$setValidity("unique", val);
			}
			
			vm.save = function() {
				vm.project.planId = vm.params.planId;

				if(!vm.project.businessPartnerContactName) vm.project.businessPartnerContactId = null;
				if(!vm.project.programManagerName) vm.project.programManagerContactId = null;
				
				vbsService.project.save(vm.project)
				.then(function(response) {
					response.isOwner = true;
					vm.original = JSON.parse(JSON.stringify(response));
					vm.modalInstance.close(response);
				})
				.catch(function(error) {
					vm.modalInstance.dismiss();
				})
			}
			
			
			vm.cancel = function() {
				vm.modalInstance.dismiss();
			}
			
			
			vm.createNewCustomer = function() {
				utilService.uibModal().open({
					component: 'submitNewCustomerModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: { 
	                	modalData: function() {
	                    	return {
	                    		customerName: vm.project.businessPartnerName,
	                    		context: 'vbs'
	                    	}
	                	}
	                }
				}).result.then(
					function(result) {
						vm.originalBusinessPartnerName = result.customerName;
						vm.project.businessPartnerName = result.customerName;
						vm.project.businessPartnerPlanId = result.planId;
					},
					function(dismiss) {
						
					}
				)
			}
			
			
			vm.$onDestroy = function () {
                angular.copy(vm.original, vm.project);
            };
		}
	}
export default vbsProjectModal;