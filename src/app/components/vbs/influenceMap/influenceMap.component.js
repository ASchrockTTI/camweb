import influenceMapTemplate from './influenceMap.html';
import influenceMapModalTemplate from './influenceMapModal.html'

const influenceGroup =  {
//		template: '<div uib-accordion-group is-open="$ctrl.isOpen" class="panel-info" style="text-decoration: none !important; margin-top: 5px;"> ' +
//		'<uib-accordion-heading> ' +
//		'<span style="font-weight: bold;">{{$ctrl.formTitle}}</span> ' +
//		'<span class="pull-right" style="font-weight: bold; color: red; text-decoration: none;"></span> ' +		  
//		'</uib-accordion-heading> ' +
//		'<influence-map mapping="$ctrl.influeMapData"></influence-map> ' +	     
//		'</div>'
		template: '<influence-map mapping="$ctrl.influenceMapData"></influence-map> '
		,
//		require: {
//			vbs: '^^vbs'
//		},
		bindings: {
			formTitle: '<',
			plan: '<'
		},
		controller: function(utilService, vbsService, $stateParams, $rootScope, $scope) {
			var vm = this;
			vm.params = $stateParams;
			var dataRetrieved = false;

			vm.$onInit = function() {
				vm.isOpen = false			
				vm.influenceMapData = {}
				vm.plan = undefined;
				
				if (dataRetrieved) {
					return;
				}

				dataRetrieved = true;

				getData();	
			}
			
			$rootScope.$on('contactsRefresh', function(event, data) {
				getData();
			});

			vm.$onDestroy = function() {
				vm.isOpen = false				
				vm.influenceMapData = {}

				for (var i = 0; i <= vbsService.customerInfluencer.roles.length - 1; i++) {
					vbsService.customerInfluencer.roles[i].data = []
				}
			}


			vm.$doCheck = function() {

				switch(vm.isOpen) {
				case true:

					if (dataRetrieved) {
						return;
					}

					dataRetrieved = true;

					getData();					

					break;
				case false:
					dataRetrieved = false;
					break;
				}
			}

			function getData() {
				//vbsService.customerInfluencer.clearRoleData();
				vbsService.customerInfluencer.getList(vm.params.planId)
				.then(function(response) {

					vm.influenceMapData = {
							materials: vbsService.customerInfluencer.getRole(0),
							engineeringDesign: vbsService.customerInfluencer.getRole(1),
							supplyChain: vbsService.customerInfluencer.getRole(2),
							quality: vbsService.customerInfluencer.getRole(3),
							finance: vbsService.customerInfluencer.getRole(4),
							oemOutsource: vbsService.customerInfluencer.getRole(5),
							cmProgram: vbsService.customerInfluencer.getRole(6),
							manufacturingProd: vbsService.customerInfluencer.getRole(7),
							executive: vbsService.customerInfluencer.getRole(8)
					}
				})
				.catch(function(error) {

				});
			}
		}
	}
	const influenceMap = {
		template: influenceMapTemplate,
//		require: {
//			vbsGroupInfluence: '^^influenceGroup'
//		},
		bindings: {
			mapping: '<'
		},	    	 
		transclude: true,
		controller: function() {	    		   	    		 

			var vm = this;

			vm.$onInit = function() {
				vm.mapData = {};
			}

			vm.$onDestroy = function() {	    			   
				vm.mapData = {};
			}

			vm.$onChanges = function(changes) {
				vm.mapData = {};
				vm.mapData = changes.mapping.currentValue;	    			   
			}
		},
		controllerAs: 'imap'
	}
	// Influence Map Data (td cell content)
	const influenceMapData = {
/* 		require: {
			influenceMap: '^^influenceMap'
		}, */
		bindings: {
			data: '<'
		},	    		    	   
		template: '<span style="display: block; height: 125px;" ng-style="{\'background\': $ctrl.getGradient($ctrl.heatmap.data)}" class="cursor-pointer" ' +
		'ng-if="$ctrl.heatmap.data.planDelegateList.length > 0" ' +
		'ng-click="$ctrl.open(\'md\', $ctrl.heatmap.data.planDelegateList)"> ' +
		'<span class="fa-layers fa-fw fa-3x" style="margin-top: 45px;"> ' +
		'<i class="fas fa-user"></i> ' +
		'<span class="fa-layers-counter" style="background:Tomato">{{$ctrl.heatmap.data.planDelegateList.length}}</span> ' +
		'</span> ' +
		
		'</span> '						 
		,
		controller: function($uibModal, $scope) {

			var vm = this;

			vm.$onInit = function() {	
				vm.heatmap = {};
			}

			vm.$onDestroy = function() {
				vm.heatmap = {};
			}
			

			vm.$onChanges = function(changes) {
				vm.heatmap = null;
				vm.heatmap = changes.data.currentValue;						
			}
			
			vm.getGradient = function(item) {

				if (!item) {
					return;
				}

				if (!item.gradient) {
					return;
				}

				var gradient = item.gradient.replace('green','#007E33').replace('red','#CC0000').replace('yellow','#FF8800');				
				var gradientList = gradient.split(',');

				if (gradientList.length == 1) {
					return gradientList[0];
				} else {
					return 'linear-gradient(' + gradient + ')';
				}				
			}

			vm.open = function() {

				$uibModal.open({
					component: 'influenceMapModal',
					resolve: {
						data: function() {
							return vm.heatmap.data.planDelegateList
						}
					}
				}).result.then(
						function(result) {

						}, function(dismissed) {

						});												
			}
		}
	}
	// Influence Map Modal
	const influenceMapModal = {
		template: influenceMapModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function() {

			var modal = this;

			modal.$onInit = function() {	    			   
				modal.modalData = modal.resolve.data;	    			   
			}

			modal.$onDestroy = function() {
				modal.modalData = {};
			} 

			modal.getLabel = function(item) {
				if (!item) {
					return;
				}

				switch(item.spw.spwID) {
				case 1:
					return 'label label-success';
				case 2:
					return 'label label-warning';
				case 3:
					return 'label label-danger';
				}
			}

			modal.buttons = {
					cancel: function() {
						modal.modalInstance.close();
					}
			}

		}
	}
export {influenceGroup, influenceMap, influenceMapModal, influenceMapData};