import productionSummaryTemplate from './productionSummary.html';

const productionSummaryGroup =  {
		template: '<div uib-accordion-group is-open="$ctrl.isOpen" class="panel-info" style="text-decoration: none !important; margin-top: 5px;"> ' +
		'<uib-accordion-heading> ' +
		'<span style="font-weight: bold;">{{$ctrl.formTitle}}</span> - ' +
		'<span>{{$ctrl.subTitle}}</span> ' +
		'<span class="pull-right" style="font-weight: bold; color: red; text-decoration: none;"></span> ' +	
		'<span id="production-summary-last-update" class="pull-right" style="text-decoration: none;">Last Update: {{$ctrl.maxUpdateDate}}</span> ' +
		'</uib-accordion-heading> ' +
		'<production-summary data="$ctrl.data" org-data="$ctrl.orgData"></production-summary> ' +	     
		'</div>',
		require: {
			vbs: '^^vbs'
		},
		bindings: {
			formTitle: '<',
			subTitle: '<',
			plan: '<'
		},
		controller: function(utilService, vbsService, userProfile) {			
			var vm = this;
			var dataRetrieved = false;
			
			vm.form;
			
			vm.$onInit = function() {
				vm.isOpen = false;
				vm.data = [];
				vm.orgData = [];
				vm.cancelCollapse = false;				
			}			
			
			vm.$onChanges = function(changes) {
				vm.plan = changes.plan.currentValue;				
			}
			
			vm.$doCheck = function() {
				
				//This is here to handle an IE bug where it doesn't update the variable after its loaded from the parent
				if (vm.vbs.productionSummaryDate) {
					vm.maxUpdateDate = vm.vbs.productionSummaryDate;
				};
				
				switch(vm.isOpen) {
				case true:
					
					if (!vm.cancelCollapse) {
						
						if (dataRetrieved) {
							return;
						}
						
						dataRetrieved = true;
						
						getData();
					} else {
						vm.cancelCollapse = true;
					}
					
					break;
				case false:
					dataRetrieved = false;
					
					if (!vm.form) {
						return;
					}
					
					if (vm.form.$dirty) {						
																		
						if (!window.confirm('Changes have been made to this form. You will lose your changes?\n\nDo you wish to continue?') ) {
							vm.cancelCollapse = true;
							vm.isOpen = true;
						} else {
							vm.cancelCollapse = false;
							vm.form.$setPristine();
						}
					}
					break;
				}
				
			}			 
			
			vm.getContainer = function() {
				return vbsService.productionSummary.getContainer();
			}
			
			vm.save = function(data) {				
				
				var container = {
					contractManufacturer: data.contractManufacturer,
					contractManufacturerPlanID: data.contractManufacturerPlanID,
					costReduction: data.costReduction,
					firstProduction: data.firstProduction ? new Date(data.firstProduction).getTime() : undefined,
					productName: data.productName,
					reDesign: data.reDesign,
					unitForecast: data.unitForecast,
					unitProduction: data.unitProduction,
					plannedModificationDate: data.plannedModificationDate ? new Date(data.plannedModificationDate).getTime() : undefined,
					vbsID: data.vbsID,
					productionSummaryID: data.productionSummaryID,
					modifiedBy: userProfile.getFullName(),
					modifiedDateTime: new Date().getTime()
				}
				
				if (data.selectedPlannedModification) {
					container.productionSummaryPlanModification = {
						planModID: data.selectedPlannedModification.value,
						planMod: data.selectedPlannedModification.display
					}
				} else {
					container.productionSummaryPlanModification = {
							planModID: undefined,
							planMod: undefined
						}
				}	
				
				vm.maxUpdateDate = utilService.convertToISODate(new Date());
				
				if (container.vbsID > 0) {
					
					vbsService.productionSummary.update(vm.plan.planId, container)
					.then(function(response) {
						data.saved = true;
						vm.form.$setPristine();
					})
					.catch(function(error) {
						
					})
					
				} else {
					vbsService.productionSummary.save(vm.plan.planId, container)
					.then(function(response) {						
						data.productionSummaryID = response.productionSummaryID;
						data.vbsID = response.vbsID
						data.saved = true;
						vm.form.$setPristine();
					})
					.catch(function(error) {						
					})
				}				
			}
			
			vm.delete = function(data) {
				vbsService.productionSummary.delete(data.productionSummaryID)
				.then(function(response) {
					var arrayLen = vm.data.length;
					var index = -1;
					for(var lv = 0; lv < arrayLen; lv++) {
						if(vm.data[lv].productionSummaryID == data.productionSummaryID) {
							index = lv;
							break;
						}
					}
					if(index>-1) {
						vm.data.splice(index, 1);							
					}
					
					vm.form.$setPristine();
				})
				.catch(function(error) {
					
				});				
			}
			
			function getData() {
				vbsService.productionSummary.getList(vm.plan.planId)
				.then(function(response) {							
					vm.data = response ? response : [];					
					vm.orgData = utilService.deepCopy(vm.data);					
				})
				.catch(function(error) {
					
				});	
			}
		}
	}
	const productionSummary = {
		template: productionSummaryTemplate,
		bindings: {
			data: '<',
			orgData: '<',
			plan: '<'
		},
		controller: function($rootScope, userProfile, utilService, permissionsFactory, accountService, roles) {
			var vm = this;
			var permissionsValidator;
			
			vm.$onInit = function() {
				vm.year = {
					current: new Date().getFullYear(),
					next: new Date().getFullYear() + 1
				};
				
				
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE]);
								
			}	
			
//			vm.$postLink = function() {
//				vm.productionSummaryGroup.form = vm.productionSummaryForm;
//			}
			
			vm.permissions = {
				validated: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);				
				},	
			}
			
			vm.selectRow = function(index) {
				vm.index = index;
				vm.row = vm.data[index];				
			}
			
			vm.addRow = function() {
				vm.data.unshift(vm.productionSummaryGroup.getContainer())							
			}			
			
			vm.removeRow = function(data) {
				
				var item = data ? data : vm.data[0];
				
				if (!item.vbsID) {
					vm.data.splice(0, 1);
				} else {
					
					utilService.deleteConfirmation('Production Summary', 'You are about to delete the selected row.', 'btn-danger').result
					.then(
						function(result) {						
							if (result === true) {								
								vm.productionSummaryGroup.delete(item);
							}
						},
						function(dismiss) {
							
						}					
					)					
				}			
			}
			
			vm.calendarFirstProductionOpen = function(index, e) {			
				vm.data[index].calendarFirstProductionOpen = !vm.data[index].calendarFirstProductionOpen;				
			}
			
			vm.calendarPlannedModicationOpen = function(index, e) {			
				vm.data[index].calendarPlannedModicationOpen = !vm.data[index].calendarPlannedModicationOpen;				
			}
			
			vm.typeChanged = function(item) {
				item.saved = false;
				item.changedValue = true;
				$rootScope.$emit('preventnav', {isDirty: true});
			}
			
			vm.changed = function(item) {
				
				var changedValue = false;
				item.changedValue = false
				item.saved = false;
				
				if (vm.index == undefined) {
					return;
				}
				
				if (vm.orgData[vm.index] == undefined) {
					return;
				}
				
				angular.forEach(item, function(value, key) {
										
					if (key != '$$hashKey') {
						if (vm.orgData[vm.index][key] != value) {
							changedValue = true;							
						}
					}					
				})
				
				if (changedValue) {
					item.changedValue = true;
					$rootScope.$emit('preventnav', {isDirty: true});
				}				
				
			}
			
			vm.save = function(item) {
				$rootScope.$emit('preventnav', {isDirty: false});
				vm.productionSummaryGroup.save(item);
			}
			
			vm.loadPlan = function(data) {
				utilService.location.path('plans/' + data.contractManufacturerPlanID);
			}
			
			vm.searched = {
				plan: undefined,
				planText: undefined,
				findPlan: function(value) {
					
					vm.noResults = undefined;
					
					return accountService.findPlans(value, false)
					.then(function(response) {
							return response;
					})
					.catch(function(error) {
						
					})
				},
				setSelectedPlan: function(item) {				
					vm.searched.plan = item;
					vm.data[vm.index].contractManufacturerPlanID = item.planId;
					vm.data[vm.index].contractManufacturer = item.customerName;				
				}
			}
			
		}
	}
export { productionSummaryGroup, productionSummary };