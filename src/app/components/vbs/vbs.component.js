import vbsTemplate from './vbs.html';

const vbs =  {
		template: vbsTemplate,
		controller: function($stateParams, vbsService, planService, utilService, $scope, $state) {
			
			var vm = this;
			vm.params = $stateParams;
			
			vm.$onInit = function() {
				vm.plan = undefined;
				vm.productionSummaryDate = undefined;
				vm.designSummaryDate = undefined;
				vm.cmMatrixDate = undefined;
				
				retrievePlanById(vm.params.planId);
				retrieveLastUpdates(vm.params.planId);
				
				getCmMatrixData();
				getProductionSummaryData();
			}
			
			vm.$onDestroy = function() {
				vm.plan = undefined;
			}
			
			vm.selectTab = function(type) {
				$state.go('.', {type: type, projectId: undefined});
			}
			
			 $scope.$watch(function(){ return vm.params.type }, function(){
				 if($stateParams.type === 'archived') {
					vm.activeTabIndex = 1;
				} else if($stateParams.type === 'oldProdSummary') {
					vm.activeTabIndex = 2;
				} else if($stateParams.type === 'oldCmMatrix') {
					vm.activeTabIndex = 3;
				} else {
					vm.activeTabIndex = 0;
				}
			 })
			
			function retrievePlanById(planId) {
				
				planService.lookupPlan(planId)
				.then(function(response) {
					vm.plan = response;
					planService.setPlan(vm.plan);				
				})
				.catch(function(error) {
					
				})
			}
			 
			function getProductionSummaryData() {
				vbsService.productionSummary.getList(vm.params.planId)
				.then(function(response) {							
					vm.productionSummaryData = response ? response : [];					
				})
				.catch(function(error) {
					
				});	
			}
				 
			 function getCmMatrixData() {
				vbsService.cmMatrix.getList(vm.params.planId)
				.then(function(response) {						
					vm.cmMatrixData = response ? response : [];					
				})
				.catch(function(error) {
					
				});
			}
			
			function retrieveLastUpdates(planId) {
				
				vbsService.forms.getList(planId)
				.then(function(response) {
					
					if (response.length == 0) {
						return;
					}
					
					for (var i = 0; i <= response.length - 1; i++) {
						switch(response[i].formID) {
						case 1:
							vm.productionSummaryDate = utilService.convertToISODate(response[i].lastUpdateDate);
							break;
						case 2:
							vm.designSummaryDate = utilService.convertToISODate(response[i].lastUpdateDate);
							break;
						case 3:
							vm.cmMatrixDate = utilService.convertToISODate(response[i].lastUpdateDate);
							break;
						}
					}
				})
				.catch(function(error) {
					
				})
			}
		}
	}
export default vbs;