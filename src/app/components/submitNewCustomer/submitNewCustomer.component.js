import submitNewCustomerModalTemplate from './submitNewCustomerModal.html';

const submitNewCustomer =  {
		template: submitNewCustomerModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
				utilService,
				pendingCustomerService,
				planService,
				userProfile,
				$state,
				$stateParams
			) {
			
			var vm = this;
			vm.params = $stateParams;
			
			vm.pendingCustomer = {
				customerName: '',
				city: undefined,
				state: undefined,
				country: undefined,
				postalCode: undefined,
				branch: undefined,
				marketSegment: undefined
			}
			
			
			vm.$onInit = function() {
				vm.title = "Submit New Customer";
				if(vm.resolve.modalData) {
					vm.data = vm.resolve.modalData;
					
					if(vm.data.pendingCustomer) {
						vm.edit = true;
						vm.title = "Edit Pending Customer";
						vm.pendingCustomer = vm.data.pendingCustomer;
					} else { 
						vm.pendingCustomer.customerName = vm.data.customerName;
						if(vm.data.context === 'oms') {
							vm.pendingCustomer.omsProjectId = vm.params.projectId;
						} else if (vm.data.context === 'vbs') {
							vm.pendingCustomer.vbsProjectId = vm.params.projectId;
						}
						vm.pendingCustomer.relatedPlanId = vm.params.planId;
					}
				}
				
				getBranches();
				getMarketSegments();

				setTimeout(function () {
					var input = angular.element(document.querySelector('#customerNameInput'));
					input.focus();
				}, 500);
			}
		
			vm.submit = function() {
				pendingCustomerService.submitCustomer(vm.pendingCustomer)
				.then(function(response) {
					if(!vm.edit) {
						response.new = true;
					}
					sessionStorage.removeItem('sessionPlans');
					sessionStorage.removeItem('sessionPlansNonTTI');
					vm.modalInstance.close(response);
				})
				.catch(function(error) {
					
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			function getBranches() {
				planService.getEntityParentBranches('All')
				.then(function(response) {
					vm.branches = response;
					
					if(!vm.branches && vm.branches.length == 0) {
						return;
					}
					
					var branchLocation = userProfile.getBranchLocation();
					var userBranch = undefined;
					

					for(var key in vm.branches) {
						var branch = vm.branches[key];
						if(branch.branch === branchLocation) {
							userBranch = branch;
							break;
						}
					}

					if(userBranch) {
						vm.pendingCustomer.branch = userBranch;
					}
				})
				.catch(function(error) {
					
				})
			}
			
			
			function getMarketSegments() {
				planService.getMarketSegments()
				.then(function(response) {
					vm.marketSegments = [];
					angular.forEach(response, function(segment) {
						vm.marketSegments.push.apply(vm.marketSegments, segment.subCategoryList)
					})
				})
				.catch(function(error) {
					
				})
			}

		}
			
	}
export default submitNewCustomer;