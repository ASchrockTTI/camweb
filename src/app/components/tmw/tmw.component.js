import tmwTemplate from "./tmw.html";
const tmw = {
  template: tmwTemplate,
  controller: function (
    $scope,
    tmwService,
    roles,
    utilService,
    userProfile,
    authService,
    userInfoService
  ) {
    var vm = this;
    $scope.Math = window.Math;

    vm.selectedQuarter = {
      year: undefined,
      quarter: undefined,
    };

    vm.loadedQuarter = {
      year: undefined,
      quarter: undefined,
    };

    vm.$onInit = function () {
      vm.search = false;
      vm.sort.type = "totalRunRate";
      vm.sort.reverse = true;
      vm.clientError = utilService.getClientErrorObject();
      loadTmw();
    };

    function canUserEditTarget() {
      let realUserType = userProfile.getUserTypeName().toLowerCase();

      if (
        realUserType === "gm" ||
        realUserType === "fsm" ||
        realUserType === "admin"
      ) {
        return true;
      }

      return false;
    }

    function getSelectedFsr() {
      if (sessionStorage.getItem("selectedFsr")) {
        return JSON.parse(sessionStorage.getItem("selectedFsr"));
      } else {
        return {
          name: userProfile.getFullName(),
          branch: userProfile.getBranchLocation(),
        };
      }
    }

    function getQuarters() {
      var tmwContainer = {
        securityProfile: {},
        quarter: undefined,
      };

      tmwContainer.securityProfile = {
        fullName: userProfile.getFullName(),
        branchLocation: userProfile.getBranchLocation(),
        physicalDeliveryOfficeName: userProfile.getBranchLocation(),
      };

      tmwService
        .getQuarterRange(tmwContainer)
        .then(function (response) {
          var currentQuarter = response[0];
          var oldestQuarter = response[1];

          vm.quarters = [];
          var year = currentQuarter.year;
          var quarter = currentQuarter.quarter;
          vm.quarters.push({ quarter: quarter, year: year });
          for (var i = 0; i < 3; i++) {
            if (quarter === oldestQuarter.quarter) {
              break;
            }
            quarter--;
            if (quarter === 0) {
              quarter = 4;
              year--;
            }
            vm.quarters.push({ quarter: quarter, year: year });
          }

          getSelectedQuarter();
        })
        .catch(function (error) {});
    }

    function getSelectedQuarter() {
      if (vm.quarters.length > 1 && sessionStorage.getItem("selectedQuarter")) {
        vm.selectedQuarter = JSON.parse(
          sessionStorage.getItem("selectedQuarter")
        );
      } else {
        vm.selectedQuarter.year = vm.quarters[0].year;
        vm.selectedQuarter.quarter = vm.quarters[0].quarter;
      }

      var userType = userProfile.getUserTypeName().toUpperCase();
      getView(userType);
    }

    vm.isAdmin = function () {
      return authService.isAdmin();
    };

    vm.isBeta = function () {
      return userProfile.isBeta();
    };

    function getView(userType) {
      switch (userType) {
        case "EXECUTIVE":
        case "ADMIN":
          vm.search = true;
          if (
            vm.selectedFsr.name !== userProfile.getFullName() &&
            vm.selectedFsr.name != "Select FSR"
          ) {
            vm.osrSelected = vm.selectedFsr.name;
          }
          getView("default");
          break;
        case "SAM_GAM":
          userInfoService
            .getUserCorpIdList(userProfile.getSAMAccountName())
            .then(function (response) {
              vm.corpIdList = response;
              if (vm.corpIdList.length > 0 && !vm.selectedCorpId) {
                vm.selectCorpId(vm.corpIdList[0]);
              }
              getView("default");
            });
          break;
        case "FSR":
          retrieveTmw();
          break;
        case "FSM":
        case "ISM":
        case "GM":
          retrieveFsrList();
          retrieveTmw();
        case "RBOM":
        case "RVP":
          vm.branchList = userProfile.getBranchList();
        default:
          if (vm.selectedFsr.name !== userProfile.getFullName()) {
            retrieveTmw();
          } else {
            vm.selectedFsr.name = "Select FSR";
            vm.tmwItems = [];
          }
      }
    }

    function loadTmw() {
      var name = userProfile.getFullName();
      // user profile information may not be immediately available
      // upon refresh
      if (name !== "undefined undefined") {
        vm.selectedFsr = getSelectedFsr();

        getQuarters();

        if (vm.selectedFsr.corpId) vm.selectCorpId(vm.selectedFsr.corpId);
        vm.selectedBranch = vm.selectedFsr.branch;
        vm.canEditTarget = canUserEditTarget();

        vm.selectBranch(vm.selectedFsr.branch, false);
      } else {
        setTimeout(loadTmw, 200);
      }
    }

    function getMonths(quarter) {
      switch (quarter) {
        case 1:
          vm.monthOne = "Jan.";
          vm.monthTwo = "Feb.";
          vm.monthThree = "Mar.";
          break;
        case 2:
          vm.monthOne = "Apr.";
          vm.monthTwo = "May";
          vm.monthThree = "Jun.";
          break;
        case 3:
          vm.monthOne = "Jul.";
          vm.monthTwo = "Aug.";
          vm.monthThree = "Sep.";
          break;
        case 4:
          vm.monthOne = "Oct.";
          vm.monthTwo = "Nov.";
          vm.monthThree = "Dec.";
          break;
        default:
          vm.monthOne = "Jan.";
          vm.monthTwo = "Feb.";
          vm.monthThree = "Mar.";
      }
    }

    function retrieveFsrList() {
      tmwService
        .retrieveFsrList(
          vm.selectedBranch,
          vm.selectedCorpId ? vm.selectedCorpId.corpId : null
        )
        .then(function (response) {
          vm.fsrList = tmwService.getFsrList();

          if (vm.fsrList.indexOf(name) !== -1) {
            vm.selectedFsr.name = name;
          }
        });
    }

    $scope.startsWith = function (actual, expected) {
      var lowerStr = (actual + "").toLowerCase();
      return lowerStr.indexOf(expected.toLowerCase()) === 0;
    };

    vm.selectBranch = function (branch, expandOSRList) {
      vm.inputBranch = branch;
      vm.selectedBranch = branch;
      vm.selectedFsr.branch = branch;
      retrieveFsrList();

      if (expandOSRList) {
        angular.element("#fsrDropdown").trigger("focus");

        setTimeout(function () {
          angular.element("#fsrDropdown").trigger("click");
        }, 250);
      }
    };

    vm.selectCorpId = function (corpId, expandOSRList) {
      vm.selectedCorpId = corpId;
      vm.inputCorpId = corpId.corpId + " | " + corpId.customerName;
      retrieveFsrList();

      vm.selectedFsr.corpId = corpId;

      if (expandOSRList) {
        angular.element("#fsrDropdown").trigger("focus");

        setTimeout(function () {
          angular.element("#fsrDropdown").trigger("click");
        }, 250);
      }
    };

    vm.selectFsr = function (name, branch) {
      if (vm.notesOpen) {
        vm.notesOpen = false;
        vm.toggleNotes();
      }
      vm.tmwNotes = undefined;
      vm.tmwItems = undefined;
      vm.selectedFsr.name = name;

      if (branch !== null) {
        vm.selectedFsr.branch = branch;
      }

      vm.selectedBranch = vm.selectedFsr.branch;

      sessionStorage.setItem("selectedFsr", JSON.stringify(vm.selectedFsr));

      retrieveTmw();
    };

    vm.selectQuarter = function (year, quarter) {
      vm.selectedQuarter.year = year;
      vm.selectedQuarter.quarter = quarter;
      sessionStorage.setItem(
        "selectedQuarter",
        JSON.stringify(vm.selectedQuarter)
      );

      if (vm.notesOpen) {
        vm.notesOpen = false;
        vm.toggleNotes();
      }

      if (vm.selectedFsr.name !== "Select FSR") {
        vm.tmwNotes = undefined;
        vm.tmwItems = undefined;
        retrieveTmw();
      }
    };

    function retrieveNotes() {
      if (vm.tmwNotes) {
        var tmwContainer = {
          securityProfile: {},
          year: vm.selectedQuarter.year,
          quarter: vm.selectedQuarter.quarter,
        };

        tmwContainer.securityProfile = {
          fullName: vm.selectedFsr.name,
          branchLocation: vm.selectedFsr.branch,
          physicalDeliveryOfficeName: vm.selectedFsr.branch,
        };
        userInfoService
          .checkUserInfo()
          .then(function () {
            tmwService
              .retrieveTmw(tmwContainer)
              .then(function (response) {
                vm.tmwNotes.tmwNotesId = tmwService.getTmwNotes().tmwNotesId;
                vm.tmwNotes.createDateTime = tmwService.getTmwNotes().createDateTime;
                vm.tmwNotes.createUser = tmwService.getTmwNotes().createUser;

                vm.tmwNotesOriginal = JSON.parse(JSON.stringify(vm.tmwNotes));
              })
              .catch(function (error) {});
          })
          .catch(function (error) {});
      }
    }

    function retrieveTmw() {
      var tmwContainer = {
        securityProfile: {},
        quarter: vm.selectedQuarter.quarter,
        year: vm.selectedQuarter.year,
      };

      tmwContainer.securityProfile = {
        fullName: vm.selectedFsr.name,
        branchLocation: vm.selectedFsr.branch,
        physicalDeliveryOfficeName: vm.selectedFsr.branch,
      };

      userInfoService
        .checkUserInfo()
        .then(function () {
          tmwService
            .retrieveTmw(tmwContainer)
            .then(function (response) {
              vm.tmwItems = tmwService.getTmwItems();

              vm.tmwItemsOriginal = JSON.parse(JSON.stringify(vm.tmwItems));

              vm.loadedQuarter.quarter = vm.tmwItems[0].quarter;
              vm.loadedQuarter.year = vm.tmwItems[0].year;
              getMonths(vm.loadedQuarter.quarter);
              getPositionTotals();

              vm.currentQtrAvg =
                vm.getPropertyTotal(vm.tmwItems, "marketShare") /
                vm.tmwItems.length;
              vm.previousQtrAvg =
                vm.getPropertyTotal(vm.tmwItems, "prevQuarterMarketShare") /
                vm.tmwItems.length;

              vm.tmwNotes = tmwService.getTmwNotes();

              if (vm.tmwNotes !== vm.tmwNotesOriginal) {
                if (vm.tmwNotes.notes !== "") {
                  vm.notesOpen = true;
                  vm.toggleNotes();
                }
              }

              vm.tmwNotesOriginal = JSON.parse(JSON.stringify(vm.tmwNotes));
            })
            .catch(function (error) {});
        })
        .catch(function (error) {});
    }

    vm.saveTmw = function () {
      var tmwContainer = {
        tmwNotes: vm.tmwNotes,
        securityProfile: {
          fullName: userProfile.getFullName(),
        },
        year: vm.loadedQuarter.year,
        quarter: vm.loadedQuarter.quarter,
      };

      if (vm.tmwNotes) {
        tmwContainer.tmwNotes.quarter = vm.loadedQuarter.quarter;
        if (vm.selectedFsr !== "Select FSR") {
          tmwContainer.tmwNotes.acctManager = vm.selectedFsr.name;
        } else {
          tmwContainer.tmwNotes.acctManager = userProfile.getFullName();
        }

        userInfoService
          .checkUserInfo()
          .then(function () {
            if (vm.tmwNotes.tmwNotesId) {
              if (vm.tmwNotesOriginal.notes !== vm.tmwNotes.notes) {
                tmwService
                  .updateTmwNotes(tmwContainer)
                  .then(function () {})
                  .catch(function (error) {
                    vm.clientError.message = error;
                    auditLog = [];
                  });
              }
            } else {
              tmwService
                .createTmwNotes(tmwContainer)
                .then(function () {})
                .catch(function (error) {
                  vm.clientError.message = error;
                  auditLog = [];
                });
            }
          })
          .catch(function () {});
      }

      angular.forEach(vm.tmwItems, function (tmwItem, key) {
        if (tmwItem.changed) {
          if (tmwItem.monthOneVisits === null) {
            tmwItem.monthOneVisits = 0;
          }
          if (tmwItem.monthTwoVisits === null) {
            tmwItem.monthTwoVisits = 0;
          }
          if (tmwItem.monthThreeVisits === null) {
            tmwItem.monthThreeVisits = 0;
          }
          if (tmwItem.totalVisits === null) {
            tmwItem.totalVisits = 0;
          }

          var tmwContainer = {
            tmwId: tmwItem.tmwId,
            year: vm.loadedQuarter.year,
            quarter: vm.loadedQuarter.quarter,
            monthOneVisits: tmwItem.monthOneVisits,
            monthTwoVisits: tmwItem.monthTwoVisits,
            monthThreeVisits: tmwItem.monthThreeVisits,
            totalVisits: tmwItem.totalVisits,
          };
          userInfoService
            .checkUserInfo()
            .then(function () {
              tmwService
                .updateTmw(tmwContainer)
                .then(function () {})
                .catch(function (error) {
                  vm.clientError.message = error;
                  auditLog = [];
                });
            })
            .catch(function () {});
        }
      });

      // Get the snackbar DIV
      var x = document.getElementById("snackbar");
      // Add the "show" class to DIV
      x.className = "show";
      // After 3 seconds, remove the show class from DIV
      setTimeout(function () {
        x.className = x.className.replace("show", "");
      }, 3000);

      vm.tmwItemsOriginal = JSON.parse(JSON.stringify(vm.tmwItems));
      retrieveNotes();
      setFormPristine();
    };

    vm.getPropertyTotal = function (items, prop) {
      var total = 0;
      if (typeof items !== "undefined") {
        items.forEach(function (item) {
          if (
            angular.isNumber(item[prop]) &&
            typeof item[prop] !== "undefined"
          ) {
            total += item[prop];
          }
        });
        return total;
      }
    };

    function getPositionTotals() {
      vm.defendTotal = 0;
      vm.expandTotal = 0;
      vm.reviewTotal = 0;
      vm.tmwItems.forEach(function (tmwItem) {
        switch (tmwItem.customerPosition) {
          case "Defend":
            vm.defendTotal++;
            break;
          case "Expand":
            vm.expandTotal++;
            break;
          case "Review":
            vm.reviewTotal++;
            break;
          default:
        }
      });
    }

    function setFormPristine() {
      vm.TMWForm.$setPristine();
    }

    vm.isFSR = function () {
      return utilService.equalsIgnoreCaseSpace(
        roles.FSR,
        userProfile.getUserType()
      );
    };

    vm.canEdit = function () {
      return vm.isFSR() || vm.selectedFsr.name === userProfile.getFullName();
    };

    vm.reset = function () {
      utilService
        .deleteConfirmation(
          "Cancel Confirmation",
          "Changes have been made to this page.",
          "btn-warning"
        )
        .result.then(
          function (result) {
            if (result == true) {
              vm.tmwItems = JSON.parse(JSON.stringify(vm.tmwItemsOriginal));
              if (vm.tmwNotesOriginal) {
                vm.tmwNotes = JSON.parse(JSON.stringify(vm.tmwNotesOriginal));
              } else {
                vm.tmwNotes = undefined;
              }
              setFormPristine();
            }
          },
          function (dismiss) {}
        );
    };

    vm.getDate = function () {
      return new Date();
    };

    vm.updateYTD = function (item) {
      tmwService.updateYTD(item);
    };

    vm.update = function () {
      setFormPristine();
      vm.changed = false;
    };

    vm.positionLabel = function (text) {
      if (!text) {
        return "";
      }

      switch (text) {
        case "Defend":
          return "label label-success";
        case "Expand":
          return "label label-warning";
        case "Review":
          return "label label-danger";
        default:
          return "";
      }
    };

    vm.isrPopover = [
      {
        templateUrl: "multipleIsrList.html",
      },
    ];

    vm.sort = {
      type: "is",
      reverse: false,
      order: function (sortType, sortReverse) {
        if (vm.sort.type !== sortType) {
          vm.sort.reverse = !sortReverse;
        } else {
          vm.sort.reverse = sortReverse;
        }
        vm.sort.type = sortType;
      },
    };

    /************* "Notes" Panel *****************/

    vm.toggleNotes = function () {
      if (!vm.notesOpen) {
        vm.notesWidth = "0";
        vm.pageMargin = "0";
      } else {
        vm.notesWidth = "350";
        vm.pageMargin = "325";
      }
    };
  },
};
export default tmw;
