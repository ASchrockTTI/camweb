import contactModalTemplate from './contactModal.html';

const contactModal =  {
		template: contactModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			utilService,
			userProfile,
			userInfoService,
			contactService) {
			
			var vm = this;
			var contactPlan = {};
			var contactFormInfo = {};
			vm.manufacturers = [];
			
			vm.$onInit = function() {
				
				vm.pageIncludes = utilService.getTemplates().getAll();
				vm.clientError = utilService.getClientErrorObject();
				
				vm.delegate = {};
				vm.action = 'Add';
				
				
				vm.options = {
					relations: [
						{value: "4", display: "None"},
						{value: "1", display: "Strong"},
						{value: "2", display: "Penetrating"},
						{value: "3", display: "Weak"}
					],
					vbsRoles: [
						{value: "10", display: "None"},
						{value: "1",  display: "Materials"},
						{value: "2",  display: "Engineering/Design"},
						{value: "3",  display: "Supply Chain"},
						{value: "4",  display: "Quality"},
						{value: "5",  display: "Finance"},
						{value: "6",  display: "OEM Outsource(CM) Mgr"},
						{value: "7",  display: "CM Program Mgr"},
						{value: "8",  display: "Manufacturing/Production"},
						{value: "9",  display: "Executive"},
					],
					foxes: [
						{value: "1", display: "Yes", boolVal: 1},
						{value: "2", display: "No", boolVal: 0},
					]
				}
				
				contactFormInfo = vm.resolve.contactFormInfo;
				
				switch(contactFormInfo.formType.toLowerCase()) {
				case 'tti':
					vm.delegateType = 'delegate';
					break;
				case 'supplier':
					loadManufacturers();
					vm.delegateType = 'supplier';
					break;
				case 'edit':
					
					switch(contactFormInfo.editContact.delegateType.delegateType.toLowerCase()) {
					case 'strategic':
						vm.delegateType = 'coowner';
						break;
					case 'tactical':
					case 'delegate':
					case 'coowner':
					case 'coowner':
					case 'owner':
						vm.delegateType = 'delegate';
						break;
					case 'customer':
						vm.delegateType = 'customer';
						break;
					case 'supplier':
						loadManufacturers();
						if(contactFormInfo.editContact.manufacturer) {
							vm.selectMfr(contactFormInfo.editContact.manufacturer);
						}
						vm.delegateType = 'supplier';
						break;
					default:
						vm.delegateType = contactFormInfo.editContact.delegateType.delegateType;
					}
					
					vm.action = 'Edit';
					break;
				case 'customer':
					vm.delegateType = 'customer';
					break;
				case '':
				default:
					vm.delegateType = 'customer';
					vm.showTypeSelector = true;
					loadManufacturers();
				}
					
				if (utilService.isNotEmpty(contactFormInfo.editContact)) {
					
					vm.contactName = contactFormInfo.editContact.delegateName;
					vm.delegate.fullName = contactFormInfo.editContact.delegateName;
					vm.delegate.title = contactFormInfo.editContact.delegateTitle;
					vm.delegate.physicalDeliveryOfficeName = contactFormInfo.editContact.delegateLocation;
					vm.delegate.phone = contactFormInfo.editContact.delegatePhone;
					vm.delegate.mail = contactFormInfo.editContact.delegateEmail;
					vm.delegate.selectedRelation = contactFormInfo.editContact.selectedRelation;
					vm.delegate.selectedVbsRole = contactFormInfo.editContact.selectedVbsRole;
					if(contactFormInfo.editContact.representativeFirm) {
						vm.isRep = true;
						vm.delegate.representativeFirm = contactFormInfo.editContact.representativeFirm;
					}
					
					vm.delegate.selectedFox = contactFormInfo.editContact.fox ? vm.options.foxes[0] : vm.options.foxes[1];
				}	
				
				contactPlan = contactFormInfo.plan;
				
				vm.formType = contactFormInfo.formType;
				
				setTimeout(function () {
					if(vm.delegateType === 'delegate') {
						var input = angular.element(document.querySelector('#lookupUser'));
					} else if(vm.delegateType === 'supplier') {
						var input = angular.element(document.querySelector('#supplierNameInput'));
					} else {
						var input = angular.element(document.querySelector('#customerNameInput'));
					}
					input.focus();
				}, 500);
			}
			
			vm.selectType = function(type) {
				vm.delegateType = type;
			}
			
			function loadManufacturers() {
				contactService.retrieveManufacturers()
				.then(function(response) {
					vm.manufacturers = response;						
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			vm.selectMfr = function(mfr) {
				vm.manufacturer = mfr.fullManufacturerName;
				vm.delegate.manufacturer = mfr;
			}
			
			vm.validateForm = function() {
				switch(vm.delegateType) {			
				case 'delegate':
					return !vm.contactForm.$dirty;								
				default:
					if (vm.contactForm.$dirty && vm.contactForm.$valid) {
						return false;
					} else {
						return true;
					}				
				}
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			vm.setToCoOwner = function() {
				vm.delegateType = 'coowner';
			}
			
			vm.setToDelegate = function() {
				vm.delegateType = 'delegate';
			}
			
			vm.ok = function(action) {
							
				var contactContainer = {}			
					
				contactContainer.securityProfile = {
					fullName: userProfile.getFullName()
				}
				
				contactContainer.planDelegate = {
					delegateName: vm.delegate.fullName == undefined ? vm.contactName : vm.delegate.fullName,
					delegateTitle: vm.delegate.title,
					delegateLocation: vm.delegate.physicalDeliveryOfficeName,
					delegatePhone: vm.delegate.phone,
					delegateEmail: vm.delegate.mail,
					spw: {},
					vbsRole: {},
					fox: undefined
				};
				
				switch(vm.delegateType.toLowerCase()) {
				case 'coowner':
				case 'co-owner':
					contactContainer.delegateTypeEnum = 'CoOwner';
					break;
				case 'delegate':
					contactContainer.delegateTypeEnum = 'Delegate';
					contactContainer.planDelegate.spw = {
						spwID: 3,
						spw  : "Weak"
					}
					contactContainer.planDelegate.vbsRole = {
							vbsRoleID: "1",
							vbsRole  : "Materials"
					}
					contactContainer.planDelegate.fox = vm.options.foxes[1].boolVal;
					break;
				case 'supplier':
					contactContainer.planDelegate.manufacturer = vm.delegate.manufacturer;
					if(vm.isRep) {
						contactContainer.planDelegate.representativeFirm = vm.delegate.representativeFirm;
					}
					contactContainer.planDelegate.spw = {
						spwID: 3,
						spw  : "Weak"
					}
					contactContainer.planDelegate.vbsRole = {
							vbsRoleID: "1",
							vbsRole  : "Materials"
					}
					if(!vm.isRep) {
						contactContainer.planDelegate.representativeFirm = '';
					}
					if(!vm.manufacturer) {
						contactContainer.planDelegate.manufacturer = null;
					}
					contactContainer.planDelegate.fox = vm.options.foxes[1].boolVal;
					contactContainer.delegateTypeEnum = 'Supplier';
					break;
				default:
					contactContainer.delegateTypeEnum = 'Customer';
					contactContainer.planDelegate.spw = {
						spwID: vm.delegate.selectedRelation.value,
						spw  : vm.delegate.selectedRelation.display
					}
					contactContainer.planDelegate.vbsRole = {
						vbsRoleID: vm.delegate.selectedVbsRole.value,
						vbsRole  : vm.delegate.selectedVbsRole.display
					}
					
					contactContainer.planDelegate.fox = vm.delegate.selectedFox ? vm.delegate.selectedFox.boolVal : vm.options.foxes[1].boolVal;
													
				}
							
				if (vm.action === 'Add') {
					
					userInfoService.checkUserInfo()
					.then(function() {
						return contactService.addContact(contactPlan.planId, contactContainer)
						.then(function(response) {
							vm.modalInstance.close(response);						
						})
						.catch(function(error) {
							vm.clientError.message = error;
						})
					})
					.catch(function() {
						vm.clientError.message = error;
					})					
				} else {
					
					contactContainer.planDelegate.delegateId = contactFormInfo.editContact.delegateId;
					
					userInfoService.checkUserInfo()
					.then(function() {
						contactService.updateContact(contactPlan.planId, contactContainer)
						.then(function() {
							vm.modalInstance.close();						
						},
						function(error) {
							vm.clientError.message = error;
						})
					})
					.catch(function(error) {
						vm.clientError.message = error;
					})
				}
				
			}
			
			vm.loadContactName = function(lastName) {
				
				return userInfoService.retrieveUserInfoByLastName(lastName)
				.then(function(response) {				
					return response;
				})
				.catch(function(error) {
					utilService.getLogger().error(error);
				})
			}
			
			vm.tagAdded = function(tag) {
				vm.delegate = tag;			
			}	
		}
	}
export default contactModal;