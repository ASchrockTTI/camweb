import customersTemplate from './customers.html';

const customers =  {
		template: customersTemplate,
		bindings: {
			ngModel: '=',
			plan: '<'
		},
		controller: function($rootScope, contactService, permissionsFactory, userProfile, utilService, roles) {
			
			var vm = this;
			var permissionsValidator;
			
			vm.$onInit = function() {
				permissionsValidator = permissionsFactory.newInstance();
				//permissionsValidator.clearPermissions();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
			}
			
			vm.$onChanges = function(changes) {				
				vm.plan = changes.plan.currentValue;	
			}
			
			vm.permissions = {
				validated: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);
				}
			}
			
			vm.sort = {
				type: 'delegateFirstName',
				reverse: false,
				order: function(sortType, sortReverse) {
					if (vm.sort.type !== sortType) {
						vm.sort.reverse = !sortReverse;
					} else {
						vm.sort.reverse = sortReverse;
					}
					
					vm.sort.type = sortType;
				}
			}
			
			vm.spwLabel = function(item) {
				switch(item.spw.spwID) {
				case 1:
					return 'label label-success';
					break;
				case 2:
					return 'label label-warning';
					break;
				case 3:
					return 'label label-danger';
					break;
				default:
					return '';
				}
			}
			
			vm.deleteMe = function(delegateId) {
				
				utilService.deleteConfirmation('Customer Contact Info', 'You are about to delete the selected customer.', 'btn-danger').result
				.then(
					function(result) {
						if (result === true) {							
							contactService.removeContact(vm.plan.planId, delegateId, 'Customer')
							.then(function(response) {
								var customers = vm.ngModel.filter(function(obj) {
									return obj.delegateId !== delegateId;
								});
																
								vm.ngModel = customers;
								$rootScope.$emit('refreshList',{data: result});
							})
							.catch(function(error) {
								
							})
						}
					},
					function(dismiss) {
						
					}
				)
			}
			
			vm.add = function(formType) {
				
				utilService.uibModal().open({
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function() {						
							return {
								plan: vm.plan,
								formType: formType,
								editContact: null
							}						
						}
					}
				}).result.then(
					function(result) {
						$rootScope.$emit('updateList',{data: result});
					},
					function(dismiss) {
						
					}
				)
				
			}
			
			vm.edit = function(contact) {
				
				utilService.uibModal().open({
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function() {
							contact.selectedRelation = {
								value: contact.spw.spwID.toString(),
								display: contact.spw.spw
							}
							
							contact.selectedVbsRole = {
								value: contact.vbsRole.vbsRoleID.toString(),
								display: contact.vbsRole.vbsRole
							}
							
							
							return {
								plan: vm.plan,
								formType: 'edit',
								editContact: contact
							}	
						}
					}
				}).result.then(
					function(result) {
						$rootScope.$emit('refreshList',{data: result});
					},
					function(dismiss) {
						
					}
				)
				
			}
		}
	}
export default customers;