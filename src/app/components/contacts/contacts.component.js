import contactsTemplate from './contacts.html';

const contacts =  {
		template: contactsTemplate,
		controller: function(
				$rootScope,
				$scope,
				$state,
				$stateParams,
				planService,
				contactService,
				utilService,
				userProfile,
				roles) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.params = $stateParams;				
				vm.clientError = utilService.getClientErrorObject();
				vm.activeTabIndex = 0;
				loadPlan();
				loadOpportunities();
				
			}

			vm.$onDestroy = function() {
				contactsSetActiveTabListener();
				updateListListener();
				refreshListListener();
			}
			
			$scope.$watch(function(){ return vm.params.contactsTab }, function(){
				 if($stateParams.contactsTab === 'suppliers') {
						vm.activeTabIndex = 1;
					} else if($stateParams.contactsTab === 'tti') {
						vm.activeTabIndex = 2;
					} else {
						vm.activeTabIndex = 0;
					}
			 });
			
			vm.selectTab = function(tab) {
				$state.go('.', {contactsTab: tab});
			}
			
			var contactsSetActiveTabListener = $rootScope.$on('contacts-set-active-tab', function(event, value) {
				loadPlan();
				vm.activeTabIndex = value.activeTabIndex;
			})
			
			// Load plan to $scope if it does not exist.
			function loadPlan() {			
				planService.lookupPlan(vm.params.planId)
				.then(function(response) {
					vm.plan = response;
					planService.setPlan(vm.plan);	
					loadContacts();
				})
				.catch(function(error) {
					
				})
			}

			function loadOpportunities() {

			}

			// Loads plan contacts
			function loadContacts() {
							
				contactService.retrieveContacts(vm.params.planId)
				.then(function(response) {				
					vm.customers = contactService.getCustomers();
					vm.suppliers = contactService.getSuppliers();
					vm.participants = {
						owners: contactService.getOwners(),
						participants: contactService.getDelegates()
					};
					$rootScope.$emit('contactsRefresh');
				})
				.catch(function(error) {
					
				})
			}
			
			var  refreshListListener = $rootScope.$on('refreshList', function(e, a) {
				loadContacts();
			})
			
			var updateListListener = $rootScope.$on('updateList', function(e, a) {			
				if (a.data.delegateType.delegateType === 'Customer') {			
					vm.customers.push(a.data);
				} else {
					vm.participants.participants.push(a.data);			
				}	
				loadContacts();
			})
			
		}
	}
export default contacts;