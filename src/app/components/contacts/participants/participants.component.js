import participantsTemplate from './participants.html';

const participants =  {
		template: participantsTemplate,
		bindings: {
			ngModel: '=',
			plan: '<'
		},
		controller: function($rootScope, $filter, contactService, permissionsFactory, userProfile, utilService, roles, $state, userInfoService) {
			
			var vm = this;
			var permissionsValidator;
			
			vm.$onInit = function() {
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
			}
			
			vm.$onChanges = function(changes) {
				vm.plan = changes.plan.currentValue;
			}
			
			vm.permissions = {
				validated: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);
				},
				ownerRemoval: function(delegateType) {
					if(utilService.equalsIgnoreCaseSpace(delegateType, 'owner')) {
						return false;
					} else return (vm.permissions.validated('delete') 
							|| roleMatch(delegateType));
				}
			}
			
			function roleMatch(delegateType) {
				return utilService.equalsIgnoreCaseSpace(delegateType, userProfile.getUserTypeName());
			}
			
			vm.sort = {
				type: 'delegateFirstName',
				reverse: false,
				order: function(sortType, sortReverse) {
					if (vm.sort.type !== sortType) {
						vm.sort.reverse = !sortReverse;
					} else {
						vm.sort.reverse = sortReverse;
					}
					
					vm.sort.type = sortType;
				}
			}
			
			vm.isRseUser = function() {
				
				if (!vm.plan || vm.ngModel === undefined) {
					return false;
				}								
				
				if (userProfile.getUserType().toLowerCase() === roles.RSE.toLowerCase()
					&& $filter('filter')(vm.ngModel.owners, {delegateType:{delegateType: userProfile.getUserTypeName()}}).length === 0) {
					return true;
				}
				
				return false;
			}
			
			vm.deleteMe = function(delegateId, delegateType) {
				
				if (delegateType == null) {
					throw 'DelegateType is a required parameter.'
				}
				
				utilService.deleteConfirmation('Participant Contact Info', 'You are about to delete the selected delegate.', 'btn-danger').result
				.then(
					function(result) {
						if (result === true) {
			
							contactService.removeContact(vm.plan.planId, delegateId, delegateType)
							.then(function(response) {
								
								if (delegateType == 'RSE') {
									$rootScope.$broadcast('contacts-set-active-tab', {activeTabIndex: 2});									
								} else {
									var participants = vm.ngModel.participants.filter(function(obj) {
										return obj.delegateId !== delegateId;
									});

									vm.ngModel.participants = participants;
								}								
								$rootScope.$emit('refreshList',{data: result});
							})
							.catch(function(error) {
								
							})
						}
					},
					function(dismiss) {
						
					}
				)
			}
			
			function setAuditLog(planId, action, feature) {
				
				var auditLog = [];
				
				auditLog.push({
					planId: planId,
					auditActionEnum: action,
					auditFeatureEnum: feature,
					modifiedBy: userProfile.getFullName()
				})
				
				return auditLog;
			}
			
			vm.addRse = function() {
				
				var contactContainer = {}			
				
				contactContainer.securityProfile = {
					fullName: userProfile.getFullName()
				}
				
				contactContainer.auditLog = setAuditLog(vm.plan.planId, 'ADD', userProfile.getUserTypeName().toUpperCase());
				
				contactContainer.planDelegate = {
					delegateName: userProfile.getFullName(),
					delegateTitle: userProfile.getTitle(),
					delegateLocation: userProfile.getPhysicalDeliveryOfficeName(),
					delegatePhone: '',
					delegateEmail: userProfile.getMail(),
					spw: {},
					vbsRole: {},
					fox: undefined
				};
				
				contactContainer.delegateTypeEnum = userProfile.getUserTypeName().toUpperCase();
				
				contactContainer.planDelegate.spw = {
					spwID: 3,
					spw  : "Weak"
				}
				contactContainer.planDelegate.vbsRole = {
						vbsRoleID: "1",
						vbsRole  : "Materials"
				}
				contactContainer.planDelegate.fox = 0;
								
				contactService.addContact(vm.plan.planId, contactContainer)
				.then(function(response) {					
					$rootScope.$broadcast('contacts-set-active-tab', {activeTabIndex: 2});
					sessionStorage.removeItem('sessionPlans');
					sessionStorage.removeItem('sessionPlansNonTTI');
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
				
			}
			
			vm.add = function(formType) {
				
				utilService.uibModal().open({
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function() {						
							return {
								plan: vm.plan,
								formType: formType,
								editContact: null
							}						
						}
					}
				}).result.then(
					function(result) {
						$rootScope.$emit('updateList',{data: result});
					},
					function(dismiss) {
						
					}
				)
				
			}
			
			vm.edit = function(contact) {
				
				utilService.uibModal().open({
					component: 'contactModal',
					size: 'md',
					resolve: {
						contactFormInfo: function() {
							contact.selectedRelation = {
								value: contact.spw.spwID.toString(),
								display: contact.spw.spw
							}
							
							contact.selectedVbsRole = {
								value: contact.vbsRole.vbsRoleID.toString(),
								display: contact.vbsRole.vbsRole
							}
							
							
							return {
								plan: vm.plan,
								formType: 'edit',
								editContact: contact
							}	
						}
					}
				}).result.then(
					function(result) {
						$rootScope.$emit('refreshList',{data: result});
					},
					function(dismiss) {
						
					}
				)
				
			}
			
		}
	}
export default participants;