import omsSearchModalTemplate from './omsSearchModal.html';

const omsSearchModal =  {
		template: omsSearchModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
					$filter,
					omsService
					) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.originalCriteria = vm.resolve.modalData.criteria;
				vm.branchList = vm.resolve.modalData.branchList;
				vm.corpIdList = vm.resolve.modalData.corpIdList; 
				
				copyOriginalCriteria();
			}
			
			function copyOriginalCriteria() {
				var originalCriteriaString = JSON.stringify(vm.originalCriteria);
				
				vm.criteria = JSON.parse(originalCriteriaString);
				
				vm.branchInput = vm.criteria.branch;
				if(vm.criteria.branch) {
					vm.selectBranch(vm.criteria.branch);
				}
				
				vm.corpIdInput = vm.criteria.corpId;
				if(vm.criteria.corpId) {
					vm.selectCorpId(vm.criteria.corpId);
				}
				
				
				vm.ownerNameInput = vm.criteria.fullName;
				
				//parse "stringified" dates
				if(vm.criteria.startDate) vm.criteria.startDate = new Date(vm.criteria.startDate);
				if(vm.criteria.endDate)   vm.criteria.endDate = new Date(vm.criteria.endDate);
			}
			
			vm.selectBranch = function(branch) {
				//clear owner selection if branch changed
				if(branch !== vm.criteria.branch) {
					vm.criteria.username = undefined;
					vm.criteria.fullName = undefined;
						
					vm.ownerNameInput = '';
				}
				
				vm.criteria.branch = branch;
				vm.branchInput = branch;
				
				omsService.getProjectOwnersByBranch(branch).then(
					function(response) {
						vm.ownerList = response;
					})
					.catch(function(error) {
						vm.ownerList = [];
					})
			}
			
			vm.selectCorpId = function(corpId) {
				vm.inputCorpId = corpId.corpId + ' | ' + corpId.customerName;
				vm.criteria.corpId = corpId;
			}
			
			vm.selectOwner = function(owner) {
				
				if(!owner) {
					vm.criteria.username = undefined;
					vm.criteria.fullName =  undefined;
					vm.ownerNameInput = '';
					return;
				}
				
				var titleCaseName = $filter('titlecase')(owner.partnerName);
				
				vm.criteria.username = owner.partnerUserName;
				vm.criteria.fullName =  titleCaseName;
				vm.ownerNameInput = titleCaseName;
			}
			
			
			vm.search = function() {
				angular.copy(vm.criteria, vm.originalCriteria);
				
				vm.modalInstance.close();
			}
			
			vm.reset = function() {
				copyOriginalCriteria();
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
		}
	}
export default omsSearchModal;