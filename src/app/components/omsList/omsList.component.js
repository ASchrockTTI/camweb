import omsListTemplate from './omsList.html';

const omsList =  {
		template: omsListTemplate,
		controller: function(
			omsService,
			planService,
			userProfile,
			userInfoService,
			utilService,
			$rootScope,
			$scope,
			$state
		) {

			var vm = this;
			
			vm.$onInit = function() {
				
				if (sessionStorage.getItem('omsSort')) {
					var sort = JSON.parse(sessionStorage.getItem('omsSort'));
					vm.sort.type = sort.type;
					vm.sort.reverse = sort.reverse;
				}
				
				if (sessionStorage.getItem('omsProjectCriteria')) {
					vm.criteria = JSON.parse(sessionStorage.getItem('omsProjectCriteria'));
					
					if (vm.criteria.endDate) {
						var endDateString = vm.criteria.endDate;
						if(endDateString == 'undefined' || endDateString == 'null') vm.criteria.endDate = undefined;
						else vm.criteria.endDate = new Date(endDateString);
					}
					
					if (vm.criteria.startDate) {
						var startDateString = vm.criteria.startDate;
						if(startDateString == "undefined" || startDateString == 'null') vm.criteria.startDate = undefined;
						else vm.criteria.startDate = new Date(startDateString);
					}
				} else {
					vm.criteria = {};
					var startDate = new Date();
					startDate.setMonth(startDate.getMonth() - 12);
					vm.criteria.startDate = startDate;
					vm.initialize = true;
				}
				
				
				if (sessionStorage.getItem('omsTextFilter')) vm.filterText = JSON.parse(sessionStorage.getItem('omsTextFilter'));
				if (sessionStorage.getItem('ownerFilter')) vm.ownerFilter = JSON.parse(sessionStorage.getItem('ownerFilter'));
				
				
				if(userProfile.getFullName() !== 'undefined undefined') intitialzeCriteria();
			}
			
			
			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {
				if(!vm.userProfile) {
					vm.userProfile = userProfile.getUserProfile();
					intitialzeCriteria();
				}
			})
			
			function intitialzeCriteria() {
				vm.userProfile = userProfile.getUserProfile();
				vm.role = userProfile.getUserType();
				
				switch(vm.role.toUpperCase()) {
					case 'ADMIN':
						planService.getParentBranchList().then(
							function(response) {
								vm.branchList = response;
							})
							.catch(function(error) {
								vm.branchList = undefined;
							})
						if(vm.initialize) {
							vm.criteria.showMine = true;
							vm.criteria.username = userProfile.getSAMAccountName();
							vm.criteria.fullName = userProfile.getFullName();
							vm.criteria.branch = userProfile.getBranch();
						}
						vm.getOmsProjects(true);
						break;
					case 'EXECUTIVE':
						planService.getEntityParentBranches(userProfile.getEntity()).then(
							function(response) {
								vm.branchList = [];
								
								for(var i = 0; i < response.length; i++) {
									if(vm.branchList.includes(response[i].fullParentBranchName)) continue;
									vm.branchList.push(response[i].fullParentBranchName);
								}
							})
							.catch(function(error) {
								vm.branchList = undefined;
							})
						if(vm.initialize) {
							vm.criteria.showMine = true;
							vm.criteria.username = userProfile.getSAMAccountName();
							vm.criteria.fullName = userProfile.getFullName();
							vm.criteria.branch = userProfile.getBranch();
						}
						vm.getOmsProjects(true);
						break;
					case 'SAM_GAM':
						userInfoService.getUserCorpIdList(userProfile.getSAMAccountName()).then(
								function(response) {
								vm.corpIdList = response;
								if(vm.corpIdList.length === 0) vm.corpIdList = undefined;
								if(vm.initialize) {
									if(vm.corpIdList) vm.criteria.corpId = vm.corpIdList[0];
									else {
										vm.criteria.username = userProfile.getSAMAccountName();
										vm.criteria.fullName = userProfile.getFullName();
									}
								}
								vm.getOmsProjects(true);
						});
						break;
					case 'FSR':
					case 'ISR':
						vm.branchList = undefined;
						if(vm.initialize) {
							vm.criteria.showMine = true;
							vm.criteria.username = userProfile.getSAMAccountName();
							vm.criteria.fullName = userProfile.getFullName();
						}
						vm.getOmsProjects(true);
						break;
					case 'RSE':
					case 'FAE':
					case 'SCM':
					case 'BDM':
					case 'PDM':
						vm.branchList = userProfile.getBranchList();
						if(vm.initialize) {
							vm.criteria.showMine = true;
							vm.criteria.branch = userProfile.getBranch();
						}
						vm.getOmsProjects(true);
						break;
					case 'RVP':
					case 'GM':
					case 'FSM':
					case 'RBOM':
					default:
						vm.branchList = userProfile.getBranchList();
						if(vm.initialize) {
							vm.criteria.branch = userProfile.getBranch();
						}
						vm.getOmsProjects(true);
						break;
				}
			}
			
			vm.getOmsProjects = function(checkCache) {
				vm.initialize = false;
				
				if(checkCache && sessionStorage.getItem('omsProjects')) {
					vm.projects = JSON.parse(sessionStorage.getItem('omsProjects'));
					return;
				}

				vm.projects = undefined;
				
				vm.role = userProfile.getUserType().toUpperCase();
				
				sessionStorage.setItem('omsProjectCriteria', JSON.stringify(vm.criteria));
				
				omsService.retrieveProjects(vm.criteria).then(
					function(response) {
						vm.projects = response;
						
						sessionStorage.setItem('omsProjects', JSON.stringify(vm.projects));
					})
					.catch(function(error) {
						vm.projects = [];
					})
			}
			
			vm.filterOwner = function(ownerName) {
				// criteria username filter supercedes "ownerFilter"
				if(ownerName && vm.criteria.fullName === ownerName) return;
				
				if(!ownerName || vm.ownerFilter === ownerName) {
					vm.ownerFilter = '';
				} else {
					vm.ownerFilter = ownerName;
				}
				sessionStorage.setItem('ownerFilter', JSON.stringify(vm.ownerFilter));
			}
			
			vm.openSearchModal = function() {
				utilService
					.uibModal()
					.open({
						component: "omsSearchModal",
						size: "md",
						resolve: {
							modalData: function () {
								return {
									criteria: vm.criteria,
									branchList: vm.branchList,
									corpIdList: vm.corpIdList
								}
							}
						}
					})
					.result.then(
						function(close) {
							vm.getOmsProjects();
						},
						function(dismiss) {
						});
			};
			
			vm.getTotalValue = function(projects) {
				var total = 0;
				for(var i = 0; i < projects.length; i++) {
					if(projects[i].dollarValue) total = total + projects[i].dollarValue;
				}
				return total;
			}
			
			vm.getTotalIncompleteActions = function(projects) {
				var total = 0;
				for(var i = 0; i < projects.length; i++) {
					total = total + projects[i].incompleteActionCount;
				}
				return total;
			}
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('omsTextFilter', JSON.stringify(vm.filterText));
			}
			
			vm.openPlan = function(planId) {
				$state.go('oms', {planId: planId});
			}
			
			vm.openProject = function(project) {
				$state.go('oms', {planId: project.customerPlanId, projectId: project.projectId});
			}
			
			vm.filterMasterProject = function(project) {
				if(project === null || project.masterProjectId === vm.selectedMasterProjectId) {
					vm.selectedMasterProjectId = undefined;
				} else {
					vm.selectedMasterProjectId = project.masterProjectId;
					vm.selectedMasterProjectName = project.masterProjectName;
				}
			}
			
			$scope.sortMasterProject = function(project) {
			   return project.projectId === vm.selectedMasterProjectId;
			};
			
			vm.getIncompleteActionCount = function(project) {
				if(!project.projectActions) return 0;
				var total = 0;
				for(var i = 0; i < project.projectActions.length; i++){
					if(!project.projectActions[i].complete) total++;
				}
				project.incompleteActionCount = total;
				return total;
			}
			
			vm.openActionItems = function(project, showComplete) {
	            utilService
	              .uibModal()
	              .open({
	                component: "actionItemsModal",
	                size: "md",
	                resolve: {
	              	  modalData: function () {
	                        return { 
	                            project: project,
	                            showComplete: showComplete
	                        }
	                    }
	                }
	              })
	              .result.then(
	          	function(result) {
	          		
	          	},
	          	function(dismiss) {
	          		
	          	});
			};
	          
	
			vm.sort = {
	          type: "lastUpdateDate",
	          reverse: false,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	            
	            sessionStorage.setItem('omsSort', JSON.stringify({type: sortType, reverse: vm.sort.reverse}));
	          }
	        };
			
			vm.$onDestroy = function() {
				userProfileListener();
			}

		}
	}
export default omsList;