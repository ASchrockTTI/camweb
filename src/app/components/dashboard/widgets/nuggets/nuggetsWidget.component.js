import nuggetsWidgetTemplate from './nuggetsWidget.html';

const nuggetsWidget =  {
		template: nuggetsWidgetTemplate,
		controller: function(
				$state,
				$scope,
				$timeout,
				nuggetsService,
				userProfile
		) {

			var vm = this;

			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getNuggetsDashboardInfo();
				}

			}

			vm.getNuggetListEntity = function(nuggetList) {
				return nuggetsService.getEntityTypeFromNuggetList(nuggetList);
			}

			vm.getNuggetEntity = function(nugget) {
				return nuggetsService.getEntityTypeFromNugget(nugget);
			}
		
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getNuggetsDashboardInfo(), 100);
			});

				
			function getNuggetsDashboardInfo() {
				if(vm.inProgress === true) return;
				vm.inProgress = true;


				var entity = userProfile.getUserProfile().entity;
				if(!entity || !entity === '') return [];

				
				nuggetsService.getNuggetsDashboardInfo()
				.then(
					function(response) {
						vm.totalValue = response.totalValue;
						vm.totalCount = response.totalCount;
						vm.nuggets = response.nuggetSummaryList;

						vm.inProgress = false;

				})
				.catch(function(error) {
					
				});

				if(nuggetsService.getEntityTypeFromNuggetList(vm.nuggets) == 'EDC') {
					vm.isEU = true;
				} else if(userProfile && userProfile.getUserProfile() && userProfile.getUserProfile().entity && userProfile.getUserProfile().entity.toUpperCase() === 'EDC') {
					vm.isEU = true;
				} else {
					vm.isEU = false;
				}

			}

			vm.goToPlan = function(nugget) {
				$state.go('nuggets', {planId: nugget.planId, nuggetID: nugget.nuggetId });
			}
		}
	}
export default nuggetsWidget;