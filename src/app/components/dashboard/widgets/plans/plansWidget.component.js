import plansWidgetTemplate from './plansWidget.html';

const plansWidget =  {
		template: plansWidgetTemplate,
		controller: function(

		) {

			var vm = this;
			
			vm.plans = [  
			      {  
			          "planId":6052,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Micron Technology Inc",
			          "totalTarget":2080000.0,
			          "totalRunRate":2508512.46,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":275,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":1,
			             "customerPosition":"Defend"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"121%",
			          "percentLabel":"label label-success"
			       },
			       {  
			          "planId":9483,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Critical Systems Inc",
			          "totalTarget":0.0,
			          "totalRunRate":3883.38,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":48735,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"",
			          "percentLabel":""
			       },
			       {  
			          "planId":6844,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Sensus Usa",
			          "totalTarget":10000.0,
			          "totalRunRate":1890.0,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":40381,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"19%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":9471,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Boondocker Llc",
			          "totalTarget":10000.0,
			          "totalRunRate":5893.25,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":48712,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"59%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":6828,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Amet Inc",
			          "totalTarget":20000.0,
			          "totalRunRate":10179.96,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":40353,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"51%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":6840,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Promed Keyboard Group",
			          "totalTarget":40000.0,
			          "totalRunRate":3304.24,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":40375,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"8%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":62611,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson",
			          "customerName":"Diversified Fluid Solutions",
			          "totalTarget":0.0,
			          "totalRunRate":41099.23,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":94217,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"",
			          "percentLabel":""
			       },
			       {  
			          "planId":9453,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Aeroleds Llc",
			          "totalTarget":10000.0,
			          "totalRunRate":0.0,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":48685,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"",
			          "percentLabel":""
			       },
			       {  
			          "planId":5492,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Curtiss-Wright/Scientech",
			          "totalTarget":170000.0,
			          "totalRunRate":54630.76,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":207,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"32%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":6831,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Dick Campbell Company",
			          "totalTarget":80000.0,
			          "totalRunRate":74097.92,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":40358,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"93%",
			          "percentLabel":"label label-danger"
			       },
			       {  
			          "planId":6842,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Simplyleds",
			          "totalTarget":0.0,
			          "totalRunRate":0.0,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":40379,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"",
			          "percentLabel":""
			       },
			       {  
			          "planId":9467,
			          "dateCreated":null,
			          "acctManager":"Kade Wilson ",
			          "customerName":"Bolens Control House Inc",
			          "totalTarget":10000.0,
			          "totalRunRate":136.56,
			          "dsam":null,
			          "planType":{  
			             "planTypeId":5,
			             "planType":"Branch"
			          },
			          "branch":"OR",
			          "familyID":48707,
			          "marketSegment":null,
			          "customerPosition":{  
			             "customerPositionID":2,
			             "customerPosition":"Expand"
			          },
			          "branchPlanList":[  

			          ],
			          "insideSalesPersonName":null,
			          "entity":"NDC",
			          "currency":"NDC",
			          "strategicFlag":false,
			          "percentToGoal":"1%",
			          "percentLabel":"label label-danger"
			       }
			    ];
			
			vm.$onInit = function() {
				
			}

		}
	}
export default plansWidget;