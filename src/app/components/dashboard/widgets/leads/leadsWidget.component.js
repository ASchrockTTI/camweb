import leadsWidgetTemplate from './leadsWidget.html';

const leadsWidget =  {
		template: leadsWidgetTemplate,
		controller: function(
				
		) {

			var vm = this;
			
			vm.leads = [  
			   {  
			      "pendingCustomerId":61,
			      "customerName":"CMURRELL",
			      "city":"IRVING",
			      "state":"TEXAS",
			      "country":"United States",
			      "submitterUserName":"CMurrell",
			      "submitterName":"Casey Murrell",
			      "planId":63378,
			      "relatedPlanId":null,
			      "omsProjectId":null,
			      "status":"Assigned",
			      "note":null,
			      "reason":null,
			      "submitDate":1557236559160,
			      "qualifierName":"Casey Murrell",
			      "qualifierUserName":"CMurrell",
			      "updaterName":"Evan Hardmeyer",
			      "updaterUserName":"EHardmeyer",
			      "updateDate":1557236641730,
			      "postalCode":"75063-7622"
			   },
			   {  
			      "pendingCustomerId":60,
			      "customerName":"CMurrell",
			      "city":null,
			      "state":null,
			      "country":null,
			      "submitterUserName":"CMurrell",
			      "submitterName":"Casey Murrell",
			      "planId":63377,
			      "relatedPlanId":null,
			      "omsProjectId":null,
			      "status":"Pending",
			      "note":null,
			      "reason":null,
			      "submitDate":1557236322377,
			      "qualifierName":"Casey Murrell",
			      "qualifierUserName":"CMurrell",
			      "updaterName":"Casey Murrell",
			      "updaterUserName":"CMurrell",
			      "updateDate":1557236418527,
			      "postalCode":"12345"
			   },
			   {  
			      "pendingCustomerId":42,
			      "customerName":"Berkshire Grey",
			      "city":"Lexington",
			      "state":"MA",
			      "country":"US",
			      "submitterUserName":"DEnes",
			      "submitterName":"Dale Enes",
			      "planId":63376,
			      "relatedPlanId":null,
			      "omsProjectId":null,
			      "status":"Pending",
			      "note":null,
			      "reason":null,
			      "submitDate":1555944076123,
			      "qualifierName":"Casey Murrell",
			      "qualifierUserName":"CMurrell",
			      "updaterName":"Casey Murrell",
			      "updaterUserName":"CMurrell",
			      "updateDate":1557181011500,
			      "postalCode":null
			   },
			   {  
			      "pendingCustomerId":43,
			      "customerName":"Keith Batchelder",
			      "city":"Lexington",
			      "state":"MA",
			      "country":"US",
			      "submitterUserName":"DEnes",
			      "submitterName":"Dale Enes",
			      "planId":63374,
			      "relatedPlanId":null,
			      "omsProjectId":null,
			      "status":"Pending",
			      "note":null,
			      "reason":null,
			      "submitDate":1555944891173,
			      "qualifierName":"Casey Murrell",
			      "qualifierUserName":"CMurrell",
			      "updaterName":"Casey Murrell",
			      "updaterUserName":"CMurrell",
			      "updateDate":1557180872567,
			      "postalCode":null
			   }
			];
			
			vm.$onInit = function() {
				
			}

		}
	}
export default leadsWidget;