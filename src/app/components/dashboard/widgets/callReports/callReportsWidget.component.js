import callReportsWidgetTemplate from './callReportsWidget.html';

const callReportsWidget =  {
		template: callReportsWidgetTemplate,
		controller: function(
			$scope,
			$timeout,
			$state,
			userProfile,
			utilService,
			callReportService
		) {

			var vm = this;
			
			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getCallReportDashboardInfo();
				}
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getCallReportDashboardInfo(), 100);
				
            });

			function getCallReportDashboardInfo() {
				if(vm.inProgress === true) return;
				vm.inProgress = true;
				
				callReportService.getCallReportDashboardInfo().then(
					function(response) {
						vm.data = response.response;
						vm.monthOneVisits = vm.data.visitsContainer.monthOneVisits;
						vm.monthTwoVisits = vm.data.visitsContainer.monthTwoVisits;
						vm.monthThreeVisits = vm.data.visitsContainer.monthThreeVisits;
						vm.targetVisits = vm.data.visitsContainer.totalVisits;
						vm.totalVisits = vm.monthOneVisits + vm.monthTwoVisits + vm.monthThreeVisits;
						vm.quarter = vm.data.visitsContainer.quarter;
						vm.year = vm.data.visitsContainer.year;
						
						
						vm.remainder = vm.targetVisits - vm.totalVisits;
						if(vm.remainder < 0) vm.remainder = 0;
						if(vm.totalVisits === 0) vm.remainder = 1;
						
						vm.drafts = vm.data.drafts;
						if(!vm.drafts) vm.drafts = [];
						
						vm.data = [vm.totalVisits, vm.remainder];
						vm.colors = [
							{
								backgroundColor: '#b545ff',
								pointBackgroundColor: '#b545ff'
							},
							{
								backgroundColor: '#e8e8e8',
								pointBackgroundColor: '#e8e8e8'
							}
						];
						
						
						vm.options = {
							tooltips: {
								enabled: false
							},
							elements: {
								arc: {
									borderWidth: 0,
									hoverBorderWidth: 0,
									borderAlign: 'inner'
								}
							},
							circumference: 2 * Math.PI,
							cutoutPercentage: 75,
							animation: {
								animateRotate: true
							}
						};
						
						vm.inProgress = false;

					})
					.catch(function(error) {
						
					})
			}
			
			vm.openDraft = function(draft) {
				$state.go('callReport', {planId: draft.planId, callReportId: draft.callReportId});
			}
			
			vm.openCallReportsPage = function() {
				$state.go('topLevelCallReports', {reporter: userProfile.getFullName()});
			}
			
			vm.createNewCallReport = function() {
	          utilService
	            .uibModal()
	            .open({
	              component: "callReportModal",
	              size: "sm",
	              resolve: {
	            	  modalData: function () {
	                      return { 
	                          selectedPlanId: vm.selectedPlanId,
	                          selectedCustomerName: vm.selectedCustomerName
	                      }
	                  }
	              }
	            })
	            .result.then(
	            	function(result) {
	            		
	            	},
	            	function(dismiss) {
	            		
	            	});
			};
		}
	}
export default callReportsWidget;