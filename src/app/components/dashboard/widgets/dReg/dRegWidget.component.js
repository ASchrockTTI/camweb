import dRegTableTemplate from './dRegTable.html';

const dRegWidget =  {
		template: dRegTableTemplate,
		controller: function(
			overviewService
		) {

			var vm = this;
			
			vm.prevYear = 2018;
			vm.currentYear = 2019;
			
			vm.$onInit = function() {
				getSummaries();
			}

			function getSummaries() {
//				overviewService.summary.queryByEntity()
//				.then(function(response) {
//					vm.summaries = response;
//				})
//				.catch(function(error) {
//
//				})
				
				vm.summaries = overviewService.getSummaries();
				vm.prevYearTotal = overviewService.getPrevYearTotal();
				vm.currentYearTotal = overviewService.getCurrentYearTotal();
				vm.goal = overviewService.getGoal();
				vm.currentYearRR = overviewService.getCurrentYearRR();
			}
			
		}
	}
export default dRegWidget;