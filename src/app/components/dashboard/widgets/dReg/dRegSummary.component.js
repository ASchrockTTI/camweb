import dRegSummaryTemplate from './dRegSummary.html';

const dRegSummary =  {
		template: dRegSummaryTemplate,
		bindings: {
	        summary: '=',
	        depth: '<',
	        expanded: '<'
		},
		controller: function() {

			var vm = this;
			
			this.color = ['', '#68a2ff', '#ffb600', '#a5ff51', '#ff51d0', '#51ffff'];

			this.childExpanded = false;
		}
	}
export default dRegSummary;