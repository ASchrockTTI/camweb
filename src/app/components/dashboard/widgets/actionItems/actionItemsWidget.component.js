import actionItemsWidgetTemplate from './actionItemsWidget.html';

const actionItemsWidget =  {
		template: actionItemsWidgetTemplate,
		controller: function(
			$scope,
			$timeout,
			$state,
			userProfile,
			utilService,
			actionItemService
		) {

			var vm = this;
			
			
			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getActionItemCounts();
				}
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getActionItemCounts(), 100);
            });

			function getActionItemCounts() {
				if(vm.inProgress === true) return;
				vm.inProgress = true;
				
				actionItemService.getActionItemCounts().then(
					function(response) {
						vm.data = response.response;
						vm.pastDue = vm.data.pastDue;
						vm.upcoming = vm.data.upcoming;
						vm.incomplete = vm.data.incomplete;
						vm.remainder = vm.incomplete - vm.pastDue - vm.upcoming;
						
						vm.labels = ['Past Due','Next 7 Days', '7+ Days'];
						vm.data = [vm.pastDue, vm.upcoming, vm.remainder];
						vm.colors = [
							{
								backgroundColor: '#eb2121',
								pointBackgroundColor: '#eb2121'
							},
							{
								backgroundColor: '#ed8e2f',
								pointBackgroundColor: '#ed8e2f'
							},
							{
								backgroundColor: '#ede240',
								pointBackgroundColor: '#ede240'
							}
						];
						
						
						vm.options = {
							tooltips: {
								enabled: true
							},
							elements: {
								arc: {
									border: 10,
									hoverBorderWidth: 0,
									borderAlign: 'inner'
								}
							},
							circumference: Math.PI/2,
							cutoutPercentage: 50,
							animation: {
								animateRotate: true
							}
						};
						
						vm.inProgress = false;
						
					})
					.catch(function(error) {
						
					})
			}
			
		}
	}
export default actionItemsWidget;