import leadsSummaryWidgetTemplate from './leadsSummaryWidget.html';

const leadsSummaryWidget =  {
		template: leadsSummaryWidgetTemplate,
		controller: function(
			$scope,
			$timeout,
			$state,
			userProfile,
			pendingCustomerService
		) {

			var vm = this;

			vm.totalCustomerLeads = -1;
			vm.loading = true;

			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getPendingCustomerCountByCurrentUser(userProfile.getSAMAccountName());
				}
			}

			vm.openPendingCustomersPage = function() {
				$state.go('leadsHome');
			}

			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getPendingCustomerCountByCurrentUser(userProfile.getSAMAccountName()), 100);
         });
			
			function getPendingCustomerCountByCurrentUser(userName) {
				pendingCustomerService.getPendingCustomerCountByCurrentUser(userName).then(
					function(response) {
						vm.loading = false;
						vm.totalCustomerLeads = response.pendingCustomerCount;
					})
					.catch(function(error) {
						
						console.log('response Error: ', error);
					})
			}

		}
	}
export default leadsSummaryWidget;