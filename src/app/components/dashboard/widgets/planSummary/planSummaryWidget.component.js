import planSummaryWidgetTemplate from './planSummaryWidget.html';

const planSummaryWidget =  {
		template: planSummaryWidgetTemplate,
		controller: function(
			$scope,
			$timeout,
			$state,
			$stateParams,
			userProfile,
			planService
		) {

			var vm = this;

			vm.totalPlans = undefined;
			vm.totalRunRate       = 0;
			vm.totalTarget        = 0;
			vm.percentToPlanString = 0;

			
			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getPlanSummaryInfo(userProfile.getSAMAccountName());
				}
			}

			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getPlanSummaryInfo(userProfile.getSAMAccountName()), 100);
			});
			
			
			vm.openLeads = function() {
				var role = userProfile.getUserType().toUpperCase();
				switch(role) {
				case 'ADMIN':
				case 'HEAD_HONCHO':
				case 'EXECUTIVE':
				case 'RVP':
				case 'GM':
				case 'FSM':
				case 'ISM':
					$state.go('leads');
					break;
				default:
					$state.go('customerList', {type: 'leads'});
				}
			}
			
			
			function getPlanSummaryInfo(userName) {
				if(vm.inProgress === true) return;
				vm.inProgress = true;
				
				planService.getTopLevelUserPlanInfo(userName).then(
					function(response) {
						vm.totalPlans           = response.totalPlans;
						vm.totalLeads			= response.totalLeads;
						vm.totalRunRate         = formatIntInternationalCurrency(response.totalRunRate, userProfile.getSelectedEntity());
						vm.totalTarget          = formatIntInternationalCurrency(response.totalTarget, userProfile.getSelectedEntity());
						vm.remainder            = formatIntInternationalCurrency((response.totalTarget - response.totalRunRate), userProfile.getSelectedEntity());
						vm.percentToPlanString   = formatFloatToPercentage(response.totalPercentToPlan);

						vm.totalPercentToPlan = formatFloatToWholeNumber(response.totalPercentToPlan);
						
						var color = '#d9534f';
						if(vm.totalPercentToPlan >= 75) color = '#ffa617';
						if(vm.totalPercentToPlan >= 90) color = '#ffe017';
						if(vm.totalPercentToPlan >= 100) color = '#5CB85C';
						
						var totalRemainder = vm.totalPercentToPlan < 100 ? 100 - vm.totalPercentToPlan : 0;

						vm.labels = ["Run Rate: " + vm.totalRunRate, 'Remainder to Target: ' + vm.remainder];
						vm.data = [vm.totalPercentToPlan, totalRemainder];

						vm.colors = [
							{
								backgroundColor: color,
								pointBackgroundColor: color
							},
							{
								backgroundColor: '#e8e8e8',
								pointBackgroundColor: '#e8e8e8'
							}
						];

						vm.options = {
							legend: {
								display: false
							},
							title: {
								display: false
							},
							tooltips: {
								enabled: true,
								callbacks: {
					                label: function(tooltipItem, data) {
					                    var label = vm.labels[tooltipItem.index] || '';

					                    return label;
					                }
					            }
							},
							elements: {
								arc: {
									borderWidth: 0
								}
							}
						};
						
						vm.inProgress = false;

					})
					.catch(function(error) {
						
						console.log('response Error: ', error);
					})
			}

			const CURRENCY_STYLE = 'currency';
			
			function convertStringToInt(string) {
				if(typeof string !== 'string') {
					return string;
				}
				return parseInt(string);
			}

			function convertStringToFloat(string) {
				if(typeof string !== 'string') {
					return string;
				}
				return parseFloat(string);
			}

			function formatFloatInternationalCurrency(number, entity, minFractionDigits, maxFractionDigits) {
				let convertedNumber = convertStringToFloat(number);
				let currencyType = entity === 'EDC' ? 'de-DE' : 'en-US';
				const currencyOptions = {
					style: CURRENCY_STYLE,
					currency: entity === 'EDC' ? 'EUR' : 'USD',
					minimumFractionDigits: minFractionDigits ? minFractionDigits : 0,
					maximumFractionDigits: maxFractionDigits ? maxFractionDigits : 0,
				};
				return new Intl.NumberFormat(currencyType, currencyOptions).format(convertedNumber);
			}

			function formatIntInternationalCurrency(number, entity, minFractionDigits, maxFractionDigits) {
				let convertedNumber = convertStringToInt(number);
				let currencyType = entity === 'EDC' ? 'de-DE' : 'en-US';
				const currencyOptions = {
					style: CURRENCY_STYLE,
					currency: entity === 'EDC' ? 'EUR' : 'USD',
					minimumFractionDigits: minFractionDigits ? minFractionDigits : 0,
					maximumFractionDigits: maxFractionDigits ? maxFractionDigits : 0,
				};
				return new Intl.NumberFormat(currencyType, currencyOptions).format(convertedNumber);
			}

			function formatFloatToWholeNumber(number) {
				let convertedNumber = convertStringToFloat(number);
				convertedNumber = convertedNumber * 100;
				convertedNumber = Math.round(convertedNumber);
				return convertedNumber;
			}

			function formatFloatToPercentage(number) {
				let convertedNumber = formatFloatToWholeNumber(number);
				convertedNumber = convertedNumber + '%';
				return convertedNumber;
			}
		}
	}
export default planSummaryWidget;