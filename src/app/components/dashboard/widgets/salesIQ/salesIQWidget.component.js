import salesIQWidgetTemplate from './salesIQWidget.html';

const salesIQWidget =  {
		template: salesIQWidgetTemplate,
		controller: function(
				$state,
				$scope,
				$timeout,
				salesIQService,
				userProfile
		) {

			var vm = this;

			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getSalesIQDashboardInfo();
				}

			}

			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getSalesIQDashboardInfo(), 100);
			});

				
			function getSalesIQDashboardInfo() {
				if(vm.inProgress === true) return;
				vm.inProgress = true;

				vm.userProfile = userProfile.getUserProfile();
				
				salesIQService.getSalesIQDashboardInfo()
				.then(
					function(response) {
						vm.topOpportunities = response.opportunities;
						vm.outstanding = response.outstanding;
						vm.pursuing = response.pursuing;
						vm.valueOutstandingGrowth = response.valueOutstandingGrowth;
						vm.valueOutstandingRecovery = response.valueOutstandingRecovery;
						vm.valuePursuingGrowth = response.valuePursuingGrowth;
						vm.valuePursuingRecovery = response.valuePursuingRecovery;
						
						vm.total = vm.outstanding + vm.pursuing;
						vm.valuePursuing = vm.valuePursuingGrowth + vm.valuePursuingRecovery;
						vm.valueTotal = vm.valuePursuing + vm.valueOutstandingGrowth + vm.valueOutstandingRecovery;

						vm.inProgress = false;

				})
				.catch(function(error) {
					
				});

			}

			vm.goToPlan = function(opportunity) {
				$state.go('salesIQ', {planId: opportunity.planId });
			}
		}
	}
export default salesIQWidget;