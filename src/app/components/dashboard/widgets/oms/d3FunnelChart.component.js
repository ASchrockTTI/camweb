import funnelChartTemplate from './funnelChart.html';

const d3FunnelChart =  {
		template: funnelChartTemplate,
		bindings: {
	        funnelData: "<",
	        funnelOptions: "<",
		},
		controller: function(
			$scope
		) {

			var vm = this;

			vm.$onInit = function() {

				var chart = new D3Funnel('#funnel');
			    chart.draw(vm.funnelData, vm.funnelOptions);
			
			}

			
		}
	}
export default d3FunnelChart;