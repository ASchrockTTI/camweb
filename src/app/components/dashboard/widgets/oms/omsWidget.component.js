import omsWidgetTemplate from './omsWidget.html';

const omsWidget =  {
		template: omsWidgetTemplate,
		controller: function(
			$filter,
			$document,
			$interval,
			$scope,
			$timeout,
			$state,
			userProfile,
			utilService,
			omsService
		) {

			var vm = this;
			
			vm.displayFunnel = false;
			
			vm.colors = [
				'#1395BA',
				'#962ea6',
				'#EBC844',
				'#F16C20',
				'#5CB85C',
				'#C02E1D'
			];
			
			vm.options = {
		        block: {
		            dynamicHeight: true,
		            minHeight: 4,
		            highlight: true,
		            fill: {
						type: 'gradient'
		            }
		        },
		        chart: {
		            bottomWidth: 1/3,
		        	bottomPinch: 3,
		        	curve: {
		            	enabled: true,
		            	height: 20,
		        	},
		        	animate: 150
		        },
		        label: {
		        	enabled: false,
					format: '{l}: {v}'
		        },
		        tooltip: {
					enabled: true,
					format: '{f}'
		        }
		    };
			
			vm.$onInit = function() {
				if($document[0].documentMode) vm.isInternetExplorer = true;
				
				if(userProfile.getFullName() !== 'undefined undefined') {
					getOmsSummary();
				}
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getOmsSummary(), 100);
            });
			
			function getOmsSummary() {
				if(vm.gettingOmsSummary === true) return;
				vm.gettingOmsSummary = true;
				
				omsService.getOmsSummary().then(
					function(response) {
						vm.projectCount = response.projectCount;
						vm.totalProjectValue = response.totalProjectValue;
						
						parseLineItems(response.lineItemPhases);
					})
					.catch(function(error) {
						
					})
			}
			
			function parseLineItems(phases) {
				vm.lineItemCount = 0;
				vm.maxValue = 0;
				
				vm.funnelData = [];
				
				for(var i = 0; i < phases.length; i++) {
					
					if(phases[i].totalValue > vm.maxValue) vm.maxValue = phases[i].totalValue;
					
					var formattedCount = $filter('number')(phases[i].count);
					var formattedTotalValue =  $filter('currency')(phases[i].totalValue, '$', 0);
					
					var formattedValue = phases[i].phaseName + ': ' + formattedCount + '\n' + formattedTotalValue;
					
					vm.lineItemCount += phases[i].count;
					
					vm.funnelData.push(
					{
						label: phases[i].phaseName,
						value: phases[i].totalValue,
						formattedValue: formattedValue,
						backgroundColor: vm.colors[i],
						formattedCount: formattedCount,
						formattedTotalValue: formattedTotalValue
					})
				}
				
				vm.gettingOmsSummary = false;
				vm.displayFunnel = true;
			}
			
			vm.goToOms = function() {
				$state.go('omsList');
			}
			
		}
	}
export default omsWidget;