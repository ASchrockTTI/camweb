import dashboardTemplate from './dashboard.html';

const dashboard =  {
		template: dashboardTemplate,
		controller: function(
			overviewService,
			$rootScope,
			$scope,
			$timeout,
			userProfile
		) {

			var vm = this;
			
			vm.gridsterOpts = {
				columns: 12,
				colWidth: '198',
				rowHeight: 'match',
				margins: [15, 15],
				resizable: {
				  enabled: false
				},
				draggable: {
				  enabled: false
				}
			};
			
			vm.displaySalesIQ = true;
			vm.displayDashboard = true;
			
			vm.$onInit = function() {
				if(userProfile.getFullName() !== 'undefined undefined') {
					getPermissions();
				}
			}

			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(getPermissions(), 100);
			});
				
			function getPermissions() {
				if(userProfile.getFullName() === 'undefined undefined') {
					return;
				}
				
				var entity = userProfile.getUserProfile().entity;
				var userType = userProfile.getUserProfile().userType;

				if(userType === 'NONE') {
					vm.displayDashboard = false; 
				}

				if(userProfile && userProfile.getUserProfile() && userProfile.getUserProfile().entity && userProfile.getUserProfile().entity.toUpperCase() !== 'NDC') {
					vm.displaySalesIQ = false;					
				}
			}
		}
	}
export default dashboard;