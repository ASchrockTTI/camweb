import releaseNotesModalTemplate from './releaseNotes.html'
const releaseNotesModal =  {
		template: releaseNotesModalTemplate,
		bindings : {
			modalInstance : '<',
			resolve : '<'
		},
		controller : function(utilService, adminService, preferencesService, $window, $sce, FileSaver) {

			var vm = this;

			vm.$onInit = function() {
				vm.releaseNotes = [];
				getReleaseNotes();
			}
			
			function getReleaseNotes() {
				adminService.getAllReleaseNotes().then(
				function(response) {
					vm.releaseNotes = response;
					vm.releaseNotes[0].isExpanded = true;
					
					preferencesService.updateLastVersion(vm.releaseNotes[0].version);
				})
				.catch(function(error) {
					
				});
			}
			
			
			vm.downloadDocument = function(document) {
				adminService.downloadReleaseNoteDocument(document.releaseNoteDocumentId)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
			}

			vm.dismiss = function() {
				vm.modalInstance.dismiss();
			}

		}
	}
export default releaseNotesModal;