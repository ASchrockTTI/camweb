import actionItemsModalTemplate from './actionItemsModal.html';

const actionItemsModal =  {
		template: actionItemsModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function($rootScope, $stateParams, omsService, $filter, $scope, $timeout) {
			
			var vm = this;
			
			vm.params = $stateParams;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.project = vm.data.project;
				vm.showComplete = vm.data.showComplete;
				
				vm.project.projectActions =  $filter('orderBy')(vm.project.projectActions, 'dueDate');
				vm.project.projectActions =  $filter('orderBy')(vm.project.projectActions, 'complete');
			}
			
			vm.$onDestroy = function() {
				for(var i = 0; i < vm.project.projectActions.length; i++) {
					if(!(vm.project.projectActions[i].actionId > 0)) {
						vm.project.projectActions.splice(i, 1);
						i--;
					}
				}
				
				$rootScope.actionItemModalOpen = false;
			}
			
			vm.addActionItem = function() {
				
				vm.project.projectActions.unshift({
					actionDescription: '',
					complete: false,
					daysToRemind: 30,
					dueDate: undefined,
					edit: true
				})
				
				var div = document.getElementById('actionItemListContainer');
				div.scrollTop = 0;
			}
			
			vm.editActionItem = function(actionItem) {
				actionItem.edit = true;
				actionItem.original = undefined;
				actionItem.original = JSON.parse(JSON.stringify(actionItem));
				actionItem.assigneeNameInput = actionItem.assigneeFullName;
			}
			
			vm.cancelEdit = function(actionItem, event) {
				event.stopPropagation();
				angular.copy(actionItem.original, actionItem);
				actionItem.edit = false;
				if(!actionItem.actionId) {
					vm.project.projectActions.splice(vm.project.projectActions.indexOf(actionItem), 1);
				}
			}
			
			vm.save = function(actionItem) {
				actionItem.original = undefined;
				
				var planId = vm.project.customerPlanId;
				if(!planId) planId = vm.params.planId;
				
				omsService.saveActionItem(vm.project.customerPlanId, vm.project.projectId, actionItem)
                .then(function(response) {
                 	actionItem.actionId = response.actionId;
                 	actionItem.daysToRemind = response.daysToRemind;
                 	actionItem.dueDate = response.dueDate;
                 	actionItem.creatorFullName = response.creatorFullName;
                 	actionItem.edit = false;
				})
				.catch(function(error) {
					
				})
			}
			
			vm.delete = function(actionItem) {
				omsService.deleteActionItem(vm.project.customerPlanId, vm.project.projectId, actionItem.actionId)
                .then(function(response) {
                	vm.project.projectActions.splice(vm.project.projectActions.indexOf(actionItem), 1);
				})
				.catch(function(error) {
					
				})
			}
			
			vm.selectAssignee = function(actionItem, user) {
				actionItem.assigneeFullName = user.fullName;
				actionItem.assigneeUsername = user.sAMAccountName;
			}
			
			vm.assigneeNameChange = function(actionItem) {
				//timeout wrapper to compensate for AngularJS defect (https://github.com/angular/angular.js/issues/4558)
				$timeout(function() {
					actionItem.assigneeFullName = actionItem.assigneeNameInput;
					actionItem.assigneeUsername = undefined;
				});
			}
			
			$scope.incompleteActionFilter = function() {
				return function( action ) {
					if(vm.showComplete) return true;
					return !action.complete;
				};
			};
			
			vm.cancel = function() {
				vm.modalInstance.dismiss();
			}
			
		}
	}
export default actionItemsModal;