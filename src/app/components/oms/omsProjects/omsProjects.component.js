import omsProjectsTemplate from "./omsProjects.html";

const omsProjects = {
  template: omsProjectsTemplate,
  bindings: {
    ngModel: "=",
    plan: "<",
    status: "<",
    project: "=",
    projects: "=",
    selectedOwner: "=",
    selected: "<",
  },
  controller: function (
    $scope,
    $state,
    $stateParams,
    permissionsFactory,
    userProfile,
    omsService,
    utilService,
    roles,
    $sce
  ) {
    var vm = this;
    var permissionsValidator;

    vm.params = $stateParams;

    vm.selectedOwner = "";

    vm.$onInit = function () {
      vm.planId = parseInt(vm.params.planId);

      permissionsValidator = permissionsFactory.newInstance();
      permissionsValidator.setAddRoles([
        roles.OWNER,
        roles.ADMIN,
        roles.GM,
        roles.ISM,
        roles.FSM,
        roles.RSE,
        roles.BRANCH_MEMBER,
      ]);
      permissionsValidator.setEditRoles([
        roles.OWNER,
        roles.ADMIN,
        roles.GM,
        roles.ISM,
        roles.FSM,
        roles.RSE,
        roles.BRANCH_MEMBER,
      ]);
      permissionsValidator.setDeleteRoles([
        roles.OWNER,
        roles.ADMIN,
        roles.GM,
        roles.ISM,
        roles.FSM,
        roles.RSE,
        roles.BRANCH_MEMBER,
      ]);
    };

    vm.isOwner = function (projectOwner) {
      return (
        projectOwner.toLowerCase() === userProfile.getFullName().toLowerCase()
      );
    };

    vm.filterOwner = function (owner) {
      if (vm.selectedOwner === "") {
        vm.selectedOwner = owner;
      } else {
        vm.selectedOwner = "";
      }
    };

    vm.filterMasterProject = function (project) {
      if (
        project === null ||
        project.masterProjectId === vm.selectedMasterProjectId
      ) {
        vm.selectedMasterProjectId = undefined;
      } else {
        vm.selectedMasterProjectId = project.masterProjectId;
        vm.selectedMasterProjectName = project.masterProjectName;
      }
    };

    $scope.sortMasterProject = function (project) {
      return project.projectId === vm.selectedMasterProjectId;
    };

    vm.getIncompleteActionCount = function (project) {
      if (!project.projectActions) return 0;
      var total = 0;
      for (var i = 0; i < project.projectActions.length; i++) {
        if (!project.projectActions[i].complete) total++;
      }
      return total;
    };

    vm.goToPlan = function (planId) {
      $state.go("businessPlan", { planId: planId });
    };

    vm.openProject = function (project) {
      vm.project = project;
      $state.go(".", { projectId: project.projectId }, { notify: false });
    };

    vm.permissions = {
      validated: function (action) {
        return permissionsValidator.hasPermission(action, vm.plan);
      },
      delete: function (project) {
        return (
          userProfile.isAdmin() ||
          utilService.equalsIgnoreCaseSpace(
            project.owner,
            userProfile.getSAMAccountName()
          )
        );
      },
    };

    $scope.$on("clearFilters", function () {
      vm.selectedOwner = "";
      vm.selectedMasterProjectId = undefined;
    });

    vm.deleteProject = function (projectId) {
      utilService
        .deleteConfirmation(
          "OMS",
          "You are about to delete the selected project.",
          "btn-danger"
        )
        .result.then(
          function (result) {
            if (result) {
              omsService
                .removeProject(vm.params.planId, projectId, vm.status)
                .then(function (response) {})
                .catch(function (error) {});
            }
          },
          function (dismiss) {}
        );
    };

    vm.showConstruction = function () {
      utilService
        .deleteConfirmation(
          "Under Construction",
          "The page you are attempting to access is not available at this time.",
          "btn-default",
          true
        )
        .result.then(
          function (result) {},
          function (dismiss) {}
        );
    };

    vm.addOpportunity = function () {
      utilService
        .uibModal()
        .open({
          component: "omsModal",
          size: "md",
          resolve: {
            modalData: function () {
              return {
                action: "add",
                projects: vm.projects,
              };
            },
          },
        })
        .result.then(
          function (result) {},
          function (dismiss) {}
        );
    };

    vm.openActionItems = function (project, showComplete) {
      utilService
        .uibModal()
        .open({
          component: "actionItemsModal",
          size: "md",
          resolve: {
            modalData: function () {
              return {
                project: project,
                showComplete: showComplete,
              };
            },
          },
        })
        .result.then(
          function (result) {},
          function (dismiss) {}
        );
    };

    vm.sort = {
      type: "lastUpdateDate",
      reverse: false,
      order: function (sortType, sortReverse) {
        if (vm.sort.type !== sortType) {
          vm.sort.reverse = !sortReverse;
        } else {
          vm.sort.reverse = sortReverse;
        }

        vm.sort.type = sortType;
      },
    };
  },
};
export default omsProjects;
