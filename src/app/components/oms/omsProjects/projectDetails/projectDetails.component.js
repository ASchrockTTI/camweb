import projectDetailsTemplate from './projectDetails.html';

const projectDetails = {
            template: projectDetailsTemplate,
            bindings: {
                project: '=',
                activeProjects: '<',
                plan: '<'
            },
            controller: function (
            		$scope,
            		$rootScope,
            		$state,
            		$location,
                    omsService,
                    pendingCustomerService,
                    $stateParams,
                    userProfile,
                    utilService,
                    ) {

                var vm = this;
                vm.params = $stateParams;
                
                vm.businessPartners = undefined;
                
                var statuses = {
                	open: {
                		statusId: 1,
                		statusName: 'Open',
                		orderBy: 1
                	},
                	closed: {
                		statusId: 2,
                		statusName: 'Closed',
                		orderBy: 2
                	}
                }


                vm.$onInit = function () {
                	vm.sort.type = 'updateDate';
                	
                	if(!vm.project.masterProjectName) {
                		vm.project.masterProjectName = '';
                	}
                	
                	vm.expandedItem = 0;
                    vm.editMode = false;
                    $scope.dateOptions = {
                    	initDate: new Date()
                    };
                    loadLineItems();
                    loadBusinessPartners();
                    
                    if(vm.params.actionItemId) {
                    	vm.openActionItems(vm.project, false);
                    }
                }

                vm.$onDestroy = function() {
                    if(vm.original) {
                        angular.copy(vm.original, vm.project);
                    }
                    
                    lineItemRefreshListener();
                };
                
                
                vm.deleteProject = function() {
                    utilService.deleteConfirmation(
                        "OMS",
                        "You are about to delete the selected project.",
                        "btn-danger"
                      )
                      .result.then(
                      	function(result) {
                      		if(result) {
          	            		omsService.removeProject(vm.params.planId, vm.project.projectId, vm.params.type)
          	            		.then(function(response) {
          	            			$state.go('.', {type: vm.params.type, projectId: undefined});
          	            		})
      							.catch(function(error) {
      								
      							})
                      		}
                      	},
                      	function(dismiss) {
                      		
                      	});
                  };
                
                
                Object.byString = function(o, s) {
                    s = s.replace(/\[(\w+)\]/g, '.$1');
                    s = s.replace(/^\./, '');
                    var a = s.split('.');
                    for (var i = 0, n = a.length; i < n; ++i) {
                        var k = a[i];
                        if (k in o) {
                            o = o[k];
                        } else {
                            return;
                        }
                    }
                    return o;
                }
                
                
                $scope.predicate = function(elem) {
                	return Object.byString(elem[0], vm.sort.type);
                }
                
                $scope.$watch(function () { return $location.search().projectId }, function (newVal, oldVal) {
                    utilService.getLogger().info('projectIDs: ' + newVal + ' ' + oldVal)
                    if(newVal !== oldVal) {
                    	loadBusinessPartners();
                    	loadLineItems();
                    }
                }, true);
                
                vm.permissions = {
            		edit: function() {
                    	if (userProfile.isAdmin()) return true;
                    	if (utilService.equalsIgnoreCaseSpace(vm.project.owner, userProfile.getSAMAccountName())) return true;
                    	var branch;
                		if(vm.project.customerPlan) {
                			if (vm.project.customerPlan && vm.project.customerPlan.owner.toLowerCase().trim() === userProfile.getFullName().toLowerCase().trim()) return true;
                			branch = vm.project.customerPlan.branch;
                		}
                		else {
                			if (utilService.equalsIgnoreCaseSpace(vm.plan.acctManager, userProfile.getFullName())) return true;
                			branch = vm.plan.branch;
                		}
                		if(utilService.equalsIgnoreCaseSpace(userProfile.getBranch(), branch) || userProfile.hasBranch(branch)) return true;
                		
                		return false;
                    
                    },
                    delete: function() {
                    	return (userProfile.isAdmin() || utilService.equalsIgnoreCaseSpace(vm.project.owner, userProfile.getSAMAccountName()));
                    			
                    }
            		
                }
                
                var lineItemRefreshListener = $rootScope.$on('refreshLineItems', function(e, a) {
    				loadLineItems();
    				
    				vm.sort.type = "updateDate";
    			})
                
                vm.popovers = {
                	businessPartnerStatus: "businessPartnerStatus.html",
                    actionItems: "popoverProjectDetailsActionItems.html",
                    lineItemDescription: "popoverProjectLineItemDescription.html",
                    lineItemNotes: "popoverProjectLineItemNotes.html"
                };
                

                var loadBusinessPartners = function() {
                	vm.expandedItem = 0;
                    omsService.getBusinessPartners(vm.params.planId, vm.params.projectId)
                        .then(function (response) {
                            vm.businessPartners = response.omsBusinessPartners;
                            //remove "Default Customer" from array
                            vm.businessPartners.shift();
                            
                            vm.pendingPartners = response.pendingPartners;
                        })
                        .catch(function (error) {
                            vm.businessPartners = [];
                        })
                }
                
                
                var loadLineItems = function() {
                	vm.expandedItem = 0;
                    omsService.getDetails(vm.params.planId, vm.params.projectId)
                        .then(function (response) {
                            vm.avlList = response.avlList;
                            vm.lineItemCount = response.lineItemCount;
                            vm.displayLimit = 10;
                            getDisplayCount();
                            
                            
                            omsService.retrieveProject(vm.params.planId, vm.params.type, vm.params.projectId)
        					.then(function (response) {
        						var project = response.response;
        						vm.project.dollarValue = project.dollarValue;
        					})
        					.catch(function (error) {

        					})
                        })
                        .catch(function (error) {
                            vm.avlList = [];
                        })
                }
                
                vm.openMasterProject = function() {
                	vm.editMode = false;
                	omsService.retrieveProject(vm.params.planId, vm.params.type, vm.project.masterProjectId)
        			.then(function (response) {
        				vm.project = response.response;
        				if(vm.project.customerPlan && vm.project.customerPlan.familyId !== vm.plan.familyID) {
    		          	  $state.go('oms', {planId: vm.project.customerPlan.planId, type: 'active', projectId: vm.project.projectId}, {reload: true}); 
    		            } else {
    		          	  $state.go('oms', {projectId: vm.project.projectId}, {notify: false});
    		            }
        			})
        			.catch(function (error) {

        			})
                  }
                
                vm.openActionItems = function(project, showComplete) {
                	if($rootScope.actionItemModalOpen) return;
                	$rootScope.actionItemModalOpen = true;
                	
                    utilService
                      .uibModal()
                      .open({
                        component: "actionItemsModal",
                        size: "md",
                        resolve: {
                      	  modalData: function () {
                                return { 
                                    project: project,
                                    showComplete: showComplete
                                }
                            }
                        }
                      })
                      .result.then(
                  	function(result) {
                  		
                  	},
                  	function(dismiss) {
                  		
                  	});
                  };
                  
                vm.toggleStatus = function() {
                	var message = 'You are about to move "' + vm.project.projectName + '" to the archive. Unsaved changes will be lost.';
                	if (vm.project.headerStatus.statusId !== 1) {
                		message = 'You are about to reactivate "' + vm.project.projectName + '". Unsaved changes will be lost.';
                	}
                	 utilService.deleteConfirmation(
	                     "OMS",
	                     message,
	                     "btn-warning"
	                   )
	                   .result.then(
	                   	function(result) {
   	            			if(vm.editMode) vm.toggleEditMode();
   	            			vm.original = JSON.parse(JSON.stringify(vm.project));
       	                 	if(vm.project.headerStatus.statusId === 1) {
       	                 		vm.project.headerStatus = statuses.closed;
       	                 	} else {
       	                 		vm.project.headerStatus = statuses.open;
       	                 	}
       	                 	vm.updateProject();
	                   	},
	                   	function(dismiss) {
	                   		
	                   	});
                }

                vm.updateProject = function() {
                	if(!vm.project.masterProjectName) vm.project.masterProjectId = null;
                	
                    omsService.updateProject(vm.params.planId, vm.params.type, vm.project);
                    
                    vm.omsProjectForm.$setPristine();
                    vm.editMode = false;
                    
                    vm.project.lastUpdateDate = new Date();
                    
                    if(vm.original.headerStatus.statusId !== vm.project.headerStatus.statusId) {
                		omsService.changeStatus(vm.project);
                		
                		var status = 'archive';
                		if(vm.project.headerStatus.statusId === 1) status = 'active';
                		
                		$state.go('.', {type: status, projectId: vm.project.projectId});
                	}
                    vm.original = JSON.parse(JSON.stringify(vm.project));
                }

                vm.toggleEditMode = function() {
                    if(vm.editMode) {
                        vm.editMode = false;
                        angular.copy(vm.original, vm.project);
                        
                        vm.omsProjectForm.$setPristine();
                    } else {
                        vm.editMode = true;
                        vm.original = JSON.parse(JSON.stringify(vm.project));
                    }
                }
                
                vm.openBusinessPartnerModal = function() {
                	utilService.uibModal().open({
                        component: 'businessPartnerModal',
                        size: 'sm',
                        scope: $scope,
                        resolve: { 
                        	modalData: function() {
	                        	return {
	                        		businessPartners: vm.businessPartners
	                        	}
                        	}
                        }
	                }).result.then(
	                    function(result) {
	                    	loadBusinessPartners();
	                    },
	                    function(dismiss) {
	                        
	                    }
	                )
                }
                
                vm.removeBusinessPartner = function(partner) {
                	utilService.deleteConfirmation('Business Partner', 'You are about to remove the selected business partner.', 'btn-danger').result
    				.then(
    					function(result) {
							omsService.removeBusinessPartner(vm.params.planId, vm.params.projectId, partner.projectCustomerId)
							.then(function(response) {
								vm.businessPartners.splice(vm.businessPartners.indexOf(partner), 1);
							})
							.catch(function(error) {

							})
    						
    					},
    					function(dismiss) {
    						
    					}
    				)
                }
                
                vm.removePendingPartner = function(partner) {
             	   utilService.deleteConfirmation('Business Partner', 'You are about to remove the selected pending business partner.', 'btn-danger').result
    				.then(
    					function(result) {
 							pendingCustomerService.deletePendingCustomer(partner.pendingCustomerId)
 							.then(function(response) {
 								vm.pendingPartners.splice(vm.pendingPartners.indexOf(partner), 1);
 							})
 							.catch(function(error) {

 							})
    						
    					},
    					function(dismiss) {
    						
    					}
    				)
                }
                
                vm.openBusinessPartnerPlan = function(partner) {
                	if(partner.partnerPlan) {
                		goToPlan(partner.partnerPlan.planId)
                	} else if(partner.planId){
                		$state.go('businessPlan', {planId: partner.planId});
                	} else {
                		createBusinessPartnerPlan(partner);
                	}
                }
                
                function createBusinessPartnerPlan(partner) {
    				utilService.uibModal().open({
    					component: 'submitNewCustomerModal',
    					size: 'md',
    					animation: true,
    					keyboard: true,
    					resolve: { 
                        	modalData: function() {
                            	return {
                            		customerName: partner.customerName,
                            		context: 'oms'
                            	}
                        	}
                        }
    				}).result.then(
    					function(result) {
    						omsService.removeBusinessPartner(vm.params.planId, vm.params.projectId, partner.projectCustomerId)
							.then(function(response) {
								loadBusinessPartners();
							})
							.catch(function(error) {
								loadBusinessPartners();
							})
    					},
    					function(dismiss) {						
    					}
    				)
    			}
                
                
                function goToPlan(planId) {
                	if(vm.params.type === 'archive') {
                		$state.go('businessPlan', {planId: planId});
                	} else {
                		$state.go('oms', {planId: planId, type: 'active', projectId: undefined});
                	}
                }

                vm.addLineItem = function() {
                    utilService.uibModal().open({
                        component: 'lineItemsModal',
                        size: 'md',
                        scope: $scope,
                        resolve: { 
                        	modalData: function() {
	                        	return {
	                        		action: 'add',
	                        		avlList: vm.avlList
	                        	}
                        	}
                        }
	                }).result.then(
	                    function(result) {
	                    	loadLineItems();
	                    },
	                    function(dismiss) {
	                        
	                    }
	                )
                }
                
                vm.editLineItem = function(selectedLineItem, primary, avl) {
                    utilService.uibModal().open({
                    	keyboard: false,
                        component: 'lineItemsModal',
                        size: 'md',
                        resolve: {
                            modalData: function () {
                                return { 
                                    action: 'edit',
                                    lineItem: selectedLineItem,
                                    avlList: vm.avlList,
                                    avl: avl,
                                    primary: angular.copy(primary)
                                }
                            }
                        }
                    }).result.then(
                        function (result) {
                        	loadLineItems();
                        },
                        function (dismiss) {

                        }
                    )
                }
                
                vm.deleteLineItem = function(selectedLineItem) {
                	utilService.deleteConfirmation('Line Item', 'You are about to delete the selected line item.', 'btn-danger').result
    				.then(
    					function(result) {
    						if (result) {
    			
    							omsService.removeLineItem(vm.params.planId, vm.params.type, vm.params.projectId, selectedLineItem)
    							.then(function(response) {
    								loadLineItems();
    							})
    							.catch(function(error) {
    								loadLineItems();
    							})
    						}
    					},
    					function(dismiss) {
    						
    					}
    				)
                }
                
                vm.loadMore = function(){
                    vm.displayLimit += 10;
                    getDisplayCount();
                }

                function getDisplayCount() {
                	if(!vm.avlList) {
                		vm.displayCount = 0;
                		return;
                	}
                    vm.displayCount = Math.min(vm.lineItemCount, vm.displayLimit);
                }

                vm.getIncompleteActionCount = function(project) {
        			if(!project.projectActions) return 0;
        			var total = 0;
        			for(var i = 0; i < project.projectActions.length; i++){
        				if(!project.projectActions[i].complete) total++;
        			}
        			return total;
        		}

                vm.sort = {
                    reverse: true,
                    order: function (sortType, sortReverse) {
                        if (vm.sort.type !== sortType) {
                            vm.sort.reverse = !sortReverse;
                        } else {
                            vm.sort.reverse = sortReverse;
                        }
                        vm.sort.type = sortType;
                    }
                };
        		vm.filterCreatedBy = function(createdBy) {
        			if(vm.selectedCreatedBy === '') {
        				vm.selectedCreatedBy = createdBy;
        			} else {
        				vm.selectedCreatedBy = '';
        			}
        		}
            }
        }
export default projectDetails;