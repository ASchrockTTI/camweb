import businessPartnerModalTemplate from './businessPartnerModal.html';

const businessPartnerModal =  {
		template: businessPartnerModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
				utilService,
				omsService,
				familyService,
				$stateParams
			) {
			
			var vm = this;
			vm.params = $stateParams;
			
			vm.businessPartner = {
				name: '',
				planId: undefined	,
				owner: ''
			}
			
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.businessPartners = vm.data.businessPartners;
				
				setTimeout(function () {
					var input = angular.element(document.querySelector('#businessPartnerLookup'));
					input.focus();
				}, 500);
			}
			
			
			vm.selectBusinessPartner = function(item) {
				vm.businessPartner.partnerPlan = { planId: item.planId };
				vm.businessPartner.customerName = item.customerName;
				if(item.branch){
					vm.businessPartner.branch = item.branch;
				} else {
					vm.businessPartner.branch = item.planType.planType;
				}
				getPartnerFamilyList(item.planId)
			}
			
			vm.selectPartnerPlan = function(plan) {
				vm.businessPartner.partnerPlan = { planId: plan.planId };
				vm.businessPartner.customerName = plan.customerName;
				if(plan.branch) {
					vm.businessPartner.branch = plan.branch;
				} else {
					vm.businessPartner.branch = plan.planType;
				}
			}
			
			
			function getPartnerFamilyList(partnerPlanId) {
				
				familyService.findFamilyByPlanId(partnerPlanId)
				.then(function(response) {
					
					var familyList = response;
					
					familyList = utilService.orderBy(familyList,['planTypeId','-totalRunRate']);
					
					vm.familyList = familyList;
					
					
				})
				.catch(function(error) {
					
				})
			}
			
			vm.createNewCustomer = function() {
				utilService.uibModal().open({
					component: 'submitNewCustomerModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: { 
                    	modalData: function() {
                        	return {
                        		customerName: vm.businessPartner.customerName,
                        		context: 'oms'
                        	}
                    	}
                    }
				}).result.then(
					function(result) {
						//add pending customer to pendingBusinessPartners list
						vm.modalInstance.close('new customer submitted');
					},
					function(dismiss) {
						
					}
				)
			}

			vm.add = function() {
				if(alreadyAdded()) {
					vm.modalInstance.dismiss('already added');
				} else {
					omsService.addBusinessPartner(vm.params.planId, vm.params.projectId, vm.businessPartner.partnerPlan.planId)
					.then(function(response) {
						response.shift();
						angular.copy(response, vm.businessPartners);
					})
					.catch(function(error) {

					})
			
					
					vm.modalInstance.dismiss('partner added');
				}
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			function alreadyAdded() {
				for(var i = 0; i < vm.businessPartners.length; i++) {
					if(vm.businessPartners[i].partnerPlan && vm.businessPartners[i].partnerPlan.planId === vm.businessPartner.partnerPlan.planId) {
						return true;
					}
				}
				return false;
			}
			
		}
	}
export default businessPartnerModal;