import lineItemsModalTemplate from './lineItemsModal.html';

const lineItemsModal = {
            template: lineItemsModalTemplate,
            bindings: {
                modalInstance: '<',
                resolve: '<'
            },
            controller: function (
                omsService,
                utilService,
                $scope,
                $stateParams) {

                var vm = this;

                vm.designTypes= [
                    {
                        designTypeId: 1,
                        designTypeName: 'New',
                        orderBy: 1
                    },
                    {
                        designTypeId: 2,
                        designTypeName: 'Repl',
                        orderBy: 2
                    },
                    {
                        designTypeId: 3,
                        designTypeName: 'Crossed',
                        orderBy: 3
                    }
                ];

                vm.commodities = [
                    'C/P',
                    'CAP',
                    'CON',
                    'CUS',
                    'DSC',
                    'E/M',
                    'EMC',
                    'MSC',
                    'PWR',
                    'RES',
                    'SEN',
                    'SPL'
                ]

                vm.phases = [
                    {
                        phaseId: 1,
                        phaseName: 'Identify Opportunity',
                        orderBy: 1
                    },
                    {
                        phaseId: 2,
                        phaseName: 'Identify Solutions',
                        orderBy: 2
                    },
                    {
                        phaseId: 3,
                        phaseName: 'Sampled',
                        orderBy: 3
                    },
                    {
                        phaseId: 4,
                        phaseName: 'AVL Approved',
                        orderBy: 4
                    },
                    {
                        phaseId: 5,
                        phaseName: 'Production Booked',
                        orderBy: 5
                    },
                    {
                        phaseId: 6,
                        phaseName: 'Lost',
                        orderBy: 6
                    }
                ]

                vm.params = $stateParams;

                vm.$onInit = function () {
                    vm.data = vm.resolve.modalData;
                    vm.avlList = vm.data.avlList;
                    if(vm.data.action === 'add') {
                        vm.lineItem = {
                    		designType: {
                    			designTypeId: 1,
                                designTypeName: 'New',
                                orderBy: 1
                    		},
                    		part: {
                    			partNumber: '',
                    			mfr: ''
                    		},
                    		phase: vm.phases[0],
                    		primary: true
                        };
                        vm.avl = [vm.lineItem];
                        vm.modalTitle = 'Add New Line Item';
                        vm.confirmLabel = 'Create';
                        vm.original = JSON.parse(JSON.stringify(vm.lineItem));
                    } else {
                    	vm.avl = vm.data.avl;
                        vm.lineItem = vm.data.lineItem;
                        if(vm.lineItem.phase && vm.lineItem.phase.phaseName === 'Lost') {
                        	vm.lineItem.lost = true;
                        }
                        vm.original = JSON.parse(JSON.stringify(vm.lineItem));
                        vm.originalAvl = JSON.parse(JSON.stringify(vm.avl));
                        vm.setAvl(vm.avl);
                        if(vm.data.primary.detailId == vm.lineItem.detailId) {
                        	vm.lineItem.primary = true;
                        }
                        vm.modalTitle = 'Edit Line Item';
                        vm.confirmLabel = 'Save';
                    }
                }
                
                $scope.$on('modal.closing', function(event) {
                	if(vm.lineItemForm.$dirty) {
                		event.preventDefault();
                        utilService.deleteConfirmation('Prevent Navigation', 'Unsaved changes will be lost if you continue.', 'btn-warning').result
                        .then(
                            function (result) {
                                if (result == true) {
                                	vm.lineItemForm.$setPristine();
                                    vm.modalInstance.dismiss();
                                } else {
                                    return;
                                }
                            },
                            function (dismiss) {
                               
                            });
                    }
                })
                
                vm.setAvl = function(avl)
                {
                	vm.avl = avl;
                	 if (avl === null){
                 		vm.avl = null;
                 		vm.lineItem.opportunity = null;
                 		vm.lineItem.primary = true;
                 	} else if(avl[0].detailId === vm.lineItem.detailId){
                		vm.lineItem.opportunity = vm.original.opportunity;
                		vm.lineItem.primary = true;
                	} else{
                		vm.lineItem.opportunity = avl[0].opportunity;
                		vm.lineItem.primary = false;
                	}
                	if(vm.lineItemForm) vm.lineItemForm.$setDirty();
                }


                vm.$onDestroy = function () {
                    angular.copy(vm.original, vm.lineItem);
                };


                vm.reset = function() {
                	angular.copy(vm.original, vm.lineItem);
                	vm.lineItemForm.$setPristine();
                }

                vm.save = function(action) {
                	if(vm.lineItem.part.partNumber) {
                		vm.lineItem.part.partNumber = vm.lineItem.part.partNumber.toUpperCase();
                	}
                	if(vm.lineItem.customerPartNumber) {
                		vm.lineItem.customerPartNumber = vm.lineItem.customerPartNumber.toUpperCase();
                	}
                	if(vm.lineItem.part.mfr) {
                		vm.lineItem.part.mfr = vm.lineItem.part.mfr.toUpperCase();
                	}
                	
                    if(vm.data.action === 'add') {
                    	vm.lineItem.extendedValue = vm.lineItem.yr1Qty * vm.lineItem.resale;
                        vm.original = JSON.parse(JSON.stringify(vm.lineItem));
                        omsService.addLineItem(vm.params.planId, vm.params.type, vm.params.projectId, vm.lineItem)
                        .then(function(response) {
                        	vm.lineItemForm.$setPristine();
                            vm.modalInstance.close();
						})
						.catch(function(error) {
							vm.lineItemForm.$setPristine();
		                    vm.modalInstance.dismiss();
						})
                    } else {
                        vm.lineItem.extendedValue = vm.lineItem.yr1Qty * vm.lineItem.resale;
                        vm.original = JSON.parse(JSON.stringify(vm.lineItem));
                        omsService.updateLineItem(vm.params.planId, vm.params.type, vm.params.projectId, vm.lineItem)
                        .then(function(response) {
                        	vm.lineItemForm.$setPristine();
                            vm.modalInstance.close();
						})
						.catch(function(error) {
							vm.lineItemForm.$setPristine();
		                    vm.modalInstance.dismiss();
						})
                    }
                    
                }
                
                vm.avlLabel = function() {
                	if(!vm.lineItem.opportunity) return 'None';
                	if(vm.avl && !(vm.avl.length === 1 && vm.avl[0].detailId === vm.lineItem.detailId)) {
	                	if(vm.lineItem.primary) {
	                		return 'Primary';
	                	} else {
	                		return vm.avl[0].part.mfr + ' | ' + vm.avl[0].part.partNumber;
	                	}
                	}
                	return 'None';
                }

                vm.cancel = function() {
                    vm.modalInstance.dismiss();
                }

               
            }
        }
export default lineItemsModal;