import omsTemplate from './oms.html';

const oms =  {
		template: omsTemplate,
		controller: function(
				$rootScope,
				$scope,
				$state,
				$location,
				$stateParams,
				planService,
				omsService,
				utilService,
				userProfile,
				roles) {
			
			var vm = this;
			
			vm.selectedProject = undefined;
			vm.selectedOwner = '';
			
			vm.projects = {
				active: undefined,
				archive: undefined,
				related: undefined
			}
			
			vm.loaded = 0;
			
			vm.$onInit = function() {
				vm.params = $stateParams;				
				vm.clientError = utilService.getClientErrorObject();
				if(vm.params.type === 'related') {
					vm.activeTabIndex = 2;
				} else if(vm.params.type === 'archive') {
					vm.activeTabIndex = 1;
				} else {
					vm.activeTabIndex = 0;
				}
				if(vm.params.projectId){
					vm.selectedProject = {}
				}
				
				loadPlan();
			}	
						
			$scope.$watch(function(){ return $location.search().projectId }, function(){
				if($stateParams.type === 'related') {
					vm.activeTabIndex = 2;
				} else if($stateParams.type === 'archive') {
					vm.activeTabIndex = 1;
				} else {
					vm.activeTabIndex = 0;
				}
				if(vm.params.projectId) {
					vm.selectedProject = findById(vm.projects[vm.params.type], parseInt($location.search().projectId));
				} else {
					vm.selectedProject = undefined;
				}
				getProjects('active');
				getProjects('archive');
				getProjects('related');
			});
			
			 $scope.$watch(function(){ return vm.params.type }, function(){
				 if($stateParams.type === 'related') {
						vm.activeTabIndex = 2;
					} else if($stateParams.type === 'archive') {
						vm.activeTabIndex = 1;
					} else {
						vm.activeTabIndex = 0;
					}
			 });
			
			function getProjects(type) {
				if(vm.projects[type]) { return;}
				if(omsService.getLoadedPlan(type) !== vm.params.planId) {
					retrieveProjects(type);
					vm.loadStarted = true;
				} else { 
					vm.projects[type] = omsService.getProjects(type);
				}
			}
			
			
			function loadPlan() {
				planService.lookupPlan(vm.params.planId)
				.then(function(response) {
					vm.plan = response;
					planService.setPlan(vm.plan);
					
				})
				.catch(function(error) {
					
				})
			}
			
			vm.selectTab = function(tab) {
				if(vm.params.type === tab) {
					$rootScope.$broadcast('clearFilters', null);
				}
				
				$state.go('.', {type: tab, projectId: undefined});
			}
			
			
			function retrieveProjects(type) {
				omsService.retrieveProjectsByPlan(vm.params.planId, type)
				.then(function(response) {
					vm.projects[type] = response;
					
					if(vm.selectedProject) {
						for(var i = 0; i < vm.projects[type].length; i++) {
							if(vm.projects[type][i].projectId === vm.selectedProject.projectId) {
								vm.projects[type][i] = vm.selectedProject;
							}
						}
					}
				})
				.catch(function(error) {
					
				})
			}

			function retrieveSelectedProject(projectId) {
				omsService.retrieveProject(vm.params.planId, vm.params.type, projectId)
					.then(function (response) {
						vm.selectedProject = response.response;
						
						if(vm.params.type === 'active' && vm.selectedProject.headerStatus.statusId !== 1) {
							$state.go('.', {type: 'archive', projectId: projectId});
						} else if (vm.params.type !== 'active' && vm.selectedProject.headerStatus.statusId === 1) {
							$state.go('.', {type: 'active', projectId: projectId});
						}
					})
					.catch(function (error) {

					})
			}
			
			
			
			function findById(list, Id) {
				if(!list) {
					retrieveSelectedProject(Id);
					return;
				};
				for (var i = 0; i < list.length; i++) {
					if (list[i].projectId === Id) {
						return list[i];
					}
				}
				return undefined;
			}

		}
	}
export default oms;