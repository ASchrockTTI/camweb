import omsModalTemplate from './omsModal.html';

const omsModal =  {
		template: omsModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			utilService,
			userProfile,
			userInfoService,
			omsService,
			$filter,
			$state,
			$stateParams,
			$scope) {
			
			var vm = this;
			
			vm.uniqueName = true;
			
			vm.params = $stateParams;
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
                if(vm.data.action === 'add') {
                	vm.project = {
                		projectName: '',
                		dollarValue: 0,
                		headerStatus: {statusName: 'Open'},
                		projectActions: [],
                		lastUpdateDate: new Date(),
                	};
                	vm.activeProjects = vm.data.projects.active;
                	vm.projects = vm.data.projects.active.concat(vm.data.projects.archive);
                    vm.modalTitle = 'Add New Opportunity';
                    vm.confirmLabel = 'Create';
                } else {
                    
                }
                
                setTimeout(function(){
					var input = angular.element( document.querySelector( 'input' ) );
					input.focus();
				}, 500);
			}
			
			vm.verifyUnique = function() {
				var matchedProjects = $filter('filter')(vm.projects, function(project) {
					return project.projectName.toLowerCase() == vm.project.projectName.toLowerCase();
				}, true);
				
				vm.projectForm.projectName.$setValidity("unique", matchedProjects.length === 0);
			}
			
//			$scope.projectFilter = function (actual, expected) {
//				var lowerStr = actual.toString().toLowerCase();
//				return lowerStr.indexOf(expected.toString().toLowerCase()) === 0;
//			}
			
			vm.openExistingProject = function(project) {
				
                var params = {
                    planId: vm.params.planId,
                    type: (project.headerStatus.statusId === 1)? 'active' : 'archive',
                    projectId: project.projectId
                };
				
				$state.go('oms', params);
				vm.modalInstance.dismiss();
			}
			
			
			
			vm.save = function() {
				if(vm.debounce) return;
				
				vm.debounce = true;
				
				omsService.addProject(vm.params.planId, vm.project)
				.then(function(response) {
					vm.debounce = false;
					
					vm.activeProjects.push(response);
					
					var params = {
	                    planId: vm.params.planId,
	                    type: 'active',
	                    projectId: response.projectId
	                };
						
					$state.go('oms', params);
					
					vm.modalInstance.dismiss();
				})
				.catch(function(error) {
					vm.debounce = false;
					vm.modalInstance.dismiss();
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss();
			}
			
		}
	}
export default omsModal;