import salesIQSummaryTemplate from './salesIQSummary.html';

const salesIQSummary =  {
		template: salesIQSummaryTemplate,
		controller: function(
			salesIQService,
			userProfile,
			userInfoService,
			utilService,
			planService,
			$rootScope,
			$state,
			$stateParams,
			$scope
		) {

			var vm = this;
			vm.criteria = {
				branches: [],
				email: undefined,
				growth: true,
				recovery: true,
				email: undefined,
				hasAged: true,
				hasPursuing: true,
				hasOutstanding: true,
				hasDismissed: true,
			};
			vm.opportunities = undefined;
			
			vm.$onInit = function() {
				
				if (sessionStorage.getItem('salesIQSort')) {
					var sort = JSON.parse(sessionStorage.getItem('salesIQSort'));
					vm.sort.type = sort.type;
					vm.sort.reverse = sort.reverse;
				}
				
				if (sessionStorage.getItem('salesIQCriteria')) {
					vm.criteria = JSON.parse(sessionStorage.getItem('salesIQCriteria'));
				} else vm.initialize = true;
				
				if (sessionStorage.getItem('salesIQTextFilter')) vm.filterText = JSON.parse(sessionStorage.getItem('salesIQTextFilter'));
				
				vm.userProfile = userProfile.getUserProfile();
				
				if(vm.userProfile) initialize();
			}
			
			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {	
				vm.userProfile = userProfile.getUserProfile();
				initialize();
			})
			
			function initialize() {
				vm.role = userProfile.getUserType();
				switch(vm.role.toUpperCase()) {
					case 'ADMIN':
						planService.getParentBranchList().then(
							function(response) {
								vm.branchList = response;
								getOpportunities();
								vm.showSearch = true;
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						if(vm.initialize) {
							vm.criteria.showMine = true;
						}
						break;
					case 'EXECUTIVE':
						planService.getEntityParentBranches(userProfile.getEntity()).then(
							function(response) {
								vm.branchList = [];
								
								for(var i = 0; i < response.length; i++) {
									if(vm.branchList.includes(response[i].fullParentBranchName)) continue;
									vm.branchList.push(response[i].fullParentBranchName);
								}
								getOpportunities();
								vm.showSearch = true;
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						vm.showList = false;
						vm.showOwnerSelection = true;
						break;
					case 'SAM_GAM':
						userInfoService.getUserCorpIdList(userProfile.getSAMAccountName()).then(
								function(response) {
								vm.corpIdList = response;
								if(vm.initialize) {
									if(vm.corpIdList.length > 0) {
										vm.criteria.corpId = vm.corpIdList[0];
									}
									else {
										vm.criteria.showMine = true;
									}
								}
								vm.showSearch = true;
								getOpportunities();
						});
						break;
					case 'FSR':
					case 'ISR':
						vm.showOwnerSelection = false;
						vm.showSearch = false;
						if(vm.initialize) {
							vm.criteria.email = userProfile.getMail();
							vm.criteria.ownerFilter = userProfile.getFullName();
						}
						getOpportunities();
						break;
					case 'RVP':
					case 'GM':
					case 'FSM':
					case 'RBOM':
					case 'ISM':
					default:
						vm.branchList = userProfile.getBranchList();
						if(vm.initialize) {
							vm.criteria.branches = [userProfile.getBranch()];
						}
						vm.showSearch = true;
						getOpportunities();
						break;
				}
			}
			
			
			
			function getOpportunities() {
				if(vm.loadingInsights) return;
				vm.initialize = false;
				
				vm.loadingOpportunities = true;
				vm.opportunities = undefined;
				
				vm.saveCriteria();
				
				vm.role = userProfile.getUserType().toUpperCase();
				
				
				salesIQService.getCustomerInsightSummariesByCriteria(vm.criteria).then(
					function(response) {
						vm.opportunities = response;
					})
					.catch(function(error) {
						vm.opportunities = [];
					})
				vm.loadingOpportunities = false;
			}
			
			vm.filterOwner = function(opportunity) {
				if(vm.criteria.email) return;
				if(!opportunity || vm.criteria.emailFilter === opportunity.ownerEmail) {
					vm.criteria.emailFilter = undefined;
					vm.criteria.ownerFilter = undefined;
				}
				else {
					vm.criteria.emailFilter = opportunity.ownerEmail;
					vm.criteria.ownerFilter = opportunity.ownerName;
				}
				vm.saveCriteria();
			}
			
			vm.filterType = function(type) {
				if(type === 'Cross Sell') {
					vm.criteria.recovery = !vm.criteria.recovery;
		    	} else {
		    		vm.criteria.growth = !vm.criteria.growth;
		    	}
			}
			
			$scope.typeFilter = function() {
				  return function( opportunity ) {
				    if(opportunity.type === 'Cross Sell') {
				    	return vm.criteria.growth;
			    	} else return vm.criteria.recovery;
			    }
			}
			
			$scope.statusFilter = function() {
				  return function( opportunity ) {
					  if(vm.criteria.hasAged && opportunity.aged > 0) return true;
					  else if(vm.criteria.hasPursuing && opportunity.pursuing > 0) return true;
					  else if(vm.criteria.hasOutstanding && opportunity.outstanding > 0) return true;
					  else if(vm.criteria.hasDismissed && opportunity.dismissed > 0) return true;
					  else return false;
			    }
			}
			

			vm.$onDestroy = function() {
				userProfileListener();
			}
			
			vm.saveCriteria = function() {
				sessionStorage.setItem('salesIQCriteria', JSON.stringify(vm.criteria));
			}
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('salesIQTextFilter', JSON.stringify(vm.filterText));
			}
			
			vm.openOpportunity = function(opportunity) {
				$state.go('salesIQ', {planId: opportunity.planId});
			}
			
			
			$scope.$watch('filteredOpportunities', function() {
		        calculateTotals($scope.filteredOpportunities);
		    });
			 
			function calculateTotals(opportunities) {
				var pursuingCount = 0;
				var pursuingValue = 0;
				var agedCount = 0;
				var outstandingCount = 0;
				var outstandingValue = 0;
				var wonCount = 0;
				var lostCount = 0;
				var dismissedCount = 0;
				
				if(opportunities) {
					for(var i = 0; i < opportunities.length; i ++) {
						pursuingCount += opportunities[i].pursuing;
						pursuingValue += opportunities[i].valuePursuing;
						agedCount += opportunities[i].aged;
						outstandingCount += opportunities[i].outstanding;
						outstandingValue += opportunities[i].valueOutstanding;
						wonCount += opportunities[i].won;
						lostCount += opportunities[i].lost;
						dismissedCount += opportunities[i].dismissed;
					}
				}
				
				vm.pursuingCount = pursuingCount;
				vm.pursuingValue = pursuingValue;
				vm.agedCount = agedCount;
				vm.outstandingCount = outstandingCount;
				vm.outstandingValue = outstandingValue;
				vm.wonCount = wonCount;
				vm.lostCount = lostCount;
				vm.dismissedCount = dismissedCount;
				
			}
			
			vm.sort = {
	          type: "lastModified",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	            
	            sessionStorage.setItem('salesIQSort', JSON.stringify({type: sortType, reverse: vm.sort.reverse}));
	          }
	        };
			
			vm.$onDestroy = function() {
				userProfileListener();
			}
	
	
			vm.openSearchModal = function() {
				utilService
					.uibModal()
					.open({
						component: "salesIQSearchModal",
						size: "md",
						resolve: {
							modalData: function () {
								return {
									criteria: vm.criteria,
									branchList: vm.branchList,
									corpIdList: vm.corpIdList
								}
							}
						}
					})
					.result.then(
						function(close) {
							getOpportunities();
						},
						function(dismiss) {
						});
			};
		}
	
	}
export default salesIQSummary;