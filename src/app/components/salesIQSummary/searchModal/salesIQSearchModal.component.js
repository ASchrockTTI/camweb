import salesIQSearchModalTemplate from './salesIQSearchModal.html';

const salesIQSearchModal =  {
		template: salesIQSearchModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
				$filter,
				userProfile,
				salesIQService,
				) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.originalCriteria = vm.resolve.modalData.criteria;
				vm.branchList = vm.resolve.modalData.branchList;
				
				copyOriginalCriteria();
				
				if(vm.branchList) initializeBranchList();
				vm.corpIdList = vm.resolve.modalData.corpIdList;
			}
			
			function initializeBranchList() {
				vm.formattedBranchList = [];

				let formattedBranch = {};
				angular.forEach(vm.branchList, function(branch, index) {
					formattedBranch.display = branch;
					formattedBranch.Id      = index;
					vm.formattedBranchList.push(formattedBranch);
					formattedBranch = {};
				});
				
				//if currently filtered by a branch list; else select default branch location
				if(vm.criteria.branches) {	
					vm.selectedBranchList = findBranchesInBranchList(vm.formattedBranchList, vm.originalCriteria.branches);
				} else {
					vm.selectedBranchList = findBranchesInBranchList(vm.formattedBranchList, [userProfile.getBranchLocation()]);
				}
				vm.getOpportunityOwnerList();
			}
			
			function findBranchesInBranchList(branchList, branchesToFind) {
				var foundBranches = [];
				var foundBranch = {};

				angular.forEach(branchList, function(branch, index) {
					if(branchesToFind.indexOf(branch.display) > -1) {
						foundBranches.push(branch);
					}
				});

				return foundBranches;
			}
			
			vm.branchFilter = function($query) {
				var filtered = [];
				angular.forEach(vm.formattedBranchList, function(branch) {
					if(branch.display.toLowerCase().indexOf($query.toLowerCase()) === 0 || !$query) filtered.push(branch);
				})
				return filtered;
			}
			
			vm.getOpportunityOwnerList = function() {
				vm.branchSearchInput = '';
				
				vm.criteria.branches = [];
				if(!vm.selectedBranchList || vm.selectedBranchList.length === 0) {
					vm.ownerList = [];
					return;
				}
				
				angular.forEach(vm.selectedBranchList, function(formattedBranch) {
					vm.criteria.branches.push(formattedBranch.display);
				})
				
				salesIQService.getOpportunityOwnersByBranches(vm.criteria.branches).then(
					function(response) {
						vm.ownerList = response;
						
					})
					.catch(function(error) {
						vm.ownerList = [];
					})
			}
			
			function copyOriginalCriteria() {
				var originalCriteriaString = JSON.stringify(vm.originalCriteria);
				
				vm.criteria = JSON.parse(originalCriteriaString);
				
				vm.branchInput = vm.criteria.branch;
				initializeBranchList();
				
				if(vm.criteria.corpId) vm.selectCorpId(vm.criteria.corpId);
				else vm.corpIdInput = '';
				
				if(vm.criteria.email) vm.ownerNameInput = vm.criteria.ownerFilter;
				else vm.ownerNameInput = '';
			}
			
			
			vm.selectOwner = function(owner) {
				
				if(!owner) {
					vm.criteria.email = undefined;
					vm.criteria.ownerFilter =  undefined;
					vm.ownerNameInput = '';
					return;
				}
				
				var titleCaseName = $filter('titlecase')(owner.ownerName);
				
				vm.criteria.emailFilter = undefined;
				vm.criteria.email = owner.ownerEmail;
				vm.criteria.ownerFilter =  titleCaseName;
				vm.ownerNameInput = titleCaseName;
			}
			
			vm.selectCorpId = function(corpId) {
				vm.inputCorpId = corpId.corpId + ' | ' + corpId.customerName;
				vm.criteria.corpId = corpId;
			}
			
			
			vm.search = function() {
				if(vm.criteria.branches) vm.criteria.branches.sort();
				
				angular.copy(vm.criteria, vm.originalCriteria);
				
				vm.modalInstance.close();
			}
			
			vm.reset = function() {
				copyOriginalCriteria();
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
		}
	}
export default salesIQSearchModal;