var totalFilter = function () {
  return function (plans, prop, reverse) {
    if (!plans) {
      return [];
    }

    var plansToSort = [];
    var emptyValues = [];

    if (prop == "totalRunRate") {
      for (var indx = 0; indx < plans.length; indx++) {
        if (plans[indx].totalRunRate && plans[indx].totalRunRate > 0.0) {
          plansToSort.push(plans[indx]);
        } else {
          emptyValues.push(plans[indx]);
        }
      }

      plansToSort.sort(function (plan1, plan2) {
        if (reverse) {
          return plan2.totalRunRate - plan1.totalRunRate;
        }
        return plan1.totalRunRate - plan2.totalRunRate;
      });
    } else if (prop == "totalTarget") {
      for (var indx = 0; indx < plans.length; indx++) {
        if (plans[indx].totalTarget && plans[indx].totalTarget > 0.0) {
          plansToSort.push(plans[indx]);
        } else {
          emptyValues.push(plans[indx]);
        }
      }

      plansToSort.sort(function (plan1, plan2) {
        if (reverse) {
          return plan2.totalTarget - plan1.totalTarget;
        }
        return plan1.totalTarget - plan2.totalTarget;
      });
    } else if (prop == "percentToGoal") {
      for (var indx = 0; indx < plans.length; indx++) {
        if (
          !plans[indx].percentToGoal ||
          plans[indx].percentToGoal == undefined ||
          plans[indx].percentToGoal.indexOf("%") == -1
        ) {
          emptyValues.push(plans[indx]);
        } else {
          plansToSort.push(plans[indx]);
        }
      }

      plansToSort.sort(function (plan1, plan2) {
        var plan1PercentToGoal = parseInt(plan1.percentToGoal.replace("%", ""));
        var plan2PercentToGoal = parseInt(plan2.percentToGoal.replace("%", ""));

        if (reverse) {
          return plan2PercentToGoal - plan1PercentToGoal;
        }
        return plan1PercentToGoal - plan2PercentToGoal;
      });
    } else if (prop == "customerName") {
      for (var indx = 0; indx < plans.length; indx++) {
        if (plans[indx].customerName) {
          plansToSort.push(plans[indx]);
        }
      }

      plansToSort.sort(function (plan1, plan2) {
        var plan1CustomerName = plan1.customerName.toLowerCase();
        var plan2CustomerName = plan2.customerName.toLowerCase();

        if (reverse) {
          return plan2CustomerName.localeCompare(plan1CustomerName);
        }
        return plan1CustomerName.localeCompare(plan2CustomerName);
      });
    }

    for (var indx = 0; indx < emptyValues.length; indx++) {
      plansToSort.push(emptyValues[indx]);
    }

    return plansToSort;
  };
};

import mainPageTemplate from "./management.html";

const mainPageComponent = {
  template: mainPageTemplate,
  bindings: {
    textFilter: "=",
  },
  controller: function (
    $filter,
    utilService,
    planService,
    userProfile,
    $rootScope,
    $scope,
    $state,
    $stateParams,
    newsfeedService,
    session,
    $document
  ) {
    var vm = this;
    var comparator = "totalRunRate";
    var reverse = true;
    var DISPLAY_LIMIT = 25;

    vm.$onInit = function () {
      vm.params = $stateParams;

      vm.isEmptyPlans = false;
      vm.isEmptyNonTTIPlans = false;
      vm.showLoading = false;
      vm.allPlan = "true";
      vm.myPlan = "false";
      vm.targetTotal = 0;
      vm.runRateTotal = 0;
      vm.totalPercentToPlans = "";
      vm.selectedCustomerListName = "customers";
      vm.filteredMessage = "";
      vm.sortType = comparator; // set the default sort type
      vm.sortReverse = reverse; // set the default sort order
      vm.clientError = utilService.getClientErrorObject();
      vm.filterMap = [];
      vm.isInternetExplorer = $document[0].documentMode ? true : false;

      vm.isrPopover = [
        {
          title: "hello",
          templateUrl: "multipleIsrPlanList.html",
        },
        {
          title: "hello",
          templateUrl: "multipleIsrChildList.html",
        },
      ];

      init();
    };

    vm.$onDestroy = function () {
      refreshMainPageListener();
      refreshMainPageBlogListener();
    };

    vm.pendingCustomerModal = function () {
      utilService
        .uibModal()
        .open({
          component: "submitNewCustomerModal",
          size: "md",
          animation: true,
          keyboard: true,
          resolve: {},
        })
        .result.then(
          function (result) {
            if (result.new) {
              refreshNonTTIPlans();
              init();
            }
          },
          function (dismiss) {}
        );
    };

    vm.clickPreferences = function () {
      utilService
        .uibModal()
        .open({
          component: "preferencesModal",
          size: "md",
        })
        .result.then(
          function (result) {
            $rootScope.$emit("clearCache", undefined);
          },
          function (dismiss) {}
        );
    };

    vm.row = {
      data: undefined,
      parentList: undefined,

      parent: {
        data: undefined,
        index: undefined,
        select: function (plan, index, init) {
          if (plan.planId)
            $rootScope.$broadcast("selectPlan", {
              planId: plan.planId,
              customerName: plan.customerName,
            });

          if (init) {
            vm.row.parent.selectedRow = index;
            vm.row.child.selectedRow = undefined;
          }

          if (utilService.isNotEmptyArray(plan.branchPlanList)) {
            return;
          }

          getPlanInfo(index, plan, init, false);
        },
      },

      child: {
        data: undefined,
        index: undefined,
        select: function (parentPlan, index, init) {
          var childPlan = parentPlan.branchPlanList[index];

          if (childPlan.planId)
            $rootScope.$broadcast("selectPlan", {
              planId: childPlan.planId,
              customerName: childPlan.customerName,
            });

          if (init) {
            vm.row.child.selectedRow = index;
            vm.row.parent.selectedRow = undefined;
          }

          getPlanInfo(index, childPlan, init, true);

          utilService.updateSideBarOptions(
            childPlan,
            userProfile.getUserType()
          );
        },
      },

      isPlanBranchListVisible: function (plan) {
        var emptyBranchList = true;
        if (utilService.isNotEmptyArray(plan.branchPlanList)) {
          emptyBranchList = false;
        }
        return !emptyBranchList;
      },
    };

    vm.positionLabel = function (text) {
      if (!text) {
        return "";
      }

      switch (text) {
        case "Defend":
          return "label label-success";
        case "Expand":
          return "label label-warning";
        case "Review":
          return "label label-danger";
        default:
          return "";
      }
    };

    vm.getSelectedCustomerListName = function () {
      return vm.selectedCustomerListName;
    };

    vm.selectCustomerList = function (customerType) {
      if (customerType == "customers") {
        vm.selectedCustomerListName = "customers";
        vm.nonTTI = false;
        loadPlans();
        $state.go(".", { type: customerType });
      } else if (customerType == "leads") {
        vm.selectedCustomerListName = "leads";
        vm.nonTTI = true;
        loadNonTTICustomers();
        $state.go(".", { type: customerType });
      } else {
        vm.selectedCustomerListName = "customers";
        vm.nonTTI = false;
        loadPlans();
        $state.go(".", { type: customerType });
      }
      console.log("selected Type: ", customerType);
    };

    vm.getPlansBySelectedCustomerList = function () {
      if (vm.selectedCustomerListName == "customers") {
        return vm.plans;
      }
      if (vm.selectedCustomerListName == "leads") {
        return vm.nonTTIPlans;
      }

      return vm.plans;
    };

    vm.order = function (sortType, sortReverse) {
      let plans = vm.getPlansBySelectedCustomerList();

      resetTotals();

      vm.sortType = sortType;
      vm.sortReverse = sortReverse;

      sessionStorage.setItem(session.MAIN_PAGE_SORT_TYPE, sortType);
      sessionStorage.setItem(session.MAIN_PAGE_SORT_ORDER, sortReverse);

      plans = sortPlans(plans);

      setPlans(plans, 0);

      if (vm.nonTTI) loadNonTTICustomers();
      else loadPlans();

      vm.entities = {};
    };

    var refreshMainPageBlogListener = $rootScope.$on(
      "refreshMainPageBlog",
      function (event, container) {
        updateSelectedPlanBlog(container);
      }
    );

    var refreshMainPageListener = $rootScope.$on("refreshMainPage", function (
      event,
      container
    ) {
      init();
    });

    function updateSelectedPlanBlog(container) {
      var plans = planService.getPlans();
      var plan = planService.getPlan();

      vm.blogEntryDate = utilService.isoDateToShortDate();
      plan.blogEntryDate = utilService.isoDateToShortDate();

      vm.blog = container.plan.blogEntry.trim();
      plan.blogEntry = container.plan.blogEntry.trim();

      var index = planService.getPlanIndex();
      plans[index] = plan;
    }

    vm.noPlansToShow = function () {
      if (vm.selectedCustomerListName == "leads") {
        return vm.isEmptyNonTTIPlans;
      }

      return vm.isEmptyPlans;
    };

    vm.setTotals = function () {
      for (var i = 0; i <= vm.plans.length - 1; i++) {
        var plan = vm.plans[i];

        if (plan.totalTarget) {
          vm.targetTotal += plan.totalTarget;
        }

        if (plan.totalRunRate) {
          vm.runRateTotal += plan.totalRunRate;
        }

        var totalPercentToPlans = vm.runRateTotal / vm.targetTotal;
        totalPercentToPlans = $filter("number")(totalPercentToPlans, 2) * 100;
        totalPercentToPlans = Math.round(totalPercentToPlans);
        vm.totalPercentToPlans =
          totalPercentToPlans > 0 ? "" + totalPercentToPlans + "%" : "";
        vm.totalPercentToPlansLabel = utilService.percentToPlanLabel(
          totalPercentToPlans
        );

        if (plan.insideSalesPerson != null) {
          var isrList = plan.insideSalesPerson
            .substring(0, plan.insideSalesPerson.length - 1)
            .trim()
            .split(",");
          plan.isrList = isrList;
        }

        for (var j = 0; j <= plan.branchPlanList.length - 1; j++) {
          if (plan.branchPlanList[j].insideSalesPerson != null) {
            var isrList = plan.branchPlanList[j].insideSalesPerson
              .substring(0, plan.branchPlanList[j].insideSalesPerson.length - 1)
              .trim()
              .split(",");
            plan.branchPlanList[j].isrList = isrList;
          }
        }
      }
    };

    function resetTotals() {
      vm.targetTotal = 0;
      vm.runRateTotal = 0;
      vm.totalPercentToPlans = "";
    }

    // set plans/selected plan into plan service
    function setPlans(plans) {
      if (utilService.isNotEmptyArray(plans)) {
        planService.setPlans(plans);
        vm.setTotals();
      }
    }

    function setNonTTIPlans(plans) {
      if (utilService.isNotEmptyArray(plans)) {
        planService.setNonTTIPlans(plans);
        //vm.setTotals();
      }
    }

    // set selected plan into plan service
    function setPlan(index) {
      var retBool = false;
      var plans = planService.getPlans();
      if (utilService.isNotEmptyArray(plans) && index <= plans.length) {
        planService.setPlanIndex(index);
        planService.setPlan(plans[index]);
        retBool = true;
      }
      return retBool;
    }

    function getPlanInfo(index, plan, init, isChild) {
      vm.activity = {
        planId: plan.planId,
      };

      planService
        .lookupPlan(plan.planId)
        .then(function (response) {
          planService.setPlan(response);
          planService.setPlanIndex(index);

          if (!isChild) {
            vm.row.parent.index = index;
            vm.row.parent.data = response;

            vm.blog =
              vm.row.parent.selectedRow >= 0 ? planService.getBlog() : "";
            vm.accountManager =
              vm.row.parent.selectedRow >= 0
                ? planService.getAccountManager()
                : "";
            vm.blogEntryDate =
              vm.row.parent.selectedRow >= 0
                ? utilService.isoDateToShortDate(
                    planService.getBlogEntryDate(),
                    true
                  )
                : "";
          } else {
            vm.row.child.index = index;
            vm.row.child.data = response;

            vm.blog =
              vm.row.child.selectedRow >= 0 ? planService.getBlog() : "";
            vm.accountManager =
              vm.row.child.selectedRow >= 0
                ? planService.getAccountManager()
                : "";
            vm.blogEntryDate =
              vm.row.child.selectedRow >= 0
                ? utilService.isoDateToShortDate(
                    planService.getBlogEntryDate(),
                    true
                  )
                : "";
          }

          utilService.updateSideBarOptions(response, userProfile.getUserType());
          //TODO $rootScope.$emit to region component
          $rootScope.$emit("region-plan-lookup", {
            plan: response,
            entity: plan.planType.planType,
          });

          getActivities(plan.planId);
        })
        .catch(function (error) {
          vm.clientError.message = error;
        });
    }

    function getActivities(planId) {
      newsfeedService
        .get(planId)
        .then(function (response) {
          vm.comments = newsfeedService.getNewsFeed();
        })
        .catch(function (error) {
          vm.clientError.message = error;
        });
    }

    var userProfileListener = $scope.$on("userProfileAvailable", function () {
      if (
        userProfile &&
        userProfile.getUserProfile() &&
        userProfile.getUserProfile().helpContactName
      ) {
        vm.helpContactName = userProfile.getUserProfile().helpContactName;

        var helpContactNumber = userProfile.getUserProfile().helpContactNumber;
        if (helpContactNumber && helpContactNumber.length == 4) {
          helpContactNumber = "Ext. " + helpContactNumber;
        }

        vm.helpContactNumber = helpContactNumber;
        vm.helpContactEmail = userProfile.getUserProfile().helpContactEmail;
      }

      if (
        vm.plans == undefined ||
        vm.nonTTIPlans == undefined ||
        vm.plans == [] ||
        vm.nonTTIPlans == []
      ) {
        vm.plans = [];
        vm.nonTTIPlans = [];
        loadNonTTICustomers();
        loadPlans();
      }
    });

    function init() {
      // Initializes the data retrieval
      // Placing the call to load the grid in an interval. The reason is because
      // userProfile may not be populated at the time the view is loading. This will
      // prevent passing in an undefined variable to the query.

      if (utilService.isEmpty(userProfile.getSelectedUserTypeName())) {
        utilService.getLogger().info("Waiting on userProfile to populate...");
      } else {
        vm.plans = [];
        vm.nonTTIPlans = [];
        loadNonTTICustomers();
        loadPlans();
        if (vm.params.type === "leads") {
          vm.selectCustomerList("leads");
          vm.activeTabIndex = 1;
        }
      }
    }

    function sortPlans(temp) {
      var thePlans = [];
      angular.copy(temp, thePlans);

      if (sessionStorage.getItem(session.MAIN_PAGE_SORT_ORDER)) {
        vm.sortType = sessionStorage.getItem(session.MAIN_PAGE_SORT_TYPE);
        vm.sortReverse =
          sessionStorage.getItem(session.MAIN_PAGE_SORT_ORDER) === "true";
      }

      switch (vm.sortType) {
        case "totalRunRate":
        case "totalTarget":
        case "percentToGoal":
        case "customerName":
          thePlans = $filter("totalFilter")(
            thePlans,
            vm.sortType,
            vm.sortReverse
          );
          break;
        default:
          var comparator = "[" + vm.sortType + ", planId]";
          thePlans = utilService.orderBy(thePlans, comparator, vm.sortReverse);
      }

      return thePlans;
    }

    vm.setIcon = function (document, size) {
      var size = size ? size : "";

      switch (document.documentExt.toLowerCase()) {
        case "pdf":
          return "far fa-file-pdf " + size;
        case "xlsx":
        case "xls":
        case "csv":
          return "far fa-file-excel " + size;
        case "docx":
        case "doc":
          return "far fa-file-word " + size;
        case "pptx":
        case "ppt":
          return "far fa-file-powerpoint " + size;
        case "png":
        case "jpeg":
        case "jpg":
        case "bmp":
        case "gif":
        case "tiff":
          return "far fa-file-image " + size;
        case "zip":
          return "far fa-file-archive " + size;
        case "msg":
          return "far fa-envelope " + size;
        default:
          return "far fa-file " + size;
      }
    };

    vm.saveFilterText = function () {
      sessionStorage.setItem("planTextFilter", JSON.stringify(vm.textFilter));
    };

    function refreshNonTTIPlans() {
      sessionStorage.removeItem("sessionPlansNonTTI");
    }

    function refreshPlans() {
      vm.plans = [];
      var allPlansBool = false;
      var userName = userProfile.getSAMAccountName();
      var type = userProfile.getUserType();

      resetTotals();

      if (utilService.isEmpty(allPlansBool)) {
        allPlansBool = vm.myPlan;
      }

      vm.showLoading = true;
      vm.isEmptyPlans = false;

      vm.filterMap = undefined;

      planService
        .retrievePlans(userName, type, allPlansBool)
        .then(function (response) {
          vm.filterMap = response.filterMap;

          sessionStorage.setItem("filterMap", angular.toJson(vm.filterMap));
          utilService.getLogger().info("CEM: end filter map storage.");
          vm.showLoading = false;

          if (response.plans) {
            utilService.getLogger().info(response.plans);

            var tempResponse = response.plans;
            tempResponse = sortPlans(tempResponse);

            vm.plans = tempResponse;
            setPlans(tempResponse);
            try {
              sessionStorage.setItem("sessionPlans", JSON.stringify(vm.plans));
            } catch (error) {
              utilService
                .getLogger()
                .warn("Unable to store list of plans in session: " + error);
            }
            utilService.getLogger().info("CEM: end plan storage.");
            vm.filtered = vm.plans;

            var count = 0;
            for (var indx = 0; indx < tempResponse.length; indx++) {
              if (
                tempResponse[indx].branchPlanList &&
                tempResponse[indx].branchPlanList.length > 0
              ) {
                count += tempResponse[indx].branchPlanList.length;
              } else {
                count++;
              }
            }

            vm.totalItems = tempResponse.length;
            vm.totalLength = count; //tempResponse.length;
            vm.scrolling.displayLimit = DISPLAY_LIMIT;
            vm.scrolling.checkScrollLimit();
            utilService.getLogger().info("CEM: end table render.");
          } else {
            vm.isEmptyPlans = true;
          }
        })
        .catch(function (error) {
          vm.clientError.message = error;
          vm.showLoading = false;
        });
    }

    function loadNonTTICustomers(allPlansBool) {
      vm.scrolling.resetScroll();
      var userName = userProfile.getSAMAccountName();
      var type = userProfile.getUserType();

      resetTotals();

      if (utilService.isEmpty(allPlansBool)) {
        allPlansBool = vm.myPlan;
      }

      vm.showLoading = true;
      vm.isEmptyNonTTIPlans = false;

      vm.filterMap = "";

      if (sessionStorage.getItem("sessionPlansNonTTI")) {
        vm.nonTTIPlans = JSON.parse(
          sessionStorage.getItem("sessionPlansNonTTI")
        );
        vm.showLoading = false;
        utilService.getLogger().info(vm.nonTTIPlans);
        if (vm.nonTTIPlans.length > 0) {
          setNonTTIPlans(vm.nonTTIPlans);
          vm.nonTTIPlans = sortPlans(vm.nonTTIPlans);
          vm.filtered = vm.nonTTIPlans;
          vm.totalItems = vm.nonTTIPlans.length;
          vm.totalLength = vm.nonTTIPlans.length;
          vm.scrolling.displayLimit = DISPLAY_LIMIT;
          vm.scrolling.checkScrollLimit();

          if (sessionStorage.getItem("totalNonTTICustomerCount")) {
            vm.totalNonTTICustomerCount = parseInt(
              JSON.parse(sessionStorage.getItem("totalNonTTICustomerCount"))
            );
          }
        } else {
          vm.isEmptyNonTTIPlans = true;
        }
      } else {
        planService
          .retrieveNonTTICustomers(userName, type, allPlansBool)
          .then(function (response) {
            utilService.getLogger().info("CEM: response completed.");

            sessionStorage.setItem("filterMap", angular.toJson(vm.filterMap));
            utilService.getLogger().info("CEM: end filter map storage.");
            vm.showLoading = false;

            if (response.plans) {
              utilService.getLogger().info(response.plans);

              var tempResponse = response.plans;
              tempResponse = sortPlans(tempResponse);

              vm.nonTTIPlans = tempResponse;
              setNonTTIPlans(tempResponse);
              try {
                sessionStorage.setItem(
                  "sessionPlansNonTTI",
                  JSON.stringify(vm.nonTTIPlans)
                );
              } catch (error) {
                utilService
                  .getLogger()
                  .warn("Unable to store list of plans in session: " + error);
              }
              utilService.getLogger().info("CEM: end plan storage.");
              vm.filtered = vm.nonTTIPlans;
              vm.totalItems = tempResponse.length;
              vm.totalLength = tempResponse.length;
              vm.scrolling.displayLimit = DISPLAY_LIMIT;
              vm.scrolling.checkScrollLimit();
              utilService.getLogger().info("CEM: end table render.");
            } else {
              vm.isEmptyNonTTIPlans = true;
            }
          })
          .catch(function (error) {
            vm.clientError.message = error;
            vm.showLoading = false;
          });
      }
    }

    function loadPlans(allPlansBool) {
      vm.scrolling.resetScroll();
      var userName = userProfile.getSAMAccountName();
      var type = userProfile.getUserType();

      resetTotals();

      if (utilService.isEmpty(allPlansBool)) {
        allPlansBool = vm.myPlan;
      }

      vm.showLoading = true;
      vm.isEmptyPlans = false;

      vm.filterMap = undefined;

      if (sessionStorage.getItem("sessionPlans")) {
        vm.plans = JSON.parse(sessionStorage.getItem("sessionPlans"));
        vm.showLoading = false;
        utilService.getLogger().info(vm.plans);
        if (vm.plans.length > 0) {
          setPlans(vm.plans);
          vm.plans = sortPlans(vm.plans);
          vm.filtered = vm.plans;
          vm.totalItems = vm.plans.length;

          var count = 0;
          for (var indx = 0; indx < vm.plans.length; indx++) {
            var plan = vm.plans[indx];
            if (plan.branchPlanList && plan.branchPlanList.length > 0) {
              count += plan.branchPlanList.length;
            } else {
              count++;
            }
          }

          vm.totalLength = count; //vm.plans.length;
          vm.scrolling.displayLimit = DISPLAY_LIMIT;
          vm.scrolling.checkScrollLimit();

          if (sessionStorage.getItem("filterMap")) {
            vm.filterMap = JSON.parse(sessionStorage.getItem("filterMap"));
          }
        } else {
          vm.isEmptyPlans = true;
        }
      } else {
        planService
          .retrievePlans(userName, type, allPlansBool)
          .then(function (response) {
            utilService.getLogger().info("CEM: start filter map storage.");

            vm.filterMap = response.filterMap;

            sessionStorage.setItem("filterMap", angular.toJson(vm.filterMap));
            utilService.getLogger().info("CEM: end filter map storage.");
            vm.showLoading = false;

            if (response.plans) {
              utilService.getLogger().info(response.plans);

              var tempResponse = response.plans;
              tempResponse = sortPlans(tempResponse);

              vm.plans = tempResponse;
              setPlans(tempResponse);
              try {
                sessionStorage.setItem(
                  "sessionPlans",
                  JSON.stringify(vm.plans)
                );
              } catch (error) {
                utilService
                  .getLogger()
                  .warn("Unable to store list of plans in session: " + error);
              }
              utilService.getLogger().info("CEM: end plan storage.");
              vm.filtered = vm.plans;

              var count = 0;
              for (var indx = 0; indx < tempResponse.length; indx++) {
                if (
                  tempResponse[indx].branchPlanList &&
                  tempResponse[indx].branchPlanList.length > 0
                ) {
                  count += tempResponse[indx].branchPlanList.length;
                } else {
                  count++;
                }
              }

              vm.totalItems = tempResponse.length;
              vm.totalLength = count; //tempResponse.length;
              vm.scrolling.displayLimit = DISPLAY_LIMIT;
              vm.scrolling.checkScrollLimit();
              utilService.getLogger().info("CEM: end table render.");
            } else {
              vm.isEmptyPlans = true;
            }
          })
          .catch(function (error) {
            vm.clientError.message = error;
            vm.showLoading = false;
          });
      }
    }

    vm.scrolling = {
      disable: false,
      displayLimit: 0,
      loadMore: function () {
        let plans = vm.getPlansBySelectedCustomerList();
        console.log("loadMorePlans: ", vm.selectedCustomerListName);
        if (plans === undefined) {
          return;
        }

        vm.scrolling.displayLimit += 25;

        var filteredItems = plans;

        if (filteredItems.length <= vm.scrolling.displayLimit) {
          vm.scrolling.displayLimit = filteredItems.length;
          vm.scrolling.disable = true;
        } else {
          vm.scrolling.disable = false;
        }
      },

      checkScrollLimit: function () {
        let plans = vm.getPlansBySelectedCustomerList();
        if (plans.length <= vm.scrolling.displayLimit) {
          vm.scrolling.displayLimit = plans.length;
          vm.scrolling.disable = true;
        } else {
          vm.scrolling.disable = false;
        }
      },

      resetScroll: function () {
        //vm.scrolling.displayLimit = 0
        vm.scrolling.disable = false;
        vm.scrolling.loadMore();
      },
    };
  },
};
export {totalFilter, mainPageComponent};
