import managementHeaderTemplate from './managementHeader.html';

const managementHeader =  {
		template: managementHeaderTemplate,
		bindings: {
            textFilter: '=',
            searchPlans: '&',
		},
		
	controller: function(
	) {

		var vm = this;
		
		vm.$onInit = function() {
			if (sessionStorage.getItem('planTextFilter')) {
				vm.textFilter = JSON.parse(sessionStorage.getItem('planTextFilter'));
			}
		}
	
		vm.saveFilterText = function() {
			sessionStorage.setItem('planTextFilter', JSON.stringify(vm.textFilter));
		}
		
	}

	}
export default managementHeader;