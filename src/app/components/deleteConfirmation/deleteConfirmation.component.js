import deleteConfirmationTemplate from './deleteConfirmation.html';

const deleteConfirmation =  {
		template: deleteConfirmationTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function() {
			
			var modal = this;
			
			modal.$onInit = function() {
				
				modal.modalData = {};
				modal.modalData.headerText = 'Delete Confirmation';
				modal.modalData.bodyText = 'Changes have been made to this page.';
				modal.modalData.iconColor = 'text-warning';
				modal.modalData.okButtonColor = 'btn-default';
				
				if (modal.resolve.data) {
					
					if (modal.resolve.data.isConstruction) {
						modal.modalData.isConstruction = modal.resolve.data.isConstruction;
					}
					
					if (modal.resolve.data.headerText) {
						modal.modalData.headerText = modal.resolve.data.headerText;
					}
					
					if (modal.resolve.data.bodyText) {
						modal.modalData.bodyText = modal.resolve.data.bodyText;
					}
					
					if (modal.resolve.data.okButtonColor) {
						modal.modalData.iconColor = modal.resolve.data.okButtonColor.replace('btn','text');
						modal.modalData.okButtonColor = modal.resolve.data.okButtonColor;
					}
				}
			}
			
			modal.$onDestroy = function() {
				modal.modalData = {};
			}
			
			modal.buttons = {
				yes: function() {
					modal.modalInstance.close(true);
				},
				no: function() {
					modal.modalInstance.dismiss(false);
				}
			}
		}
	}
export default deleteConfirmation;