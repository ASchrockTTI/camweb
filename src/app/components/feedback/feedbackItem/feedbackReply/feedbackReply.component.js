import feedbackReplyTemplate from './feedbackReply.html';

const feedbackReply =  {
		template: feedbackReplyTemplate,
		bindings: {
			reply: '=',
			item: '<',
		},
		controller: function($window, feedbackService, userProfile, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				
			}
			
			vm.isCreator = function() {
				return utilService.equalsIgnoreCaseSpace(vm.reply.userName, userProfile.getSAMAccountName());
			}
			
			vm.editReply = function() {
				utilService.uibModal().open({
					component: 'replyModal',
					size: 'sm',
					resolve: {
						item: function() {
							return vm.item;
						},
						reply: function() {
							return vm.reply;
						}
					}
				}).result.then(
					function(result) {
						//OK Button
					},
					function(dismiss) {
						//Cancel button
					}					
				)
			}
			
			vm.deleteReply = function() {
				if (confirm('Delete this reply?')) {
					
					feedbackService.deleteReply(vm.item.feedbackId, vm.reply.feedbackReplyId)
					.then(function() {
						 $window.location.reload();
					})
					.catch(function(error) {
						
					})
				}
			}
			
		}
	}
export default feedbackReply;