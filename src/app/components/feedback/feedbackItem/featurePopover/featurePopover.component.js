import featurePopoverTemplate from './featurePopover.html';

const featurePopover = {
            template: featurePopoverTemplate,
            bindings: {
            	isOpen: '=',
            	item: '=',
            	features: '='
            },
            controller: function ($state, userProfile, feedbackService) {

                var vm = this;
                
                vm.selectFeature = function(feature) {
                	vm.item.feature = feature;
                	
                	feedbackService.updateFeedback(vm.item, vm.item.feedbackId);
                }
                

            }
        }
export default featurePopover;