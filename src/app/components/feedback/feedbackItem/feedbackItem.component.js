import feedbackItemTemplate from './feedbackItem.html';
import feedbackDetailsModalTemplate from './feedbackDetailsModal.html'

const feedbackItem =  {
		template: feedbackItemTemplate,
		bindings: {
			ngModel: '=',
			index: '<',
			features: '='
		},
		controller: function($window, feedbackService, userProfile, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.item = vm.ngModel;
				vm.voteDisabled = false;
				
				angular.forEach(vm.item.suggestionVoters, function(value, key) {
					if (utilService.equalsIgnoreCaseSpace(value.voterName , userProfile.getFullName())) {
						vm.item.voteDisabled = true;
						return;
					}
				})
			}
			
			vm.$onChanges = function(changes) {				
					
			}
			
			vm.isUsers = function() {
				return vm.item.userName === userProfile.getFullName();
			}
			
			vm.isAdmin = function() {
				
				if (!userProfile) return false;
				
				if (userProfile.isAdmin()) {
					return true;
				}
				
				return false;
			}
			
			vm.voteUp = function(index, suggestionId) {
				
				feedbackService.upVote(suggestionId)
				.then(function(response) {
					vm.item.voteCount += 1;					
					vm.item.voteDisabled = true;
					
					var voter = {
						voterName: userProfile.getFullName(),
						voteUp: true
					}
					
					vm.item.suggestionVoters.push(voter);
					
				})
				.catch(function(error) {
					
				})
			}
			
			vm.voteDown = function(index, suggestionId) {
				
				feedbackService.downVote(suggestionId)
				.then(function(response) {
					
					if (vm.item.voteCount > 0) {
						vm.item.voteCount -= 1;
					}
					
					vm.item.voteDisabled = true;
					
					var voter = {
						voterName: userProfile.getFullName(),
						voteUp: false
					}
					
					vm.item.suggestionVoters.push(voter);
				})
				.catch(function(error) {
					
				})
			}
			
			
			function isDownVote(vote) {
				return !vote.voteUp;
			}
			
			vm.getVoteScore = function() {
				var downVotes = vm.item.suggestionVoters.filter(isDownVote);
				return vm.item.suggestionVoters.length - (2 * downVotes.length);
			}
			
			
			vm.getHeaderClass = function() {
				if(vm.item.isBug) {
					if(vm.isUsers()) return 'bg-brightred';
					else return 'bg-danger'
				}else {
					if(vm.isUsers()) return 'bg-royalblue';
					else return 'bg-info';
				}
			}
			
			vm.getVoteScoreColor = function() {
				if(vm.item.isBug) {
					if(vm.getVoteScore() > 9) return 'red';
					else return 'darkred'
				}else {
					if(vm.getVoteScore() > 9) return 'limegreen';
					else return '';
				}
			}
			
			vm.openReplyModal = function() {
				utilService.uibModal().open({
					component: 'replyModal',
					size: 'sm',
					resolve: {
						item: function() {
							return vm.item;
						}
					}
				}).result.then(
					function(result) {
						//OK Button
					},
					function(dismiss) {
						//Cancel button
					}					
				)
			}
			
			vm.openFeedbackDetailsModal = function() {
				utilService.uibModal().open({
					component: 'feedbackDetailsModal',
					size: 'md',
					resolve: {
						suggestion: function() {
							return {
								info: vm.item
							}
						}
					}
				}).result.then(
					function(result) {
						//OK Button
					},
					function(dismiss) {
						//Cancel button
					}					
				)
			}
			
		}
	}
	const feedbackDetailsModal = {
		template: feedbackDetailsModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function($window, feedbackService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				
				vm.item = vm.resolve.suggestion.info;
				
				if(vm.item.isBug) {
					vm.title = 'Bug Report';
				} else {
					vm.title = 'Suggestion'
				}
				
				vm.votes = {
					up: 0,
					down: 0
				}
				
				angular.forEach(vm.item.suggestionVoters, function(value, key) {
					if (value.voteUp === true) {
						vm.votes.up += 1;
					}
					else {
						vm.votes.down += 1;
					}
					
				})
			}
			
			vm.markAsInProgress = function(suggestionId) {
				
				if (confirm('This suggestion will be marked as "In Progress".\n\nDo you wish to continue?')) {
				
					feedbackService.markAsInProgress(suggestionId)
					.then(function() {
						 $window.location.reload();
					})
					.catch(function(error) {
						
					})
				
				}
			}
			
			vm.markAsReleased = function(suggestionId) {
				
				if (confirm('This suggestion will be marked as "Released".\n\nDo you wish to continue?')) {
				
					feedbackService.markAsReleased(suggestionId)
					.then(function() {
						 $window.location.reload();
					})
					.catch(function(error) {
						
					})
				}
			}
			
			vm.discard = function(suggestionId) {
				
				if (confirm('This suggestion will be marked as "Discarded".\n\nDo you wish to continue?')) {
				
					feedbackService.discard(suggestionId)
					.then(function() {
						 $window.location.reload();
					})
					.catch(function(error) {
						
					})
				}
			}
			
			vm.$onDestroy = function() {
				
			}			
						
			vm.buttons = {
				cancel: function() {
					vm.modalInstance.close();
				}
			}
		}
	}
export {feedbackItem, feedbackDetailsModal};