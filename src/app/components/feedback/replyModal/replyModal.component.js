import replyModalTemplate from './replyModal.html';

const replyModal =  {
		template: replyModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function($window, $state, feedbackService) {
			
			var vm = this;
			
			
			vm.$onInit = function() {
				if(vm.resolve.reply !== undefined) {
					vm.reply = vm.resolve.reply;
					vm.editMode = true;
				} else {
					vm.reply = {
						replyText: ''
					}
				}
				vm.item = vm.resolve.item;
			}
			
			vm.saveReply = function() {
				feedbackService.reply(vm.item.feedbackId, vm.reply)
				.then(function() {
					$window.location.reload();
				})
				.catch(function(error) {
					
				})
			}
			
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
		}
	}
export default replyModal;