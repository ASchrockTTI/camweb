import feedbackTemplate from "./feedback.html";

const suggestions = {
  template: feedbackTemplate,
  controller: function (
    $window,
    feedbackService,
    utilService,
    userInfoService,
    $state,
    userProfile,
    $rootScope
  ) {
    var vm = this;
    vm.isBugPage = false;

    vm.$onInit = function () {
      vm.openIssues = [];
      vm.addressedIssues = [];

      if ($state.current.name === "suggestions") {
        vm.headerTitle = "HAVE A GREAT IDEA?";
        vm.headerMessage =
          "As a Customer Account Management user, do you have an idea on " +
          "how to improve the management of your plans? Create a suggestion" +
          "below and vote on suggestions created by your peers.";
        vm.feedbackHeader = "Suggestion";
      } else {
        vm.isBugPage = true;
        vm.headerTitle = "Found a Bug?";
        vm.headerMessage =
          "Please report any bugs you encounter while using CAM here.";
        vm.feedbackHeader = "Bug Report";
      }

      vm.newIssue = "";
      getFeatureList();
      getAllIssues();
    };

    function getFeatureList() {
      feedbackService
        .retrieveFeatureList()
        .then(function (response) {
          vm.features = response;
          vm.selectedFeature = vm.features[0];
        })
        .catch(function (error) {});
    }

    vm.selectFeature = function (feature) {
      vm.selectedFeature = feature;
    };

    function getAllIssues() {
      feedbackService
        .retrieveAllIssuesByType(vm.isBugPage)
        .then(function (response) {
          var openIssues = feedbackService.getOpenIssues();
          vm.openIssues = utilService.orderBy(openIssues, ["-voteCount"]);

          //vm.openIssues = feedbackService.getopenIssues();
          vm.addressedIssues = feedbackService.getInProgressIssues();
          Array.prototype.push.apply(
            vm.addressedIssues,
            feedbackService.getReleasedIssues()
          );
          sortCurrentUserSubmissionsToTop();
        })
        .catch(function (error) {});
    }

    function sortCurrentUserSubmissionsToTop() {
      var name = userProfile.getFullName();
      // user profile information may not be immediately available
      // upon refresh
      if (name !== "undefined undefined") {
        vm.openIssues.sort(function (a, b) {
          return (b.userName === name) - (a.userName === name);
        });

        vm.addressedIssues.sort(function (a, b) {
          return (b.userName === name) - (a.userName === name);
        });
        vm.addressedIssues = utilService.orderBy(vm.addressedIssues, [
          "released",
        ]);
      } else {
        setTimeout(sortCurrentUserSubmissionsToTop, 200);
      }
    }

    vm.filterRelated = function () {
      vm.featureFilter = vm.selectedFeature;
    };

    vm.form = {
      reset: function () {
        vm.feedbackForm.$setPristine();
        vm.newIssue = "";
      },

      submit: function () {
        var issueContainer = {
          issue: vm.newIssue,
          feature: vm.selectedFeature,
          isBug: vm.isBugPage,
        };

        userInfoService
          .checkUserInfo()
          .then(function () {
            feedbackService
              .addFeedback(issueContainer)
              .then(function () {
                $window.location.reload();
              })
              .catch(function (error) {});
          })
          .catch(function (error) {});
      },
    };
  },
};
export default suggestions;
