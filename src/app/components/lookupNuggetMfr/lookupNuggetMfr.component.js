import lookupNuggetMfrTemplate from "./lookupNuggetMfr.html";

const lookupNuggetMfr = {
  template: lookupNuggetMfrTemplate,
  bindings: {
    ngModel: "=",
    minLength: "<",
    onSelect: "&",
    lookupPlaceholder: "<",
    inputId: "<",
    tooltipPosition: "<",
    tooltipText: "<",
    hideInfo: "<",
  },
  controller: function (
    $attrs,
    $rootScope,
    nuggetsService,
    utilService,
    $filter,
    $stateParams
  ) {
    var vm = this;

    vm.$onInit = function () {
      vm.minLength = vm.minLength === undefined ? 2 : vm.minLength;
      vm.inputId = vm.inputId === undefined ? "mfrValueInput" : vm.inputId;
      if (!vm.lookupPlaceholder) {
        vm.lookupPlaceholder = "Lookup Mfr...";
      }
      loadManufacturers();
    };

    vm.$onDestroy = function () {
      lookupMfrClearTextListener();
    };

    function loadManufacturers() {
      nuggetsService
        .retrieveNuggetManufacturers()
        .then(function (response) {
          vm.manufacturers = response;
        })
        .catch(function (error) {
          vm.clientError.message = error;
        });
    }

    vm.findMfr = function (value) {
      if (!value || !vm.manufacturers) {
        return [];
      }

      var filtered = [];
      angular.forEach(vm.manufacturers, function (item) {
        if (
          item.fullManufacturerName
            .toUpperCase()
            .indexOf(value.toUpperCase()) >= 0
        )
          filtered.push(item);
      });

      return filtered;
    };

    var lookupMfrClearTextListener = $rootScope.$on(
      "lookupMfr-clear-text",
      function (event, value) {
        vm.ngModel = undefined;
      }
    );
  },
};
export default lookupNuggetMfr;
