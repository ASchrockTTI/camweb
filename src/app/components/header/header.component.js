import headerTemplate from "./header.html";

var headerComponent = {
  template: headerTemplate,
  bindings: {
    userName: "<",
  },
  controller: headerController,
};

function headerController(
  $scope,
  $rootScope,
  app,
  utilService,
  server,
  environment,
  mockSessionService,
  notificationService,
  preferencesService,
  userInfoService,
  userProfile,
  $interval
) {
  var vm = this;
  var userType = "User Type";

  vm.$onInit = function () {
    vm.mockSession = false;

    vm.appInfo = {
      title: app.NAME,
      userType: userType,
    };

    switch (utilService.location.host()) {
      case server.DEV:
        vm.appInfo.environment = environment.DEV;
        vm.appInfo.version = app.VERSION;
        break;
      case server.UAT:
        vm.appInfo.environment = environment.UAT;
        vm.appInfo.version = app.VERSION;
        break;
      case server.QA1:
      case server.QA2:
      case server.QA3:
      case server.QA4:
        vm.appInfo.environment = environment.QA;
        vm.appInfo.version = app.VERSION;
        break;
      case server.LOCAL_HOST:
        vm.appInfo.environment = environment.LOCAL_HOST;
        vm.appInfo.version = app.VERSION;
        break;
      default:
        vm.appInfo.environment = "";
        vm.appInfo.version = app.VERSION;
    }

    vm.userProfilePopover = {
      content: "Hello World",
      templateUrl: "myPopoverTemplate.html",
      title: "Title",
    };

    vm.notificationPopover = {
      templateUrl: "popoverNotifications.html",
    };

    vm.init(vm.userName);
  };

  vm.$onDestroy = function () {
    resetMockListener();
    initListener();
    clearCacheListener();
  };

  $scope.$on("notifications", function (event, data) {
    vm.notificationCount = notificationService.getNotificationCount();
  });

  var resetMockListener = $rootScope.$on("resetMock", function (event, value) {
    vm.resetMock();
  });

  vm.resetMock = function () {
    if (!mockSessionService.hasSession()) return;

    vm.mockSession = false;

    var originalUser = mockSessionService.getOriginalUser();

    sessionStorage.removeItem("selectedFsr");
    sessionStorage.removeItem("sessionPlans");
    sessionStorage.removeItem("sessionPlansNonTTI");
    sessionStorage.removeItem("filterMap");
    sessionStorage.removeItem("actionItemCriteria");
    sessionStorage.removeItem("omsProjects");
    sessionStorage.removeItem("omsProjectCriteria");
    sessionStorage.removeItem("nuggetCriteria");
    sessionStorage.removeItem("salesIQCriteria");
    sessionStorage.removeItem("callReportSearchCriteria");

    mockSessionService.clearSession();

    vm.init(originalUser.sAMAccountName, true);
  };

  vm.clickHeaderLink = function () {
    utilService.updateSideBarOptions(undefined);
    utilService.location.path("/#/");
  };

  vm.clickUSerProfile = function () {
    utilService.location.path("/#/");
  };

  var initListener = $rootScope.$on("init", function (
    event,
    ntlm,
    profileChanged
  ) {
    vm.init(ntlm, profileChanged);
  });

  vm.init = function (ntlm, profileChanged) {
    vm.mockSession = mockSessionService.hasSession();
    vm.trueMock = !mockSessionService.isMocking();

    var userName = "";
    var userProfileLoaded = false;

    if (vm.mockSession) {
      var mockSession = mockSessionService.getMockSession();
      ntlm = mockSession.userName;
    }

    for (var i = 0, n = ntlm.length; i < n; i++) {
      if (ntlm.charCodeAt(i) < 255) {
        userName += ntlm.charAt(i);
      }
    }

    userName = userName.trim();

    // retrieve user information
    userInfoService
      .retrieveUserInfo(userName)
      .then(function (response) {
        if (utilService.isNotEmpty(response)) {
          userProfile.setUserProfile(response);
          vm.name = userProfile.getFullName();
          vm.userProfile = userProfile;

          userProfileLoaded = true;
          $rootScope.$emit("refreshMainPage", undefined);

          if (profileChanged) {
            utilService.location.path("/#");
            $state.go("home", {}, { reload: true });
          }
        }
      })
      .catch(function (error) {
        vm.message = error;
      });

    var loading = $interval(function () {
      if (!userProfileLoaded) {
        utilService.getLogger().info("Waiting on userProfile to populate...");
      } else {
        vm.appInfo.userType = userProfile.getSelectedUserTypeName();
        notificationService.retrieveNotifications(
          userProfile.getSAMAccountName()
        );
        $rootScope.$broadcast("userProfileAvailable");
        preferencesService.checkLastVersion(userProfile.getSAMAccountName());
        $interval.cancel(loading);
      }
    }, 500);
  };

  function setHtmlPageTitleTag() {
    vm.appInfo.title =
      $scope.appInfo.userType +
      " - " +
      title +
      (environment == "" ? " " : " - ") +
      environment;
  }

  vm.setSelectedPlan = function (plan) {
    vm.planText = undefined;
    utilService.location.path("plans/" + plan.planId);
  };

  vm.isAdmin = function () {
    return userProfile.isAdmin() || mockSessionService.isMocking();
  };

  vm.showNotificationIcon = function () {
    return true;
    //return userProfile.isAdmin() || mockSessionService.isMocking() || userProfile.isBeta();
  };

  var clearCacheListener = $rootScope.$on("clearCache", function (
    event,
    value
  ) {
    vm.clearCache();
  });

  vm.clearCache = function () {
    sessionStorage.removeItem("sessionPlans");
    sessionStorage.removeItem("sessionPlansNonTTI");
    $rootScope.$emit("refreshMainPage", undefined);
    $state.go("customerList");
  };
}

export default headerComponent;
