import actionItemsTemplate from './actionItems.html';

const actionItems =  {
		template: actionItemsTemplate,
		controller: function(utilService, userProfile, userInfoService, actionItemService, planService, $state, $rootScope, $document) {

			var vm = this;
			
			vm.criteria = {
					status: {
						open: true,
						complete: false
					},
					type: {
						oms: true,
						tactic: true,
						callReport: true
					},
					showMine: true
				};

			vm.$onInit = function() {
				if (sessionStorage.getItem('actionItemCriteria')) {
					vm.criteria = JSON.parse(sessionStorage.getItem('actionItemCriteria'));
					
					if (vm.criteria.endDate) {
						var endDateString = vm.criteria.endDate;
						if(endDateString == 'undefined' || endDateString == 'null') vm.criteria.endDate = undefined;
						else vm.criteria.endDate = new Date(endDateString);
					}
					
					if (vm.criteria.startDate) {
						var startDateString = vm.criteria.startDate;
						if(startDateString == "undefined" || startDateString == 'null') vm.criteria.startDate = undefined;
						else vm.criteria.startDate = new Date(startDateString);
					}
					
				} else {
					var startDate = new Date();
					startDate.setMonth(startDate.getMonth() - 1);
					vm.criteria.startDate = startDate;
					vm.criteria.showMine = true;
					vm.initialize = true;
				}
				
				
				if (sessionStorage.getItem('actionItemTextFilter')) vm.textFilter = JSON.parse(sessionStorage.getItem('actionItemTextFilter'));
				
				vm.userProfile = userProfile.getUserProfile();
				if(vm.userProfile) initializeCriteria();
			}
			
			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {
				if(!vm.userProfile) {
					vm.userProfile = userProfile.getUserProfile();
					initializeCriteria();
				}
			})
			
			function initializeCriteria () {
				vm.role = userProfile.getUserType();
				
				switch(vm.role.toUpperCase()) {
					case 'ADMIN':
						planService.getParentBranchList().then(
							function(response) {
								vm.branchList = response;
								if(vm.initialize) {
									vm.criteria.branches = [userProfile.getBranch()];
									vm.initialize = false;
								}
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						break;
					case 'EXECUTIVE':
						planService.getEntityParentBranches(userProfile.getEntity()).then(
							function(response) {
								vm.branchList = [];
								
								for(var i = 0; i < response.length; i++) {
									if(vm.branchList.includes(response[i].fullParentBranchName)) continue;
									vm.branchList.push(response[i].fullParentBranchName);
								}
								
								if(vm.initialize) {
									vm.criteria.branches = [userProfile.getBranch()];
									vm.initialize = false;
								}
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						break;
					case 'SAM_GAM':
						userInfoService.getUserCorpIdList(userProfile.getSAMAccountName()).then(
								function(response) {
								vm.corpIdList = response;
								if(vm.initialize) {
									vm.criteria.corpId = vm.corpIdList[0];
									vm.initialize = false;
								}
						});
						break;
					case 'FSR':
					case 'ISR':
						break;
					case 'RVP':
					case 'GM':
					case 'FSM':
					case 'RBOM':
					case 'ISM':
					default:
						vm.branchList = userProfile.getBranchList();
						if(vm.initialize) {
							vm.criteria.branches = [userProfile.getBranch()];
							vm.initialize = false;
						}
						break;
				}
				getActionItems();
				
			}
			
			function getActionItems() {
				if(vm.loadingActionItems) return;
				vm.loadingActionItems = true;
				vm.actionITems = [];
				vm.saveCriteria();
				
				vm.actionItems = undefined;
				actionItemService.retrieveActionItemsByCriteria(vm.criteria).then(
					function(response) {
						vm.actionItems = response;
						vm.loadingActionItems = false;
					})
					.catch(function(error) {
						vm.actionItems = [];
						vm.loadingActionItems = false;
					})
			}
			
			vm.openSearchModal = function() {
				utilService
					.uibModal()
					.open({
						component: "actionItemsSearchModal",
						size: "md",
						resolve: {
							modalData: function () {
								return {
									criteria: vm.criteria,
									branchList: vm.branchList,
									corpIdList: vm.corpIdList
								}
							}
						}
					})
					.result.then(
						function(criteria) {
							getActionItems();
						},
						function(dismiss) {
							
						});
			};

			
			vm.saveCriteria = function() {
				sessionStorage.setItem('actionItemCriteria', JSON.stringify(vm.criteria));
			}
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('actionItemTextFilter', JSON.stringify(vm.textFilter));
			}
		}
	}
export default actionItems;