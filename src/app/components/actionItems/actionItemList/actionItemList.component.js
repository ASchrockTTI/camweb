import actionItemListTemplate from './actionItemList.html';

const actionItemList =  {
		template: actionItemListTemplate,
		bindings:{
			refresh: '&',
			type: '<',
			criteria: '=',
			textFilter: '=',
			actionItems: '<'
	  },
		controller: function(utilService, userProfile, actionItemService, $filter, $scope, $state) {
			
			var vm = this;
			var DISPLAY_LIMIT = 25;

			vm.$onInit = function() {
				vm.currentDate = new Date();
				vm.scrolling.displayLimit = DISPLAY_LIMIT;
				vm.scrolling.checkScrollLimit();
			}
			
			$scope.actionItemFilter = function() {
			  return function( actionItem ) {
				if(actionItem.deleted) return false;
				  
				var display = true;
				switch(actionItem.isComplete || actionItem.completeDate !== null) {
				case true:
					display = vm.criteria.status.complete;
					break;
				case false:
					display = vm.criteria.status.open;
					break;
				}
				if(!display) return false;
				
			    switch(actionItem.actionItemSummaryType) {
			    case 'CALL_REPORT':
			    	return vm.criteria.type.callReport;
			    case 'OMS':
			    	return vm.criteria.type.oms;
			    case 'PLAN_TACTIC':
			    	return vm.criteria.type.tactic;
			    default:
			    	return true;
			    }
			  };
			};
			
			vm.openContext = function(actionItem) {
				
				var state;
				var params;

				switch(actionItem.actionItemSummaryType) {
					case "OMS":
						state = 'oms';
						params = {
							planId: actionItem.planId,
							projectId: actionItem.projectId,
							actionItemId: actionItem.actionId
						};
						break;
					case "CALL_REPORT":
						state = 'callReport';
						params = {
							planId: actionItem.planId,
							callReportId: actionItem.callReportId
						};
						break;
					case "PLAN_TACTIC":
						state = 'strategyTactics';
						params = {
							planId: actionItem.planId,
							planStrategyId: actionItem.planStrategyId,
							planTacticId: actionItem.planTacticId
							//callReportId: actionItem.callReportId
						};
						break;
					default:
						state = 'businessPlan';
						params = {
							planId: actionItem.planId,
							//callReportId: actionItem.callReportId
						};
				};

				$state.go(state, params);
			}
			
			
			vm.completeActionItemModal = function(actionItem) {
				utilService.uibModal().open({					
					component: 'completeActionItemModal',
					size: 'sm',
					resolve: {
						modalData: function () {
							return { 
								actionItem: actionItem
							}
						}
					}
				}).result.then(
					function (actionItemContainer) {
						actionItem.completeDate = new Date();
						if(actionItemContainer.comment) {
							actionItem.comment = actionItemContainer.comment;
							actionItem.commenter = userProfile.getFullName();
						}
						actionItem.isComplete = true;
						
					},
					function (dismiss) {

					}
				)
			}
			
			
			vm.deleteActionItem = function(actionItem) {
				var message;
				if(actionItem.whoName) {
					message = 'You are about to delete this action item for ' + $filter('titlecase')(actionItem.whoName).trim() + '.';
				} else {
					message = 'You are about to delete this action item.'
				}
				
				utilService.deleteConfirmation('Delete Confirmation', message, 'btn-danger').result
				.then(function(results) {
					var actionItemId;
					switch(actionItem.actionItemSummaryType) {
					case 'CALL_REPORT':
						actionItemId = actionItem.actionItemId;
						break;
					case 'PLAN_TACTIC':
						actionItemId = actionItem.planTacticDelegateId;
						break;
					case 'OMS':
						actionItemId = actionItem.actionId;
						break;
					default:
						return;
				}
					
					actionItemService.deleteActionItem(actionItem.actionItemSummaryType, actionItemId)
					.then(function(response) {
						actionItem.deleted = true;
					})
					.catch(function(error) {
						
					});
				},
				function(dismiss) {
					
				})
			}
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('actionItemTextFilter', JSON.stringify(vm.textFilter));
			}


			vm.scrolling = {
				disable: false,
				displayLimit: 0,
				loadMore: function() {
			
					if (vm.actionItems === undefined) {
						return;
					}
					
					vm.scrolling.displayLimit += 25;
					
					
					vm.totalLength = vm.actionItems.length;
					
					if (vm.actionItems.length <= vm.scrolling.displayLimit) {
						vm.scrolling.displayLimit = vm.actionItems.length
						vm.scrolling.disable = true;
					} else {
						vm.scrolling.disable = false;
					}
				},
				
				checkScrollLimit: function() {

					if (vm.actionItems && vm.actionItems.length <= vm.scrolling.displayLimit) {
						vm.scrolling.displayLimit = vm.actionItems.length
						vm.scrolling.disable = true;
					} else {
						vm.scrolling.disable = false;
					}
				},

				resetScroll: function() {
					//vm.scrolling.displayLimit = 0
					vm.scrolling.disable = false;
					vm.scrolling.loadMore();
				}
			};
			
		}
	}

export default actionItemList;