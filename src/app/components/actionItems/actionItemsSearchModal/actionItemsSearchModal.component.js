import actionItemsSearchModalTemplate from './actionItemsSearchModal.html';

const actionItemsSearchModal =  {
		template: actionItemsSearchModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
						$scope,
						userProfile
					) {
			
			var vm = this;
			

			vm.$onInit = function() {
				vm.originalCriteria = vm.resolve.modalData.criteria;
				copyOriginalCriteria();
				
				vm.branchList = vm.resolve.modalData.branchList;
				vm.corpIdList = vm.resolve.modalData.corpIdList;
				
				initializeBranchList();
			}
			
			function initializeBranchList() {
				vm.formattedBranchList = [];

				let formattedBranch = {};
				angular.forEach(vm.branchList, function(branch, index) {
					formattedBranch.display = branch;
					formattedBranch.Id      = index;
					vm.formattedBranchList.push(formattedBranch);
					formattedBranch = {};
				});
				
				//if currently filtered by a branch list; else select default branch location
				if(vm.criteria.branches && vm.criteria.branches.length > 0) {	
					vm.selectedBranchList = findBranchesInBranchList(vm.formattedBranchList, vm.originalCriteria.branches);
				} else {
					vm.selectedBranchList = findBranchesInBranchList(vm.formattedBranchList, [userProfile.getBranchLocation()]);
				}
			}
			
			function findBranchesInBranchList(branchList, branchesToFind) {
				var foundBranches = [];
				var foundBranch = {};

				angular.forEach(branchList, function(branch, index) {
					if(branchesToFind.indexOf(branch.display) > -1) {
						foundBranches.push(branch);
					}
				});

				return foundBranches;
			}
			
			vm.branchFilter = function($query) {
				var filtered = [];
				angular.forEach(vm.formattedBranchList, function(branch) {
					if(branch.display.toLowerCase().indexOf($query.toLowerCase()) === 0 || !$query) filtered.push(branch);
				})
				return filtered;
			}
			
			function copyOriginalCriteria() {
				var originalCriteriaString = JSON.stringify(vm.originalCriteria);
				
				vm.criteria = JSON.parse(originalCriteriaString);
				
				initializeBranchList();
				
				vm.ownerNameInput = vm.criteria.ownerFullName;
				
				if(vm.criteria.corpId) vm.selectCorpId(vm.criteria.corpId);
				
				//parse "stringified" dates
				if(vm.criteria.startDate) vm.criteria.startDate = new Date(vm.criteria.startDate);
				if(vm.criteria.endDate)   vm.criteria.endDate = new Date(vm.criteria.endDate);
			}
			
			vm.branchListChanged = function() {
				if(!vm.selectedBranchList || vm.selectedBranchList.length === 0) {
					vm.criteria.branches = undefined;
					return;
				}
				
				vm.criteria.branches = [];
				angular.forEach(vm.selectedBranchList, function(formattedBranch) {
					vm.criteria.branches.push(formattedBranch.display);
				})
			}
			
			
			vm.selectCorpId = function(corpId) {
				vm.inputCorpId = corpId.corpId + ' | ' + corpId.customerName;
				vm.criteria.corpId = corpId;
			}
			
			vm.search = function() {
				if(vm.criteria.branches) vm.criteria.branches.sort();
				
				angular.copy(vm.criteria, vm.originalCriteria);
				
				vm.modalInstance.close();
			}
			
			vm.reset = function() {
				copyOriginalCriteria();
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}

		}
	}

export default actionItemsSearchModal;