const lookupUser =  {
		template: '<input id="lookupUser" type="text" ng-model="$ctrl.ngModel" ng-change="$ctrl.onChange()"' +
			   	  'autocomplete="off" ' +
			   	  'placeholder="{{$ctrl.placeholder}}" ' +
                  'uib-typeahead="users.givenName + \' \' + users.sn + \' (\' + users.branchLocation + \')\' for users in $ctrl.findUser($viewValue)" ' + 
                  'typeahead-append-to-body="true"' +
                  'typeahead-min-length="$ctrl.minLength" ' +                  
                  'typeahead-on-select="$ctrl.onSelect({$item})" ' +
                  'typeahead-wait-ms="250" ' +                  
                  'class="form-control">' + 
                  '<style>.dropdown-menu{z-index: 1070 !important}</style>',
        bindings: {
        	ngModel: '=',
        	onChange: '&',
        	minLength: '<',
        	onSelect: '&',
        	osr: '<?',
        	callReporters: '<?',
        	placeholder: '<?'
        },
		controller: function(userInfoService, utilService, $attrs) {
			
			var vm = this;
			
			vm.$onInit = function() {				
				vm.minLength = vm.minLength === undefined ? 2 : vm.minLength;
				if(typeof vm.placeholder === 'undefined') {
					vm.placeholder = 'Search by last name...';
				}
			}
			
			vm.$postLink = function() {
								
				var elem = angular.element('#lookupUser');
				
				if ($attrs.style) {
					elem[0].setAttribute('style', $attrs.style);					
				}
				
			}
			
			vm.findUser = function(lastName) {
				if (typeof vm.osr !== 'undefined') {
					return userInfoService.retrieveOsrInfoByLastName(lastName)
					.then(function(response) {
						return response;
					})
					.catch(function(error) {
						utilService.getLogger().info(error);
					})
				} else if (typeof vm.callReporters !== 'undefined') {
					return userInfoService.lookupCallReportCreatorsByLastName(lastName)
					.then(function(response) {
						return response;
					})
					.catch(function(error) {
						utilService.getLogger().info(error);
					})
				} else {
					return userInfoService.retrieveUserInfoByLastName(lastName)
					.then(function(response) {
						return response;
					})
					.catch(function(error) {
						utilService.getLogger().info(error);
					})
				}
			}
		}
	}
export default lookupUser;