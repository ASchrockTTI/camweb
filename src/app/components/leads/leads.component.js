import leadsTemplate from './leads.html';

const leads =  {
		template: leadsTemplate,
		controller: function(
			pendingCustomerService,
			userProfile,
			utilService,
			$scope,
			$state
		) {

			var vm = this;
			
			vm.filter = {
				submitted: true,
				pending: true,
				assigned: true,
				notSupporting: true,
				discarded: true
			}
			
			
			vm.$onInit = function() {
				getPendingCustomers();
			}
			
			function getPendingCustomers() {
				var name = userProfile.getFullName();
				// user profile information may not be immediately available
				// upon refresh
				if(name !== 'undefined undefined') {
					pendingCustomerService.getPendingCustomersByCurrentUser().then(
						function(response) {
							vm.pendingCustomers = response;
						})
						.catch(function(error) {
							vm.pendingCustomers = [];
						})
				}
				else {
					setTimeout(getPendingCustomers, 200);
				}
			}
			
			vm.pendingCustomerModal = function(pendingCustomer) {
				
				utilService.uibModal().open({
					component: 'submitNewCustomerModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: {
                	  modalData: function () {
                          return { 
                              pendingCustomer: pendingCustomer
                          }
                      }
                  }
				}).result.then(
					function(result) {
						if(result.new) {
							vm.pendingCustomers.push(result);
						}
					},
					function(dismiss) {
						
					}
				)
			}
			
			vm.openSubmitNewNuggetModal = function(newNugget) {
				
				utilService.uibModal().open({
					component: 'submitNewNuggetModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: {
						modalData: function () {
							return {
								isEdit: true,
								nugget: {
									planID: 4987,
									nuggetID: 32
								}
							}
						}
                  }
				}).result.then(
					function(result) {
						if(result.new) {
							vm.pendingCustomers.push(result);
						}
					},
					function(dismiss) {
						
					}
				)
			}
			
			vm.canEdit = function(pendingCustomer) {
				if(utilService.equalsIgnoreCaseSpace(pendingCustomer.status, 'Submitted')) {
					return true;
				} else return false;
			}
			
			vm.canDelete = function(pendingCustomer) {
				if(utilService.equalsIgnoreCaseSpace(pendingCustomer.status, 'Pending')) {
					return false;
				} else return true;
			}
			
			vm.goToPlan = function(planId) {
				$state.go('businessPlan', {planId: planId}); 
			}
			
			vm.deletePendingCustomer = function(pendingCustomer, index) {
	          utilService.deleteConfirmation(
	              "Customer Lead",
	              "You are about to delete the selected customer lead.",
	              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
	            			pendingCustomerService.deletePendingCustomer(pendingCustomer.pendingCustomerId)
		            		.then(function(response) {
		            			vm.pendingCustomers.splice(vm.pendingCustomers.indexOf(pendingCustomer), 1);
            				})
							.catch(function(error) {
								
							})
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
	        }
			
			
			vm.showSubmitter = function() {
				if(userProfile.getUserType() === "gm" || userProfile.getUserType() === "fsm") {
					return true;
				} else return false;
			}
			
			$scope.statusFilter = function(customer)
			{
				switch(customer.status) {
				case 'Submitted':
					return vm.filter.submitted;
				case 'Pending':
					return vm.filter.pending;
				case 'Assigned':
					return vm.filter.assigned;
				case 'Wrong Branch':
					return vm.filter.wrongBranch;
				case 'Not Supporting':
					return vm.filter.notSupporting;
				case 'Discarded':
					return vm.filter.discarded;
				default:
					return true;
				}
			};
			
			vm.sort = {
	          type: "updateDate",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
			

		}
	}
export default leads;