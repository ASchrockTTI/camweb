import generalTemplate from './general.html';

const general =  {
		template: generalTemplate,		
		controller: function($rootScope, $window, mockSessionService, userInfoService, utilService, tmwService, $state, $timeout) {
			
			var vm = this;			
						
			vm.setSelected = function(user) {				
				vm.selectedUser = user;
			},
				
			vm.mock = function(trueMock) {
				sessionStorage.removeItem('sessionPlans');
				sessionStorage.removeItem('sessionPlansNonTTI');
				sessionStorage.removeItem('filterMap');

				if (vm.selectedUser.givenName !== undefined) {					
					mockSessionService.createSession(vm.selectedUser.sAMAccountName, trueMock);
				}
				
				$rootScope.$emit('init', vm.selectedUser.sAMAccountName);
				
				$timeout(function() {
					utilService.location.path('/#');
					$state.go('home', {}, {reload: true});
				}, 100);
				$rootScope.$emit('init', vm.selectedUser.sAMAccountName, true);

				sessionStorage.removeItem('selectedFsr');
				sessionStorage.removeItem('selectedOmsUser');
				sessionStorage.removeItem('actionItemCriteria');
				sessionStorage.removeItem('omsProjects');
				sessionStorage.removeItem('omsProjectCriteria');
				sessionStorage.removeItem('nuggetCriteria');
				sessionStorage.removeItem('salesIQCriteria');
				sessionStorage.removeItem('callReportSearchCriteria');
			}
			
			vm.reports = {
				camDownload: function() {
					$window.open('/kam/api/admin/reports/export/cam','_blank');
				}
			}
		}
	}
export default general;