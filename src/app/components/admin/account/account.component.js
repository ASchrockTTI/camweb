import accountTemplate from './account.html';

const account =  {
		template: accountTemplate,
		controller: function($window, adminService, utilService, $sce, FileSaver) {
			
			var vm = this;
			
			vm.selectedEntities = [];
			vm.entities = [
				{'Id': '1', 'display': 'NDC'},
				{'Id': '2', 'display': 'EDC'},
				{'Id': '3', 'display': 'ADC'},
			]
			vm.entitiesConfig = {
				displayProp: 'display',
				showCheckAll: false,
				showUncheckAll: false
			};
			vm.selectedEntitiesStringDisplay = 'Select Entity';
			
			vm.$onInit = function() {
				
				var d = new Date();
				var yesterday = d.setDate(d.getDate());
				
				vm.form = {
					isCalendarOpen: false,
					value: yesterday,
					valueDate: new Date(yesterday)
				}
				vm.dateOptions = {
				    maxDate: new Date()
				};
				
				vm.loadingAccounts = false;
				vm.responseEmtpy = false;
				
				vm.accounts = [];
			}
			
			vm.openCalendar = function() {
				vm.form.isCalendarOpen = !vm.form.isCalendarOpen;
			}
			
			vm.formatSelectedEntities = function() {
				vm.entityArray = vm.selectedEntities.map(function(a) {return a.display;});
				if(vm.entityArray.length > 0) {
					vm.selectedEntitiesStringDisplay = vm.entityArray.join(', ');
					vm.selectedEntitiesString = vm.entityArray.join(',');
				}
				else {
					vm.selectedEntitiesStringDisplay = 'Select Entity';
					vm.selectedEntitiesString = '';
				}
			}
			
			vm.retrieveAccounts = function() {
				
				var todaysDate = new Date(vm.form.value);
				
				var tomorrowsDate = new Date(vm.form.value);
				tomorrowsDate.setDate(tomorrowsDate.getDate() + 1);
				
				vm.accounts = undefined;
				vm.loadingAccounts = true;
				vm.responseEmtpy = false;
				
				adminService.accounts.getAccounts(todaysDate.getTime(), tomorrowsDate.getTime())
				.then(function(response) {
					vm.accounts = response;
					vm.loadingAccounts = false;
					vm.requestSubmitted = true;
					
					vm.exportDisabled = false;
				})
				.catch(function(error) {
					vm.loadingAccounts = false;
				})
			}
			
			vm.retrieveAccountsByAccountAndEntity = function() {
				
				vm.accounts = undefined;
				vm.loadingAccounts = true;
				vm.responseEmtpy = false;
				vm.exportDisabled = true;
				
				adminService.accounts.getAccountsByAccountAndEntity(vm.account, vm.entityArray)
				.then(function(response) {
					vm.accounts = response;
					vm.loadingAccounts = false;
					
					vm.requestSubmitted = true;
				})
				.catch(function(error) {
					vm.loadingAccounts = false;
				})
			}
			
			vm.exportList = function() {
				
				var todaysDate = new Date(vm.form.value);
				var tomorrowsDate = new Date(vm.form.value);
				tomorrowsDate.setDate(tomorrowsDate.getDate() + 1);
				
				adminService.accounts.export(todaysDate.getTime(), tomorrowsDate.getTime())
				.then(function(response) {
					var header = response.headers('Content-Disposition');
					var fileName = header.split("=")[1].replace(/\"/gi,'');
					
					var downloadableBlob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, fileName);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
				
				//$window.open('/kam/api/admin/accounts/export?dateToday=' + todaysDate.getTime() + '&dateTomorrow=' + tomorrowsDate.getTime(),'_blank');
			}
			
			vm.showDeletionLog = function() {
				utilService.uibModal().open({
	              component: "deletedAccountsModal",
	              size: "md"
	            })
	            .result.then(
            	function(result) {
            		
            	},
            	function(dismiss) {
            		
            	});
			}
			
			vm.deleteAccount = function(account) {
				utilService.deleteConfirmation(
	              "Account",
	              "You are about to delete account " + account.account + " (" + account.entity + ").",
	              "btn-danger"
	            ).result.then(
            	function(result) {
            		if(result) {
            			adminService.accounts.deleteAccount(account.accountId)
	            		.then(function(response) {
	            			for (var i = 0; i < vm.accounts.length; i++) {
		            			if (vm.accounts[i].accountId === account.accountId) {
		            				vm.accounts.splice(i, 1);
		            				i--;
		            		    }
		            		}
            				})
							.catch(function(error) {
								
							})
            		}
            	},
            	function(dismiss) {
            		
            	});
	        };
		}
	}
export default account;