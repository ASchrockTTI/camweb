import deletedAccountsModalTemplate from './deletedAccountsModal.html';

const deletedAccountsModal =  {
		template: deletedAccountsModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			adminService) {
			
			var vm = this;
			
			vm.accountDeletionLogs = undefined;
			
			vm.$onInit = function() {
				getDeletedAccounts();
			}

			function getDeletedAccounts() {
				adminService.accounts.getDeletedAccounts()
				.then(function(response) {
					vm.accountDeletionLogs = response;
				})
				.catch(function(error) {
					vm.accountDeletionLogs = [];
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss();
			}
			
		}
	}
export default deletedAccountsModal;