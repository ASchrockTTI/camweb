import userTemplate from './user.html';

const user =  {
		template: userTemplate,
		controller: function(adminService, utilService, roles, userProfile, mockSessionService) {
			
			var vm = this;			
			var userContainer = {};
			
			vm.$onInit = function() {
				
				vm.toAdd = undefined,
				vm.selectUserObj = undefined,
				vm.kamTitles = [
					roles.BDG,
					roles.BDM,
					roles.BPM,
					roles.CPM,
					roles.CUSTOMER_SERVICE,
					roles.EXEC,
					roles.FAE,
					roles.FSM,
					roles.FSR,
					roles.GM,
					roles.HEAD_HONCHO,
					roles.ISM,
					roles.ISR,
					roles.PDM,
					roles.RBOM,
					roles.RSE,
					roles.RVP,
					roles.SAM_GAM,
					roles.SCM,
					roles.SUPPORT];
				
				getData();
			}
			
			vm.isAdmin = function() {
				return (userProfile.isAdmin() && !mockSessionService.hasSession());
			}
			
			vm.updateUserTable = function() {
				utilService.deleteConfirmation('Update User Table', 'Updating the User Table will take ~2 minutes.', 'btn-danger').result
				.then(
					function(result) {
						if (result === true) {			
							vm.updatingUserTable = true;
							adminService.users.updateUserTable().then(function(response) {
								vm.updatingUserTable = false;
							})
							.catch(function(error) {
								
							})
						}
					},
					function(dismiss) {
						
					}
				)
			}
			
			vm.add = function() {
							
				adminService.users.add(userContainer)				
				.then(function(response) {
					vm.clean();
					getData();
				})
				.catch(function(error) {
					
				})
			}
			
			vm.update = function() {

				adminService.users.update(userContainer)
				.then(function(response) {
					getData();
				})
				.catch(function(error) {
					utilService.getLogger().error('adminService.updateUser() error ');
				})
			},
			
			vm.delete = function() {

				adminService.users.delete(userContainer.userId)
				.then(function(response) {
					getData();
				})
				.catch(function(error) {
					throw error;
				})
			},
			
			vm.setSelectedUserToAdd = function(user) {				
				if (utilService.isNotEmptyArray(utilService.filterBy(vm.users, {fullName: user.fullName}, true))) {
					alert('User ' + user.fullName + " already exists!");
					vm.clean();
				} else {
					userContainer.userName = user.sAMAccountName;
					userContainer.fullName = user.fullName;
					userContainer.jobTitle = user.title;
					userContainer.branches = user.branchLocation;
					vm.selectUserObj = user; 
				}
				
			},
			
			vm.isUserFormNotDirty = function() {
				var bDirtyForm = false;
				
				// user wipes out the input
				if (vm.userForm.$dirty && 
					utilService.isEmpty(vm.toAdd)){
					vm.clean();
				}
				
				// user selects row and then switches to add user, refresh data from backup
				if (vm.userForm.$dirty && 
					utilService.isNotEmpty(vm.userRow.index)) {					
						vm.users = utilService.deepCopy(vm.backupUsers);
						vm.userRow.clean();
				}
				
				if (vm.userForm.$dirty &&
						utilService.isNotEmpty(vm.selectUserObj)) {
						bDirtyForm = false;
					} else {
						bDirtyForm = true;
					}
					
				
				return bDirtyForm;
			},
			
			vm.clean = function() {
				vm.toAdd = undefined;
				vm.selectUserObj = undefined;
				vm.userForm.$setPristine();
			}

			vm.insertSpaces = function(branches) {
				if(branches) {
					return branches.replace(/,/g, ', ');
				}
				else {
					return '';
				}
			}
			
			function getData() {

				adminService.users.findAll()
				.then(function(response) {										
					vm.users 	   = utilService.orderBy(response, 'fullName', false);
					
					vm.backupUsers = utilService.deepCopy(vm.users);
				})
				.catch(function(error) {
					
				})
			}			
			
			vm.userRow = {
				data: undefined,
				index: undefined,
				previousIndex: undefined,
				
				select: function(userData, index) {
					vm.userRow.index = index;
					
					if (vm.userRow.previousIndex !== index) {
						// user switches row, refresh users data from backup
						vm.users  = utilService.deepCopy(vm.backupUsers);
						vm.userRow.previousIndex = index;
					}
										
					vm.userRow.data = userData;
					
					vm.clean();
				},
				
				deleteSelectedRowUser: function(userData, index) {
					if ( !window.confirm('Are you sure you want to delete this user?') ) {
						return;
					}
					userContainer.userId = userData.userId;
					vm.delete();
					vm.userRow.clean();
				},
				
				editSelectedRowUser: function(userData, index) {
					userContainer.userName = userData.sAMAccountName;
					userContainer.fullName = userData.fullName;
					userContainer.jobTitle = userData.jobTitle;
					userContainer.branches = userData.branches;
					userContainer.userId   = userData.userId;
					vm.update();
					vm.userRow.clean();
				},
				
				isEditable: function(index) {
					return (vm.userRow.index == index) ? true : false;
				},
				
				setSelectedRowKamTitle: function(theKamTitle) {
					vm.userRow.data.jobTitle = theKamTitle;
				},
				
				clean: function() {
					vm.userRow.data = undefined;
					vm.userRow.index = undefined;
					vm.userRow.previousIndex = undefined;
				}
			}
		}
	}
export default user;