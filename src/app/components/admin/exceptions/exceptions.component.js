import exceptionsTemplate from './exceptions.html';

const exceptions =  {
		template: exceptionsTemplate,
		controller: function(adminService, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				
				resetNewException();
				
				vm.actions = [
					{value: 1, action: 'Exclude'},
//					{value: 2, action: 'Include'},
//					{value: 3, action: 'Hide'},
				];
				
				vm.types = [
					{value: 1, type: 'Branch'},
//					{value: 2, type: 'Sales Person Exception'},
//					{value: 3, type: 'Family Exception'},
//					{value: 4, type: 'Plan Exception'},
				];
				
				vm.fields = [
//					{value: 1, field: 'Family ID'},
//					{value: 1, field: 'Plan ID'},
//					{value: 1, field: 'Account'},
					{value: 1, field: 'Branch'},
				]
				
				findExceptions();
								
			}
			
			function resetNewException() {
				vm.newException = {
					type: undefined,					
					action: undefined,
					field: undefined,
					value: undefined,
					maxValueLength: 256,										
				};
			}
			
			vm.setDefaults = function(type) {
				
				if (!type) {
					return;
				}
				
				switch(type.type) {
				case 'Branch':
					vm.newException.action = vm.actions[0];
					vm.newException.field = vm.fields[0]
					vm.newException.maxValueLength = 2					
				default:
						
				}	
			}
			
			vm.validateForm = function() {
				if (vm.exceptionForm.$dirty && vm.exceptionForm.$valid) {
					return false;
				} else {
					return true;
				}	
			}
			
			vm.add = function() {
				
				var container = {
					exceptionValue: vm.newException.value.toUpperCase(),
					exceptionType: {exceptionTypeID: 1, exceptionType: 'Branch'},
					exceptionAction: {exceptionActionID: 1, exceptionAction: 'Exclude'},
					exceptionColumn: {exceptionColumnID: 3, exceptionColumn: 'Branch'}
				}
				
				adminService.exceptions.add(container)
				.then(function(response) {
					findExceptions();
					resetNewException();
				})
				.catch(function(error) {
					
				})
			}
			
			vm.delete = function(exception) {
				adminService.exceptions.delete(exception.exceptionID)
				.then(function(response) {
					vm.newException
					findExceptions();
				})
				.catch(function(error) {
					
				})
			}
			
			function findExceptions() {
				
				vm.exceptions = [];
				
				adminService.exceptions.findAll()
				.then(function(response) {
					console.log(response);
					vm.exceptions = response;
				})
				.catch(function(error) {
					
				})
								
			}
		}
	}
export default exceptions;