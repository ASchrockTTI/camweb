import planTemplate from './plan.html';

const plan =  {
		template: planTemplate,
		controller: function(adminService, userProfile, utilService) {
			var vm = this;
			
			vm.$onInit = function() {
				vm.sections = [
					{id: 1, value: 'Business Plan', jsonValue: "BUSINESS_PLAN"},
					{id: 2, value: 'News', jsonValue: "NEWSFEED"},
					{id: 3, value: 'Call Reports', jsonValue: "CALL_REPORTS"},
					{id: 4, value: 'Strategies & Tactics', jsonValue: "STRATEGIES_TACTICS"},
					{id: 5, value: 'Contacts', jsonValue: "CONTACTS"},
					{id: 6, value: 'VBS', jsonValue: "VBS"},
					{id: 7, value: 'OMS', jsonValue: "OMS"},
					{id: 8, value: 'Discard Old Plan ', jsonValue: "DISCARD_PLAN"},
					{id: 9, value: 'OPB', jsonValue: "NUGGETS"}
				];
				
				vm.data = {
					fromPlanId: undefined,
					toPlanId: undefined,
					selected: []
				}
			}
			

			vm.validateForm = function() {
				
				if (vm.transferForm.$valid && vm.data.selected.length > 0) {
					return true;
				}
				
				return false;
			}
			
			vm.checkAll = function() {
				vm.data.selected = vm.sections.map(function(item) { return item.id; });
			};
			
			vm.unCheckAll = function() {
				vm.data.selected = [];
			}
			
			vm.transfer = function() {
				
				var container = adminService.plans.getContainer();
				
				container.userName = userProfile.getSAMAccountName();
				container.fromPlanId = vm.data.fromPlanId;
				container.toPlanId = vm.data.toPlanId;
				
				for (var i = 0; i <= vm.data.selected.length - 1; i++) {
					var item = utilService.filterBy(vm.sections, {id: vm.data.selected[i]});
					if (item) {
						container.portOptionList.push(item[0].jsonValue);
					}				
				}
				
				utilService.uibModal().open({
					component: 'planModal',
					resolve: {
						data: function() {
							return container
						}
					}
				}).result.then(
						
					function(result) {
						adminService.plans.transfer(container)
						.then(function(response) {
							utilService.location.path('plans/' + vm.data.toPlanId);
						})
						.catch(function(error) {
							
						})	
					},
					function(dismissed) {
						
					});
			}
		}
	}
	const planModal = {
		template: planTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(planService, utilService) {
			
			var modal = this;
			
			modal.$onInit = function() {
				modal.modalData = modal.resolve.data;
				modal.oldPlan = {};
				modal.newPlan = {};
				
				modal.sections = [
					{id: 1, plainTextValue: 'Business Plan', jsonValue: "BUSINESS_PLAN"},
					{id: 2, plainTextValue: 'News', jsonValue: "NEWSFEED"},
					{id: 3, plainTextValue: 'Call Reports', jsonValue: "CALL_REPORTS"},
					{id: 4, plainTextValue: 'Strategies & Tactics', jsonValue: "STRATEGIES_TACTICS"},
					{id: 5, plainTextValue: 'Contacts', jsonValue: "CONTACTS"},
					{id: 6, plainTextValue: 'VBS', jsonValue: "VBS"},
					{id: 7, plainTextValue: 'OMS', jsonValue: "OMS"},
					{id: 8, plainTextValue: 'Discard Old Plan ', jsonValue: "DISCARD_PLAN"},
					{id: 9, plainTextValue: 'OPB', jsonValue: "NUGGETS"}
				];

				planService.lookupPlan(modal.modalData.fromPlanId)
				.then(function(response) {
					modal.oldPlan = response;					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})	
								
				planService.lookupPlan(modal.modalData.toPlanId)
				.then(function(response) {
					modal.newPlan = response;					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
				
				modal.plainTextPortOptionsList = this.convertOptionsToText(modal.modalData.portOptionList).toString();
			}
			
			modal.convertOptionsToText = function(portOptionList) {
				
				let plainTextPortOptionsList = [];
				let plainText = undefined;
				let selection = undefined;

				angular.forEach(portOptionList, function(element) {
					
					selection = modal.sections.filter(function(section) {
						return section.jsonValue === element;
					});
					
					plainText = selection[0].plainTextValue;
					
					plainTextPortOptionsList.push(plainText);
				});

				return plainTextPortOptionsList;
			}

			modal.$onDestroy = function() {
				modal.modalData = {};
			}
			
			modal.buttons = {
				cancel: function() {
					modal.modalInstance.dismiss();
				},
				continue: function() {
					modal.modalInstance.close();
				}
			}
		}
	}
export { plan, planModal};