import qualifyPendingCustomerModalTemplate from './qualifyPendingCustomerModal.html';

const qualifyPendingCustomerModal =  {
		template: qualifyPendingCustomerModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '='
		},
		controller:
			function(
				planService,
				userProfile,
				pendingCustomerService
			) {
			
			var vm = this;
			
			vm.pendingCustomerPlan = {};
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.pendingCustomer = vm.data.pendingCustomer;
				vm.pendingCustomerPlan.customerName = vm.pendingCustomer.customerName;
				
				vm.original = JSON.parse(JSON.stringify(vm.pendingCustomer));
				
				getBranches();
				getMarketSegments();
				
				setTimeout(function () {
					var input = angular.element(document.querySelector('#customerNameInput'));
					input.focus();
				}, 500);
			}
			
			vm.$onDestroy = function () {
                angular.copy(vm.original, vm.pendingCustomer);
            };
			
			
			vm.qualify = function() {
				vm.original = JSON.parse(JSON.stringify(vm.pendingCustomer));
				
				vm.creatingPlan = true;
				
				var mainPageContainer = {
					plan: {},
					securityProfile: {},
					mockingProfile: {}
				};
				
				if (vm.owner == undefined) {
					vm.owner = {
						fullName: userProfile.getFullName()					
					}
				}
				
				mainPageContainer.plan = {
					planId: vm.pendingCustomerPlan.planId,
					acctManager: 'Unassigned Owner',				
					customerName: vm.pendingCustomerPlan.customerName,
					branch: vm.pendingCustomerPlan.branch.branch,
					marketSegment: vm.pendingCustomerPlan.marketSegment,
					yearFounded: vm.pendingCustomerPlan.yearFounded,
				    dunsNumber:  vm.pendingCustomerPlan.dunsNumber,
				    parentDunsNumber:  vm.pendingCustomerPlan.parentDunsNumber,
				    ultimateDunsNumber:  vm.pendingCustomerPlan.ultimateDunsNumber,
				    annualRevenue:  vm.pendingCustomerPlan.annualRevenue ? vm.pendingCustomerPlan.annualRevenue.replace(/[^0-9]/g, '') : undefined,
				    numberEmployees:  vm.pendingCustomerPlan.numberEmployees ? vm.pendingCustomerPlan.numberEmployees.replace(/[^0-9]/g, '') : undefined,
				    numberSqFt:  vm.pendingCustomerPlan.numberSqFt ? vm.pendingCustomerPlan.numberSqFt.replace(/[^0-9]/g, '') : undefined,
				    address: vm.pendingCustomerPlan.address,
				    city: vm.pendingCustomer.city,
				    state: vm.pendingCustomer.state,
				    country: vm.pendingCustomer.country,
				    postalCode: vm.pendingCustomer.postalCode,
					familyID: 0
				}
				mainPageContainer.securityProfile = {
					fullName: userProfile.getFullName(),
					title: userProfile.getTitle(),
					branchLocation: userProfile.getBranchLocation(), 
					physicalDeliveryOfficeName: userProfile.getBranchLocation(),
					mail: userProfile.getMail(),
					selectedEntity: userProfile.getEntity()
				}
				
				pendingCustomerService.qualifyCustomer(mainPageContainer, vm.pendingCustomer.pendingCustomerId)
				.then(function(response) {
					vm.pendingCustomer.planId = response.response.planId;
					vm.pendingCustomer.status = response.response.status;
					vm.modalInstance.close('created');
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
				
			}
			
			function loadPlan(planId) {
				planService.lookupPlan(planId)
				.then(function(response) {
					
					vm.pendingCustomerPlan = response;
					
					angular.forEach(vm.branches, function(branch){
						if(branch.fullParentBranchName === vm.pendingCustomerPlan.branch) {
							vm.pendingCustomerPlan.branch = branch;
						}
					})
					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			
			function getBranches() {
				planService.getEntityParentBranches('All')
				.then(function(response) {
					vm.branches = response;
					
					if(vm.pendingCustomer.planId) {
						loadPlan(vm.pendingCustomer.planId);
					}
				})
				.catch(function(error) {
					
				})
			}
			
			
			function getMarketSegments() {
				planService.getMarketSegments()
				.then(function(response) {
					vm.marketSegments = [];
					angular.forEach(response, function(segment) {
						vm.marketSegments.push.apply(vm.marketSegments, segment.subCategoryList)
					})
				})
				.catch(function(error) {
					
				})
			}
			
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			
		}
	}
export default qualifyPendingCustomerModal;