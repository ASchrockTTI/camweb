import pendingCustomersTemplate from './pendingCustomers.html';

const pendingCustomers =  {
		template: pendingCustomersTemplate,
		controller: function($scope, utilService, pendingCustomerService, $state) {
			
			var vm = this;
			
			vm.pendingCustomers = undefined;
			
			vm.filter = {
				submitted: true,
				pending: true,
				assigned: true,
				wrongBranch: true,
				notSupporting: false,
				hideQualified: true
			}
			
			
			vm.$onInit = function() {
				getPendingCustomers();
			}
			
			
			function getPendingCustomers() {
				pendingCustomerService.getPendingCustomers().then(
				function(response) {
					vm.pendingCustomers = response;
				})
				.catch(function(error) {
					vm.pendingCustomers = [];
				})
			}
			
			
			$scope.statusFilter = function(customer)
			{
				if(customer.qualifierUserName)
				{
					return !vm.filter.hideQualified;
				}
				
				switch(customer.status) {
				case 'Submitted':
					return vm.filter.submitted;
				case 'Pending':
					return vm.filter.pending;
				case 'Assigned':
					return vm.filter.assigned;
				case 'Wrong Branch':
					return vm.filter.wrongBranch;
				case 'Not Supporting':
					return vm.filter.notSupporting;
				case 'Discarded':
					return vm.filter.discarded;
				default:
					return true;
				}
			};
			
			vm.goToPlan = function(planId) {
				$state.go('businessPlan', {planId: planId}); 
			}
			
			vm.qualifyPendingCustomer = function(pendingCustomer) {
            	utilService.uibModal().open({
                    component: 'qualifyPendingCustomerModal',
                    size: 'md',
                    resolve: { 
                    	modalData: function() {
                        	return {
                        		pendingCustomer: pendingCustomer
                        	}
                    	}
                    }
                }).result.then(
                    function(result) {
                    	getPendingCustomers();
                    },
                    function(dismiss) {
                        
                    }
                )
            }
			
			vm.discardPendingCustomer = function(pendingCustomer) {
            	utilService.uibModal().open({
                    component: 'discardPendingCustomerModal',
                    size: 'md',
                    scope: $scope,
                    resolve: { 
                    	modalData: function() {
                        	return {
                        		pendingCustomer: pendingCustomer
                        	}
                    	}
                    }
                }).result.then(
                    function(result) {
                    	getPendingCustomers();
                    },
                    function(dismiss) {
                        
                    }
                )
            }
			
			
			vm.sort = {
	          type: "lastUpdateDate",
	          reverse: false,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	          }
	        };
			
		}
	}
export default pendingCustomers;