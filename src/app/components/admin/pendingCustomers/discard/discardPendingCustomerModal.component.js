import discardPendingCustomerModalTemplate from './discardPendingCustomerModal.html';

const discardPendingCustomerModal =  {
		template: discardPendingCustomerModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(pendingCustomerService) {
			
			var vm = this;
			
			vm.reason = '';
			
			vm.$onInit = function() {
				vm.data = vm.resolve.modalData;
				vm.pendingCustomer = vm.data.pendingCustomer;
				
				setTimeout(function () {
					var input = angular.element(document.querySelector('#discardReasonInput'));
					input.focus();
				}, 500);
			}
			
			
			vm.discard = function() {
				pendingCustomerService.discardPendingCustomer(vm.pendingCustomer.pendingCustomerId, vm.reason)
				.then(function(response) {
					vm.modalInstance.close('discarded');
				})
				.catch(function(error) {
					vm.modalInstance.close('error');
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			
		}
	}
export default discardPendingCustomerModal;