import titleTemplate from './title.html';

const title =  {
		template: titleTemplate,
		controller: function(adminService, userInfoService, utilService, roles) {
			
			var vm = this;
			
			vm.$onInit = function() {				
				vm.kamTitles = [
					roles.BDM,
					roles.BPM,
					roles.CPM,
					roles.CUSTOMER_SERVICE,
					roles.EXEC,
					roles.FAE,
					roles.FSM,
					roles.FSR,
					roles.GM,
					roles.HEAD_HONCHO,
					roles.ISM,
					roles.ISR,
					roles.PDM,
					roles.RBOM,
					roles.RSE,
					roles.RVP,
					roles.SAM_GAM,
					roles.SCM,
					roles.SUPPORT];
				
				vm.titleContainer = {};
				vm.adTitle = undefined;
				vm.kamTitle = undefined;
				vm.selectedAdTitleObj = undefined;
				
				vm.findAll();
			}
												
			vm.findTitle = function(title) {
				return userInfoService.retrieveTitleInfo(title)
				.then(function(response) {
					return response;
				})
				.catch(function(error) {
					utilService.getLogger().info(error);
				})
			},
			
			vm.findAll = function() {				
				adminService.titles.findAll()
				.then(function(response) {
//					vm.titles = utilService.orderBy(response, 'adTitle', false);
					vm.titles = response;
					vm.backupTitles  = utilService.deepCopy(vm.titles);
				})
				.catch(function(error) {
					
				})
			},
			
			vm.add = function() {
				if (utilService.isNotEmptyArray(utilService.filterBy(vm.titles, {adTitle: vm.adTitle}, true))) {
					alert('AD title ' + vm.adTitle + " already exists!");
					vm.clean();
					return;
				}

				vm.titleContainer.adTitle = vm.adTitle;
				vm.titleContainer.kamTitle = vm.kamTitle;
				
				adminService.titles.add(vm.titleContainer)
				.then(function(response) {
					vm.clean();
					vm.findAll();
				})
				.catch(function(error) {
					
				})
			},
			
			vm.update = function() {
				
				adminService.titles.update(vm.titleContainer)
				.then(function(response) {
					vm.findAll();
				})
				.catch(function(error) {
					
				})
			},
			
			vm.setSelectedAdTitle = function(theAdTitle) {
				vm.selectedAdTitleObj = theAdTitle;
				vm.adTitle = theAdTitle.title;
			},
			
			vm.setSelectedKamTitle = function(theKamTitle) {
				vm.kamTitle = theKamTitle;
			},
			
			vm.delete = function() {
				
				adminService.titles.delete(vm.titleContainer.jobTitleId)
				.then(function(response) {
					vm.findAll();
				})
				.catch(function(error) {
					throw error;
				})
			},
			
			vm.isAddToTitleDisable = function() {
				var bDirtyForm = false;
				
				if (vm.titleForm.$dirty && 
					utilService.isEmpty(vm.adTitle)) {
					vm.adTitle = undefined;
					vm.titleForm.$setPristine();
				}
				
				if (vm.titleForm.$dirty &&
					utilService.isNotEmpty(vm.titleRow.index)) {
					vm.titles = utilService.deepCopy(vm.backupTitles);
					vm.titleRow.clean();
				}
				
				if (vm.titleForm.$dirty && 
					utilService.isNotEmpty(vm.selectedAdTitleObj) &&
					utilService.isNotEmpty(vm.kamTitle)			){
					bDirtyForm = false;
				} else {
					bDirtyForm = true;
				}
				
				return bDirtyForm;
			},
			
			vm.clean = function() {
				vm.adTitle = undefined;
				vm.kamTitle = undefined;
				vm.selectedAdTitleObj = undefined;
				vm.titleForm.$setPristine();
			}
			
			vm.titleRow = {
					data: undefined,
					index: undefined,
					previousIndex: undefined,
						
					select: function(userData, index) {
						vm.titleRow.index = index;
						
						if (vm.titleRow.previousIndex !== index) {
							// user switches row, refresh titles data from backup
							vm.titles = utilService.deepCopy(vm.backupTitles);
							vm.titleRow.previousIndex = index;
						}
						
						vm.titleRow.data = userData;
						vm.clean();
					},
					
					deleteSelectedRowTitle: function(userData, index) {
						if ( !window.confirm('Are you sure you want to delete this title?') ) {
							return;
						}
						vm.titleContainer.jobTitleId = userData.jobTitleId;
						vm.delete();
						vm.titleRow.clean();
					},
					
					editSelectedRowTitle: function(userData, index) {
						utilService.getLogger().info('Edit title by title Id ..... ' + ', ' + vm.titles[index].jobTitleId);
						vm.titleContainer.jobTitleId = userData.jobTitleId;
						vm.titleContainer.adTitle = userData.adTitle;
						vm.titleContainer.kamTitle = userData.kamTitle;
						vm.update();
						vm.titleRow.clean();
					},
					
					isEditable: function(index) {
						return (vm.titleRow.index == index) ? true : false;
					},
					
					setSelectedRowKamTitle: function(theKamTitle) {
						vm.titleRow.data.kamTitle = theKamTitle;
					},
					
					clean: function() {					
						vm.titleRow.data = undefined;
						vm.titleRow.index = undefined;
						vm.titleRow.previousIndex = undefined;
					}
				}
		}
	}
export default title;