import adminTemplate from './admin.html';

const admin =  {
		template: adminTemplate,
		controller: function(
			$scope,
			$state,
			$stateParams,
			userProfile,
			mockSessionService
			) {
			
			var vm = this;
			vm.params = $stateParams;
			
			$scope.$watch(function(){ return vm.params.adminTab }, function(){
				switch($stateParams.adminTab) {
				case 'users':
					vm.activeTabIndex = 1;
					break;
				case 'titles':
					vm.activeTabIndex = 2;
					break;
				case 'release-notes':
					vm.activeTabIndex = 3;
					break;
				case 'exceptions':
					vm.activeTabIndex = 4;
					break;
				case 'plans':
					vm.activeTabIndex = 5;
					break;
				case 'accounts':
					vm.activeTabIndex = 6;
					break;
				case 'pending-customers':
					vm.activeTabIndex = 7;
					break;
				default:
					vm.activeTabIndex = 0;
				}
			 });
			
			vm.isAdminRBOM = function() {
				var isRBOM = userProfile && userProfile.getUserType() && userProfile.getUserType().toUpperCase() == 'RBOM';
				return vm.isAdmin() || isRBOM;
			}
			
			vm.isAdmin = function() {
				return (userProfile.isAdmin() && !mockSessionService.hasSession());
			}
			
			vm.selectTab = function(tab) {
				$state.go('.', {adminTab: tab});
			}
		}
	}
export default admin;