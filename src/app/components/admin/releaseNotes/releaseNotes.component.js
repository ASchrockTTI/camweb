import releaseNotesContentTemplate from './releaseNotes.html';

const releaseNotesContent =  {
		template: releaseNotesContentTemplate,
		controller: function(adminService, utilService, FileSaver, $window, $sce) {
			
			var vm = this;
			
						
			vm.$onInit = function() {
				vm.releaseNotes = [];
				getReleaseNotes();
			}
			
			function getReleaseNotes() {
				adminService.getAllReleaseNotes().then(
				function(response) {
					vm.releaseNotes = response;
				})
				.catch(function(error) {
					
				});
			}
			
			vm.delete = function(releaseNote) {
				utilService.deleteConfirmation(
	              "Customer Lead",
	              "You are about to delete release note \"" + releaseNote.version + "\".",
	              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
	            			adminService.deleteReleaseNote(releaseNote.releaseNoteId)
		            		.then(function(response) {
		            			vm.releaseNotes.splice(vm.releaseNotes.indexOf(releaseNote), 1);
            				})
							.catch(function(error) {
								
							})
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
						
			vm.openReleaseNoteModal = function(releaseNote) {
				utilService
                .uibModal()
                .open({
                  component: "releaseNotesUploadModal",
                  size: "lg",
                  resolve: {
                	  modalData: function () {
                          return {
                        	  releaseNote: releaseNote,
                        	  downloadDocument: vm.downloadDocument
                          }
                      }
                  }
                })
                .result.then(
            	function(result) {
            		getReleaseNotes();
            	},
            	function(dismiss) {
            		
            	});
			}
			
			vm.downloadDocument = function(document) {
				adminService.downloadReleaseNoteDocument(document.releaseNoteDocumentId)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
			}
		}
	}
export default releaseNotesContent;