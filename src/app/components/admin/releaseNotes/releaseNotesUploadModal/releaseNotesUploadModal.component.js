import releaseNotesUploadModalTemplate from './releaseNotesUploadModal.html';

const releaseNotesUploadModal =  {
	   template: releaseNotesUploadModalTemplate,
	   bindings: {
		   modalInstance: '<',
		   resolve: '<'
	   },
	   controller: function(utilService, adminService) {
		   
		   var vm = this;
		   
		   vm.fileList = [];
		   
		   vm.$onInit = function() {
			   vm.data = vm.resolve.modalData;
			   vm.downloadDocument = vm.data.downloadDocument;
			   if(vm.data.releaseNote) {
				   vm.edit = true;
				   vm.releaseNote = vm.data.releaseNote;
				   vm.document = vm.releaseNote.documentList[0];
			   } else {
				   vm.releaseNote = {
					   releaseNoteId: null,
					   version: '',
					   releaseDate: new Date(),
					   summary: ''
				   }
			   }
		   }
		   
		   
		   vm.addSelectedFiles = function() {
			   Array.prototype.push.apply(vm.fileList, vm.selectedFiles);
		   }
			
		   vm.removeFile = function(file) {
			   vm.fileList.splice(vm.fileList.indexOf(file), 1);
		   }
		   
		   vm.deleteDocument = function(document) {
				utilService.deleteConfirmation('Delete Confirmation', 'You are about to delete "' + document.documentName + '.' + document.documentExt + '".', 'btn-danger').result
				.then(function(results) {
					adminService.deleteReleaseNoteDocument(document.releaseNoteDocumentId);
					for(var i = 0; i < vm.releaseNote.documentList.length; i++) {
						if(vm.releaseNote.documentList[i].documentID === document.documentID) {
							vm.releaseNote.documentList.splice(i, 1);
						}
					}
				},
				function(dismiss) {
					
				})
			}
		   
		   
		   vm.upload = function() {
			    adminService.uploadReleaseNote(vm.releaseNote, vm.fileList)
				.then(function (response) {
					vm.modalInstance.close();
				})
				.catch(function (error) {
	
				})
		   }
		   
		   
		   
		   vm.cancel = function() {
               vm.modalInstance.dismiss();
           }
		   
	   }
   }
export default releaseNotesUploadModal;