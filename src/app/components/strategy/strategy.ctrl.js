import activityDivTemplate from './activity.html';
	
	Strategies.$inject = [
		'$scope',
		'$stateParams',
		'$interval',
		'contactService',
		'strategyService',
		'tacticService',
		'commentService',
		'utilService',
		'planService',
		'userProfile',
		'tacticDelegateService',		
		'$anchorScroll',
		'$location',
		'roles',
		'userInfoService',
		'permissionsFactory'
		];	
	
	function Strategies(
			$scope,
			$stateParams,
			$interval,
			contactService,
			strategyService,
			tacticService,
			commentService,
			utilService,
			planService,
			userProfile,
			tacticDelegateService,			
			$anchorScroll,
			$location,
			roles,
			userInfoService,
			permissionsFactory
			) {		
		
		$scope.html = {
			activityDiv: activityDivTemplate
		}
		
		$scope.isEmptyStrategy = true;
		$scope.isEmptyTactic   = true;		
				
		$scope.contacts = {
			customers: [],
			delegates: []
		}
		
		$scope.params = $stateParams;		
		
		$scope.pageIncludes = utilService.getTemplates().getAll();		
		
		$scope.clientError = utilService.getClientErrorObject();
		$scope.notFound = {};
		
		var auditLog = [];
		var permissionsValidator;
		
		lookupPlan();		
		
		$scope.formats = ['dd/MM/yyyy','MM/dd/yyyy','yyyy-MM-dd','shortDate'];
		$scope.format = $scope.formats[3];
		$scope.altInputFormats = ['MM/dd/yyyy','yyyy-MM-dd'];
		
		$scope.dynamicPopover = {
			    content: 'Hello, World!',
			    templateUrl: 'markAsComplete.html',
			    title: 'Title'
			  };		
		
		permissionsValidator = permissionsFactory.newInstance();
		
		permissionsValidator.setCustomPermission('strategy-add', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('strategy-edit', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('strategy-delete', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('strategy-drag', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		
		// Handles any functionality pertaining to the strategic row
		$scope.strategyRow = {
			data: undefined,
			index: undefined,
			parentList: undefined,
			hasPermission: function(action) {
				return permissionsValidator.hasPermission(action, $scope.plan);				
			},
			dragOptions: {
				orderChanged: function(event) {
					
					utilService.getLogger().info(event.dest.sortableScope.modelValue[event.dest.index]);
					utilService.getLogger().info(event.source.index);
					utilService.getLogger().info(event.dest.index);
					
					var container = {
						sortOrders: event.dest.sortableScope.modelValue,
						securityProfile: {
							fullName: userProfile.getFullName()
						}
					}
					
					strategyService.updateSortOrders($scope.params.planId, container)
					.then(function(response) {
						var dragged = event.dest.sortableScope.modelValue[event.dest.index];					
						$scope.strategyRow.select(dragged, event.dest.index,true, true);
					})
					.catch(function(error) {
						
					})
									
				},				
				containment: '#strategy-container',
				containerPositioning: 'relative',
			},
			select: function(strategy, index, reloadAction, dragAction) {
				
				// Disable 'select' if in edit mode
				if ($scope.updateTactics.isAddEditMode || $scope.updateComments.isAddEditMode) {
					return;
				}
				
				var reloadAgain = utilService.isNotEmpty(reloadAction) ? reloadAction : false; 
				var drapOption  = utilService.isNotEmpty(dragAction) ? dragAction :  false;
				var strategyServiceStoredIndex = strategyService.getStrategyIndex();
				
				if (!reloadAgain &&	strategyServiceStoredIndex == index) {
					return;
				} else {
					// user clicks different strategy row
					if (strategyServiceStoredIndex != index && !dragAction) {
						strategyService.setStrategyIndex(index);
						tacticService.setTacticsIndex(0);
						commentService.setDelegateIndex(0);
					}
				}
				
				$scope.strategyRow.index 	   = index;
				$scope.strategyRow.data 	   = strategy;
				strategyService.setStrategy(strategy);
				
				if (!strategyService.isAddStrategyAction()) {
					tacticService.retrieveTactics($scope.params.planId, strategy.planStrategyId)
					.then(function(response) {
						$scope.tactics = response;
						utilService.getLogger().info(response);
						$scope.isEmptyTactic = utilService.isEmptyArray($scope.tactics) ? true : false;
						$scope.strategyRow.convertTacticsDueDateToISODate();
						var tacticsIndex = tacticService.getTacticsIndex();
						// refresh delegate/comment pane
						if (utilService.isNotEmptyArray($scope.tactics)) {
							$scope.tacticRow.select($scope.tactics[tacticsIndex], tacticsIndex, true);
						} else {
							$scope.tactics  		= undefined;
							$scope.tacticRow.index 	= undefined;
							$scope.comments 		= undefined;
							$scope.delegateRow.index = undefined;
						}
					})
					.catch(function(error) {
						utilService.getLogger().error(error.data.detail);
						$scope.clientError.message = error;
					})
				} 
				
				if (strategyService.isAddStrategyAction()) {
					strategyService.setAddStrategyAction(false);
				}
			},
			
			convertTacticsDueDateToISODate: function() {
				if (utilService.isNotEmptyArray($scope.tactics)) {					
					var i;
					for (i = 0; i < $scope.tactics.length; i++) {
						$scope.tactics[i].isoDate = utilService.convertToISODate($scope.tactics[i].dueDate);					
					}
					
				}
			},
			
			clearSelection: function() {
				$scope.strategyRow.data = undefined;
				$scope.strategyRow.index = undefined;
			}
		}
		
		permissionsValidator.setCustomPermission('tactic-add', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('tactic-edit', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('tactic-delete', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('tactic-drag', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);

		// Handles any functionality pertaining to the tactic row
		$scope.tacticRow = {
				data: undefined,
				index: undefined,
				parentList: undefined,
				hasPermission: function(action) {
					return permissionsValidator.hasPermission(action, $scope.plan);				
				},
				dragOptions: {
					orderChanged: function(event) {
						
						utilService.getLogger().info(event.dest.sortableScope.modelValue[event.dest.index]);
						utilService.getLogger().info(event.source.index);
						utilService.getLogger().info(event.dest.index);
						
						var container = {
							sortOrders: event.dest.sortableScope.modelValue,
							securityProfile: {
								fullName: userProfile.getFullName()
							}
						}
						
						tacticService.updateSortOrders($scope.params.planId, $scope.strategyRow.data.planStrategyId, container)
						.then(function(response) {
							var dragged = event.dest.sortableScope.modelValue[event.dest.index];
							$scope.tacticRow.select(dragged, event.dest.index, true, true);
						})
						.catch(function(error) {
							
						})
										
					},				
					containment: '#tactic-container',
					containerPositioning: 'relative',
				},
				select: function(tactic, index, reloadAction, dragAction) {
					
					// Disable 'select' if in edit mode
					if ($scope.updateComments.isAddEditMode) {
						return;
					}
					
					var reloadAgain = utilService.isNotEmpty(reloadAction) ? reloadAction : false; 
					var drapOption  = utilService.isNotEmpty(dragAction) ? dragAction :  false;
					
					// Return if user clicks same row
					var    addTacticsAction = tacticService.isAddTacticsAction();
					var updateTacticsAction = tacticService.isUpdateTacticsAction();
					var tactServiceStoredIndex = tacticService.getTacticsIndex();
					
					if (!reloadAgain &&	tactServiceStoredIndex == index) {
						return;
					} else {
						// If user clicks different tactics row
						if (tactServiceStoredIndex != index  && !dragAction) {
							tacticService.setTacticsIndex(index);
							commentService.setDelegateIndex(0);
						}
					}
					
					$scope.tacticRow.index 	   = index;
					$scope.tacticRow.data 	   = tactic;
					
					//gotoAnchor(index);
					
					
					loadDelegates();
					var i;
					if (utilService.isNotEmpty(tactic) && utilService.isNotEmptyArray(tactic.planTacticDelegates)) {
						for (i = 0; i < tactic.planTacticDelegates.length; i++) {
						    var addedUser = utilService.filterBy($scope.listDelegates,{delegateName: tactic.planTacticDelegates[i].planDelegate.delegateName},true);
						    if(addedUser.length === 0) continue;
						    var participant = {
						    	action: undefined,
						    	fullName: addedUser[0].delegateName,
						    	branchLocation: addedUser[0].delegateLocation.substring(0,2)
						    }
						    $scope.addRemoveDelegates.push(participant);
//						    var index;
//						    for (index = 0; index < $scope.listDelegates.length; index++) {
//						    	if (tactic.planTacticDelegates[i].planDelegate.delegateName === $scope.listDelegates[index].delegateName) {
//						    		$scope.listDelegates[index].planTacticDelegateId = tactic.planTacticDelegates[i].planTacticDelegateId;
//									$scope.addRemoveDelegates.push($scope.listDelegates[index]);
//									$scope.listDelegates.splice(index, 1);
//						    		break;
//						    	}
//						    }
						}
					}
						
					// focus delegate in Comments
					if (utilService.isNotEmpty(tactic) && utilService.isNotEmptyArray(tactic.planTacticDelegates)) {
						var delegateIndex = utilService.isEmpty(commentService.getDelegateIndex()) ? 0 : commentService.getDelegateIndex();  
						$scope.delegateRow.select(tactic.planTacticDelegates[delegateIndex], delegateIndex);
					}
						
					if (utilService.isNotEmpty(tactic)) {
						commentService.retrieveComments($scope.params.planId, $scope.strategyRow.data.planStrategyId, tactic.planTacticId)
						.then(function(response) {
							$scope.comments = response;
						})
						.catch(function(error) {
							utilService.getLogger().error(error.data.detail);
							$scope.clientError.message = error;
						})
					}

					if (addTacticsAction) {
						tacticService.setAddTacticsAction(false);
					}
					
					if (updateTacticsAction) {
						tacticService.setUpdateTacticsAction(false);	
					}
				},
				
				clearSelection: function() {
					$scope.tacticRow.data = undefined;
					$scope.tacticRow.index = undefined;
				}
			}
		
		
		permissionsValidator.setCustomPermission('delegate-add', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('delegate-edit', [roles.ADMIN, roles.GM, roles.OWNER, roles.DELEGATE, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('delegate-delete', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('delegate-mail', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		
		$scope.delegateRow = {
				data: undefined,
				index: undefined,
				hasPermission: function(action) {
					return permissionsValidator.hasPermission(action, $scope.plan);				
				},
				enableDone: function(delegate) {										
					
					if ( utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), roles.OWNER) || utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), roles.ADMIN) ) {
						return false;
					}
					
					if (utilService.equalsIgnoreCaseSpace(userProfile.getFullName() , delegate.planDelegate.delegateName)) {
						return false;
					}
										
					return true;
					
				},
				
				select: function(delegate, index) {
					commentService.setDelegateIndex(index);
					$scope.delegateRow.index = index;
					$scope.delegateRow.data = delegate;
				},
				
				done: function() {
					$scope.updateComments.add(true);
				},
				
				remove: function(delegate) {
					tacticDelegateService.remove($scope.params.planId, $scope.strategyRow.data.planStrategyId, $scope.tacticRow.data.planTacticId, delegate.planTacticDelegateId)
					.then(function() {
						commentService.setDelegateIndex(0);
						loadTacticsOnly();
					})
					.catch(function(error) {
						
					})
				}
		}
		
		var removeDelegateId = tacticService.getRemoveDelegateId();
		var assignDelegateId = tacticService.getAssignDelegateId();
		// for html
		$scope.assigned = assignDelegateId;
				
		var Actions = tacticService.getActions();
		
		$scope.STCButtons={
			enabled: true,
						
			enableButtons: function() {
				$scope.STCButtons.enabled = true;
			},
			
			disableButtons: function() {
				$scope.STCButtons.enabled = false;
			},
			
			clear: function() {
				$scope.STCButtons.enableButtons();
			}
		}
	
		$scope.STCPens={
			enabled: true,
			
			enablePens: function() {
				$scope.STCPens.enabled = true;
			},
			disablePens: function() {
				$scope.STCPens.enabled = false;
			},
			
			clear: function() {
				$scope.STCPens.enablePens();
			}
		}

		$scope.STCTrashes={
				enabled: true,
				
				enableTrashes: function() {
					$scope.STCTrashes.enabled = true;
				},
				
				disableTrashes: function() {
					$scope.STCTrashes.enabled = false;
				},
				
				clear: function() {
					$scope.STCTrashes.enableTrashes();
				}
		}
				
		$scope.updateStrategy={
			data: undefined,
			show: false,
			index: undefined,
			strategy: undefined,
			isAddEditMode: false,			
			
			update: function() {
				if ($scope.updateStrategy.isAddEditMode) { // add strategy
					
					var sIndex = utilService.isEmptyArray($scope.strategies) ? 0 :  $scope.strategies.length - 1;
					strategyService.setStrategyIndex(sIndex);
					
					$scope.updateStrategy.setUpStrategyData();
					
					userInfoService.checkUserInfo()
					.then(function() {
						strategyService.addStrategy($scope.params.planId)
						.then(function(response) {
							loadStrategiesOnly();
							$scope.updateStrategy.clear();
						})
						.catch(function(error) {
							utilService.getLogger().error(error);
							$scope.updateStrategy.errorMsg = error;
						});
					})
					.catch(function(error) {
						$scope.updateStrategy.errorMsg = error;
					})
				} else { // edit strategy
					$scope.updateStrategy.setUpStrategyData();
					
					userInfoService.checkUserInfo()
					.then(function() {
						strategyService.updateStrategy($scope.params.planId)
						.then(function(response) {
							loadStrategies();
							$scope.updateStrategy.clear();
						})
						.catch(function(error) {
							utilService.getLogger().error(error);
							$scope.updateStrategy.errorMsg = error;
						});
					})
					.catch(function() {
						$scope.updateStrategy.errorMsg = error;
					})
				}
			},
			
			add: function() {
				
				$scope.updateStrategy.clear();
				$scope.updateStrategy.isAddEditMode = true;
				disableSTCFeatures();
				$scope.updateStrategy.swapShow();				
			},
			
			close: function() {
				$scope.updateStrategy.clear();
			},
			
			swapShow: function() {
				$scope.updateStrategy.show = !$scope.updateStrategy.show; 
			},
			
			setUpStrategyData: function() {
				var strategy = strategyService.getStrategy();
				strategy.planStrategy = $scope.updateStrategy.data;
				strategyService.setStrategy(strategy);
			},
			
			clear: function() {
				enableSTCFeatures();
				$scope.updateStrategy.data = undefined;
				$scope.updateStrategy.show = false;
				$scope.updateStrategyForm.$setPristine();
				$scope.updateStrategy.isAddEditMode = false;				
				strategyService.setStrategy({});
			},
			
			edit: function(selectedStrategy, strategyIndex) {				
				$scope.updateStrategy.index = strategyIndex;
				
				$scope.updateStrategy.data = selectedStrategy.planStrategy;
				$scope.updateStrategy.isAddEditMode = false;
				disableSTCFeatures();
				strategyService.setStrategy(selectedStrategy);
				$scope.updateStrategy.swapShow();
			},
			
			isUpdateTacticsDisabled: function() {
				utilService.getLogger().info(updateStrategyForm.$dirty + ' updateTacticsForm.$dirty');
				return !updateStrategyForm.$dirty && updateTacticsForm.$dirty;   
			}
		}
		
		$scope.showDelegatesTable = function() {
			return utilService.isNotEmptyArray($scope.addRemoveDelegates) ? true : false;
		}
		
		$scope.showActionImage = function(actionId) {
			return true;
//			switch(actionId) {
//				case removeDelegateId:
//				case assignDelegateId:
//					return true;
//				default:
//					return false;
//				}
		}
		
		$scope.showActionTooltip = function(actionId) {
			if (utilService.isEmpty(actionId)) {
				return '';
			} else {
				return Actions[actionId].toolTip;
			}
		}
		
		$scope.showClassContent = function(actionId) {
			if (utilService.isEmpty(actionId)){
				return '';
			} else {
				return Actions[actionId].classContent;
			}
			
		}
		
		function enableSTCFeatures() {
			$scope.STCButtons.clear();
			$scope.STCPens.clear();
			$scope.STCTrashes.clear();
		}

		function disableSTCFeatures() {
			$scope.STCButtons.disableButtons();
			$scope.STCPens.disablePens();
			$scope.STCTrashes.disableTrashes();
		}	
	
		function setUpdateTacticForm() {
			if (utilService.isNotEmptyArray($scope.addRemoveDelegates)) {
				var assignDelegatesList = utilService.filterBy($scope.addRemoveDelegates,{action: assignDelegateId},true);
				var removeDelegatesList = utilService.filterBy($scope.addRemoveDelegates,{action: removeDelegateId},true);
			
				if (utilService.isNotEmptyArray(assignDelegatesList) || utilService.isNotEmptyArray(removeDelegatesList)) {
					$scope.updateTacticsForm.$dirty = true;
				}
			}
		}
		
		$scope.updateDelegates = {
				selected: undefined,
				dropDownText: 'Add',
				
				changeTitle: function(selectedDelegateTitle, index) {
					var j;
					var remaingDelegateList = [];
					for (j = 0; j < $scope.listDelegates.length; j++) {
						if ($scope.listDelegates[j].delegateTitle === selectedDelegateTitle) {
							$scope.listDelegates[j].action = assignDelegateId;
							$scope.addRemoveDelegates.push($scope.listDelegates[j]);
						} else {
							remaingDelegateList.push($scope.listDelegates[j]);
						}
					}
					
					$scope.listDelegates = remaingDelegateList;
					
					$scope.listDelegateTitles.splice(index, 1);
					
					setUpdateTacticForm();
				},
				setSelected: function(item) {
					$scope.updateDelegates.selected = item;
					$scope.updateDelegates.change();
				},
				
//				change: function(selectedDelegate, index) {
//					//$scope.updateDelegates.selected = selectedDelegate;	
//					$scope.addRemoveDelegates.push($scope.updateDelegates.selected);
////					if (utilService.isEmpty(selectedDelegate)) {
////						var i;
////						for (i = 0; i < $scope.listDelegates.length; i++) {
////							$scope.listDelegates[i].action = assignDelegateId;
////						}
////						$scope.addRemoveDelegates = $scope.addRemoveDelegates.concat($scope.listDelegates);
////						$scope.listDelegates = [];
////						$scope.listDelegateTitles = [];
////					} else {
////						if (utilService.isNotEmpty(index) && 
////							utilService.isNotEmptyArray($scope.listDelegates) &&
////							$scope.listDelegates.length > index) {
////							$scope.listDelegates[index].action = assignDelegateId;
////							$scope.addRemoveDelegates.push($scope.listDelegates[index]);
////							$scope.listDelegates.splice(index, 1);
////							loadDelegateTitles();
////						}
////					}
//					setUpdateTacticForm();
//				},
				change: function() {
										
//					var participant = {
//						action: assignDelegateId,
//						delegateName: $scope.updateDelegates.selected.fullName,
//						delegateLocation: $scope.updateDelegates.selected.branchLocation
//					}
//					
					$scope.updateDelegates.selected.action = 1;
					
					$scope.addRemoveDelegates.push($scope.updateDelegates.selected);
					loadDelegateTitles();
					setUpdateTacticForm();
					
					$scope.updateDelegates.test = undefined;
				},
				
				done: function() {
					
					var container = tacticDelegateService.getNewPlanTacticDelegateContainer();
					
					container.securityProfile.fullName = userProfile.getFullName();
					container.planTacticDelegate.planTacticDelegateId = $scope.delegateRow.data.planTacticDelegateId;
					container.planTacticDelegate.planTacticComplete = $scope.updateComments.planTacticComplete;
				
					userInfoService.checkUserInfo()
					.then(function() {
						tacticDelegateService.update($scope.params.planId, $scope.strategyRow.data.planStrategyId, $scope.tacticRow.data.planTacticId, container)
						.then(function() {
							$scope.updateComments.close();						
						})
						.catch(function(error) {
							
						})
					})
					.catch(function(error) {
						
					})
					
				},
				reset: function() {
					$scope.updateDelegates.selected = undefined;
				}
		}
		
		$scope.resetRemovedDelegate = function(index) {
			if (utilService.isNotEmpty(index) && 
				utilService.isNotEmptyArray($scope.addRemoveDelegates) &&
				$scope.addRemoveDelegates.length > index) {
				//var action = $scope.addRemoveDelegates[index].action;
				
				if ($scope.addRemoveDelegates[index].action == undefined) {
					$scope.addRemoveDelegates[index].action = 0;
				} else {
					$scope.addRemoveDelegates[index].action = undefined;
				}				 
				
				setUpdateTacticForm();				
			}
		}
		
		$scope.removeAddedDelegate = function(index) {
			if (utilService.isNotEmpty(index) &&
				utilService.isNotEmptyArray($scope.addRemoveDelegates) &&
				$scope.addRemoveDelegates.length > index) {
				
				$scope.addRemoveDelegates[index].action = undefined;
				$scope.listDelegates.push($scope.addRemoveDelegates[index]);
				loadDelegateTitles();
				$scope.addRemoveDelegates.splice(index, 1);
				setUpdateTacticForm();
			}
		}
		
		function setUpTacticsDelegatesDate() {
			var assignDelegates = [];
			var removeDelegates = [];
			if (utilService.isNotEmptyArray($scope.addRemoveDelegates)) {
				var i;
				var actionId;
				
			
				for (i = 0; i < $scope.addRemoveDelegates.length; i++) {
					actionId = $scope.addRemoveDelegates[i].action;
					if (utilService.isNotEmpty(actionId)) {
						switch(actionId) {
							case removeDelegateId:
								removeDelegates.push($scope.addRemoveDelegates[i]);
								break;
							case assignDelegateId:
								assignDelegates.push($scope.addRemoveDelegates[i]);
								break;
							default:
								break;
						}					
					}
				}
			}
			
			tacticService.setAssignDelegates(assignDelegates);
			tacticService.setRemoveDelegates(removeDelegates);
		}
		
		function setUpTacticsDueDate() {
			if (utilService.isNotEmpty($scope.updateTactics.dueDate)) {
				
				utilService.getLogger().info(new Date($scope.updateTactics.dueDate).toISOString().substr(0, 10))
				
				tacticService.setDueDate($scope.updateTactics.dueDate);
			}
		}
		
		function setUpTacticsDescription() {
			tacticService.setPlanTacticDescription($scope.updateTactics.data);
		}
		
		
		
		function setUpTacticsUpdateData() {
			if (utilService.isNotEmpty($scope.updateTactics.tactic)) {
				tacticService.setPlanTactic($scope.updateTactics.tactic);
			} else {
				tacticService.setPlanTactic({});
			}
			
			tacticService.setPlanId($scope.params.planId);
			tacticService.setPlanStrategyId($scope.strategyRow.data.planStrategyId);
			
			setUpTacticsDelegatesDate();
			setUpTacticsDueDate();
			setUpTacticsDescription();
			tacticService.setUpPlanTacticContainer();
			var testTactic = tacticService.getPlanTacticContainer(); 
			tacticService.createUpdateTacticsResource();
			testTactic = tacticService.getPlanTacticContainer();
		}
		
		$scope.updateTactics = {
			isDatePickerOpened: false,
			dueDate:			undefined,
			shortDueDate:		{},
			data:				undefined,
			delegates:			[],
			index: 				undefined,
			strategyIndex:		undefined,
			show: 				false,
			tactic: 			undefined,
			isAddEditMode:      false,			
			dateOptions: 		{
									dateDisabled: false,
								    formatYear: 'yyyy',
								    startingDay: 1,
								    showWeeks: false
								},
				
			update: function() {
				if (utilService.isEmpty($scope.updateTactics.dueDate)) {
					alert("Please enter date!");
					return;
				}
				
				if (utilService.isEmpty($scope.updateTactics.data)) {
					alert("Please enter plan tactic!");
					return;
				}

				// disable html strategy
				setUpTacticsUpdateData();
				if (tacticService.isAddPlanTactics()) {
					
					userInfoService.checkUserInfo()
					.then(function() {
						tacticService.addPlanTactic()
						.then(function(response) {
							$scope.updateTactics.strategyIndex	= $scope.strategyRow.index;;
							$scope.updateTactics.index = utilService.isEmptyArray($scope.tactics) ? 0 :  $scope.tactics.length;
							var addTacticsIndex = utilService.isEmptyArray($scope.tactics) ? 0 :  $scope.tactics.length;
							tacticService.setTacticsIndex(addTacticsIndex);
							loadTacticsOnly();
						})
						.catch(function(error) {
							utilService.getLogger().error(error.data.detail);
							$scope.clientError.message = error;
						})
					})
					.catch(function(error) {
						$scope.clientError.message = error;
					})
				}
				if (tacticService.isUpdatePlanTactics()) {
					
					userInfoService.checkUserInfo()
					.then(function() {
						tacticService.updatePlanTactic()
						.then(function(response) {
							$scope.updateTactics.index 			= $scope.tacticRow.index;
							$scope.updateTactics.strategyIndex	= $scope.strategyRow.index;;
							loadTacticsOnly();
						})
						.catch(function(error) {
							utilService.getLogger().error(error.data.detail);
							$scope.clientError.message = error;
						})
					})
					.catch(function(error) {
						$scope.clientError.message = error;
					})
				}
				$scope.updateTactics.swapShow();
				$scope.updateTactics.clear();
			},
			
			add: function() {				
				loadDelegates();
				$scope.updateTactics.isAddEditMode = true;
				disableSTCFeatures();
				$scope.updateTactics.swapShow();
			},
				
			close: function() {
				
				// This resets the actions of the selected tactic delegate list
				// 0 - Removed User
				// 1 - Added User
				angular.forEach($scope.addRemoveDelegates, function(value, key) {
					
					switch(value.action) {
					case 1:						
						$scope.addRemoveDelegates.splice(key);
						break;
					default:
						value.action = undefined;
					}					
				})
				
				$scope.updateTactics.clear();
			},
				
			setUpTacticsData: function() {
				
			},
			
			openDatePicker: function() {
				$scope.updateTactics.isDatePickerOpened = true;
			},
			
			formatDueDate: function() {
			},
			
			clear: function() {
				$scope.updateTacticsForm.$setPristine();
				$scope.updateTactics.isDatePickerOpened = false;
				$scope.updateTactics.dueDate = undefined;
				$scope.updateTactics.data = undefined;
				$scope.updateTactics.delegates = [];
				$scope.updateTactics.show = false;
				$scope.updateTactics.isAddEditMode = false;				
				$scope.updateTactics.tactic = undefined;				
				enableSTCFeatures();
			},
			
			edit: function(tactic, index) {
				disableSTCFeatures();
				$scope.updateTactics.isAddEditMode = true;
				if (utilService.isNotEmpty(tactic)) {
					$scope.updateTactics.tactic = tactic;
					$scope.updateTactics.data = tactic.planTactic;
					$scope.updateTactics.dueDate = tactic.dueDate;
				}
				if (utilService.isNotEmpty(index)) {
					$scope.updateTactics.index = index;
				}
				
				$scope.updateTactics.swapShow();
			},
				
			swapShow: function() {
				$scope.updateTactics.show = !$scope.updateTactics.show; 
			},
		}
		
		permissionsValidator.setCustomPermission('comment-add', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('comment-edit', [roles.ADMIN, roles.GM, roles.OWNER, roles.DELEGATE, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		permissionsValidator.setCustomPermission('comment-delete', [roles.ADMIN, roles.GM, roles.OWNER, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER], undefined);
		
		$scope.commentRow = {
			data: undefined,
			index: undefined,
			deepCopy: undefined,
			hasPermission: function(action) {
				return permissionsValidator.hasPermission(action, $scope.plan);				
			},						
			select: function(comment, index) {				
				$scope.commentRow.index = index;
				$scope.commentRow.data = comment;				
				$scope.commentRow.deepCopy = utilService.deepCopy(comment);				
			},
			permitEdit: function(comment) {
				
				if (userProfile.isAdmin()) {
					return true;
				}
				
				if (utilService.equalsIgnoreCaseSpace(comment.user,userProfile.getFullName())) {
					return true;
				}
				
				return false;
			},
			updateComment: function(comment) {
				
				var container = commentService.getNewPlanTacticCommentContainer();
				
				container.securityProfile.fullName = userProfile.getFullName();
				container.planTacticComment.tacticCommentId = comment.tacticCommentId
				container.planTacticComment.comment = comment.comment;
				
				commentService.updateComment($scope.params.planId, $scope.strategyRow.data.planStrategyId, $scope.tacticRow.data.planTacticId, container)
				.then(function(response) {
					$scope.updateComments.setUpdateFlag();
				})
				.catch(function(error) {
					
				})
			}
		}
		
		$scope.updateComments = {
				data: undefined,
				show: false,
				isAddEditMode: false,
				showDoneCheckbox: undefined,
				updating: false,
				byPass: false,
				edit: function(comment, index) {
					$scope.commentRow.select(comment, index);
					$scope.updateComments.setUpdateFlag();
				},
				cancel: function() {					
					$scope.commentRow.data.comment = $scope.commentRow.deepCopy.comment;
					$scope.updateComments.setUpdateFlag();
				},
				setUpdateFlag: function(comment, index) {					
					if (utilService.equalsIgnoreCaseSpace($scope.commentRow.data.user,userProfile.getFullName())){						
						return $scope.updateComments.updating = !$scope.updateComments.updating;
					}
					
					return false;
					
				},
				delete: function(comment, index) {
					
					utilService.deleteConfirmation('Delete Comment', 'You are about to delete the selected comment.', 'btn-danger', false).result
					.then(
						function(results) {
							if (results == true) {
								
								commentService.deleteComment($scope.params.planId, $scope.strategyRow.data.planStrategyId, $scope.tacticRow.data.planTacticId, comment.tacticCommentId)
								.then(function(response) {
									$scope.comments.splice(index,1);
								})
								.catch(function(error) {
									
								})
								
								
							}
						},
						function(dismiss) {
							
						}
					)
					
					
				},
				add: function(isDelegateDone) {					
					$scope.updateDelegates.isDelegateDone = isDelegateDone;
					$scope.updateComments.data = undefined;
					$scope.updateComments.isAddEditMode = true;
					disableSTCFeatures();
					$scope.updateComments.swapShow();
				},
				
				close: function() {
					
					if ($scope.updateDelegates.isDelegateDone) {
						$scope.delegateRow.data.planTacticComplete = !$scope.delegateRow.data.planTacticComplete;
					}
					
					$scope.updateComments.clear();
					$scope.tacticRow.select($scope.tacticRow.data, $scope.tacticRow.index);
				},
				
				showDone: function() {
					
					if ($scope.updateComments.showDoneCheckbox !== undefined) {
						return $scope.updateComments.showDoneCheckbox;
					}
											
					if ($scope.tacticRow.index === undefined) {						
						return $scope.updateComments.showDoneCheckbox;
					}
					
					angular.forEach($scope.tactics[$scope.tacticRow.index].planTacticDelegates, function(value, key) {
						if (utilService.equalsIgnoreCaseSpace(value.planDelegate.delegateName , userProfile.getFullName())) {
							$scope.updateComments.showDoneCheckbox = true;
							return $scope.updateComments.showDoneCheckbox;
						}
					})
					
					return false;
					
				},
				
				clear: function() {
					$scope.updateCommentsForm.$setPristine();
					$scope.updateCommentsForm.data = undefined;
					$scope.updateComments.show = false;
					$scope.updateComments.isAddEditMode = false;
					$scope.updateComments.planTacticComplete = false;
					enableSTCFeatures();
				},
				
				swapShow: function() {
					$scope.updateComments.show = !$scope.updateComments.show; 
				},
				
				update: function() {
					
					if ($scope.updateComments.planTacticComplete) {
												
						angular.forEach($scope.tactics[$scope.tacticRow.index].planTacticDelegates, function(value, key) {
							if (utilService.equalsIgnoreCaseSpace(value.planDelegate.delegateName , userProfile.getFullName())) {
								$scope.delegateRow.data.planTacticDelegateId = value.planTacticDelegateId;
							}
						})
						
						//$scope.delegateRow.data.planTacticComplete = $scope.updateComments.planTacticComplete;
						$scope.updateDelegates.done();
					}
					
					var container = commentService.getNewPlanTacticCommentContainer();
					
					container.securityProfile.fullName = userProfile.getFullName();
					container.planTacticComment.comment = $scope.updateComments.data;
					
					userInfoService.checkUserInfo()
					.then(function() {
						commentService.addComment($scope.params.planId, $scope.strategyRow.data.planStrategyId, $scope.tacticRow.data.planTacticId, container)
						.then(function() {
							$scope.updateComments.close();
							$scope.tacticRow.select($scope.tacticRow.data, $scope.tacticRow.index, true);
						})
						.catch(function(error) {
							
						})
					})
					.catch(function(error) {
						
					})
				}
		}
				
		$scope.trashStrategy = {
			remove: function(strategy) {
				if ( !window.confirm('Are you sure you want to remove this strategy?') ) {
					return;
				} else {
					strategyService.removeStrategy($scope.params.planId, strategy.planStrategyId)
					.then(function(response) {
						$scope.tactics  = undefined;
						$scope.comments = undefined;
						$scope.isEmptyTactic = true;
						strategyService.setStrategyIndex(0);						
						tacticService.setTacticsIndex(0);
						commentService.setDelegateIndex(0);
						loadStrategies();
					})
					.catch(function(error) {
						utilService.getLogger().error(error);
					});
				}
			}
		}
				
		$scope.trashTactics = {
				remove: function(tactic) {
					if ( !window.confirm('Are you sure you want to remove this tactic?') ) {
						return;
					} else {
						utilService.getLogger().info($scope.params.planId);
						utilService.getLogger().info($scope.strategyRow.data.planStrategyId);
						utilService.getLogger().info(tactic.planTacticId);
						tacticService.removeTactic($scope.params.planId, $scope.strategyRow.data.planStrategyId, tactic.planTacticId)
						.then(function(response) {
							tacticService.setTacticsIndex(0);
							commentService.setDelegateIndex(0);
							$scope.comments = undefined;
							loadTacticsOnly();
						})
						.catch(function(error) {
							utilService.getLogger().error(error);
						});
					}
				}
			}
		
//		$scope.canEdit = function() {
//			return userProfile.canEdit();
//		}
		
		function lookupPlan() {
			planService.lookupPlan($scope.params.planId)
			.then(function(response) {
				$scope.plan = response;
				planService.setPlan($scope.plan);
			})
			.catch(function(error) {
				utilService.getLogger().error(error.data.detail);
				$scope.clientError.message = error;
			})
		}
		
		function getStrategyIndex() {
			var sIndex = 0;
			
			if (utilService.isNotEmpty($scope.updateStrategy.index)) {
				sIndex = $scope.updateStrategy.index;
			}  
			
			return sIndex;
		}
				
		// $scope.isPlanNotEditable = function() {
		// 	return planService.isNotCurrentYear();
		// }
		
		// $scope.isPlanEditable = function() {
		// 	return planService.isCurrentYear();
		// }
		
		function loadStrategiesOnly() {
			strategyService.retrieveStrategies($scope.params.planId)
			.then(function(response) {
				$scope.strategies = response;
				$scope.isEmptyStrategy = utilService.isEmptyArray($scope.strategies) ? true : false;
				$scope.strategyRow.data		= $scope.strategies;
				$scope.strategyRow.index 	= utilService.isEmptyArray($scope.strategies) ? 0 :  $scope.strategies.length;
				var strategyIndex = utilService.isEmptyArray($scope.strategies) ? 0 :  $scope.strategies.length - 1;
				strategyService.setAddStrategyAction(true);
				strategyService.setStrategyIndex(strategyIndex);
				// This is used as focus only.
				$scope.strategyRow.select($scope.strategies[strategyIndex],strategyIndex, true);
				// All contacts/comments will be reset
				$scope.tactics  		= undefined;
				$scope.tacticRow.index 	= undefined;
				$scope.comments 		= undefined;
				$scope.delegateRow.index = undefined;
			})
			.catch(function(error) {
				utilService.getLogger().error(error.data.detail);
				$scope.clientError.message = error;
			})
		}
		
		function loadTacticsOnly() {
			var strategy = strategyService.getStrategy();
			
			if (utilService.isNotEmpty(strategy)) {
				tacticService.retrieveTactics($scope.params.planId, strategy.planStrategyId)
				.then(function(response) {
					$scope.tactics = response;
					$scope.isEmptyTactic = utilService.isEmptyArray($scope.tactics) ? true : false;
					$scope.strategyRow.convertTacticsDueDateToISODate();
					var tacticsIndex = tacticService.getTacticsIndex();
					$scope.tacticRow.select($scope.tactics[tacticsIndex], tacticsIndex, true);
				})
				.catch(function(error) {
					utilService.getLogger().error(error.data.detail);
					$scope.clientError.message = error;
				})
			}
		}
		
		function waitingRetrieveTacticsAsyncCallToFinish() {
			var stopRetrieveTactics = undefined;
			var delayMilliseconds = utilService.getDelayMilliseconds();
			stopRetrieveTactics = $interval(function() {
				if (!tacticService.getRetrieveTacticsDone()) {
					utilService.getLogger().info('waiting on tacticService retrieve tactics to complete...');
				} else {
					$interval.cancel(stopRetrieveTactics);
					stopRetrieveTactics = undefined;
					tacticService.setRetrieveTacticsDone(false);
				}
			}, delayMilliseconds);
		}

		
		function handleParamsStrategyIdAndTacticId() {
			if (utilService.isNotEmpty($scope.params.planStrategyId) && 
				utilService.isNotEmpty($scope.params.planTacticId) 	) {
					strategyService.setStrategies($scope.strategies);
					// Fetch plan data
					lookupPlan();
					
					var foundStrategy = strategyService.findPlanStrategyIndexByPlanStrategyId($scope.params.planStrategyId);
					
					// Display Not Found message if strategy doesn't exist
					if (!foundStrategy) {
						$scope.notFound.item = 'Strategy';
						return;
					}
					
					tacticService.findPlanTacticIndexByPlanTacticId($scope.params.planId, $scope.params.planStrategyId, $scope.params.planTacticId)
					.then(function(response) {
					
						// Display Not Found message if tactic doesn't exist
						if (!response) {
							$scope.notFound.item = 'Tactic'
						}
						
					})
					.catch(function(response) {
						utilService.getLogger().error(error.data.detail);
						$scope.clientError.message = error;
					})					
				}
		}
		
		var stop;
			
		function loadStrategies() {
			
			strategyService.retrieveStrategies($scope.params.planId)
			.then(function(response) {
				$scope.strategies = response;
				$scope.isEmptyStrategy = utilService.isEmptyArray($scope.strategies) ? true : false;
				handleParamsStrategyIdAndTacticId();
				
				// If incoming plan Id is not the same in stored plan id (if set), then reset all indices
				var storedPlanId = strategyService.getPlanId();
				if (utilService.isNotEmpty(storedPlanId) && 
					storedPlanId != $scope.params.planId	) {
					strategyService.setStrategyIndex(0);
					tacticService.setTacticsIndex(0);
					commentService.setDelegateIndex(0);
				} 
				
				strategyService.setPlanId($scope.params.planId);
				
				var strategyIndex = utilService.isEmpty(strategyService.getStrategyIndex()) ? 0 : strategyService.getStrategyIndex();
				
				if (utilService.isEmptyArray($scope.strategies)) {
					return;
				}
				
				$scope.strategyRow.select(response[strategyIndex],strategyIndex, true);
				strategyService.setStrategy($scope.strategies[strategyIndex]);
				
//				stop = $interval(function() {
//					if (utilService.isEmpty($scope.tactics) == true) {
//						utilService.getLogger().info('waiting on strategyRow.select to complete...');
//					} else {
//						$interval.cancel(stop);
//						stop = undefined;
//						var tacticsIndex = utilService.isEmpty(tacticService.getTacticsIndex()) ? 0 : tacticService.getTacticsIndex();
//						// retrieve tactics data
//						$scope.tacticRow.select($scope.tactics[tacticsIndex], tacticsIndex, true);
//						if (utilService.isNotEmpty($scope.updateTactics.index)) {
//							$scope.updateTactics.index  = undefined;
//						}
//					}
//				}, 500)
		
			})
			.catch(function(error) {
				utilService.getLogger().error(error.data.detail);
				$scope.clientError.message = error;
			})
		}
		
		function loadDelegateTitles() {
			var i, j;
			$scope.listDelegateTitles = [];
			for (i = 0; i < $scope.listDelegates.length; i++) {
				var found = false;
				if (utilService.isNotEmptyArray($scope.listDelegateTitles)) {
					for (j = 0; j < $scope.listDelegateTitles.length; j++) {
						if ($scope.listDelegateTitles[j].delegateTitle === $scope.listDelegates[i].delegateTitle) {
							found = true;
							break;
						}
					}
					if (!found) {
						$scope.listDelegateTitles.push($scope.listDelegates[i]);
					}
				} else {
					$scope.listDelegateTitles.push($scope.listDelegates[i]);
				}
			}			
		}

		$scope.isCoOwner = function() {
			var isCoOwnerB = false;
			if (utilService.isNotEmptyArray($scope.listDelegates)) {
				var i;
				for (i = 0; i < $scope.listDelegates.length; i++) {
					if (utilService.equalsIgnoreCaseSpace(userProfile.getFullName() ,  $scope.listDelegates[i].delegateName)) {
						if ($scope.listDelegates[i].delegateType.delegateType === 'CoOwner') {
							isCoOwnerB = true;
						}
					}
				}
			}
			return isCoOwnerB;
		}
		
		function loadDelegates() {
			
			$scope.listDelegates 	  = utilService.deepCopy(contactService.getDelegates());
			
			loadDelegateTitles();
			
			$scope.addRemoveDelegates 	  = [];
			
			$scope.isCoOwnerType =  $scope.isCoOwner();
		}
		
		// Loads plan contacts
		function loadContacts() {
			contactService.retrieveContactsWithOwnerDelegate($scope.params.planId)
			.then(function(response) {
				$scope.listDelegates 	  = contactService.getDelegates();
				$scope.addRemoveDelegates 	  = [];
			})
			.catch(function(error) {
				$scope.clientError.message = error;
			})
		}
		
		function gotoAnchor(index) {
			var newHash = 'tactic' + index;
		     
			if ($location.hash() !== newHash) {
				$location.hash(newHash);
			} else {
				$anchorScroll();
			}
			
		}
		
		loadContacts();
		
		loadStrategies();

		
	}

export default Strategies;