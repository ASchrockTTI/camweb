import nuggetSummaryTemplate from './nuggetSummary.html';

const nuggetSummary =  {
		template: nuggetSummaryTemplate,
		controller: function(
			userProfile,
			nuggetsService,
			planService,
			userInfoService,
			utilService,
			$rootScope,
			$scope,
			$state
		) {

			var vm = this;
			vm.criteria = {
				status: {
					open: true,
					hold: false,
					won: false,
					lost: false
				}
			};
			
			vm.$onInit = function() {
				if (sessionStorage.getItem('nuggetSort')) {
					var sort = JSON.parse(sessionStorage.getItem('nuggetSort'));
					vm.sort.type = sort.type;
					vm.sort.reverse = sort.reverse;
				}
				
				if (sessionStorage.getItem('nuggetCriteria')) {
					vm.criteria = JSON.parse(sessionStorage.getItem('nuggetCriteria'));
					
					if (vm.criteria.endDate) {
						var endDateString = vm.criteria.endDate;
						if(endDateString == 'undefined' || endDateString == 'null') vm.criteria.endDate = undefined;
						else vm.criteria.endDate = new Date(endDateString);
					}
					
					if (vm.criteria.startDate) {
						var startDateString = vm.criteria.startDate;
						if(startDateString == "undefined" || startDateString == 'null') vm.criteria.startDate = undefined;
						else vm.criteria.startDate = new Date(startDateString);
					}
					
				} else {
					var startDate = new Date();
					startDate.setMonth(startDate.getMonth() - 12);
					vm.criteria.startDate = startDate;
					vm.initialize = true;
				}
				
				
				if (sessionStorage.getItem('nuggetTextFilter')) vm.filterText = JSON.parse(sessionStorage.getItem('nuggetTextFilter'));
				
				vm.userProfile = userProfile.getUserProfile();
				if(vm.userProfile) initializeCriteria();
			}
			
			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {
				if(!vm.userProfile) {
					vm.userProfile = userProfile.getUserProfile();
					initializeCriteria();
				}
			})
			
			function initializeCriteria () {
				vm.role = userProfile.getUserType();
				
				switch(vm.role.toUpperCase()) {
					case 'ADMIN':
						planService.getParentBranchList().then(
							function(response) {
								vm.branchList = response;
								vm.getNuggets(true);
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						if(vm.initialize) {
							vm.criteria.showMine = true;
						}
						break;
					case 'EXECUTIVE':
						planService.getEntityParentBranches(userProfile.getEntity()).then(
							function(response) {
								vm.branchList = [];
								
								for(var i = 0; i < response.length; i++) {
									if(vm.branchList.includes(response[i].fullParentBranchName)) continue;
									vm.branchList.push(response[i].fullParentBranchName);
								}
								if(vm.initialize) {
									vm.criteria.branches = [userProfile.getBranch()];
								}
								vm.getNuggets(true);
							})
							.catch(function(error) {
								vm.branchList = [];
							})
						break;
					case 'SAM_GAM':
						userInfoService.getUserCorpIdList(userProfile.getSAMAccountName()).then(
								function(response) {
								vm.corpIdList = response;
								if(vm.initialize) {
									if(vm.corpIdList.length > 0) vm.criteria.corpId = vm.corpIdList[0];
									else {
										vm.criteria.showMine = true;
										vm.criteria.ownerUsername = userProfile.getSAMAccountName();
										vm.criteria.ownerFullName = userProfile.getFullName();
									}
								}
								vm.getNuggets(true);
						});
						break;
					case 'FSR':
					case 'ISR':
						if(vm.initialize) {
							vm.criteria.ownerUsername = userProfile.getSAMAccountName();
							vm.criteria.ownerFullName = userProfile.getFullName();
						}
						vm.getNuggets(true);
						break;
					case 'RVP':
					case 'GM':
					case 'FSM':
					case 'RBOM':
					case 'ISM':
					default:
						vm.branchList = userProfile.getBranchList();
						if(vm.initialize) {
							vm.criteria.branches = [userProfile.getBranch()];
						}
						vm.getNuggets(true);
						break;
				}
			}
			
			vm.getNuggets = function(checkCache) {
				if(vm.loadingNuggets) return;
				vm.loadingNuggets = true;
				vm.saveCriteria();
				
				vm.initialize = false;
				
				vm.nuggets = undefined;
				nuggetsService.getNuggetsByCriteria(vm.criteria).then(
					function(response) {
						vm.nuggets = response;
						vm.loadingNuggets = false;
					})
					.catch(function(error) {
						vm.nuggets = [];
						vm.loadingNuggets = false;
					})
			}
			
			vm.openSearchModal = function() {
				utilService
					.uibModal()
					.open({
						component: "nuggetSearchModal",
						size: "md",
						resolve: {
							modalData: function () {
								return {
									criteria: vm.criteria,
									branchList: vm.branchList,
									corpIdList: vm.corpIdList
								}
							}
						}
					})
					.result.then(
						function(close) {
							vm.getNuggets();
						},
						function(dismiss) {
						});
			};
			
			vm.filterOwner = function(owner) {
				if(owner && vm.criteria.ownerFullName === owner) return;
				
				if(!owner || vm.criteria.ownerFilter === owner) {
					vm.criteria.ownerFilter = undefined;
				} else vm.criteria.ownerFilter = owner;
				vm.saveCriteria();
			}
			
			vm.filterManufacturer = function(manufacturer) {
				if(!manufacturer || vm.criteria.manufacturerFilter === manufacturer.manufacturerCode) {
					vm.criteria.manufacturerFilter = undefined;
				} else vm.criteria.manufacturerFilter = manufacturer.manufacturerCode;
				vm.saveCriteria();
			}
			
			vm.filterCommodity = function(commodity) {
				if(!commodity || vm.criteria.commodityFilter === commodity.commodity) {
					vm.criteria.commodityFilter = undefined;
				} else vm.criteria.commodityFilter = commodity.commodity;
				vm.saveCriteria();
			}
			
			$scope.statusFilter = function() {
			  return function( nugget ) {
			    switch(nugget.nuggetStatus.nuggetStatusID) {
			    case 1:
			    	return vm.criteria.status.open;
			    case 2:
			    	return vm.criteria.status.won;
			    case 3:
			    	return vm.criteria.status.lost;
			    case 4:
			    	return vm.criteria.status.hold;
			    default:
			    	return true;
			    }
			  };
			};
			
			vm.nuggetStatusColor = function(nuggetStatus) {
				switch(nuggetStatus.nuggetStatusID) {
			    case 1:
			    	return '#276ab0';
			    case 2:
			    	return '#4c964c';
			    case 3:
			    	return '#d74545';
			    case 4:
			    	return '#d79230';
			    default:
			    	return 'gray';
			    }
			}
			
			vm.getNuggetEntity = function(nugget) {
				return nuggetsService.getEntityTypeFromNugget(nugget);
			}

			vm.getNuggetListEntity = function(nuggetList) {
				return nuggetsService.getEntityTypeFromNuggetList(nuggetList);
			}

			vm.getTotals = function(nuggets) {
				var total = 0;
				vm.statusCounts = {
					open: 0,
					hold: 0,
					won: 0,
					lost: 0
						
				}
				for(var i = 0; i < nuggets.length; i++) {
					if(nuggets[i].value) total = total + nuggets[i].value;
					switch(nuggets[i].nuggetStatus.nuggetStatusID) {
					case 1:
						vm.statusCounts.open += 1;
						break;
				    case 2:
				    	vm.statusCounts.won += 1;
						break;
				    case 3:
				    	vm.statusCounts.lost += 1;
						break;
				    case 4:
				    	vm.statusCounts.hold += 1;
						break;
					}
				}
				return total;
			}
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('nuggetTextFilter', JSON.stringify(vm.filterText));
			}
			
			vm.openNugget = function(nugget) {
				$state.go('nuggets', {planId: nugget.planId, nuggetID: nugget.nuggetId });
			}
			
			vm.saveCriteria = function() {
				sessionStorage.setItem('nuggetCriteria', JSON.stringify(vm.criteria));
			}
			
	
			vm.sort = {
	          type: "value",
	          reverse: true,
	          order: function(sortType, sortReverse) {
	            if (vm.sort.type !== sortType) {
	              vm.sort.reverse = !sortReverse;
	            } else {
	              vm.sort.reverse = sortReverse;
	            }

	            vm.sort.type = sortType;
	            
	            sessionStorage.setItem('nuggetSort', JSON.stringify({type: sortType, reverse: vm.sort.reverse}));
	          }
	        };
			
			vm.$onDestroy = function() {
				userProfileListener();
			}

		}
	}
export default nuggetSummary;