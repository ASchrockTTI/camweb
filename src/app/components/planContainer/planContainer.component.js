import planContainerTemplate from './planContainer.html';

const planContainer =  {
		template: planContainerTemplate,
		controller: function(
				utilService,
				planService,
				userInfoService,
				$stateParams,
				$state,
				$scope,
				$rootScope) {
		
			var vm = this;
			var planId = $stateParams.planId;
			
			
			vm.$onInit = function() {
				
				vm.params = $stateParams;
				
				vm.plan = undefined;
				
				retrievePlanById(planId);	
			}
			
			$scope.$watch(function(){
			    return $stateParams.planId
			}, function(newPlanId, oldPlanId){
			    if(newPlanId !== oldPlanId) {
			    	vm.plan = undefined;
			    	retrievePlanById(newPlanId);
			    }
			})
			
			$scope.$on('businessPlanEdited', function () {
				retrievePlanById(vm.plan.planId);
			});
			
			vm.getState = function() {
				return $state.current.name;
			}
			
			function retrievePlanById(planId) {
				
				planService.lookupPlan(planId)
				.then(function(response) {
					
					vm.plan = response;
					
					utilService.getLogger().info(response);
					if (response.insideSalesPerson != null) {
						var isrList = response.insideSalesPerson.substring(0,response.insideSalesPerson.length).trim().split(',');
						vm.insideSalesPersonList = isrList;
					}
					
					planService.setPlan(vm.plan);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
				
				planService.findRelatedPlans(planId)
				.then(function(response) {
					
					vm.relatedPlans = response;
					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
		}
	}
export default planContainer;