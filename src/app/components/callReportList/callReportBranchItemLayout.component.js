import callReportBranchItemLayoutTemplate from './callReportBranchItemLayout.html';

const callReportBranchItemLayout =  {
		template: callReportBranchItemLayoutTemplate,
		bindings: {
			callReport: '<'
		},
		controller: function(
			$stateParams,
			$state,
			callReportService,
			newsfeedService,
			$window,
			$sce,
			FileSaver,
			userProfile,
			utilService
		) {

			var vm = this;
			vm.params = $stateParams;
			
			
			vm.downloadDocument = function(document, planId) {
				
				newsfeedService.downloadDocument(planId, document.documentID)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
			}

			vm.openCallReport = function(callReportId, planId) {
					$state.go('callReport', {planId:planId, callReportId: callReportId});
			};

			vm.goToPlan = function(planId) {

				$state.go('businessPlan', {planId:planId});
				//return  {reporter: fullName};
			}
			
			vm.isAdmin = function() {
				return userProfile.isAdmin();
			}

			vm.isReporter = function(callReport) {
				return utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), callReport.reporter)
			}
			
			vm.deleteCallReport = function(callReportId, planId) {
				utilService.deleteConfirmation(
			              "Call Report",
			              "You are about to discard the selected call report and any attached files. \nIf this call report is from the current quarter, your TMW will be affected.",
			              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		callReportService.deleteCallReport(planId, callReportId);
	            			for (var i = 0; i < vm.callReports.length; i++) {
		            			if (vm.callReports[i].callReport.callReportId === callReportId) {
		            				vm.callReports.splice(i, 1);
		            		    }
		            		}
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
		
		}
	}
export default callReportBranchItemLayout;