import topLevelCallReportsTemplate from './topLevelCallReports.html';

const topLevelCallReports =  {
		template: topLevelCallReportsTemplate,
		controller: function(
			$scope,
			$stateParams,
			$timeout,
			callReportService,
			planService,
			utilService,
			userProfile,
			userInfoService,
			$document
		) {

			var vm = this;
			vm.params = $stateParams;
			vm.textFilter = '';
			vm.isInternetExplorer = $document[0].documentMode ? true : false;
			vm.searchCriteria = {};

			vm.$onInit = function() {
				vm.params = $stateParams;
				vm.textFilter = '';
				vm.isInternetExplorer = $document[0].documentMode ? true : false;
				
				if (sessionStorage.getItem('callReportTextFilter')) {
					vm.textFilter = JSON.parse(sessionStorage.getItem('callReportTextFilter'));
				}
				
				if(userProfile.getFullName() !== 'undefined undefined') vm.initializeCallReportCriteria();
			}
			
			var userProfileListener = $scope.$on('userProfileAvailable', function () {
				$timeout(vm.initializeCallReportCriteria(), 100);
			});
			
			vm.initializeCallReportCriteria = function() {
				var role = userProfile.getUserType().toUpperCase();
				
				switch(role) {
				case 'SAM_GAM':
					userInfoService.getUserCorpIdList(userProfile.getSAMAccountName()).then(function(response) {
						vm.corpIdList = response;
						if(response.length > 0) {
							vm.searchCriteria.availableCorpIds = response;
							if(vm.corpIdList.length > 0) vm.searchCriteria.selectedCorpId = vm.corpIdList[0];
							vm.searchCallReportsByCriteria();
						} else {
							getCallReportsByName();
						}
						
					});
					break;
				case 'FSR':
				case 'ISR':
					getCallReportsByName();
					break;
				case 'GM':
				case 'RVP':
				case 'EXECUTIVE':
				case 'FSM':
				case 'ISM':
				case 'SUPPORT':
				case 'BPM':
				case 'CPM':
				case 'ADMIN':
				case 'RSE':
				default:
					if(!vm.selectedBranch && !vm.planId) vm.searchCriteria.selectedBranches = [userProfile.getBranchLocation()];
					vm.branchList = [];
					getBranches();
					vm.searchCallReportsByCriteria();
					break;
				}
			}

			function getCallReportsByName() {
				vm.searchCriteria.fullName = userProfile.getFullName();
				vm.searchCallReportsByCriteria();
			}
			
			function getBranches() {
				var role = userProfile.getUserType();
				
				switch(role.toUpperCase()) {
				case 'ADMIN':
				case 'EXECUTIVE':
					planService.getParentBranchList().then(function(response) {
						vm.branchList = response;
						vm.searchCriteria.availableBranches =  vm.branchList;
					});
					break;
				default:
					if(userProfile.getBranchList().length > 0) {
						vm.branchList = userProfile.getBranchList();
					} else {
						vm.branchList = [userProfile.getBranchLocation()];
					}
					vm.customerBranches = vm.branchList;
				}
				vm.searchCriteria.availableBranches =  vm.branchList;
			}
			
			vm.searchCallReportsByCriteria = function() {
				if (sessionStorage.getItem('callReportSearchCriteria')) {
					var searchCriteria = JSON.parse(sessionStorage.getItem('callReportSearchCriteria'));
				
					vm.searchCriteria = Object.assign(vm.searchCriteria, searchCriteria);
						
					if(searchCriteria.startDate) {
						vm.searchCriteria.startDate = new Date(searchCriteria.startDate);
					} 
					if(searchCriteria.endDate) {
						vm.searchCriteria.endDate = new Date(searchCriteria.endDate);
					}
				} else {
					var startDate = new Date();
					startDate.setMonth(startDate.getMonth() - 3);
					vm.searchCriteria.startDate = startDate;
				}
				
				
				vm.callReports = undefined;
				
				var criteria = {
						planId: vm.searchCriteria.planId,
						customerName: vm.searchCriteria.customerName,
						selectedBranches: vm.searchCriteria.selectedBranches,
						selectedCorpId: vm.searchCriteria.selectedCorpId,
						fullName: vm.searchCriteria.fullName,
						startDate: vm.searchCriteria.startDate,
						endDate: vm.searchCriteria.endDate,
						showMine: vm.searchCriteria.showMine
					}
					
				vm.callReportDrafts = undefined;
				callReportService.retrieveAllCallReportDraftsByCriteria(criteria)
				.then(function(response) {
					vm.callReportDrafts = response.response;
				})
				.catch(function(error) {

				});
				
				vm.submittedCallReports = undefined;
				callReportService.retrieveAllSubmittedCallReportsByCriteria(criteria)
				.then(function(response) {
					vm.submittedCallReports = response.response;
				})
				.catch(function(error) {
					
				});
			}
			
			
			vm.saveFilterText = function() {
				sessionStorage.setItem('callReportTextFilter', JSON.stringify(vm.textFilter));
			}
			
			vm.openCallReport = function() {
				utilService
					.uibModal()
					.open({
						component: "callReportModal",
						size: "sm",
						resolve: {
							modalData: function () {
										return { 
												selectedPlanId: vm.selectedPlanId,
												selectedCustomerName: vm.selectedCustomerName
										}
								}
						}
					})
					.result.then(
						function(result) {
							
						},
						function(dismiss) {
							
						});
			};
			
			
			vm.searchCallReports = function() {
				utilService
					.uibModal()
					.open({
						component: "callReportSearchModal",
						size: "md",
						resolve: {
							modalData: function () {
										return {searchCriteria: vm.searchCriteria}
								}
						}
					})
					.result.then(
						function(searchCriteria) {
							vm.searchCriteria = searchCriteria;
							vm.searchCallReportsByCriteria();
						},
						function(dismiss) {
						});
			};
		}
	}
export default topLevelCallReports;