import callReportSearchModalTemplate from './callReportSearchModal.html';

const callReportSearchModal =  {
		template: callReportSearchModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
					$state,
					$scope,
					userProfile, 
					roles,
					utilService,
					callReportService,
					userInfoService,
					planService) {
			
			var vm = this;
			vm.searchCriteria = {};
			
			vm.$onInit = function() {
				
				var role = userProfile.getUserType().toUpperCase();
				var planSearchRoles = [roles.ADMIN, roles.EXEC, roles.GM, roles.FSM, roles.ISM, roles.RSE, roles.RBOM];
				if(planSearchRoles.includes(role)) vm.showPlanSearch = true;
				
				loadCriteria();
			}
			
			function loadCriteria() {
				var criteria = vm.resolve.modalData.searchCriteria;
				angular.copy(criteria, vm.searchCriteria);
				
				vm.corpIdList = vm.searchCriteria.availableCorpIds;
				vm.branchList = vm.searchCriteria.availableBranches;
				
				vm.startDate = vm.searchCriteria.startDate;
				vm.endDate = vm.searchCriteria.endDate;
				
				vm.showMine = vm.searchCriteria.showMine;
				
				if(vm.searchCriteria.planId) vm.customerName = vm.searchCriteria.customerName;
				if(vm.searchCriteria.selectedBranches) vm.selectBranch(vm.searchCriteria.selectedBranches[0]);
				if(vm.searchCriteria.selectedCorpId) vm.selectCorpId(vm.searchCriteria.selectedCorpId);
				if(vm.searchCriteria.fullName) vm.selectReporter(vm.searchCriteria.fullName);
			}
			
			$scope.startsWith = function (actual, expected) {
			    var lowerStr = (actual + "").toLowerCase();
			    return lowerStr.indexOf(expected.toLowerCase()) === 0;
			}


			vm.selectBranch = function(branch) {
				vm.inputBranch = branch;
				vm.searchCriteria.selectedBranches = [branch];
				vm.fullName = undefined;
				retrieveFsrList(branch);
			}
			
			vm.selectCorpId = function(corpId) {
				vm.inputCorpId = corpId.corpId + ' | ' + corpId.customerName;
				vm.searchCriteria.selectedCorpId = corpId;
				vm.fullName = undefined;
			}

			vm.branchFilter = function($query) {
				var filtered = [];
				angular.forEach(vm.branchList, function(branch) {
					if(branch.display.toLowerCase().indexOf($query.toLowerCase()) === 0) filtered.push(branch);
				})
				
				return filtered;
			}
		
			function retrieveFsrList(branch) {
				return userInfoService.getCallReportCreatorsByBranch(branch)
				.then(function(response) {
					vm.fsrList = response;

					if(vm.fsrList) {
						vm.fsrList.unshift('All');
					}

				})
				.catch(function(error) {
					utilService.getLogger().info(error);
				})
			}

			
			vm.selectReporter = function(name) {
				if(name === 'All') {
					vm.fullName = undefined;
					vm.searchCriteria.fullName = undefined;
				} else {
					vm.fullName = name;
					vm.searchCriteria.fullName = name;
				}
			}

			vm.selectPlan = function(plan) {
				if(plan.branch) {
					vm.selectBranch(plan.branch)
				} else {
					vm.selectedBranch = undefined;
					vm.inputBranch = undefined;
				}
				vm.searchCriteria.planId = plan.planId;
				vm.searchCriteria.customerName = plan.customerName;
			}
			

			vm.search = function() {
				if(!vm.customerName || vm.customerName === '') {
					vm.searchCriteria.planId = undefined;
					vm.searchCriteria.customerName = undefined;
				}
				
				vm.searchCriteria.showMine = vm.showMine;
				
				sessionStorage.setItem('callReportSearchCriteria', JSON.stringify(vm.searchCriteria));
				
				vm.modalInstance.close(vm.searchCriteria);
			}

			vm.cancel = function() {
				loadCriteria();
				vm.modalInstance.dismiss('cancel');
			}
			
			vm.reset = function() {
				loadCriteria();
			}
		}
	}
export default callReportSearchModal;