import submitNewNuggetModalTemplate from './submitNewNuggetModal.html';

const submitNewNuggetModal =  {
		template: submitNewNuggetModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
				utilService,
				planService,
				userProfile,
				nuggetsService,
				$state,
				$stateParams
			) {
			
			var vm = this;
			vm.params = $stateParams;
			
			vm.nugget = {
				planID: undefined,
				selectedValue: undefined,
				selectedMfr: undefined,
				selectedCommodity: undefined,
				selectedCompetitor: undefined,
				selectedStatus: undefined
			}
			vm.invalidNugget = false;
			
			vm.$onInit = function() {
				if(vm.resolve.modalData) {
					vm.data = vm.resolve.modalData;
					if(vm.data.nugget) {
						vm.nugget = vm.data.nugget;
						loadNuggetsByPlanID(vm.nugget.planID);
					}

					
					vm.title = vm.data.isEdit === true ? "Edit Existing OPB" : "Create New OPB";
					vm.submitBtnTxt = vm.data.isEdit === true ? "Save" : "Create";
				}

				loadCommodities();
				loadStatuses();

				if(vm.data.isEdit === true) {
					loadNugget();
				} else {

				}

				setTimeout(function () {
					var input = angular.element(document.querySelector('#mfrValueInput'));
					input.focus();
				}, 500);
			}

			vm.submit = function() {
				vm.invalidNugget = false;
				var nugget = vm.nugget;
				
				var value = vm.nugget.selectedValue ? parseInt(vm.nugget.selectedValue.replace(/,/g, '')) : 0;
				nugget.value = value;
				if(vm.data.isEdit === true) {
					nuggetsService.updateNugget(vm.nugget)
					.then(function(response) {
						vm.modalInstance.close(response);
					})
					.catch(function(error) {
						
					})
				} else {
					nuggetsService.createNugget(vm.nugget)
					.then(function(response) {
						vm.modalInstance.close(response);
					})
					.catch(function(error) {
						
					})
				}
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
			
			vm.setSelectedMfr = function(selectedMfr) {
				vm.nugget.selectedMfr = selectedMfr;
			}

			vm.setSelectedCompetitor = function(selectedCompetitor) {
				vm.nugget.selectedCompetitor = selectedCompetitor;
			}

			function loadCommodities() {
				nuggetsService.getNuggetCommodities()
				.then(function(response) {
					vm.commodities = response;
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			function loadStatuses() {
				nuggetsService.getNuggetStatuses()
				.then(function(response) {
					vm.nuggetStatuses = response;
					if(vm.data.isEdit != true) {
						vm.nugget.nuggetStatus = vm.nuggetStatuses[0];
					}
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}

			function loadNugget() {
				nuggetsService.getNuggetById(vm.nugget.nuggetID)
				.then(function(response) {
					vm.nugget = response;
					if(vm.nugget) {
						vm.nugget.selectedValue = vm.nugget.value ? vm.nugget.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : undefined;
						vm.nugget.selectedMfr = vm.nugget.manufacturer;
						vm.nugget.selectedCommodity = vm.nugget.commodity;
						vm.nugget.selectedCompetitor = vm.nugget.nuggetCompetitor;
						vm.nugget.selectedStatus = vm.nugget.nuggetStatus;
						vm.competitor = vm.nugget.selectedCompetitor.competitor;
					}
					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}

			function loadNuggetsByPlanID(planID) {
				nuggetsService.getNuggetsByPlanId(planID)
				.then(function(response) {
					vm.existingNuggets = response;
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
		}
			
	}
export default submitNewNuggetModal;