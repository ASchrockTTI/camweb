import nuggetCommentListTemplate from './nuggetCommentList.html';

const nuggetCommentList =  {
		template: nuggetCommentListTemplate,
		bindings: {
			selectedNugget: '='
		},
		controller: function(
			nuggetsService,
			userProfile,
			utilService,
			$rootScope,
			$stateParams
		) {

			var vm = this;
			
			vm.params = $stateParams;
			vm.newCommentId = 0;
			
			vm.$onInit = function() {
				vm.userProfile = userProfile.getUserProfile();
				
				vm.getNuggetComments();
			}
			
			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {
				if(!vm.userProfile) {
					vm.userProfile = userProfile.getUserProfile();
				}
			})
			
			vm.getNuggetComments = function() {
				
				if(vm.loadingComments) {
					return;
				}
				vm.loadingComments = true;

				vm.comments = undefined;
				
				nuggetsService.getNuggetCommentsByPlanId(vm.params.planId).then(
					function(response) {
						vm.comments = response;
					})
					.catch(function(error) {
						vm.comments = [];
					})
				
				vm.loadingComments = false;
			}
			
			vm.newComment = function() {
				vm.newCommentId += 1;
				var id = 'newComment' + vm.newCommentId;
				vm.comments.push({
					commentInputId: id,
					nuggetID: vm.selectedNugget.nuggetID,
					createdDateTime: new Date(),
					editMode: true,
					createdBy: vm.userProfile.fullName
				})
				
				var container = document.getElementById('commentContainer');
				container.scrollTop = 0;
				
				
				 setTimeout(function(){
					 container = document.getElementById(id);
					 container.focus();
				}, 200);
			}
			
			vm.$onDestroy = function() {
				userProfileListener();
			}

		}
	}
export default nuggetCommentList;