import nuggetCommentTemplate from './nuggetComment.html';

const nuggetComment =  {
		template: nuggetCommentTemplate,
		bindings: {
			userProfile: '<',
			comment: '=',
			comments: '=',
			selectedNuggetId: '<'
		},
		controller: function(
			nuggetsService,
			utilService,
			$stateParams
		) {

			var vm = this;
			
			vm.params = $stateParams;
			
			vm.$onInit = function() {
			}
			
			vm.isCreator = function() {
				if(vm.userProfile != undefined && vm.comment.createdBy != undefined) {
					return (vm.userProfile.fullName.toLowerCase() == vm.comment.createdBy.toLowerCase());
				} else return false;
			}
			
			vm.validComment = function() {
				if(vm.comment.comment && vm.comment.comment.length > 0 && vm.comment.comment.length <= 4000) {
					return true;
				} else return false;
				
			}
			
			vm.save = function() {
				if(!vm.comment.comment) return;
				
				nuggetsService.saveNuggetComment(vm.comment, vm.selectedNuggetId).then(
					function(response) {
						angular.copy(response, vm.comment);
					})
					.catch(function(error) {
						//TODO: display error message
					})
			}
			
			vm.enableEditMode = function() {
				vm.original = JSON.parse(JSON.stringify(vm.comment));
				vm.comment.editMode = true;
			}
			
			vm.cancelEdit = function() {
				angular.copy(vm.original, vm.comment);
				vm.comment.editMode = false;
				if(!vm.comment.nuggetCommentsID) {
					vm.comments.splice(vm.comments.indexOf(vm.comment), 1);
				}
			}
			
			vm.deleteComment = function() {
		          utilService.deleteConfirmation(
		              "Comment",
		              "You are about to delete the selected comment.",
		              "btn-danger"
		            )
		            .result.then(
		            	function(result) {
		            		if(result) {
			            		nuggetsService.deleteNuggetComment(vm.comment.nuggetCommentsID)
			            		.then(function(response) {
			            			vm.comments.splice(vm.comments.indexOf(vm.comment), 1);
	            				})
								.catch(function(error) {
									
								})
		            		}
		            	},
		            	function(dismiss) {
		            		
		            	});
		        }

		}
	}
export default nuggetComment;