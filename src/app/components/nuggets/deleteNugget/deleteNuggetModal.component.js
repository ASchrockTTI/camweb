import deleteNuggetModalTemplate from './deleteNuggetModal.html';

const deleteNuggetModal =  {
		template: deleteNuggetModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
			) {
			
			var vm = this;
			
			vm.selectedNugget     = undefined;
			vm.selectedNuggetPart = undefined;
			vm.nuggetName         = '';


			vm.$onInit = function() {

				vm.selectedNugget     = undefined;
				vm.selectedNuggetPart = undefined;
				vm.nuggetName         = '';

				loadNuggetData();
			}

			function loadNuggetData() {

				setTimeout(function(){
					//set nugget data
					console.log("vm.resolve: ", vm.resolve);
					vm.selectedNugget     = vm.resolve.modalData.activeNugget;
					vm.nuggetName         = vm.selectedNugget.manufacturer.manufacturerName + ' + ' + vm.selectedNugget.commodity.commodity;
					
				}, 1);
			
			}

			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}

			vm.delete = function() {
				vm.modalInstance.close({
					isDelete: true
				});
			}
			
		}
	}
export default deleteNuggetModal;