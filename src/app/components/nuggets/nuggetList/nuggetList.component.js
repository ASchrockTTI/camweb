import nuggetListTemplate from './nuggetList.html';

const nuggetList =  {
		template: nuggetListTemplate,
		bindings: {
			nuggets: '=',
			activeNugget: '=',
			activeNuggetPart: '=',
			onNuggetClick: '&',
			onNuggetPartClick: '&',
			addNuggetPart: '&',
			updateNuggetPart: '&',
			deleteNuggetPart: '&',
			addNewNugget: '&',
			editActiveNugget: '&',
			deleteActiveNugget: '&',
			canCreateNugget: '=',
			canEditActiveNugget: '='
		},
		controller: function(
			$stateParams,
			utilService,
			$document,
			$scope,
			userProfile
		) {
			
			
			var vm = this;
			vm.params = $stateParams;
			vm.isInternetExplorer = $document[0].documentMode ? true : false;
			
			$scope.$watch(angular.bind(vm, function () {
				return vm.activeNugget;
			}), function (nugget) {
				if(!nugget) {
					return;
				}
				$scope.$parent.scrollToNugget(nugget.nuggetID);
			});
			

			vm.setActiveNugget = function(nugget) {
				vm.onNuggetClick({nugget: nugget});
			}

			vm.setActiveNuggetPart = function(nuggetPart) {
				vm.onNuggetPartClick({nuggetPart: nuggetPart});
				vm.addNewNuggetPart(undefined, true);
			}

			vm.nuggetStatusColor = function(nuggetStatus) {
				switch(nuggetStatus.nuggetStatusID) {
			    case 1:
			    	return '#276ab0';
			    case 2:
			    	return '#4c964c';
			    case 3:
			    	return '#d74545';
			    case 4:
			    	return '#d79230';
			    default:
			    	return 'gray';
			    }
			}
			
			vm.truncateNuggetPartName = function(nuggetPartName) {

				var nuggetPartLength = 27;
				var newNuggetPartName = '';
				if(nuggetPartName.length > nuggetPartLength) {
					newNuggetPartName = nuggetPartName.slice(0, nuggetPartLength - 1) + '...';
					return newNuggetPartName;
				}

				return nuggetPartName;
				
			}

			vm.canEditNuggetParts = function(nugget) {
				
				if(!vm.canCreateNugget) {
					return false;
				}

				var userName = userProfile.getSAMAccountName();

				if(!userName) {
					console.log("@List Can edit Nugget: (userName)", vm.canEditActiveNugget)
					return false;
				}

				if(!nugget) {
					return false;
				}

				var nuggetOwner = nugget.owner;

				if(nuggetOwner.toUpperCase() == userName.toUpperCase()) {
					vm.canEditActiveNugget = true;
					console.log("@List Can edit Nugget: (userEqual)", vm.canEditActiveNugget)
					return true;
				}

				console.log("@List Can edit Nugget: (userNotEqual)", vm.canEditActiveNugget)
				return false;
			}

			vm.addNewNuggetPart = function(nugget, isEditMode) {
				
				if(nugget && !vm.canEditNuggetParts(nugget)) {
					return;
				}

				setTimeout(function () {
					
					utilService
						.uibModal()
						.open({
							component: "nuggetPartModal",
							size: "md",
							resolve: {
								activeNugget: function() {
									return vm.activeNugget 
								},
								isEditMode: function() {
									return isEditMode;
								},
								activeNuggetPart: function() {
									return vm.activeNuggetPart;
								}
							}
						})
						.result.then(
							function(close) {

								var nuggetPartToAdd = close.nuggetPartToAdd;
								var isEditMode      = close.isEditMode;
								var isDelete        = close.isDelete;

								if(isDelete === true) {
									vm.deleteNuggetPart({nuggetPartToDelete: nuggetPartToAdd});
									return;
								}

								if(isEditMode === true) {
									vm.updateNuggetPart({nuggetPartToUpdate: nuggetPartToAdd});
									return;
								} 
								
								vm.addNuggetPart({nuggetPartToAdd: nuggetPartToAdd})
								
							},
							function(dismiss) {
							});

				}, 1);

			};
		}
	}
export default nuggetList;