import nuggetsTemplate from './nuggets.html';

const nuggets =  {
		template: nuggetsTemplate,
		controller: function(
			$scope,
			$rootScope,
			$state,
			$stateParams,
			$timeout,
			$location,
			$anchorScroll,
			planService,
			permissionsFactory,
			nuggetsService,
			roles,
			userProfile,
			utilService
		) {
			
			var vm = this;

			vm.nuggets             = undefined;
			vm.activeNugget        = undefined;
			vm.activeNuggetPart    = undefined;
			vm.canCreateNugget     = undefined;
			vm.canEditActiveNugget = undefined;
			
			vm.$onInit = function() {
				vm.nuggets             = undefined;
				vm.activeNugget        = undefined;
				vm.activeNuggetPart    = undefined;
				//vm.canCreateNugget     = false;
				//vm.canEditActiveNugget = false;

				vm.params = $stateParams;
				
                retrievePlanById(vm.params.planId);
			}

			var initListener = $scope.$on('initRelatedPlans', function () {
				$timeout(initializeRelatedPlans(), 100);
			});
			
			function initializeRelatedPlans() {
				if(!vm.plan || !userProfile || !userProfile.getUserType() || userProfile.getUserType().toUpperCase() != 'SAM_GAM') {
					return;
				}
				
				if(vm.relatedPlansChecked) {
					return;
				}

				planService.findRelatedPlans(vm.plan.planId)
				.then(function(response) {
					vm.relatedPlansChecked = true;
					vm.relatedPlans = response;
					setCreatePermissions(vm.plan, vm.relatedPlansChecked, vm.relatedPlans);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				});

			}

			vm.onNuggetClick = function(nuggetClicked) {
				vm.activeNugget = nuggetClicked.nugget;
				$state.go('.', {nuggetID: vm.activeNugget.nuggetID}, {notify: false});
				setEditPermissions();
			}

			vm.onNuggetPartClick = function(nuggetPartClicked) {
				vm.activeNuggetPart = nuggetPartClicked.nuggetPart;
			}

			vm.addNewNugget = function() {
				utilService.uibModal().open({
					component: 'submitNewNuggetModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: {
						modalData: function () {
							return {
								nugget: {
									planID: vm.plan.planId
								}
							}
						}
                  }
				}).result.then(
					function(result) {
						if(result.response) {
							addNewlyCreatedNuggetToList(result.response.nuggetID, result.response);
						}
					},
					function(dismiss) {
						
					}
				)

			}

			vm.editActiveNugget = function() {
				utilService.uibModal().open({
					component: 'submitNewNuggetModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: {
						modalData: function () {
							return {
								isEdit: true,
								nugget: vm.activeNugget
							}
						}
                  }
				}).result.then(
					function(result) {
						if(result) {
							updateNuggetInList(result.nuggetID, result);
						}
					},
					function(dismiss) {
						
					}
				)
			}

			vm.deleteActiveNugget = function() {

				utilService.uibModal().open({
					component: 'deleteNuggetModal',
					size: 'md',
					animation: true,
					keyboard: true,
					resolve: {
						modalData: function () {
							return {
								activeNugget: vm.activeNugget
							}
						}
                  }
				}).result.then(
					function(result) {
						if(result) {

							nuggetsService.deleteNugget(vm.activeNugget.nuggetID)
							.then(function(response) {
								removeNuggetInList(response);
							})
							.catch(function(error) {
								vm.clientError.message = error;
							});
							
						}
					},
					function(dismiss) {
						
					}
				)

			}

			vm.addNuggetPart = function(nuggetPartToAdd) {
				
				var nuggetPart = {
					part: nuggetPartToAdd.nuggetPartToAdd,
					nuggetID: vm.activeNugget.nuggetID
				};
			
				nuggetsService.addNuggetPart(nuggetPart)
					.then(function(response) {
						addNewlyCreatedNuggetPartToList(response, nuggetPart);
					})
					.catch(function(error) {
						vm.clientError.message = error;
					});
			}

			vm.updateNuggetPart = function(nuggetPartToUpdate) {
				
				var nuggetPart = {
					part: nuggetPartToUpdate.nuggetPartToUpdate,
					nuggetID: vm.activeNugget.nuggetID,
					nuggetPartsID: vm.activeNuggetPart.nuggetPartsID
				};
			
				nuggetsService.updateNuggetPart(nuggetPart)
					.then(function(response) {
						updateNuggetPartInList(response, nuggetPart);
					})
					.catch(function(error) {
						vm.clientError.message = error;
					});
			}

			vm.deleteNuggetPart = function(nuggetPartToDelete) {
				
				var nuggetPart = {
					part: nuggetPartToDelete.nuggetPartToUpdate,
					nuggetID: vm.activeNugget.nuggetID,
					nuggetPartsID: vm.activeNuggetPart.nuggetPartsID
				};
			
				nuggetsService.deleteNuggetPart(nuggetPart)
					.then(function(response) {
						removeNuggetPartInList(response, nuggetPart);
					})
					.catch(function(error) {
						vm.clientError.message = error;
					});
			}
			
			function setCreatePermissions(plan, relatedPlansChecked, relatedPlans) {
				var role = userProfile.getUserType();
				var branchList = userProfile.getBranchList();
				var userFullName = userProfile.getFullName();

				if(!role) {
					vm.canCreateNugget = false;
					return;
				}

				if(role.toUpperCase() === 'ADMIN') {
					vm.canCreateNugget = true;
					return;
				}

				var acctManager = plan.acctManager.toUpperCase();
				var insideSalesPersonName = plan.insideSalesPersonName.toUpperCase();
				if(userFullName && (userFullName.toUpperCase() === acctManager || userFullName.toUpperCase() === insideSalesPersonName)) {
					vm.canCreateNugget = true;
					return;
				}
				

				if(!branchList) {
					vm.canCreateNugget = false;
					return;
				}
				
				if(branchList && plan.branch && userProfile.hasBranch(plan.branch)) {
					vm.canCreateNugget = true;
					return;
				}
				
				if(role.toUpperCase() === 'SAM_GAM' && relatedPlansChecked && relatedPlans && relatedPlans.length > 0) {
					var isSamGamOwner = false;
					for(var i = 0; i < relatedPlans.length; i++) {
						if(relatedPlans[i].acctManager === userFullName) {
							isSamGamOwner = true;
							break;
						}
					}
					
					if(!isSamGamOwner) {
						vm.canCreateNugget = false;
						return;
					}

					var currentPlanId = vm.plan.planId;
					for(var i = 0; i < relatedPlans.length; i++) {
						if(relatedPlans[i].planId === currentPlanId) {
							vm.canCreateNugget = true;
							vm.canEditActiveNugget = false;
							return;
						}
					}
				}
				
				vm.canCreateNugget = false;
			}

			function setEditPermissions() {
				var role = userProfile.getUserType();
				var branchList = userProfile.getBranchList();

				if(!vm.canCreateNugget) {
					vm.canEditActiveNugget = false;
					return;
				}

				if(!vm.activeNugget) {
					vm.canEditActiveNugget = false;
					return;
				}

				if(!branchList) {
					vm.canEditActiveNugget = false;
					return;
				}
				
				if(branchList && vm.plan.branch && userProfile.hasBranch(vm.plan.branch)) {
					vm.canEditActiveNugget = true;
					return;
				}
				
				if(role.toUpperCase() === 'SAM_GAM' && vm.canCreateNugget) {
					vm.canEditActiveNugget = true;
					return;
				}

				vm.canEditActiveNugget = false;
			}

			function addNewlyCreatedNuggetToList(nuggetID, nugget) {

				var newlyCreatedNugget = {
					nuggetID: nuggetID,
					commodity: nugget.commodity,
					manufacturer: nugget.manufacturer,
					nuggetCompetitor: nugget.nuggetCompetitor,
					nuggetParts: nugget.nuggetParts,
					nuggetStatus: nugget.nuggetStatus,
					owner: nugget.owner,
					ownerFullName: nugget.ownerFullName,
					planId: nugget.planId,
					value: nugget.value,
					createdBy: userProfile.getSAMAccountName(),
					modifiedBy: userProfile.getSAMAccountName(),
					createdDateTime: Date.now(),
					modifiedDateTime: Date.now()
				}

				vm.nuggets.push(newlyCreatedNugget);

				vm.activeNugget = vm.nuggets.find( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === newlyCreatedNugget.nuggetID; 
				});

				vm.activeNuggetPart = undefined;

			}

			function updateNuggetInList(nuggetID, nugget) {

				var updatedNugget = {
					nuggetID: nuggetID,
					commodity: nugget.commodity,
					manufacturer: nugget.manufacturer,
					nuggetCompetitor: nugget.nuggetCompetitor,
					nuggetParts: nugget.nuggetParts,
					nuggetStatus: nugget.nuggetStatus,
					owner: nugget.owner,
					ownerFullName: nugget.ownerFullName,
					planId: nugget.planId,
					value: nugget.value,
					createdBy: nugget.createdBy,
					modifiedBy: userProfile.getSAMAccountName(),
					createdDateTime: nugget.createdDateTime,
					modifiedDateTime: Date.now()
				}

				var nuggetToModifyIndex = vm.nuggets.findIndex( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === updatedNugget.nuggetID; 
				});

				if(nuggetToModifyIndex !== -1) {
					vm.nuggets[nuggetToModifyIndex] = updatedNugget;
					vm.activeNugget                 = updatedNugget;
				}

			}

			function removeNuggetInList(nuggetID) {

				var nuggetToModifyIndex = vm.nuggets.findIndex( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === nuggetID; 
				});

				if(nuggetToModifyIndex !== -1) {
					vm.activeNuggetPart = undefined;
					vm.activeNugget     = undefined;
					vm.nuggets.splice(nuggetToModifyIndex, 1);
				}

			}

			function addNewlyCreatedNuggetPartToList(nuggetPartId, nuggetPart) {

				var newlyCreatedNuggetPart = {
					part: nuggetPart.part,
					nuggetPartsID: nuggetPartId,
					nuggetID: nuggetPart.nuggetID,
					deleted: 0,
					createdBy: userProfile.getSAMAccountName(),
					modifiedBy: userProfile.getSAMAccountName(),
					createdDateTime: Date.now(),
					modifiedDateTime: Date.now()
				}

				var nuggetToModifyIndex = vm.nuggets.findIndex( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === nuggetPart.nuggetID; 
				});

				if(nuggetToModifyIndex !== -1) {
					vm.activeNugget.nuggetParts.push(newlyCreatedNuggetPart);
					vm.nuggets.splice(nuggetToModifyIndex, 1, vm.activeNugget);
				}

			}

			function updateNuggetPartInList(nuggetPartId, nuggetPart) {

				var updatedNuggetPart = {
					part: nuggetPart.part,
					nuggetPartsID: nuggetPartId,
					nuggetID: nuggetPart.nuggetID,
					deleted: 0,
					createdBy: userProfile.getSAMAccountName(),
					modifiedBy: userProfile.getSAMAccountName(),
					createdDateTime: Date.now(),
					modifiedDateTime: Date.now()
				}

				var nuggetToModifyIndex = vm.nuggets.findIndex( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === nuggetPart.nuggetID; 
				});

				if(nuggetToModifyIndex !== -1) {
					
					var nuggetPartToModifyIndex = vm.activeNugget.nuggetParts.findIndex( function(nuggetPartToFind) { 
						return  nuggetPartToFind.nuggetPartsID === nuggetPartId; 
					});

					vm.activeNugget.nuggetParts[nuggetPartToModifyIndex] = updatedNuggetPart;
				}

			}

			function removeNuggetPartInList(nuggetPartId, nuggetPart) {


				var nuggetToModifyIndex = vm.nuggets.findIndex( function(nuggetToFind) { 
					return  nuggetToFind.nuggetID === nuggetPart.nuggetID; 
				});

				if(nuggetToModifyIndex !== -1) {
					
					var nuggetPartToModifyIndex = vm.activeNugget.nuggetParts.findIndex( function(nuggetPartToFind) { 
						return  nuggetPartToFind.nuggetPartsID === nuggetPartId; 
					});

					vm.activeNugget.nuggetParts.splice(nuggetPartToModifyIndex, 1);
				}

			}

			function retrievePlanById(planId) {
				
				planService.lookupPlan(planId)
				.then(function(response) {
					
					vm.plan = response;
					
					if (vm.plan.insideSalesPersonName != null) {
						var isrList = vm.plan.insideSalesPersonName.toLowerCase().trim().split(',');
						vm.insideSalesPersonList = isrList;
					}
					
					vm.acctManager = vm.plan.acctManager;
					vm.ownerEmail = vm.plan.ownerEmail;
					
					planService.setPlan(vm.plan);
					getNuggetsByPlanId(vm.plan.planId);
					setCreatePermissions(vm.plan);
					$rootScope.$broadcast('initRelatedPlans');
				})
				.catch(function(error) {
					vm.clientError.message = error;
				});

			}

			function getNuggetsByPlanId(planId) {
				
				nuggetsService.getNuggetsByPlanId(planId)
				.then(function(response) {
					vm.nuggets = response;
					
					if(vm.nuggets.length > 0) {
						if(vm.params.nuggetID) {
							let selectedNuggetID = parseInt(vm.params.nuggetID);
							for(var i = 0; i < vm.nuggets.length; i++) {
								if(vm.nuggets[i].nuggetID === selectedNuggetID) vm.activeNugget = vm.nuggets[i];
							}
							$timeout(function () { $scope.scrollToNugget(vm.activeNugget.nuggetID); }, 200);
						} else {
							vm.activeNugget = vm.nuggets.reduce( function(prev, current) {
								return (prev.value > current.value) ? prev : current;
							});
						}

						setEditPermissions();

					}
					
				})
				.catch(function(error) {
					vm.clientError.message = error;
				});

			}
			
			$scope.scrollToNugget = function(nuggetId) {
				// set the location.hash to the id of
				// the element you wish to scroll to.
				$location.hash('nugget-' + nuggetId);

				$anchorScroll();
			};


			
		}
	}
export default nuggets;