import nuggetPartModalTemplate from './nuggetPartModal.html';

const nuggetPartModal =  {
		template: nuggetPartModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
			) {
			
			var vm = this;
			
			vm.selectedNugget     = undefined;
			vm.selectedNuggetPart = undefined;
			vm.nuggetName         = '';
			vm.nuggetPartName     = '';
			vm.nuggetPartToAdd    = undefined;
			vm.isEditMode         = false;


			vm.$onInit = function() {

				vm.selectedNugget     = undefined;
				vm.selectedNuggetPart = undefined;
				vm.nuggetName         = '';
				vm.nuggetPartName     = '';
				vm.nuggetPartToAdd    = undefined;
				vm.isEditMode         = false;

				loadNuggetData();
			}

			vm.isValidNuggetPart = function() {
				return (!vm.partExists() && vm.nuggetPartToAdd !== undefined && vm.nuggetPartToAdd !== '')
			}

			vm.partExists = function() {
				if(vm.selectedNugget && vm.nuggetPartToAdd) {
					return vm.selectedNugget.nuggetParts.find( function(nuggetPartToFind) { return  nuggetPartToFind.part.toLowerCase() === vm.nuggetPartToAdd.toLowerCase() });
				}
				return false;
			}

			function focusInput(elementId) {
				setTimeout(function(){
					var container = document.getElementById(elementId);
					container.focus();
				}, 200);
			}

			function loadNuggetData() {

				setTimeout(function(){
					//set nugget data
					vm.selectedNugget     = vm.resolve.activeNugget;
					vm.isEditMode         = vm.resolve.isEditMode;
					vm.nuggetName         = vm.selectedNugget.manufacturer.manufacturerName + ' + ' + vm.selectedNugget.commodity.commodity;
					
					vm.selectedNuggetPart = vm.resolve.activeNuggetPart;
					vm.nuggetPartName     = vm.selectedNuggetPart ? vm.selectedNuggetPart.part : undefined;

					if(vm.isEditMode === true) {
						vm.nuggetPartToAdd = vm.nuggetPartName;
						focusInput('nuggetPartModalEdit')
						return;
					}

					focusInput('nuggetPartModal');

				}, 1);
			
			}

			vm.addNuggetPart = function() {
				
				if(!vm.isValidNuggetPart()) {
					return;
				}
				
				vm.modalInstance.close({
					nuggetPartToAdd: vm.nuggetPartToAdd,
					isEditMode: vm.isEditMode
				});
			}

			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}

			vm.delete = function() {
				vm.modalInstance.close({
					nuggetPartToAdd: vm.nuggetPartToAdd,
					isEditMode: vm.isEditMode,
					isDelete: true
				});
			}

			vm.isCharacterLimitReached = function() {

				if(!vm.nuggetPartModal.$valid) {
					return false;
				}

				return true;
				
			}
			
		}
	}
export default nuggetPartModal;