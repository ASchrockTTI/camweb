import notificationsTemplate from './notifications.html';

const notifications = {
            template: notificationsTemplate,
            controller: function ($state, notificationService, $location, $scope, $rootScope) {

                var vm = this;

                vm.$onInit = function () {
                    vm.notifications = notificationService.getNotifications();
                    vm.notificationCount = notificationService.getNotificationCount();
                }

                $rootScope.$on('notifications', function (event, data) {
                	vm.notifications = notificationService.getNotifications();
                    vm.notificationCount = notificationService.getNotificationCount();
                });
                
                vm.confirmation = {
                	template: notificationsTemplate,
                	clearAllReadConfirmationOpen: false,
                	clear: function(event){
                		//prevent popover from closing due to clicked item disappearing from DOM
                		event.stopPropagation();
                		vm.dismissRead();
                		vm.confirmation.clearAllReadConfirmationOpen = false;
                	},
                	cancel: function(event) {
                		event.stopPropagation();
                		vm.confirmation.clearAllReadConfirmationOpen = false;
                	}
                }


                vm.linkTo = function(state, params) {
                    $state.go(state, params);
                }
                
                vm.readToggle = function(notification) {
                	notification.markedRead = !notification.markedRead;
                	notificationService.readToggle(notification);
                	notificationService.updateCount();
                }
                
                
                vm.dismiss = function(notificationId, event) {
                	//prevent popover from closing due to clicked item disappearing from DOM
                    event.stopPropagation();
                    
                	notificationService.dismiss(notificationId);
                }
                
                vm.dismissRead = function() {
                	notificationService.dismissRead();
                }

                vm.dismissAll = function() {
                   notificationService.dismissAll();
                }
                
                vm.markAllRead = function(){
                	angular.forEach(vm.notifications, function(notification){
                		notification.markedRead = true;
                		notificationService.readToggle(notification);
                	})
                	notificationService.updateCount();
                }

            }
        }
export default notifications;