import modalErrorTemplate from './modalError.html';

const modalError =  {
		template: modalErrorTemplate,
		bindings: {
			ngModel: '='
		},
		controller: function() {
			
		}
	}
export default modalError;