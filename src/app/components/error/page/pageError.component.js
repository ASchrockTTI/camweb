import pageErrorTemplate from './pageError.html';

const pageError =  {
		template: pageErrorTemplate,
		bindings: {
			ngModel: '='
		},
		controller: function() {
			
		}
	}
export default pageError;