import sidebarTemplate from './sidebar.html'
const sidebarComponent = {
  template: sidebarTemplate,
  controller: function (
    $rootScope,
    utilService,
    userProfile,
    roles,
    permissionsFactory,
    mockSessionService,
    userInfoService,
    $document,
    $interval,
    $scope,
    $timeout,
    $state
  ) {
    var vm = this;
    var permissionsValidator;
    var tmwAccessRoles = [
      roles.ADMIN,
      roles.FSR,
      roles.GM,
      roles.FSM,
      roles.EXEC,
      roles.HEAD_HONCHO,
      roles.RBOM,
      roles.ISM,
      roles.RVP,
      roles.SAM_GAM,
    ];

    vm.$onInit = function () {
      permissionsValidator = permissionsFactory.newInstance();
      permissionsValidator.setCustomPermission(
        "tmw",
        tmwAccessRoles,
        null,
        null
      );
    };

    if ($document[0].documentMode)
      var resetTimeout = $interval(function () {
        userInfoService.checkUserInfo();
      }, 30000);

    $scope.$on("selectPlan", function (event, params) {
      vm.selectedPlanId = params.planId;
      vm.selectedCustomerName = params.customerName;
    });

    $scope.$on("closePlan", function (event, params) {
      vm.planOpen = false;
      vm.selectedPlanId = undefined;
      vm.selectedCustomerName = "";
    });

    $scope.$on("openPlan", function (event, params) {
      vm.planOpen = true;
      vm.selectedPlanId = params.planId;
      vm.selectedCustomerName = params.customerName;
    });

    var lastVersionListener = $rootScope.$on("showReleaseNoteAlert", function (
      event,
      showReleaseNoteAlert
    ) {
      vm.alertReleaseNotes = showReleaseNoteAlert;
    });

    var userProfileListener = $scope.$on("userProfileAvailable", function () {
      $timeout(function () {
        switch (userProfile.getEntity()) {
          case "NDC":
            vm.showSalesIQ = true;
            vm.isEDC = false;
            break;
          case "EDC":
            vm.showSalesIQ = false;
            vm.isEDC = true;
            break;
          default:
            vm.showSalesIQ = false;
            vm.isEDC = false;
        }

        if (
          userProfile &&
          userProfile.getUserProfile() &&
          userProfile.getUserProfile().entity &&
          userProfile.getUserProfile().entity.toUpperCase() !== "NDC"
        ) {
          vm.sidebar.salesIQ.display = false;
        }
      }, 100);
    });

    vm.sidebar = {
      adminConsole: {
        show: false,
        hasPermission: function () {
          return userProfile.isAdmin() || userProfile.getUserType() === "rbom";
        },
      },
      overview: {
        hasPermission: function () {
          if (userProfile.isAdmin() || mockSessionService.isMocking()) {
            return true;
          } else return false;
        },
      },
      tmwButton: {
        hasPermission: function () {
          return permissionsValidator.userTypeHasPermission("tmw");
        },
      },
      actionItems: {
        hasPermission: function () {
          return true;
        },
      },
      leads: {
        hasPermission: function () {
          return !vm.isUserTypeSalesRep(userProfile.getUserType());
        },
      },
      opb: {
        isEU: undefined,
      },
      salesIQ: {
        display: true,
      },
    };

    vm.popover = {
      templateUrl: "popoverNotifications.html",
    };

    vm.clickHelp = function () {
      utilService
        .uibModal()
        .open({
          component: "helpModal",
          size: "sm",
        })
        .result.then(
          function (result) {},
          function (dismiss) {}
        );
    };

    vm.clickPreferences = function () {
      utilService
        .uibModal()
        .open({
          component: "preferencesModal",
          size: "md",
        })
        .result.then(
          function (result) {
            $rootScope.$emit("clearCache", undefined);
          },
          function (dismiss) {}
        );
    };

    vm.clickHome = function () {
      $state.go("customerList");
    };

    vm.isMockSession = function () {
      return mockSessionService.hasSession();
    };

    vm.isUserTypeSalesRep = function (userType) {
      if (userType === undefined) {
        return false;
      }

      var userTypeUpperCased = userType.toUpperCase();

      if (userTypeUpperCased === "FSR" || userTypeUpperCased === "ISR") {
        return true;
      }

      return false;
    };

    vm.resetMock = function () {
      $rootScope.$emit("resetMock", undefined);

      sessionStorage.removeItem("selectedFsr");
      sessionStorage.removeItem("selectedOmsUser");
      sessionStorage.removeItem("omsProjects");
      sessionStorage.removeItem("omsEndDate");
      sessionStorage.removeItem("omsStartDate");
      sessionStorage.removeItem("omsTextFilter");
    };

    vm.clearCache = function () {
      sessionStorage.removeItem("omsProjects");
      $rootScope.$emit("clearCache", undefined);
    };

    vm.openCallReport = function () {
      if (vm.planOpen) {
        $state.go("callReport", {
          planId: vm.selectedPlanId,
          callReportId: null,
        });
        return;
      }
      utilService
        .uibModal()
        .open({
          component: "callReportModal",
          size: "sm",
          resolve: {
            modalData: function () {
              return {
                selectedPlanId: vm.selectedPlanId,
                selectedCustomerName: vm.selectedCustomerName,
              };
            },
          },
        })
        .result.then(
          function (result) {},
          function (dismiss) {}
        );
    };

    vm.openCallReportList = function () {
      $state.go("topLevelCallReports", { reporter: userProfile.getFullName() });
      return;
    };

    vm.$onDestroy = function () {
      lastVersionListener();
    };
  },
};
export default sidebarComponent;