import helpMenuTemplate from './helpMenu.html';

const helpMenu =  {
            template: helpMenuTemplate,
            bindings: {
            	isOpen: '=',
        		alertReleaseNotes: '<'
            },
            controller: function (
            		$state,
        			$rootScope,
        			$scope,
        			$timeout,
            		userProfile,
            		utilService) {

                var vm = this;
                
				if(userProfile.getFullName() !== 'undefined undefined') {
					initializeHelpMenu();
				}

    			var initListener = $scope.$on('userProfileAvailable', function () {
    				$timeout(initializeHelpMenu(), 100);
    			});
    			
    			function initializeHelpMenu() {
    				if(!userProfile) {
    					return;
    				}
    				
    				if(!userProfile.getUserProfile().helpContactName) {
    					return;
    				}

                    vm.helpContactName = userProfile.getUserProfile().helpContactName;

                    var helpContactNumber = userProfile.getUserProfile().helpContactNumber;
    				if(helpContactNumber && helpContactNumber.length == 4 ) {
    					helpContactNumber = "Ext. " + helpContactNumber;
    				}

                    vm.helpContactNumber = helpContactNumber;
                    vm.helpContactEmail = userProfile.getUserProfile().helpContactEmail;
    			}

                
                vm.isAdmin = function() {
                	return userProfile.isAdmin();
                }
                
                vm.openReleaseNotesModal = function() {
            	utilService.uibModal().open({
	              component: "releaseNotesModal",
	              size: "lg",
	            })
	            .result.then(
	            	function(result) {
	            		
	            	},
	            	function(dismiss) {
	            		
	            	});
                }

            }
        }
export default helpMenu;