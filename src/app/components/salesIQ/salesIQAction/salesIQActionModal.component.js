import salesIQActionModalTemplate from './salesIQActionModal.html';

const salesIQActionModal =  {
		template: salesIQActionModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller:
			function(
				utilService,
				planService,
				userProfile,
				salesIQService,
				$state,
				$stateParams
			) {
			
			var vm = this;
			vm.params = $stateParams;
			vm.reasonCdList = [];

			vm.opportunity = undefined;
			vm.action = undefined;
			vm.selectedOpportunityActionReason = undefined;
			vm.opportunityActionComments = undefined;


			vm.$onInit = function() {
				if(vm.resolve.modalData) {
					vm.data = vm.resolve.modalData;
					if(vm.data.opportunity) {
						vm.opportunity = vm.data.opportunity;
					}

					if(vm.data.action) {
						vm.action = vm.data.action;
					}

				var titlePrefix = vm.action === 'PURSUING' ? 'Pursue ' : vm.action === 'DISMISSED' ? 'Dismiss ' : vm.action === 'WON' ? 'Close-Win ' : vm.action === 'LOST' ? 'Close-Loss ' : '';
					vm.title = titlePrefix + 'Opportunity';
					vm.submitBtnTxt = 'Submit';

					loadActionReasons();
				}

				setTimeout(function () {
//					var input = angular.element(document.querySelector('#mfrValueInput'));
//					input.focus();
				}, 500);
			}

			vm.submit = function() {
				var opportunityAction = vm.opportunity; 
				opportunityAction.selectedOpportunityActionReason = vm.selectedOpportunityActionReason;
				opportunityAction.opportunityActionComments = vm.opportunityActionComments;
				opportunityAction.productGroupingInsightActionState = vm.action;
				opportunityAction.createdBy = userProfile.getSAMAccountName();
				opportunityAction.modifiedBy = userProfile.getSAMAccountName();
				opportunityAction.createdDateTime = Date.now();
				opportunityAction.modifiedDateTime = Date.now();

				salesIQService.actionOpportunity(opportunityAction)
				.then(function(response) {
					vm.modalInstance.close(response);
				})
				.catch(function(error) {
					
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}

			function loadActionReasons() {
				salesIQService.getOpportunityActionReasons()
				.then(function(response) {
					var allReasonCodes = response;

					if(!allReasonCodes || allReasonCodes.length === 0) {
						return;
					}
					var reasonCdList = [];

					for(let indx = 0; indx < allReasonCodes.length; indx++) {
						var reasonCode = allReasonCodes[indx];

						if(reasonCode.productGroupingInsightState === vm.action) {
							reasonCode.dropDownDescr = reasonCode.opportunityActionReasonCd + " = " + reasonCode.opportunityActionReasonDescr;
							reasonCdList.push(reasonCode);
						}
					}
					
					vm.reasonCdList = reasonCdList;
				})
				.catch(function(error) {
					
				})
			}
		}
	}
export default salesIQActionModal;