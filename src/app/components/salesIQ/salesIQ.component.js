import salesIQTemplate from './salesIQ.html';

const salesIQ =  {
		template: salesIQTemplate,
		controller: function(
			accountService,
			userProfile,
			utilService,
			planService,
			salesIQService,
			$rootScope,
			$state,
			$stateParams
		) {

			var vm = this;
			vm.editMode = true;
			vm.params = $stateParams;
			vm.opportunities = undefined;
			vm.accountList = undefined;
			vm.selectedAccount = undefined;
			
			vm.$onInit = function() {
				getSalesIQInfo();
			}
			
			
			function getSalesIQInfo() {
				var name = userProfile.getFullName();
				if(name !== 'undefined undefined') {
					vm.selectedUser = userProfile.getUserProfile();
					vm.params = $stateParams;
					vm.clientError = utilService.getClientErrorObject();
					vm.activeTabIndex = 0;
					
					loadPlan();
					loadOpportunities();
				}
				else {
					setTimeout(getSalesIQInfo, 200);
				}
			}


			var userProfileListener = $rootScope.$on('userProfileAvailable',function(event, container) {	
				vm.selectedUser = userProfile.getUserProfile();
			})
			
			// Load plan to $scope if it does not exist.
			function loadPlan() {			
				planService.lookupPlan(vm.params.planId)
				.then(function(response) {
					vm.plan = response;
					planService.setPlan(vm.plan);	
					loadContacts();
				})
				.catch(function(error) {
					
				})
			}

			function loadOpportunities() {

				var role = userProfile && userProfile.getUserType() ? userProfile.getUserType().toUpperCase() : undefined;
				var email = userProfile && userProfile.getMail() ? userProfile.getMail().toUpperCase() : undefined;

				salesIQService.getOpportunities(vm.params.planId)
				.then(function(response) {
					vm.opportunities = response;

					if(vm.opportunities && vm.opportunities.activeOpportunityList && vm.opportunities.activeOpportunityList.length > 0) {
						var activeOpptyList = vm.opportunities.activeOpportunityList;

						var acctList = [];
						var acctMap = new Map();

						var customerInsightKeyList = [];
						var customerInsightKeyMap = new Map();

						for (let indx = 0; indx < activeOpptyList.length; indx++) {
							var oppty = activeOpptyList[indx];

							var modifiedDate = oppty.productGroupingInsightLastModifiedDate;

							if(oppty.opportunityActionList && oppty.opportunityActionList.length > 0) {

								opportunityActionList = oppty.opportunityActionList.sort(
										function(oppty1, oppty2) {
											return (oppty1.modifiedDateTime > oppty2.modifiedDateTime) ? -1 : (oppty2.modifiedDateTime > oppty1.modifiedDateTime) ? 1 : 0;
										}
								);

								oppty.opportunityActionList = opportunityActionList;
								oppty.productGroupingInsightActionState = opportunityActionList[0].productGroupingInsightActionState;
								modifiedDate = opportunityActionList[0].modifiedDateTime;
							} else {
								oppty.productGroupingInsightActionState = oppty.productGroupingInsightState;
							}

							if(oppty.productGroupingInsightActionState === 'PURSUING' && new Date().getTime() > (modifiedDate + (30 * 24 * 60 * 60 * 1000))) {
								oppty.productGroupingInsightActionState = 'Aged';
								oppty.msg = 'Pursuing opportunity on ';
								oppty.msgDt = modifiedDate;
							}  else if(oppty.productGroupingInsightActionState === 'PURSUING' || oppty.productGroupingInsightActionState === 'WON'
								|| oppty.productGroupingInsightActionState === 'LOST' || oppty.productGroupingInsightActionState === 'DISMISSED') {
								var dispAction = oppty.productGroupingInsightActionState === 'PURSUING' ? 'Pursing' : oppty.productGroupingInsightActionState === 'WON' ? 'Won' : 
									oppty.productGroupingInsightActionState === 'DISMISSED' ? 'Dismissed' : 'Lost';
								oppty.msg = dispAction + ' opportunity on ';
								oppty.msgDt = modifiedDate;
								
							} else {
								oppty.productGroupingInsightActionState = 'No Action';								
							}

							oppty.productGroupingInsightActionStateDisplay =  oppty.productGroupingInsightActionState ? oppty.productGroupingInsightActionState.replace(/_/g,' ').toLowerCase() : undefined;

						    if(!acctMap.has(oppty.accountNumber)){
						    	acctMap.set(oppty.accountNumber, true);
						        acctList.push(oppty.accountNumber);
						    }

						    if(!customerInsightKeyMap.has(oppty.customerInsightKey)){
						    	customerInsightKeyMap.set(oppty.customerInsightKey, true);
						    	customerInsightKeyList.push(oppty.customerInsightKey);
						    }

						}

						vm.accountList = acctList;
						vm.selectedAccount = acctList[0];
						vm.selectedCustomerInsightKey = customerInsightKeyList[0];

						var selectedOpportunities = [];
						
						for (let indx = 0; indx < activeOpptyList.length; indx++) {
							var oppty = activeOpptyList[indx];

							if(oppty.accountNumber === vm.selectedAccount){
						    	selectedOpportunities.push(oppty);
						    }
						}
						
						selectedOpportunities = selectedOpportunities.sort(
								function(oppty1, oppty2) {
									return (oppty1.amount > oppty2.amount) ? -1 : (oppty2.amount > oppty1.amount) ? 1 : 0;
								}
						);
						
						vm.opportunities.selectedOpportunities = selectedOpportunities;
						loadVisualization(vm.params.planId, customerInsightKeyList);
						
					} else {
						vm.accountList = [];
					}
				})
				.catch(function(error) {
					
				})
			}
			
			vm.canAction = function(oppty) {
				var userEmail = userProfile.getMail().trim().toUpperCase();
				if(!vm.plan || !userEmail) return false;
				return ((oppty.productGroupingOpportunityType === 'Cross Sell' && vm.plan.ownerEmail.trim().toUpperCase() === userEmail)
						|| oppty.productGroupingOpportunityType === 'Lost Sales' && oppty.username.trim().toUpperCase() === userEmail);
			}
			
			function loadVisualization(planId, customerInsightKeyList) {
				if(!customerInsightKeyList || customerInsightKeyList.length == 0) {
					return;
				}

				vm.visualizations = [];

				for(let indx = 0; indx< customerInsightKeyList.length; indx++) {
					var customerInsightKey = customerInsightKeyList[indx];

					salesIQService.getVisualization(planId, customerInsightKey)
					.then(function(response) {
						var visualizations = response;
	
						var moneyMapSummary = [];
						var lostSalesOpportunities = [];
						var productSpends = [];
						var totalCustomerSpends = [];
						var partsToRecover = [];
						var partsToGrow = [];
						var salesIQMoneyMap = [];

						if(visualizations && visualizations.salesIQVisualizations && visualizations.salesIQVisualizations.length > 0) {
							for (let i = 0; i < visualizations.salesIQVisualizations.length; i++) {
								var visualization = visualizations.salesIQVisualizations[i];
								if(visualization.sectionName === "Money Map Summary") {
									moneyMapSummary.push(visualization);	
								}
								else if(visualization.sectionName === "Lost Sales Opportunity") {
									lostSalesOpportunities.push(visualization);
	
								}
								else if(visualization.sectionName === "Product Spend") {
									productSpends.push(visualization);
								}
								else if(visualization.sectionName === "Total Customer Spend") {
									totalCustomerSpends.push(visualization);
								}
								else if(visualization.sectionName === "Parts To Recover") {
									partsToRecover.push(visualization);
								}
								else if(visualization.sectionName === "Parts To Grow") {
									partsToGrow.push(visualization);
								}
								else if(visualization.sectionName === "Sales IQ Money Map") {
									salesIQMoneyMap.push(visualization);
								}
							}
						}
	
						var visualization = {
							customerInsightId: visualizations.customerInsightId,
							moneyMapSummary: moneyMapSummary,
							lostSalesOpportunities: lostSalesOpportunities,
							productSpends: productSpends,
							totalCustomerSpends: totalCustomerSpends,
							partsToRecover: partsToRecover,
							partsToGrow: partsToGrow,
							salesIQMoneyMap: salesIQMoneyMap
						};
						

						vm.visualizations.push(visualization);
						
						if(visualizations.customerInsightId === vm.selectedCustomerInsightKey) {
							vm.selectedVisualization = visualization;
						}
					})
					.catch(function(error) {
						
					})

					
				}
			}

			vm.actionOpportunity = function(action, opportunity) {
				utilService.uibModal().open({
					component: 'salesIQActionModal',
					size: 'sm',
					animation: true,
					keyboard: true,
					resolve: {
						modalData: function () {
							return {
								action: action,
								opportunity: opportunity
							}
						}
                  }
				}).result.then(
					function(close) {
						for(let indx = 0; indx < vm.opportunities.selectedOpportunities.length; indx++) {
							var oppty = vm.opportunities.selectedOpportunities[indx];
							if(oppty.customerInsightKey == close.customerInsightKey && oppty.productGroupingKey == close.productGroupingKey) {
								oppty.productGroupingInsightActionStateDisplay =  oppty.productGroupingInsightActionState ? oppty.productGroupingInsightActionState.replace(/_/g,' ').toLowerCase() : undefined;
							}
						}
					},
					function(dismiss) {
					}
				)
			}

			vm.setAccount = function(account) {
				vm.selectedAccount = account;
				var selectedOpportunities = [];
				
				for (let indx = 0; indx < vm.opportunities.activeOpportunityList.length; indx++) {
					var oppty = vm.opportunities.activeOpportunityList[indx];
				    if(oppty.accountNumber === vm.selectedAccount){
				    	selectedOpportunities.push(oppty);
				    }
				}
				
				selectedOpportunities = selectedOpportunities.sort(
						function(oppty1, oppty2) {
							return (oppty1.amount > oppty2.amount) ? -1 : (oppty2.amount > oppty1.amount) ? 1 : 0;
						}
				);
				
				vm.opportunities.selectedOpportunities = selectedOpportunities;
				
				if(vm.opportunities.selectedOpportunities && vm.opportunities.selectedOpportunities.length > 0) {
					vm.selectedCustomerInsightKey = vm.opportunities.selectedOpportunities[0].customerInsightKey;
					for(let indx = 0; indx < vm.visualizations.length; indx++) {
						var visualization  = vm.visualizations[indx];
						if(visualization.customerInsightId === vm.selectedCustomerInsightKey) {
							vm.selectedVisualization = visualization;
							break;
						}
					}
					
				}
			}
			
			vm.$onDestroy = function() {
				userProfileListener();
			}
		}
	}
export default salesIQ;