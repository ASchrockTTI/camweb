import businessPlanTemplate from './businessPlan.html';

const businessPlan =  {
		template: businessPlanTemplate,
		controller: function(
				$interval,				
				utilService,
				planService,
				permissionsFactory,
				userProfile,				
				userInfoService,
				roles,
				$stateParams,
				$rootScope,
				$scope,
				$location) {
		
			var vm = this;
			var isEditName = true;			
			var editFunctionName = 'Edit';
			var resetFunctionName = 'Cancel';
			var planId = $stateParams.planId;
			var orgPlan;
			var auditLog = [];
			
			var permissionsValidator;
			
			vm.$onInit = function() {
				
				vm.clientError = utilService.getClientErrorObject();
				vm.params = $stateParams;
				$location.search({}).replace();
				
				vm.isEditable = false;			
				vm.EditOrReset = editFunctionName;
				vm.plan = undefined;
				
				permissionsValidator = permissionsFactory.newInstance();
				
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.EXEC, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.EXEC, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.EXEC, roles.GM, roles.ISM, roles.FSM, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setCustomPermission('archive', [roles.OWNER, roles.FSM, roles.GM, roles.EXEC, roles.RBOM, roles.ADMIN, roles.BDG], null, null);
				
				vm.businessPlan = {
					actionable: {
						editPlan: false
					},
					hasPermission: function(action) {
						return permissionsValidator.hasPermission(action, vm.plan);		
					},
					customerPositions: [
						{value: '1', display: 'Defend'},
						{value: '2', display: 'Expand'},
						{value: '3', display: 'Review'},
					]
				}
				
				vm.isrPopover = [
					{
						title: 'hello',
						templateUrl: 'multipleIsrList.html'
					}
				]
				
				retrievePlanById(planId);	
			}			
			
			
			vm.showVisits = function() {
				if(typeof vm.plan !== 'undefined') {
					return true;
				}
			}
			
			vm.showStrategicSwitch = function() {
				if( utilService.equalsIgnoreCaseSpace(userProfile.getUserTypeName(), 'ADMIN')) { return true;}
				if( utilService.equalsIgnoreCaseSpace(userProfile.getUserTypeName(), 'SAM_GAM')) { return true;}
				return false;
			}
			
			vm.positionLabel = function(text) {
				
				if (!text) {
					return '';
				}
				
				switch(text) {
				case 'Defend':
					return 'label label-success';				
				case 'Expand':
					return 'label label-warning';
				case 'Review':
					return 'label label-danger';
				default:
					return '';
				}
				
			}
			
			
			function retrievePlanById(planId) {
				
				planService.getBusinessPlan(planId)
				.then(function(response) {
					
					if (response.customerObjectiveAuditLog != null) {
						response.customerObjectiveAuditLog.modifyDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(response.customerObjectiveAuditLog.modifyDateTime));
					}
					
					if (response.ttiObjectiveAuditLog != null) {
						response.ttiObjectiveAuditLog.modifyDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(response.ttiObjectiveAuditLog.modifyDateTime));
					}
					
					if (response.landscapeAuditLog != null) {
						response.landscapeAuditLog.modifyDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(response.landscapeAuditLog.modifyDateTime));
					}
					
					vm.plan = response;
					
					utilService.getLogger().info(response);
					if (response.insideSalesPersonName != null) {
						var isrList = response.insideSalesPersonName.substring(0,response.insideSalesPersonName.length).trim().split(',');
						vm.insideSalesPersonList = isrList;
					}
					
					if(vm.plan.planType.planTypeId < 5) {
						if(userProfile.getUserType() === 'sam_gam') vm.allowEdit = true;
					}
					
					planService.setPlan(vm.plan);
					
				
					orgPlan = utilService.deepCopy(vm.plan);
					
					if (vm.plan.customerPosition) {
						vm.plan.selectedCustomerPosition = {
							value: vm.plan.customerPosition.customerPositionID,
							display: vm.plan.customerPosition.customerPosition
						}
					}
					
					_loadPlan(response);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			function swapIsEditable() {
				vm.isEditable = !vm.isEditable;
			}
			
			function setFormPristine() {
				vm.BPForm.$setPristine();
			}
			
			function _loadPlan(plan) {
				vm.plan = plan;		
				vm.businessPlan.actionable.editPlan = vm.businessPlan.hasPermission('edit', vm.plan);
			}
			
			function setArchiveButtons() {
				vm.showArchiveButton = false;
				vm.showUnarchiveButton = false;
				let hasPermission = permissionsValidator.hasPermission('archive', vm.plan);
				if(!vm.isEditable || vm.plan.planType.planTypeId !== 6 || !hasPermission) {
					return;
				}
				if(vm.plan.planStatus.planStatusId === 4) vm.showUnarchiveButton = true;
				else vm.showArchiveButton = true;
			}
			
			
			vm.editOrReset = function( ) {
				switch(vm.EditOrReset) {
				    case editFunctionName:
				    	vm.edit();
				        break;
				    case resetFunctionName:
				    	vm.reset();
				        break;
				    default:
				        break;
				}
				setArchiveButtons();
			}
			
			vm.disableSave = function() {
				
				if (!vm.plan) {
					return true;
				}
				
				if (vm.plan.customerName == '') {
					return true;
				}
				if (!vm.BPForm.$dirty) {
					return true;
				}
				return false;
			}
			
//			vm.isPlanNotEditable = function() {
//				return planService.isNotCurrentYear();
//			}
			
			vm.edit = function( ) {	
				swapIsEditable();
				vm.EditOrReset = resetFunctionName;
			}
					
			vm.update = function( ) {
				
				var container = planService.getNewPlanContainer();
				
				if (vm.plan.selectedCustomerPosition && (!vm.plan.customerPosition || vm.plan.selectedCustomerPosition.value != vm.plan.customerPosition.customerPositionID)) {
					
					vm.plan.customerPosition = {
						customerPositionID: vm.plan.selectedCustomerPosition.value,
						customerPosition: vm.plan.selectedCustomerPosition.display
					}
				}
				
				container.plan = vm.plan;								
				container.securityProfile = userProfile.getUserProfile();
				
				container.auditLog = auditLog;
				
				userInfoService.checkUserInfo()
				.then(function() {
					planService.updatePlan(vm.plan.planId, container)
					.then(function() {
						// reset to original state
						vm.EditOrReset = editFunctionName;
						vm.isEditable = false;
						isEditName = true;
						setFormPristine();
						sessionStorage.removeItem('sessionPlans');
						sessionStorage.removeItem('sessionPlansNonTTI');
						auditLog = [];
						retrievePlanById(vm.plan.planId);
						$scope.$emit('businessPlanEdited');
					})
					.catch(function(error) {
						vm.clientError.message = error;
						auditLog = [];
					})
				})
				.catch(function(error) {
					vm.clientError.message = error;
					auditLog = [];
				})
				
			}
			
			vm.reset = function( ) {
				
				if (vm.BPForm.$dirty) {
					
					utilService.deleteConfirmation('Cancel Confirmation', 'Changes have been made to this page.', 'btn-warning').result					
					.then(
						function(result) {
							if (result == true) {
								retrievePlanById(planId);
								swapIsEditable();
								setFormPristine();
								vm.EditOrReset = editFunctionName;
							}
						},
						function(dismiss) {
							
						}
					)
				} else {
					swapIsEditable();
					vm.EditOrReset = editFunctionName;
				}
				
			}
			
			vm.archive = function() {
				if (vm.BPForm.$dirty) {
					
					utilService.deleteConfirmation('Cancel Confirmation', 'Changes have been made to this page.', 'btn-warning').result					
					.then(
						function(result) {
							if (result == true) {
								openArchiveModal();
							}
						},
						function(dismiss) {
							
						}
					)
				} else {
					openArchiveModal();
				}
			}
			
			
			function openArchiveModal( ) {
				utilService.uibModal().open({
                    component: 'archiveModal',
                    size: 'sm',
                    scope: $scope,
                    resolve: { 
                    	modalData: function() {
                        	return {
                        		planId: vm.plan.planId
                        	}
                    	}
                    }
                }).result.then(
                    function(result) {
                    	vm.plan.planStatus = {planStatus: 'Archived', planStatusId: 4};
                    	sessionStorage.removeItem('sessionPlansNonTTI');
                    	$scope.$emit('businessPlanEdited');
						swapIsEditable();
						setFormPristine();
						vm.EditOrReset = editFunctionName;
						setArchiveButtons();
                    },
                    function(dismiss) {
                        
                    }
                )
			} 
			
			vm.swapYN = function( ) {
				queryScordCardYN();
			}
		}
	}
export default businessPlan;