import archiveModalTemplate from './archiveModal.html';

const archiveModal =  {
		template: archiveModalTemplate,
		bindings: {
			modalInstance: '<',
			resolve: '<'
		},
		controller: function(
			$scope,
			$stateParams,
			planService
		) {
		
			var vm = this;
			
			vm.$onInit = function() {
				vm.reason = '';
				vm.planId = vm.resolve.modalData.planId;
			}
			
			
			vm.archive = function() {
				planService.archivePlan(vm.planId, vm.reason)
				.then(function(response) {
					vm.modalInstance.close(response);
				})
				.catch(function(error) {
					vm.error = error;
				})
			}
			
			vm.cancel = function() {
				vm.modalInstance.dismiss('cancel');
			}
		}
	}
export default archiveModal;