import regionTemplate from './region.html';

const region =  {
		template: regionTemplate,
		bindings: {
			plan: '<'
		},
		controller: function($rootScope, financialService, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.loadingEntities = false;
			}

			vm.$onDestroy = function() {
				regionPlanLookupListener();
			}
			
			vm.$onChanges = function(changes) {
				if (changes.plan.currentValue) {
					vm.plan = changes.plan.currentValue;
					getBIData(vm.plan.planId, 'Global');
				}
			}
			
			vm.financeRow = {
				data: undefined,
				index: undefined,
				
				select: function(region, index) {
					
					var plan = vm.plan;
					var entity = region.entity === 'Total' ? 'GLOBAL' : region.entity;
					
					utilService.location.path('plans/' + vm.plan.planId + '/financials/' + entity);
					
				}
			}
			
			var regionPlanLookupListener = $rootScope.$on('region-plan-lookup', function(event, value) {
				vm.plan = value.plan;
				getBIData(value.plan.planId, value.entity)
			})
			
			function getBIData(planId, entity) {
				vm.loadingEntities = true;
				vm.entities = [];
				financialService.getEntityFinancials(planId)
				.then(function(response) {
					
					vm.entities = response;	
					
				})
				.catch(function(error) {
					
					vm.clientError.message = error;				
				})
				.finally(function() {
					vm.loadingEntities = false;
				})
			}
		}
	}
export default region;