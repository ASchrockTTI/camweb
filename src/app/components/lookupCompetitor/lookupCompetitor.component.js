import lookupCompetitorTemplate from "./lookupCompetitor.html";

const lookupCompetitor = {
  template: lookupCompetitorTemplate,
  bindings: {
    ngModel: "=",
    minLength: "<",
    onSelect: "&",
    lookupPlaceholder: "<",
    inputId: "<",
    tooltipPosition: "<",
    tooltipText: "<",
    hideInfo: "<",
  },
  controller: function (
    $attrs,
    $rootScope,
    nuggetsService,
    utilService,
    $filter,
    $stateParams
  ) {
    var vm = this;

    vm.$onInit = function () {
      vm.minLength = vm.minLength === undefined ? 2 : vm.minLength;
      if (!vm.lookupPlaceholder) {
        vm.lookupPlaceholder = "Lookup Competitor...";
      }
      loadCompetitors();
    };

    vm.$onDestroy = function () {
      lookupCompetitorClearTextListener();
    };

    function loadCompetitors() {
      nuggetsService
        .getNuggetCompetitors()
        .then(function (response) {
          vm.competitors = response;
        })
        .catch(function (error) {
          vm.clientError.message = error;
        });
    }

    vm.findCompetitors = function (value) {
      if (!value || !vm.competitors) {
        return [];
      }

      var filtered = [];
      angular.forEach(vm.competitors, function (item) {
        if (!item.competitor || !item.competitor === "") return;
        if (item.competitor.toUpperCase().indexOf(value.toUpperCase()) >= 0)
          filtered.push(item);
      });

      return filtered;
    };

    vm.addCompetitor = function () {
      if (!vm.ngModel || vm.ngModel === "") {
        return;
      }

      var competitorStruct = {
        competitor: vm.ngModel,
      };

      nuggetsService
        .createCompetitor(competitorStruct)
        .then(function (response) {
          var competitor = {
            competitor: response.competitor,
            createdBy: response.createdBy,
            createdDateTime: response.createdDateTime,
            modifiedBy: response.modifiedBy,
            modifiedDateTime: response.modifiedDateTime,
            nuggetCompetitorID: response.nuggetCompetitorID,
          };

          if (vm.competitors) {
            vm.competitors.push(competitor);
          }

          vm.onSelect({ competitor: competitor });
        })
        .catch(function (error) {
          vm.clientError.message = error;
        });
    };

    var lookupCompetitorClearTextListener = $rootScope.$on(
      "lookupCompetitor-clear-text",
      function (event, value) {
        vm.ngModel = undefined;
      }
    );
  },
};
export default lookupCompetitor;
