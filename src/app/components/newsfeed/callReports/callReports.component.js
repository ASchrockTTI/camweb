import callReportsTemplate from './callReports.html';

const callReports =  {
		template: callReportsTemplate,
		bindings: {
			plan: '<',
		},
		controller: function(
			$stateParams,
			$state,
			callReportService,
			newsfeedService,
			$window,
			$sce,
			FileSaver,
			userProfile,
			utilService,
			roles,
			permissionsFactory
		) {
			
			
			var vm = this;
			vm.params = $stateParams;
			
			var permissionsValidator;
			vm.callReports = undefined;
			
			vm.$onInit = function() {
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setCustomPermission('download', [roles.ADMIN, roles.OWNER, roles.BRANCH_MEMBER]);
				
				loadCallReports();
			}
			
			function loadCallReports() {
				callReportService.retrieveSubmittedCallReports(vm.params.planId)
				.then(function(response) {
					vm.callReports = response.response;
				})
				.catch(function(error) {
					
				});
			}
			
			vm.hasDownloadPermission = function() {
				return permissionsValidator.hasPermission('download', vm.plan);
			}
			
			vm.downloadDocument = function(document) {
				if(!vm.hasDownloadPermission()) return;
				
				newsfeedService.downloadDocument(vm.params.planId, document.documentID)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
			}
			
			vm.openCallReport = function(callReportId) {
				$state.go('callReport', {planId: $stateParams.planId, callReportId: callReportId}); 
			}
			
			vm.isAdmin = function() {
				return userProfile.isAdmin();
			}
			vm.isReporter = function(callReport) {
				return utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), callReport.reporter)
			}
			
			vm.deleteCallReport = function(callReportId) {
				utilService.deleteConfirmation(
			              "Call Report",
			              "You are about to discard the selected call report and any attached files. \nIf this call report is from the current quarter, your TMW will be affected.",
			              "btn-danger"
	            )
	            .result.then(
	            	function(result) {
	            		if(result) {
		            		callReportService.deleteCallReport(vm.params.planId, callReportId);
	            			for (var i = 0; i < vm.callReports.length; i++) {
		            			if (vm.callReports[i].callReport.callReportId === callReportId) {
		            				vm.callReports.splice(i, 1);
		            		    }
		            		}
	            		}
	            	},
	            	function(dismiss) {
	            		
	            	});
			}
			
		}
	}
export default callReports;