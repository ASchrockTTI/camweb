const uploadFile = {
		template: '<input id="selectedFile" class="form-control" type="file" file-model="$ctrl.myFile" style="position: absolute; visibility: hidden; margin-top: 10px; float: right;" />' +
				  '<button style="margin-top: 10px;" class="btn btn-default" type="button" value="" onclick="selectedFile.click()">Browse...</button>',
		bindings: {
			
		},
		controller: function() {
			var vm = this;
			
			vm.$onInit = function() {
				vm.myFile = {};
			}
			
			vm.selectedFile = function(myFile) {
				//This makes sure the input box clears a previous selection. 
				//This fixes a bug where the same file is attempted to be uploaded twice in a row.				
				
				//This opens the file browser dialog
				angular.element('#selectedFile').click();
				
			}
		}
	}
	const fileModel = function($parse, $rootScope, utilService) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;
				
				element.bind('change', function() {
					scope.$apply(function() {
						if (element[0].files[0]) {
							//modelSetter(scope,element[0].files[0]);
							$rootScope.$emit('fileAdded', element[0].files[0]);							
						} 
					})
					element[0].value = null;
				})
				element.on('$destroy', function() {
					element.off();
				})
			}
		}
	}
export  {uploadFile, fileModel};