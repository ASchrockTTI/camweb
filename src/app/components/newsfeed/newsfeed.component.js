import newsfeedTemplate from './newsfeed.html';

const newsfeed =  {
		template: newsfeedTemplate,
		controller: function($rootScope, $stateParams, planService, utilService, newsfeedService, $state, $scope, userProfile, mockSessionService) {
			
			var vm = this;
			vm.activeTabIndex = 0;
			
			vm.$onInit = function() {
				vm.params = $stateParams;
				
				if($stateParams.activityTab === 'call-reports') {
					vm.activeTabIndex = 1;
				} else {
					vm.activeTabIndex = 0;
				}
				
				
				retrievePlanById();
				getNewsFeed();
			}
			
			vm.$onDestroy = function() {
				newsfeedRefreshListener();
			}
			
			
			vm.isAdmin = function() {
				if(userProfile.isAdmin() || mockSessionService.isMocking()) {
					return true;
				}
				return false;
			}
			
			
			$scope.$watch(function(){ return vm.params.activityTab }, function(){
				 if($stateParams.activityTab === 'call-reports') {
						vm.activeTabIndex = 1;
					} else {
						vm.activeTabIndex = 0;
					}
			 });
			
			vm.selectTab = function(tab) {
				$state.go('.', {activityTab: tab});
			}
			
			function retrievePlanById() {
				
				planService.lookupPlan(vm.params.planId)
				.then(function(response) {
					vm.plan = response;
					planService.setPlan(vm.plan);
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			var newsfeedRefreshListener = $rootScope.$on('newsfeed-refresh', function(event, value) {
				getNewsFeed();
			})

			function getNewsFeed() {
				
				newsfeedService.get(vm.params.planId)
				.then(function(response) {
					
					vm.comments = newsfeedService.getNewsFeed();
					vm.documents = newsfeedService.getDocuments();
		
				})
				.catch(function(error) {
					
				})
			}
		}
	}
    const searchContent = function(utilService) {
	   return function(items, search){
		   
		   if (!items) {
			   return items;
		   }
		   
		   for (var i = 0; i <= items.length - 1; i++) {
			   items[i].highlightMe = false;
			   
			   if (items[i].replies) {
				   
				   for (var j = 0; j <= items[i].replies.length - 1; j++) {
					   items[i].replies[j].highlightMe = false;
					   
					   if (items[i].replies[j].replies) {
						   
						   for (var z = 0; z <= items[i].replies[j].replies.length - 1; z++) {
							   items[i].replies[j].replies[z].highlightMe = false;
						   }
						   
					   }
				   }
			   }
		   }				   
		   
		   if (!search || search.$ == '') {
			   return items;
		   }
		   
		   if (search.$.length < 4) {
			   return items;
		   }

		   var found = [];
		   var highlightSet = false;
		   var list = [];
		   var temp = [];
		   var count = 0;
		   
		   found = utilService.filterBy(items, search.$);				   				   				 				   

		   for (var a = 0; a <= items.length - 1; a++) {
			   
			   if (items[a].replies == undefined) {
				   
				   angular.forEach(items[a], function(value, key) {
					   var s = String(value);
					   if (s.search(new RegExp(search.$, "i")) != -1) {			
						   utilService.getLogger().info(s);
						   items[a].highlightMe = true;
						   list.push(items[a]);								   
					   }												   
				   })
				   
				   continue;
			   }
			   					   
			   for (var b = 0; b <= items[a].replies.length - 1; b++) {
				   						   
				   if (items[a].replies[b].replies == undefined) {							   
					   angular.forEach(items[a].replies[b], function(value, key) {
						   var s = String(value);
						   if (s.search(new RegExp(search.$, "i")) != -1) {												   
							   items[a].replies[b].highlightMe = true;									   
							   list.push(items[a].replies[b])								   
						   }
						   
						   if (typeof value === 'object' && key == 'documentList') {
							   utilService.getLogger().info(key);
						   }
					   })
					   continue;
				   }
				   
				   for (var c = 0; c <= items[a].replies[b].replies.length - 1; c++) {
					   							   
					   angular.forEach(items[a].replies[b].replies[c], function(value, key) {
						   var s = String(value);
						   if (s.search(new RegExp(search.$, "i")) != -1) {												   
							   items[a].replies[b].replies[c].highlightMe = true;									   
							   list.push(items[a].replies[b]);									   									   
						   }								  
						   
						   if (typeof value === 'object' && key == 'documentList') {
							   utilService.getLogger().info(key);
						   }
					   })
					   
					   angular.forEach(items[a].replies[b], function(value, key) {
						   var s = String(value);
						   if (s.search(new RegExp(search.$, "i")) != -1) {
							   items[a].replies[b].highlightMe = true;
						   }
						   
						   if (typeof value === 'object' && key == 'documentList') {

							   angular.forEach(value, function(value, key) {
								   if (s.search(new RegExp(search.$, "i")) != -1) {												   
									   items[a].replies[b].replies[c].highlightMe = true;									   
									   list.push(items[a].replies[b]);									   									   
								   }
							   })
							   
						   }
					   })
				   }

				   angular.forEach(items[a].replies[b], function(value, key) {
					   var s = String(value);
					   if (s.search(new RegExp(search.$, "i")) != -1) {												   
						   items[a].replies[b].highlightMe = true;								   
						   list.push(items[a])								   
					   }												   
				   })
				   
			   }
			   
			   angular.forEach(items[a], function(value, key) {
				   var s = String(value);
				   if (s.search(new RegExp(search.$, "i")) != -1) {										   
					   items[a].highlightMe = true;							   
					   list.push(items[a])
				   }												   
			   })
		   }
		  				   
		   return found;
		   
	   }
   }
export {newsfeed, searchContent};