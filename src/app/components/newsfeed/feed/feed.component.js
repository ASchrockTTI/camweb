import feedTemplate from './feed.html';

const feed =  {
		template: feedTemplate,
		bindings: {
			plan: '<',
			commentList: '<',
			hideNewComment: '<',
			hideControls: '<',
			hideAttachments: '<'
		},
		controller: function($rootScope, $anchorScroll, permissionsFactory, newsfeedService, userProfile, utilService, roles,
				userInfoService, $attrs) {
			
			var vm = this;			
			var permissionsValidator;
			
			vm.$onInit = function() {
				vm.newFile = {};
				vm.addingNewComment = false;
				vm.clientError = utilService.getClientErrorObject();
				
				permissionsValidator = permissionsFactory.newInstance();
				
				//permissionService.clearPermissions();
				permissionsValidator.setAddRoles([roles.OWNER, roles.ADMIN, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setEditRoles([roles.OWNER, roles.ADMIN, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setDeleteRoles([roles.OWNER, roles.ADMIN, roles.RSE, roles.BRANCH_MEMBER]);
				permissionsValidator.setCustomPermission('download', [roles.ADMIN, roles.OWNER, roles.EXECUTIVE, roles.RVP, roles.RSE, roles.BRANCH_MEMBER]);
			}
			
			vm.$postLink = function() {
				var row = angular.element('#newCommentRow');
								
				if (vm.hideNewComment) {
					row[0].setAttribute('style','display: none');
				}
				
				var div = angular.element('#commentsDiv');
				
				if ($attrs.style) {
					div[0].setAttribute('style', $attrs.style + 'overflow-x: none; overflow-y: auto;');
				}
				
			}
			
			vm.setDocument = function(item) {
				vm.newFile = item;
			}
			
			vm.$onChanges = function(changes) {
				if (changes.plan) {
					vm.plan = changes.plan.currentValue;
				}
				if (changes.commentList) {
					vm.commentList = changes.commentList.currentValue;
				}
			}
			
			vm.permissions ={
				validated: function(action) {
					return permissionsValidator.hasPermission(action, vm.plan);				
				},	
			} 
			
			vm.downloadDocument = function(document) {			
				$rootScope.$broadcast('downloadDocument', document);			
			}
			
			vm.replyComment = function(comment) {
				
				var reply = {
					newsFeedID: undefined, planID: vm.plan.planId, newsFeed: undefined, author: undefined, dateTime: undefined,
					documentList: [], newComment: true, parentNewsFeedID: comment.newsFeedID, newReply1: true
				}
				
				if (comment.replies) {
					comment.replies.unshift(reply);
				} else {
					comment.replies = [];
					comment.replies.push(reply);
				}
				
				vm.addingNewComment = true;
				
			}
			
			vm.addComment = function() {
				
				var newComment = {
					newsFeedID: undefined, planID: vm.plan.planId, newsFeed: undefined, author: undefined, dateTime: undefined,
					documentList: [], newComment: true
				}
				
				vm.commentList.unshift(newComment);	
				
				vm.addingNewComment = true;
				
				angular.element('#commentsDiv').animate({scrollTop: 0}, 'fast');
			}
			
			vm.replyComment = function(comment) {
				
				var reply = {
					newsFeedID: undefined, planID: vm.plan.planId, newsFeed: undefined, author: undefined, dateTime: undefined,
					documentList: [], newComment: true, parentNewsFeedID: comment.newsFeedID, newReply1: true
				}
				
				if (comment.replies) {
					comment.replies.unshift(reply);
				} else {
					comment.replies = [];
					comment.replies.push(reply);
				}
				
				vm.addingNewComment = true;
				
			}
			
			vm.replyReply = function(reply) {
				var replyAgain = {
						newsFeedID: undefined, planID: vm.plan.planId, newsFeed: undefined, author: undefined, dateTime: undefined,
						documentList: [], newComment: true, parentNewsFeedID: reply.newsFeedID, newReply2: true
					}
					
					if (reply.replies) {
						reply.replies.unshift(replyAgain);
					} else {
						reply.replies = [];
						reply.replies.push(replyAgain);
					}
				
				vm.addingNewComment = true;
			}
			
			vm.cancelComment = function(comment) {
				
				if (comment.edit) {
					comment.newsFeed = oldComment;
					comment.edit = false;
				} else {
					vm.commentList.shift();
				}				
				
				vm.editingComment = false;
				vm.addingNewComment = false;
			}			
			
			vm.cancelReply = function(comment, reply) {
				
				if (reply.edit) {
					reply.newsFeed = oldComment
					reply.edit = false;
				} else {
					comment.replies.shift();
				}
						
				vm.addingNewComment = false;
			}
			
			vm.cancelReplyAgain = function(reply, replyAgain) {
				
				if (replyAgain.edit) {
					replyAgain.newsFeed = oldComment;
					replyAgain.edit = false;
				} else {
					reply.replies.shift();
				}
							
				vm.addingNewComment = false;
			}
			
			vm.saveReply = function(item, document) {
				var container = newsfeedService.getNewsFeedContainer();
								
				container = {
					planID: vm.plan.planId,
					newsFeedID: item.edit ? item.newsFeedID : undefined,
					parentNewsFeedID: item.parentNewsFeedID,
					newsFeed: item.newsFeed,
					author: userProfile.getFullName(),
					dateTime: new Date().getTime()
				}				
								
				newsfeedService.saveNewsFeed(vm.plan.planId, container)
				.then(function(response) {
					item.newsFeedID = response.newsFeedID;
					item.author = response.author;
					item.dateTime = response.dateTime;
					item.documentList = response.documentList;
					item.highlightMe = false
					item.newReply1 = false;
					
					uploadDocument(vm.plan.planId, response.newsFeedID, item, document);
					
					vm.addingNewComment = false;
					
					if (item.edit) {
						item.edit = false;
						item.modifiedBy = response.modifiedBy;
						item.modifiedDateTime = response.modifiedDateTime;
						if (document.file == undefined) {
							item.documentList = oldDocumentList;
						}
					}
									
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}	
			
			vm.saveReplyAgain = function(item, document) {
				var container = newsfeedService.getNewsFeedContainer();
								
				container = {
					planID: vm.plan.planId,
					newsFeedID: item.edit ? item.newsFeedID : undefined,
					parentNewsFeedID: item.parentNewsFeedID,
					newsFeed: item.newsFeed,
					author: userProfile.getFullName(),
					dateTime: new Date().getTime()
				}
				
				newsfeedService.saveNewsFeed(vm.plan.planId, container)
				.then(function(response) {
					item.newsFeedID = response.newsFeedID;
					item.author = response.author;
					item.dateTime = response.dateTime;
					item.documentList = response.documentList;
					item.highlightMe = false
					item.newReply2 = false;
					
					uploadDocument(vm.plan.planId, response.newsFeedID, item, document);
					
					vm.addingNewComment = false;
					
					if (item.edit) {
						item.edit = false;
						item.modifiedBy = response.modifiedBy;
						item.modifiedDateTime = response.modifiedDateTime;
						
						if (document.file == undefined) {
							item.documentList = oldDocumentList;
						}
						
					}
									
				})
				.catch(function(error) {
					vm.clientError.message = error;
				})
			}
			
			vm.saveNewsFeed = function(item, document) {
				if(vm.debounce) return;
				vm.debounce = true;
				
				var container = newsfeedService.getNewsFeedContainer();				
				container = {
					planID: vm.plan.planId,
					newsFeedID: item.edit ? item.newsFeedID : undefined,
					parentNewsFeedID: item.parentNewsFeedID,
					newsFeed: item.newsFeed,
					author: userProfile.getFullName(),
					dateTime: new Date().getTime()
				}

				userInfoService.checkUserInfo()
				.then(function() {
					newsfeedService.saveNewsFeed(vm.plan.planId, container)
					.then(function(response) {
						vm.debounce = false;
						item.newsFeedID = response.newsFeedID;
						item.author = response.author;
						item.dateTime = response.dateTime;
						item.documentList = response.documentList;
						item.highlightMe = false
						item.newComment = false;
						
						
						uploadDocument(vm.plan.planId, response.newsFeedID, item, document);
						
						vm.addingNewComment = false;
						
						if (item.edit) {
							item.edit = false;
							item.modifiedBy = response.modifiedBy;
							item.modifiedDateTime = response.modifiedDateTime;							
							if (document.file == undefined) {
								item.documentList = oldDocumentList;
							}
						}
						
					})
					.catch(function(error) {
						vm.debounce = false;
						//vm.clientError.message = error;
						vm.saveNewsFeed(item,document);
					})
				})
				.catch(function(error) {
					vm.debounce = false;
					throw error;
				})
				
				
			}
			
			vm.setIcon = function(document, size) {
				
				var size = size ? size : '';
				
				switch(document.documentExt.toLowerCase()) {
				case 'pdf':
					return 'far fa-file-pdf ' + size;
				case 'xlsx':
				case 'xls':
				case 'csv':
					return 'far fa-file-excel ' + size;
				case 'docx':
				case 'doc':
					return 'far fa-file-word ' + size;
				case 'pptx':
				case 'ppt':
					return 'far fa-file-powerpoint ' + size;
				case 'png':
				case 'jpeg':
				case 'jpg':
				case 'bmp':
				case 'gif':
				case 'tiff':
					return 'far fa-file-image ' + size;
				case 'zip':
					return 'far fa-file-archive ' + size;
				case 'msg':
					return 'far fa-envelope ' + size;
				default:
					return 'far fa-file ' + size;
				}
				
			}
			
			var oldComment;
			var oldDocumentList;
			vm.editComment = function(comment) {
				oldComment = comment.newsFeed;
				oldDocumentList = comment.documentList;
				comment.edit = true;
				
			}
			
			vm.deleteReply = function(parent, reply) {
				
				utilService.deleteConfirmation('Delete Confirmation', 'You are about to delete the selected comment and any replies associated to it.', 'btn-danger').result
				.then(
					function(results) {
						if (results == true) {
							
							newsfeedService.deleteNewsFeed(vm.plan.planId, reply.newsFeedID)
							.then(function(response) {
								parent.replies = parent.replies.filter(function(item) {
									return item.newsFeedID != reply.newsFeedID
								})
								
								$rootScope.$emit('newsfeed-refresh', undefined);

							})
							.catch(function(error) {
								
							})
						}
					},
					function(dismiss) {
						vm.clientError.message = error;
					}
				)
			}
			
			vm.deleteComment = function(comment) {
								
				utilService.deleteConfirmation('Delete Confirmation', 'You are about to delete the selected comment and any replies associated to it.', 'btn-danger').result
				.then(
					function(results) {
						if (results == true) {
							
							newsfeedService.deleteNewsFeed(vm.plan.planId, comment.newsFeedID)
							.then(function(response) {
								vm.commentList = vm.commentList.filter(function(item) {
									return item.newsFeedID != comment.newsFeedID
								})
								$rootScope.$emit('newsfeed-refresh', undefined);
							})
							.catch(function(error) {
								
							})
						}
					},
					function(dismiss) {
						vm.clientError.message = error;
					}
				)
			}
			
			vm.permitEdit = function(comment) {
				
				if (utilService.equalsIgnoreCaseSpace(comment.author, userProfile.getFullName())) {
					return true;
				}
				
				return false;
			}
			
			
			vm.deleteAttachment = function(comment, document) {
				
				utilService.deleteConfirmation('Delete Confirmation', 'You are about to delete "' + document.fullName + '".', 'btn-danger').result
				.then(function(results) {
					newsfeedService.deleteDocument(vm.plan.planId, document.documentID)
					.then(function(response) {
						comment.documentList = comment.documentList.filter(function(doc) {
							return doc.documentID !== document.documentID
						})
						
						$rootScope.$emit('document-delete', {document: document});
					})
					.catch(function(error) {
						vm.clientError.message = error;
					})
				},
				function(dismiss) {
					
				})	
								
			}
			
			function uploadDocument(planId, newsFeedId, item, newFile) {
				if (utilService.isNotEmpty(newFile.name)) {
					
					var document = newsfeedService.uploadDocument(planId, newsFeedId, newFile)
					
					if (document.documentID > 0) {

						var document = {
							documentID: document.documentID,
							fullName: document.documentName + '.' + document.documentExt,
							documentName: newFile.name,
							documentExt: newFile.ext,
							documentType: newFile.type,
							modifiedBy: userProfile.getFullName(),
							modifiedDateTime: utilService.isoDateToShortDate(utilService.convertToISODate(new Date()))
						}

						item.documentList.push(document);
						
						//TODO updated document component with new document
						$rootScope.$emit('pushDocument', document);						

					}
					
					vm.newFile = {};
					
				}
			}			
		}
	}
export default feed;