const documents =  {
		template: '<div style="display: table; width: 100%;"> ' +
			'<div style="display: table-header-group"> ' +
		'<strong>Uploaded Documents</strong> ' +
	'</div> ' +
	'<div style="max-height: 600px; overflow-x: none; overflow-y: auto;"> ' +
	'<div style="border-bottom: 1px solid #ccc; padding: 5px; width: 100%;" ng-repeat="document in $ctrl.documentList | orderBy : \'modifiedDateTime\' : true"> ' +						
			'<span style="display: table-cell; vertical-align: middle; padding-right: 5px; width: 13%; text-align: center;"> ' +
				'<i ng-class="$ctrl.setIcon(document, \'fa-2x\')"></i> ' +
			'</span> ' +
			'<span style="display: table-cell; vertical-align: middle; width: 100%;"> ' +
				'<span style="font-weight: bold; display: block;"> ' +
					'<a ng-class="$ctrl.hasDownloadPermission()? \'cursor-pointer\' : \'disabled\'" ng-click="$ctrl.downloadDocument(document)"> ' +
						'<span ng-if="document.fullName.length > 30" uib-tooltip="{{document.fullName}}"> ' +
							'{{document.fullName | truncate:false:30:\'...\'}} ' +
						'</span> ' +
						'<span ng-if="document.fullName.length <= 30"> ' +
							'{{document.fullName}} ' +
						'</span> ' +
					'</a> ' +
				'</span> ' +
				'<span style="font-size: smaller;"> ' +
					'<em>{{document.modifiedBy}} - {{document.modifiedDateTime}}</em> ' +
				'</span> ' +
			'</span> ' +
	'</div> ' +
	'</div> ' +
	'</div>',
		bindings: {
			plan: '<',
			documentList: '<'
		},
		controller: function($rootScope, $window, $sce, newsfeedService, permissionsFactory, FileSaver, roles) {
			
			var vm = this;
			
			var permissionsValidator;
			
			vm.$onInit = function () {
				permissionsValidator = permissionsFactory.newInstance();
				permissionsValidator.setCustomPermission('download', [roles.ADMIN, roles.OWNER, roles.RSE, roles.BRANCH_MEMBER]);
			}

			vm.$onDestroy = function() {
				downloadDocumentListener();
				pushDocumentListener();
				documentDeleteListener();
			}
			
			vm.$onChanges = function(changes) {
				
				if (changes.documentList) {
					vm.documentList = changes.documentList.currentValue;
				}
				
				if (changes.plan) {
					vm.plan = changes.plan.currentValue;
				}								
			}
			
			vm.hasDownloadPermission = function() {
				return permissionsValidator.hasPermission('download', vm.plan);
			}
			
			vm.setIcon = function(document, size) {
				var size = size ? size : '';
				
				switch(document.documentExt.toLowerCase()) {
				case 'pdf':
					return 'far fa-file-pdf ' + size;
				case 'xlsx':
				case 'xls':
				case 'csv':
					return 'far fa-file-excel ' + size;
				case 'docx':
				case 'doc':
					return 'far fa-file-word ' + size;
				case 'pptx':
				case 'ppt':
					return 'far fa-file-powerpoint ' + size;
				case 'png':
				case 'jpeg':
				case 'jpg':
				case 'bmp':
				case 'gif':
				case 'tiff':
					return 'far fa-file-image ' + size;
				case 'zip':
					return 'far fa-file-archive ' + size;
				case 'msg':
					return 'far fa-envelope ' + size;
				default:
					return 'far fa-file ' + size;
				}
			}
			
			var downloadDocumentListener = $rootScope.$on('downloadDocument', function(event, value) {
				vm.downloadDocument(value);
			})
			
			vm.downloadDocument = function(document) {
				if(!vm.hasDownloadPermission()) return;
				
				newsfeedService.downloadDocument(vm.plan.planId, document.documentID)
				.then(function(response) {
									
					var downloadableBlob = new Blob([response.data], {type: response.headers['content-type']})
					var url = ($window.URL || $window.webkitURL).createObjectURL( downloadableBlob );				
					var urls = $sce.trustAsResourceUrl(url);
					
					//File-Saver.js (Needed to allow IE to download file)
					FileSaver.saveAs(downloadableBlob, response.headers['x-filename']);
				})
				.catch(function(error) {
					
				})
				
			}
			
			var pushDocumentListener = $rootScope.$on('pushDocument', function(event, value) {				
				vm.documentList.push(value);
			})
			
			var documentDeleteListener = $rootScope.$on('document-delete', function(event, value) {
				vm.documentList = vm.documentList.filter(function(doc) {
					return doc.documentID !== value.document.documentID
				})
			})
		}
	}
export default documents;