const dragDrop =  {
		template: '<input ng-model="$ctrl.newFile" type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" /> ' +										
				  '<div drop-zone level="1" class="dropzone rounded-corners" id="filedrag"> ' +
				  '<h4 id="fileDragText" class="text-center text-muted" style="margin: 0; line-height: 65px;">Drag/Drop Document Here</h4> ' +
				  '</div> ' +
				  '<div class="input-group" style="margin-top: 10px;"> ' +
				  '<input id="{{$ctrl.fileNameId}}" type="text" class="form-control" ng-model="$ctrl.newFile.name" autocomplete="off" /> ' +
				  '<span id="{{$ctrl.fileExtId}}" class="input-group-addon">&nbsp;&nbsp;&nbsp;</span> ' +
				  '</div>',
		bindings: {
			level: '<',
			onDrop: '&'
		},
		controller: function($rootScope, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.newFile = {};
				vm.level = vm.level == undefined ? 1 : vm.level;
			}
			
			vm.$onDestroy = function() {
				fileAddedListener();
				fileClearListener();
			}

			vm.$onChanges = function(changes) {
				//utilService.getLogger().info(changes.level);
			}
			
			vm.$postLink = function() {
				
				var fileNameId = 'fileName';
				var fileExtId = 'fileExt';
				switch(vm.level) {
				case 1:
					vm.fileNameId = fileNameId;
					vm.fileExtId = fileExtId;
					break;
				case 2:
					vm.fileNameId = fileNameId + '-reply';
					vm.fileExtId = fileExtId + '-reply';
					break;
				case 3:
					vm.fileNameId = fileNameId + '-replyAgain';
					vm.fileExtId = fileExtId + '-replyAgain';
					break;
				default:
					vm.fileNameId = fileNameId;
					vm.fileExtId = fileExtId;
				}
			}
			
			var fileAddedListener = $rootScope.$on('fileAdded', function(event, value) {
				fileAdded(value);
			})
			
			function fileAdded(file) {
				
				var fileExtIndex = file.name.lastIndexOf('.');
				var fileName = file.name.substring(0, fileExtIndex);
				var fileExt = file.name.substring(fileExtIndex+1, file.name.length);
				
				var newFile = {
					file: file,
					name: fileName,
					ext: fileExt,
					fullName: file.name,
					type: file.type,
					lastModified: file.lastModified
				}
				
				vm.newFile = newFile;
				
				var elem = angular.element('#' + vm.fileNameId)
				
				elem.val(newFile.name);
				angular.element('#' + vm.fileExtId)[0].innerText = newFile.ext;

				vm.onDrop({file: newFile})
				
			}
			
			var fileClearListener = $rootScope.$on('file-clear', function(event, value) {
				vm.newFile = {};
				angular.element('#' + vm.fileNameId).val('')
				angular.element('#' + vm.fileExtId)[0].innerText = '';
			})
		}
	}
	const dropZone = function($rootScope, $window) {
		return {
			restrict: 'A',
			link: function(scope, elem, attrs) {
				var fileDrag = 'filedrag';
	        	var fileDragText = 'fileDragText'
	        	var $win = angular.element($window);
	        	
	        	var level = attrs.level;	        	
	        	
	        	$win.on('dragover', function(evt) {	        		
	        		if (evt.target.id != fileDrag) {
	        			evt.preventDefault();
	        			evt.originalEvent.dataTransfer.effectAllowed = 'none';
	        			evt.originalEvent.dataTransfer.dropEffect = 'none';
	        		}	        		
	        	})
	        	
	        	$win.on('dragenter', function(evt) {
	        		if (evt.target.id != fileDrag) {
	        			evt.preventDefault();
	        			evt.originalEvent.dataTransfer.effectAllowed = 'none';
	        			evt.originalEvent.dataTransfer.dropEffect = 'none';
	        		}	        		
	        	})
	        	
	        	$win.on('dragleave', function(evt) {
	        		if (evt.target.id != fileDrag) {
	        			evt.preventDefault();
	        			evt.originalEvent.dataTransfer.effectAllowed = 'none';
	        			evt.originalEvent.dataTransfer.dropEffect = 'none';
	        		}	        		
	        	})
	        	
	        	elem.bind('dragover', function(evt) {
		        		evt.stopPropagation();
		        		evt.preventDefault();
		        						        		
		        		evt.currentTarget.className = (evt.type == "dragover" ? "dropzone dropzonehover" : "");
	        	})
	        	
	        	elem.bind('dragenter', function(evt) {
		        		evt.stopPropagation();
		        		evt.preventDefault();
		        		
		        		evt.currentTarget.className = (evt.type == "dragenter" ? "dropzone dropzonehover" : "");	        		
	        	})
	        	
	        	elem.bind('dragleave', function(evt) {
        			evt.stopPropagation();
	        		evt.preventDefault();
	        			        		
	        		evt.currentTarget.className = (evt.type == "dragover" ? "dropzone dropzonehover" : "dropzone");		        		        		
	        	})
	        	
	            elem.bind('drop', function(evt) {
	                evt.stopPropagation();
	                evt.preventDefault();
	                
	                var files = evt.originalEvent.dataTransfer.files;
	                for (var i = 0, f; f = files[i]; i++) {
	                    var reader = new FileReader();
	                    reader.readAsArrayBuffer(f);

	                    reader.onload = (function(theFile) {
	                        return function(e) {	                        	
	                        	$rootScope.$emit('fileAdded', theFile);	                            
	                        };
	                    })(f);
	                    
	                }
	                
	                evt.currentTarget.className = 'dropzone';
	            });
			}
		}
	}
export {dragDrop, dropZone};