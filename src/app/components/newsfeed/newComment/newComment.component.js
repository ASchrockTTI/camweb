import newCommentTemplate from './newComment.html';

const newComment =  {
		template: newCommentTemplate,
		bindings: {
			ngModel: '=',
			level: '<',
			parent: '<',
			plan: '<',
			onSave: '&',
			onCancel: '&',
			isEdit: '<',
			onEdit: '&'
		},
		controller: function($rootScope, newsfeedService, userProfile, utilService) {
			
			var vm = this;
			
			vm.$onInit = function() {
				vm.newFile = {};	
				vm.showDocumentPanel = true;				
			}
			
			vm.$onChanges = function(changes) {
				
				vm.showDocumentPanel = true;
				
				if (changes.plan) {
					vm.plan = changes.plan.currentValue;
				}
				if (changes.isEdit) {
					
					if (changes.isEdit.currentValue) {
						if (vm.ngModel.documentList.length > 0) {
							vm.showDocumentPanel = false;
						} else {
							vm.showDocumentPanel = true;
						}
					}
				}
			}			
			
			vm.changes = function() {				
				$rootScope.$emit('preventnav', {isDirty: true});				
			}
			
			vm.addFile = function(file) {
				vm.newFile = file;
			}
			
			vm.cancel = function() {
				
				$rootScope.$emit('preventnav', {isDirty: false});
				
				if (vm.parent) {
					vm.onCancel({item: vm.parent});
				} else {
					vm.onCancel();
				}
			}
			
			vm.save = function(item) {
				$rootScope.$emit('preventnav', {isDirty: false});
				$rootScope.$emit('file-clear',{});
				vm.onSave({item: vm.ngModel, document: vm.newFile});
				vm.newFile = {};
				angular.element('#selectedFile').val(null);
			}
			
			vm.edit = function(item) {
				$rootScope.$emit('preventnav', {isDirty: false});
				$rootScope.$emit('file-clear',{});
				vm.onEdit({item: vm.ngModel, document: vm.newFile})
				vm.newFile = {};
			}
			
			vm.disableSave = function() {
				if (!vm.newCommentForm.$valid && !vm.newCommentForm.$dirty) {
					return true;
				} 
				
				if (vm.ngModel.newsFeed == '' || vm.ngModel.newsFeed == undefined) {
					return true;
				}
			}
			
		}
	}
export default newComment;