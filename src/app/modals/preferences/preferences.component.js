import preferencesModalTemplate from "./preferences.html";

const preferencesModal = {
  template: preferencesModalTemplate,
  bindings: {
    modalInstance: "<",
    resolve: "<",
  },
  controller: function (
    utilService,
    userProfile,
    preferencesService,
    userInfoService,
    permissionsFactory,
    planService,
    roles,
    $filter
  ) {
    var vm = this;

    vm.$onInit = function () {
      var permissionsValidator = permissionsFactory.newInstance();

      permissionsValidator.setCustomPermission(
        "userFilter",
        [
          roles.OWNER,
          roles.ADMIN,
          roles.GM,
          roles.ISM,
          roles.FSM,
          roles.FSR,
          roles.RSE,
          roles.RVP,
          roles.RBOM,
          roles.EXEC,
          roles.HEAD_HONCHO,
        ],
        null,
        null
      );

      vm.clientError = utilService.getClientErrorObject();

      vm.preferenceOptions = {
        der: {
          list: [
            { value: "A", display: "All" },
            { value: "D", display: "Defend" },
            { value: "E", display: "Expand" },
            { value: "R", display: "Review" },
          ],
          selectedValue: undefined,
        },
        regions: {
          list: [],
          selectedValue: undefined,
        },
        branches: {
          list: [],
          selectedBranchesString: "",
        },
        tiers: {
          list: [
            { value: "1", display: "Global" },
            { value: "2", display: "NDC" },
            { value: "3", display: "EDC" },
            { value: "4", display: "ADC" },
            { value: "5", display: "Branch" },
          ],
          selectedValue: undefined,
        },
        runRateRanges: {
          list: [],
          selectedValue: undefined,
        },
        targetRanges: {
          list: [],
          selectedValue: undefined,
        },
        percentToPlan: {
          selectedValue: undefined,
        },
      };

      vm.sliders = {
        tiers: {
          value: 0,
          options: {
            showTicksValues: true,
            stepsArray: [
              { value: 0, legend: "All" },
              { value: 1, legend: "Global" },
              { value: 2, legend: "NDC" },
              { value: 3, legend: "EDC" },
              { value: 4, legend: "ADC" },
              { value: 5, legend: "Branch" },
            ],
            disabled: true,
          },
        },
        runRate: {
          minValue: 0,
          maxValue: 1000000,
          options: {
            floor: 0,
            ceil: 1000000,
            step: 50000,
            noSwitching: true,
            translate: function (value) {
              return $filter("currency")(value, "$", 0);
            },
            ticksArray: [0, 50000, 100000, 250000, 500000, 1000000],
            ticksTooltip: function (v) {
              return $filter("currency")(v, "$", 0);
            },
            disabled: true,
          },
        },
        target: {
          minValue: 0,
          maxValue: 5000000,
          options: {
            floor: 0,
            ceil: 5000000,
            step: 100000,
            noSwitching: true,
            translate: function (value) {
              return $filter("currency")(value, "$", 0);
            },
            ticksArray: [0, 100000, 250000, 500000, 1000000, 5000000],
            ticksTooltip: function (v) {
              return $filter("currency")(v, "$", 0);
            },
            disabled: true,
          },
        },
        percentToPlan: {
          value: 0,
          options: {
            floor: 0,
            ceil: 100,
            step: 5,
            translate: function (value) {
              return value + "%";
            },
            getPointerColor: function (value) {
              if (vm.sliders.percentToPlan.options.disabled) {
                return "#D8DFF2";
              }

              if (value < 100) {
                return "red";
              }

              return "#2AE02A";
            },
            ticksArray: [0, 25, 50, 75, 100],
            ticksTooltip: function (v) {
              return v + "%";
            },
            disabled: true,
          },
        },
      };

      vm.checkPermission = function (permission) {
        return permissionsValidator.userTypeHasPermission(permission);
      };

      vm.dv = {
        listOne: [
          { value: 0, display: "All" },
          { value: 1000, display: "1,000" },
          { value: 10000, display: "10,000" },
          { value: 50000, display: "50,000" },
          { value: 100000, display: "100,000" },
          { value: 500000, display: "500,000" },
          { value: 1000000, display: "1,000,000" },
          { value: 5000000, display: "5,000,000" },
          { value: 10000000, display: "10,000,000" },
          { value: 25000000, display: "25,000,000" },
          { value: 50000000, display: "50,000,000" },
        ],
        listTwo: [
          { value: 500000, display: "500,000" },
          { value: 1000000, display: "1,000,000" },
          { value: 5000000, display: "5,000,000" },
          { value: 10000000, display: "10,000,000" },
          { value: 25000000, display: "25,000,000" },
          { value: 50000000, display: "50,000,000" },
        ],
        selectedList: [],
        selectedValue: undefined,

        tiers: [
          {
            Id: 1,
            display: "Tier 1: > $1,000,000",
          },
          {
            Id: 2,
            display: "Tier 2: $500,000 - $1,000,000",
          },
          {
            Id: 3,
            display: "Tier 3: $200,000 - $500,000",
          },
          {
            Id: 4,
            display: "Tier 4: $50,000 - $200,000",
          },
          {
            Id: 5,
            display: "Tier 5: < $50,000",
          },
        ],

        selectedTiersString: "",
        selectedTiersStringDisplay: "",
      };

      vm.dollarTiersConfig = {
        displayProp: "display",
      };

      vm.multiselectText = {
        checkAll: "All",
        uncheckAll: "Un-Select All",
      };

      //converts selected tiers into comma delimited string
      vm.formatSelectedTiers = function () {
        var tierIdArray = vm.formFields.selectedTiers.map(function (a) {
          return a.Id;
        });
        tierIdArray.sort();
        if (tierIdArray.length > 0) {
          if (tierIdArray.length == 5) {
            vm.dv.selectedTiersStringDisplay = "All";
            vm.dv.selectedTiersString = "0";
          } else {
            vm.dv.selectedTiersStringDisplay =
              "Tiers: " + tierIdArray.join(", ");
            vm.dv.selectedTiersString = tierIdArray.join(",");
          }
        } else {
          vm.dv.selectedTiersStringDisplay = "None Selected";
          vm.dv.selectedTiersString = "";
        }
      };

      vm.userSelected = undefined;
      vm.selectedUser = undefined;

      vm.formFields = {
        marketSegment: undefined,
        subCategory: undefined,
        selectedTiers: [],
        selectedBranches: [],
      };

      switch (userProfile.getUserType().toUpperCase()) {
        case roles.HEAD_HONCHO:
        case roles.EXEC:
        case roles.RVP:
          vm.dv.selectedList = vm.dv.listTwo;
          break;
        default:
          vm.dv.selectedList = vm.dv.listOne;
      }

      planService.getParentBranchList().then(function (response) {
        buildBranchList(response);
      });

      getMarketSegments();
    };

    vm.branchFilter = function ($query) {
      var filtered = [];
      angular.forEach(vm.preferenceOptions.branches.list, function (branch) {
        if (branch.display.toLowerCase().indexOf($query.toLowerCase()) === 0)
          filtered.push(branch);
      });

      return filtered;
    };

    function buildBranchList(branchList) {
      if (branchList.length > 0) {
        for (var i = 0; i <= branchList.length - 1; i++) {
          vm.preferenceOptions.branches.list.push({
            Id: i + 1,
            display: branchList[i],
          });
        }
      }

      switch (userProfile.getUserType().toUpperCase()) {
        case roles.HEAD_HONCHO:
        case roles.EXEC:
        case roles.ADMIN:
          vm.userBranches = branchList;
          break;
        default:
          vm.userBranches = userProfile.getBranchList();
      }
    }

    function getMarketSegments() {
      planService
        .getMarketSegmentFilters()
        .then(function (response) {
          //vm.marketSegments = response;
          utilService.getLogger().info(response);
          //TODO: Load vm.categories with only market segments that end with 0
          vm.allSegments = response;
          vm.marketSegments = vm.allSegments;

          vm.formFields.marketSegment = undefined;

          vm.preferences.getAll();
        })
        .catch(function (error) {});
    }

    vm.marketSegmentFilter = {
      updateSubCategory: function () {
        //if "All" is selected
        if (vm.formFields.marketSegment == null) {
          return;
        }

        vm.subCategories = vm.formFields.marketSegment.subCategoryList;

        return false;
      },
      enableSubCategory: function () {
        if (vm.formFields.marketSegment == undefined) {
          vm.subCategories = undefined;
          vm.formFields.subCategory = undefined;
          return true;
        }

        if (vm.formFields.marketSegment.categoryId === "0") {
          vm.subCategories = undefined;
          vm.formFields.subCategory = undefined;
          return true;
        }

        if (vm.formFields.subCategory) {
          if (
            vm.formFields.marketSegment.categoryId !=
            vm.formFields.subCategory.marketSegmentCode[0]
          ) {
            //vm.subCategories = undefined;
            return true;
          }
        } else {
          vm.formFields.subCategory =
            vm.formFields.marketSegment.subCategoryList[0];
        }

        return false;
      },
    };

    vm.setSelected = function (user) {
      vm.selectedUser = user;
    };

    vm.buttons = {
      save: function () {
        vm.preferences.save();
      },
      cancel: function () {
        vm.modalInstance.dismiss("cancel");
      },
      reset: function () {
        vm.userSelected = undefined;
        vm.selectedUser = undefined;
        vm.formFields.selectedTiers.length = 0;

        vm.formFields.selectedBranches = [];

        switch (userProfile.getUserType().toUpperCase()) {
          case roles.HEAD_HONCHO:
          case roles.EXEC:
          case roles.RVP:
          case roles.ADMIN:
            vm.formFields.selectedTiers.push(vm.dv.tiers[0]);
            break;
          default:
            for (var i = 0; i < vm.dv.tiers.length; i++) {
              vm.formFields.selectedTiers.push(vm.dv.tiers[i]);
            }
        }
        vm.formatSelectedTiers();

        vm.formFields.marketSegment = undefined;
        vm.formFields.subCategory = undefined;
        vm.preferencesForm.$dirty = true;
      },
    };

    vm.disableSave = function () {
      return (
        !vm.preferencesForm.$dirty ||
        vm.clientError.message ||
        vm.formFields.selectedTiers.length == 0
      );
    };

    vm.preferences = {
      data: [],
      getAll: function () {
        preferencesService
          .getAll(userProfile.getSAMAccountName())
          .then(function (response) {
            if (response) {
              vm.preferences.data = response;

              utilService.getLogger().info(vm.preferences.data);

              for (var i = 0; i <= response.preferenceList.length - 1; i++) {
                var preference = response.preferenceList.filter(function (
                  item
                ) {
                  return item.preference.preferenceID == i + 1;
                });

                switch (i + 1) {
                  case 1:
                    vm.dv.selectedTiersString = preference[0].preferenceValue;
                    //to accommodate existing dollar value preferences
                    if (parseInt(vm.dv.selectedTiersString) > 5) {
                      vm.dv.selectedTiersString = "0";
                    }
                    vm.formFields.selectedTiers.length = 0;
                    if (vm.dv.selectedTiersString.charAt(0) == "0") {
                      vm.dv.selectedTiersString = "1,2,3,4,5";
                    }
                    for (var j = 0; j < vm.dv.selectedTiersString.length; j++) {
                      if (vm.dv.selectedTiersString.charAt(j) != ",") {
                        vm.formFields.selectedTiers.push(
                          vm.dv.tiers[
                            parseInt(vm.dv.selectedTiersString.charAt(j)) - 1
                          ]
                        );
                      }
                    }
                    vm.formatSelectedTiers();
                    break;
                  case 2:
                    vm.userSelected = preference[0].preferenceValue;

                    vm.selectedUser = {};
                    vm.selectedUser.fullName = vm.userSelected;

                    break;
                  case 3:
                    var selectedSegmentCode = preference[0].preferenceValue;

                    if (
                      selectedSegmentCode === "0" ||
                      selectedSegmentCode === ""
                    ) {
                      break;
                    }

                    angular.forEach(vm.allSegments, function (segment) {
                      if (segment.categoryId === selectedSegmentCode[0]) {
                        vm.formFields.marketSegment = segment;
                      }
                    });

                    vm.subCategories =
                      vm.formFields.marketSegment.subCategoryList;

                    if (selectedSegmentCode.length < 2) {
                      vm.formFields.subCategory = vm.subCategories[0];
                      break;
                    }

                    for (
                      var subCategoryIndex = 0;
                      subCategoryIndex < vm.subCategories.length;
                      subCategoryIndex++
                    ) {
                      if (
                        vm.subCategories[subCategoryIndex].marketSegmentCode ===
                        selectedSegmentCode
                      ) {
                        vm.formFields.subCategory =
                          vm.subCategories[subCategoryIndex];
                        break;
                      }
                    }

                    //		            				var selectedValue = vm.allSegments.filter(function(item) {
                    //		            					return item.marketSegmentCode == preference[0].preferenceValue
                    //		            				})
                    //
                    //		            				if (selectedValue.length > 0) {
                    //		            					var marketSegmentIndex = selectedValue[0].marketSegmentCode[selectedValue[0].marketSegmentCode.length - 1]
                    //
                    //		            					if (marketSegmentIndex == 0) {
                    //		            						vm.formFields.marketSegment = selectedValue[0];
                    //
                    //			            					vm.marketSegmentFilter.updateSubCategory();
                    //		            					} else {
                    //
                    //		            						var parentMarketSegmentCode = selectedValue[0].marketSegmentCode[0] + '-0';
                    //
                    //		            						var parentMarketSegment = vm.allSegments.filter(function(item) {
                    //		            							return item.marketSegmentCode == parentMarketSegmentCode;
                    //		            						})
                    //
                    //		            						vm.formFields.marketSegment = parentMarketSegment[0];
                    //
                    //			            					vm.marketSegmentFilter.updateSubCategory();
                    //
                    //		            						vm.formFields.subCategory = selectedValue[0];
                    //		            					}
                    //		            				}

                    break;
                  case 4:
                    vm.preferenceOptions.branches.selectedBranchesString =
                      preference[0].preferenceValue;

                    vm.formFields.selectedBranches = [];

                    if (
                      vm.preferenceOptions.branches.selectedBranchesString ===
                      "All"
                    ) {
                      //leave selectedBranchList empty
                    } else {
                      var selectedBranches = vm.preferenceOptions.branches.selectedBranchesString.split(
                        ","
                      );

                      var k = 0;

                      for (
                        var j = 0;
                        j < vm.preferenceOptions.branches.list.length;
                        j++
                      ) {
                        if (
                          vm.preferenceOptions.branches.list[j].display ===
                          selectedBranches[k]
                        ) {
                          vm.formFields.selectedBranches.push(
                            vm.preferenceOptions.branches.list[j]
                          );
                          if (k === selectedBranches.length - 1) break;
                          k++;
                        }
                      }
                    }

                    break;
                  default:
                }
              }
            }
          })
          .catch(function (error) {
            vm.clientError.message = error;
          });
      },
      save: function () {
        var container = preferencesService.getContainer();
        container = vm.preferences.data;

        if (vm.selectedUser && vm.userSelected) {
          var preference = vm.preferences.find(2);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(2);
            container.preferenceList.push(preference);
          }

          preference.preferenceValue = vm.selectedUser.fullName;
        } else {
          var preference = vm.preferences.find(2);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(2);
            container.preferenceList.push(preference);
          }

          preference.preferenceValue = "";
        }

        if (vm.formFields.marketSegment) {
          var preference = vm.preferences.find(3);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(3);
            container.preferenceList.push(preference);
          }

          if (vm.formFields.subCategory) {
            var subCategory =
              vm.formFields.subCategory.marketSegmentCode == ""
                ? vm.formFields.marketSegment.marketSegmentCode
                : vm.formFields.subCategory.marketSegmentCode;
            preference.preferenceValue = subCategory == "" ? "" : subCategory;
          } else {
            preference.preferenceValue =
              vm.formFields.marketSegment.marketSegmentCode == ""
                ? ""
                : vm.formFields.marketSegment.marketSegmentCode;
          }
        } else {
          var preference = vm.preferences.find(3);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(3);
            container.preferenceList.push(preference);
          }

          preference.preferenceValue = "";
        }

        if (vm.dv.selectedTiersString) {
          var preference = vm.preferences.find(1);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(1);
            container.preferenceList.push(preference);
          }

          vm.formatSelectedTiers();
          preference.preferenceValue = vm.dv.selectedTiersString;
        }

        if (vm.formFields.selectedBranches) {
          var preference = vm.preferences.find(4);

          if (preference == undefined) {
            preference = preferencesService.getNewPreference(4);
            container.preferenceList.push(preference);
          }

          if (
            vm.formFields.selectedBranches.length ===
            vm.preferenceOptions.branches.list.length
          ) {
            preference.preferenceValue = "All";
          } else if (vm.formFields.selectedBranches.length === 0) {
            preference.preferenceValue = "All";
          } else {
            var selectedBranchesString = "";

            vm.formFields.selectedBranches = $filter("orderBy")(
              vm.formFields.selectedBranches,
              "display"
            );

            angular.forEach(vm.formFields.selectedBranches, function (branch) {
              selectedBranchesString =
                selectedBranchesString + branch.display + ",";
            });
            selectedBranchesString = selectedBranchesString.substring(
              0,
              selectedBranchesString.length - 1
            );
            preference.preferenceValue = selectedBranchesString;
          }
        }

        userInfoService
          .checkUserInfo()
          .then(function () {
            preferencesService
              .save(container)
              .then(function (response) {
                sessionStorage.removeItem("sessionPlans");
                sessionStorage.removeItem("sessionPlansNonTTI");
                vm.modalInstance.close();
              })
              .catch(function (error) {
                vm.clientError.message = error;
              });
          })
          .catch(function (error) {
            vm.clientError.message = error;
          });
      },
      find: function (id) {
        var preference = vm.preferences.data.preferenceList.filter(function (
          item
        ) {
          return item.preference.preferenceID == id;
        });

        return preference[0];
      },
    };
  },
};
export default preferencesModal;
