function CharLimit() {
  return {
    require: "ngModel",
    link: function (scope, element, attrs, ngModelCtrl) {
      var maxlength = Number(attrs.charLimit);

      function fromUser(text) {
        ngModelCtrl.$setValidity("charLimit", text.length <= maxlength);
        return text;
      }
      ngModelCtrl.$parsers.push(fromUser);
    },
  };
}

export default CharLimit;
