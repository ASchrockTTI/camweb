
	
	SetClassWhenAtTop.$inject = ['$window'];
	
	function SetClassWhenAtTop($window) {
		
		 // wrap window object as jQuery object
		
		return {
			restrict: 'A',
	        link: function (scope, element, attrs) {
	        	
	        	var mainClass = 'fix-to-main';
	        	
	            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
	                offsetTop = element.offset().top; // get element's top relative to the document

	            var $main = angular.element($(".main-page"));
	            var $win = angular.element($window);
	            
	            var winHeight = $(document).height() - $win.height();
	            
	            
	            $main.on('scroll', function (e) {	            	
	                if ($main.scrollTop() > winHeight) {
	                    element.addClass(mainClass);	                                        
	                } else {
	                	element.removeClass(mainClass);
	                }
	                
	                if ($main.scrollTop() == 0) {
	                	element.removeClass(topClass);
	                }
	            });
	            	            
	            $win.on('scroll', function (e) {	            	
	            	if ($win.scrollTop() == 0) {
	            		element.removeClass(mainClass);
	                	element.removeClass(topClass);
	            	}
	            	if ($win.scrollTop() == winHeight) {
	            		if ($main.scrollTop() > 0) {
	            			element.addClass(topClass);
		            		element.removeClass(mainClass);
	            		}
	            			                	
	            	} 
            	});
	        }
		}
	}

export default SetClassWhenAtTop;