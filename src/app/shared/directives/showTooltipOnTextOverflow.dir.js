function ShowTooltipOnTextOverflow() {
  return {
    restrict: "A",
    link: function (scope, element, attrs) {
      var el = element[0];
      scope.$watch(
        function () {
          return el.scrollWidth;
        },
        function () {
          var el = element[0];
          if (el.scrollWidth > el.clientWidth) {
            attrs.tooltipEnable = "true";
          } else {
            attrs.tooltipEnable = "false";
          }
        }
      );
    },
  };
}

export default ShowTooltipOnTextOverflow;
