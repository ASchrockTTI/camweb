
	
	ConfirmOnExit.$inject = ['utilService', '$state', '$rootScope'];
	
	function ConfirmOnExit(utilService, $state, $rootScope) {
		return {
			require: '^form',
			link: function($scope, elem, attrs, formCtrl) {			
				
				window.onbeforeunload = function(){
                    if (formCtrl.$dirty) {
                        return "All unsaved changes will be lost. Do you wish to continue?";
                    }
				}
				
				//prevent redundant modals on mutliple firings of $stateChangeStart
				$rootScope.modalOpen = false;
				//prevent additional warnings after state change confirmed (if multiple forms with this directive on page)
				$rootScope.bypassWarning = false;
				
				$scope.$on("$stateChangeStart", function(event, toState, toParams) {
					if (formCtrl.$dirty && !$rootScope.bypassWarning) {
						event.preventDefault();
						if(!$rootScope.modalOpen) {
							$rootScope.modalOpen = true;
				            utilService.deleteConfirmation('Prevent Navigation', 'Unsaved changes will be lost if you continue.', 'btn-warning').result
							.then(
							function (result) {
								if (result == true) {
									formCtrl.$setPristine();
									$rootScope.modalOpen = false;
									$rootScope.bypassWarning = true;
									$state.transitionTo(toState.name, toParams, {reload: true});
									return;
								} else {
									$rootScope.modalOpen = false;
									return;
								}
							},
							function (dismiss) {
								$rootScope.modalOpen = false;
							});
						}
						return;
					}
				});
				
				
                
                $scope.$on('$destroy', function() {
                    window.onbeforeunload = null;
                });
			}
		};
	}

export default ConfirmOnExit;