function CharLimitLabel($compile) {
  return {
    scope: {
      limit: "<",
      showOnFocus: "<",
      model: "<ngModel",
    },
    require: "ngModel",
    link: function ($scope, element, attr, ngModel) {
      function setCharLimit(text) {
        ngModel.$setValidity("charLimit", text.length <= $scope.limit);
        return text;
      }
      ngModel.$parsers.push(setCharLimit);

      var label = angular.element(
        "<span class=\"character-limit-label\" ng-style=\"{color: ((limit - model.length) < 0)? 'red' : ''," +
          "opacity: (showLabel)? '1' : '0'}\" style=\"display: inline-block; position: absolute;right: 0px; pointer-events: none;" +
          'padding-right: inherit; font-size: 12px; white-space: nowrap;">{{limit - model.length | number}} characters remaining</span>'
      );

      $scope.showLabel = false;
      if ($scope.showOnFocus) {
        element.bind("blur", function (e) {
          $scope.showLabel = false;
          $scope.$apply();
        });
        element.bind("focus", function (e) {
          $scope.showLabel = true;
          $scope.$apply();
        });
      } else {
        $scope.showLabel = true;
      }

      label.insertAfter(element);
      $compile(label)($scope);
    },
  };
}

export default CharLimitLabel;
