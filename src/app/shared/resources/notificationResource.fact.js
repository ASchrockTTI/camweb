
import { environment } from './../../../environments/environment';
	
	notificationResource.$inject = [
		'$resource',
		'keys',
		'utilService'
		];
	
	function notificationResource(
			$resource,
			keys,
			utilService) {
		
		var API_NOTIFICATION_URL = environment.apiURL + 'notifications/';
		
		var transform = function(data, header, status) {
			
			var response = {};
			
			if (data != '') {
				response = angular.fromJson(data);
			}
			
			return {
				response: response
			}
		}
		
		return {
			notifications: $resource(API_NOTIFICATION_URL, {}, {
				query: {
			    	url: API_NOTIFICATION_URL + ':userName',
			    	method: 'GET',
			    	isArray:false,			    	
			    	transformResponse: transform
				},
				deleteNotification: {
					url: API_NOTIFICATION_URL + ':notificationId',
					method: 'DELETE'
				},
				setNotificationReadStatus: {
					url: API_NOTIFICATION_URL + ':notificationId/read/:markedRead',
					method: 'PUT'
				}
			})
		}	
	}

export default notificationResource;