
import { environment } from './../../../environments/environment';
	
	FeedbackResource.$inject = [
		'$resource'
	];
	
	function FeedbackResource(
			$resource) {
		
		var API_FEEDBACK_URL = environment.apiURL + 'feedback';
		
		return {
			feedback: $resource(API_FEEDBACK_URL, {}, {
				queryIssuesByType: {
					url: API_FEEDBACK_URL + '/:type',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						var issues = {};
						
						if (data != '') {
							issues = JSON.parse(data);
						}
						
						angular.forEach (issues, function(value, key) {
							value.dateAddedAsString = new Date(value.dateAdded).toISOString().substring(0, 10)
						})
						
						return {
							response: issues
						}
					}
				},
				
				add: {
					method: 'POST'
				},
				update: {
					url: API_FEEDBACK_URL + '/:feedbackId',
					method: 'PUT'
				},
				addVote: {
					url: API_FEEDBACK_URL + '/:feedbackId/vote',
					method: 'PUT'
				},
				markAsReleased: {
					url: API_FEEDBACK_URL + '/:feedbackId',
					method: 'PUT',
					params: {markAsRelease: '@markAsReleased'}
				},
				markAsInProgress: {
					url: API_FEEDBACK_URL + '/:feedbackId',
					method: 'PUT',
					params: {markAsInProgress: '@markAsInProgress'}
				},
				reply: {
					url: API_FEEDBACK_URL + '/:feedbackId/reply',
					method: 'POST'
				},
				deleteReply: {
					url: API_FEEDBACK_URL + '/:feedbackId/reply/:feedbackReplyId',
					method: 'DELETE',
				},
				discard: {
					url: API_FEEDBACK_URL + '/:feedbackId',
					method: 'DELETE',
				},
				queryFeatures: {
					method: 'GET',
					url: API_FEEDBACK_URL + '/features',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
		}
	}

export default FeedbackResource;