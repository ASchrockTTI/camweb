
import { environment } from './../../../environments/environment';
	
	PreferencesResource.$inject = [
		'$resource'
	]
	
	function PreferencesResource(
		$resource) {
		
		var API_PREFERENCES_URL = environment.apiURL + 'preferences';
		
		var transform = function(data, header, status) {
			
			var response = {};
			
			if (data != '') {
				response = angular.fromJson(data);
			}
			
			return {
				response: response
			}
		}
		
		return {
			preferences: $resource(API_PREFERENCES_URL, {}, {
				query: {
					params: {userName: '@userName'},
					method: 'GET',
					isArray: false,
					transformResponse: transform
				},
				update: {
					method: 'PUT'
				}
			}),
			version: $resource(API_PREFERENCES_URL, {}, {
				checkLastVersion: {
					method: 'GET',
					url: API_PREFERENCES_URL + '/alert-release',
					params: {userName: '@userName'},
					transformResponse: transform
				},
				updateLastVersion: {
					method: 'PUT',
					url: API_PREFERENCES_URL + '/version',
					params: {userName: '@userName', version: '@version'},
					transformResponse: transform
				}
			})
		}
	}

export default PreferencesResource;