
import { environment } from './../../../environments/environment';
	
	entityResource.$inject = ['$resource'];
	
	function entityResource($resource) {
		
		var API_PLAN_ENTITY_URL = environment.apiURL + 'plans/:planId/entities/:entity';
		
		return {
			entities: $resource(API_PLAN_ENTITY_URL, {planId: '@planId', entity: '@entity'}, {
				// This method is used to return the business plan data
				query: {
					method: 'GET',
					isArray: false,
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
				},
				
			}),					
		}
	}

export default entityResource;