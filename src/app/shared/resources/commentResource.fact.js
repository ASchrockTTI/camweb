
import { environment } from './../../../environments/environment';
	
	CommentResource.$inject = ['$resource'];
	
	function CommentResource(
			$resource) {
		
		var API_COMMENT_URL = environment.apiURL + 'plans/:planId/strategies/:planStrategyId/tactics/:planTacticId/comments/';
		
		return {
			comment: $resource(API_COMMENT_URL, {}, {
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return [];
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				add: {
					method: 'POST'
				},				
				update: {
					method: 'PUT'
				},
				delete: {
					method: 'DELETE',
					url: API_COMMENT_URL + '/:planTacticCommentId',
					hasBody: true
				}
			})
		}
	}

export default CommentResource;