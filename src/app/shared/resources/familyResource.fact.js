
import { environment } from './../../../environments/environment';
	
	FamilyResource.$inject=[
		'$resource'
	]
	
	function FamilyResource(
		$resource) {
		
		var API_FAMILY_URL = environment.apiURL + 'plans/:planId/family/';
		
		var transform = function(data, header, status) {
			
			var response = {};
			
			if (data != '') {
				response = angular.fromJson(data);
			}
			
			return {
				response: response
			}
		}
		
		return {
			family: $resource(API_FAMILY_URL, {}, {
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: transform
				}
			})
		}
		
	}

export default FamilyResource;