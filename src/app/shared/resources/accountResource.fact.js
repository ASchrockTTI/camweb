
import { environment } from './../../../environments/environment';
	
	AccountResource.$inject = ['$resource'];
	
	function AccountResource(
			$resource
	) {
		
		var API_ACCOUNTS_URL = environment.apiURL +'accounts/';
		
		return {
			accounts: $resource(API_ACCOUNTS_URL, {}, {
				findAccountsByCorpId: {
					method: 'GET',
					params: {'corpId': '@corpId', 'account': '@account'},
					isArray: false,
					transformResponse: function(data, header) {
						
						var jsonData = JSON.parse(data);
												
						for (var i = 0; i < jsonData.length; i++) {
							jsonData[i].index = i + 1;
							jsonData[i].key = jsonData[i].account + jsonData[i].entity; 
							jsonData[i].display = jsonData[i].account.trim() + ' (' + jsonData[i].entity + ')';
						}
						
						return {
							response: jsonData
						};
					}
				},
				
				findAccountCorpId: {
					method: 'GET',
					params: {'acctCorpId': '@acctCorpId','acctType': '@acctType'},
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				findPlans: {
					method: 'GET',
					params: {'search': '@search'},
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				getAccountsByFullName: {
					method: 'GET',
					url: API_ACCOUNTS_URL + ':fullName',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
		}
	}

export default AccountResource;