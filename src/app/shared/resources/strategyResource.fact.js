
import { environment } from './../../../environments/environment';
	
	StrategyResource.$inject = [
		'$resource'
	]
	
	function StrategyResource(
			$resource) {
		
		var API_STRATEGY_URL = environment.apiURL + 'v1/plans/:planId/strategies/';
		
		return {
			strategy: $resource(API_STRATEGY_URL, {}, {
			
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data,header) {
						return {
							response: JSON.parse(data)
						};
					}
				},
				updateStrategy: {
			    	url: API_STRATEGY_URL + ':planStrategyId',
			    	method: 'PUT'
			    },
			    
			    updateSortOrders: {
			    	method: 'PUT'
			    },
				
			    addStrategy: {
			    	method: 'POST'
			    },
				
			    removeStrategy: {
			    	url: API_STRATEGY_URL + ':planStrategyId',
			    	method: 'DELETE',
			    	hasBody: true
			    }

			})
		}
	}

export default StrategyResource;