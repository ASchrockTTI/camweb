
import { environment } from './../../../environments/environment';
	
	mfrResource.$inject = ['$resource'];
	
	function mfrResource($resource) {
		
		var API_PLAN_MFR_URL = environment.apiURL + 'plans/:planId/mfrs';
		
		return {
			mfrs: $resource(API_PLAN_MFR_URL, {}, {
				// This method is used to return the business plan data
				query: {
					params: {entity: '@entity'},
			    	method: 'GET',
					isArray: false,
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
				},
				
				update: {
					method: 'PUT'					
				}
				
			}),
		}
	}

export default mfrResource;