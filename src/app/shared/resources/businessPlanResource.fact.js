
import { environment } from './../../../environments/environment';
	
	businessPlanResource.$inject = ['$resource'];
	
	function businessPlanResource($resource) {
		
		var API_BUSINESS_PLAN_URL = environment.apiURL + 'plans/:planId';
		
		return {
			businessPlan: $resource(API_BUSINESS_PLAN_URL, {}, {
				// This method is used to return the business plan data
				query: {
			    	method: 'GET',
					isArray: false,					
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}			
			    	}
				},
				
			}),					
		}
	}

export default businessPlanResource;