import { environment } from "./../../../environments/environment";

userInfoResource.$inject = ["$resource", "$filter", "utilService"];

function userInfoResource($resource, $filter, utilService) {
  var API_BASE_URL = environment.apiURL;
  var API_BASE_USERINFO_URL = API_BASE_URL + "userInfo";

  return {
    userInfo: $resource(
      API_BASE_USERINFO_URL,
      {},
      {
        checkUserInfo: {
          url: API_BASE_URL + "checkInfo",
          method: "GET",
        },
        getByUserName: {
          url: API_BASE_USERINFO_URL,
          params: { userName: "@userName" },
          method: "GET",
        },
        getByLastName: {
          params: { lastName: "@lastName" },
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        getOsrByLastName: {
          params: { lastName: "@lastName" },
          url: API_BASE_URL + "/osrInfo",
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        lookupCallReportCreatorsByLastName: {
          params: { lastName: "@lastName" },
          url: API_BASE_URL + "/callReporters",
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        getCallReportCreatorsByBranch: {
          params: { branch: "@branch" },
          url: API_BASE_URL + "/callReporters",
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        getByTitle: {
          params: { title: "@title" },
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            var titleArray = [];
            var filteredTitleArray = [];
            var i;
            if (utilService.isNotEmptyArray(data)) {
              titleArray = JSON.parse(data);
              //							utilService.getLogger().info('------------ from AD titles ------------');
              for (i = 0; i < titleArray.length; i++) {
                //								utilService.getLogger().info(titleArray[i].title);
                if (utilService.isEmptyArray(filteredTitleArray)) {
                  filteredTitleArray.push(titleArray[i]);
                } else if (
                  utilService.isEmptyArray(
                    $filter("filter")(
                      filteredTitleArray,
                      { title: titleArray[i].title },
                      true
                    )
                  )
                ) {
                  filteredTitleArray.push(titleArray[i]);
                }
              }
              filteredTitleArray = $filter("orderBy")(
                filteredTitleArray,
                "title",
                false
              );
              //							utilService.getLogger().info('------------ after filtered AD titles ------------');
              //							for (i = 0; i < filteredTitleArray.length; i++) {
              //								utilService.getLogger().info(filteredTitleArray[i].title);
              //							}
            }
            return {
              response: filteredTitleArray,
            };
          },
        },
        getUserCorpIdList: {
          params: { username: "@username" },
          url: API_BASE_USERINFO_URL + "/corpIds",
          method: "GET",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
      }
    ),
  };
}

export default userInfoResource;
