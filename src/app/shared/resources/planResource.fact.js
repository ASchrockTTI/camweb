
import { environment } from './../../../environments/environment';
	
	planResource.$inject = [
		'$resource',
		'keys',
		'utilService'
		];
	
	function planResource(
			$resource,
			keys,
			utilService) {
		
		var API_PLAN_URL = environment.apiURL;
		
		var transform = function(data, header, status) {
			
			var response = {};
			
			if (data != '') {
				response = angular.fromJson(data);
			}
			
			return {
				response: response
			}
		}
		
		return {
			plan: $resource(API_PLAN_URL, {}, {
				query: {
			    	url: API_PLAN_URL + 'plans',
					params: {userName: '@owner', type:'@type', allPlans:'@allPlans'},
			    	method: 'GET',
			    	isArray:false,			    	
			    	transformResponse: transform
			    },
				
				nonTTI: {
					url: API_PLAN_URL + 'plans/nonTTICustomers',
					params: {userName: '@owner', type:'@type', allPlans:'@allPlans'},
			    	method: 'GET',
			    	isArray:false,			    	
			    	transformResponse: transform
				},

			    getPlanByPlanId: {
			    	url: API_PLAN_URL + 'plans/:planId',
			    	method: 'GET',
				    isArray:false,
				    transformResponse: transform
			    },
			    
			    getBusinessPlanByPlanId: {
			    	url: API_PLAN_URL + 'plans/:planId/business-plan',
			    	method: 'GET',
				    isArray:false,
				    transformResponse: transform
			    },
			    
			    findRelatedPlans: {
			    	url: API_PLAN_URL + 'plans/:planId/related',
			    	method: 'GET',
				    isArray:false,
				    transformResponse: transform
			    },
			    
			    savePlan: {
			    	url: API_PLAN_URL + 'plans',
			    	method: 'POST'
			    },
			    
			    updatePlan: {
			    	url: API_PLAN_URL + 'plans/:planId',
			    	method: 'PUT'
			    },
			    
			    updateBlog: {
			    	url: API_PLAN_URL + 'plans/:planId/blogs',
			    	method: 'PUT'
			    },
			    
			    updateStatus: {
			    	url: API_PLAN_URL + 'plans/:planId/status',
			    	params: {planStatus: '@planStatus'},
			    	method: 'PUT',
			    	transformResponse: transform
			    },
			    
			    deletePlan: {
			    	url: API_PLAN_URL + 'plans/:planId',
			    	params: {planId: '@planId', isDeletePlan:'@isDeletePlan', fullName:'@fullName'},
			    	method: 'DELETE'
			    },
			    
			    discardPlan: {
			    	url: API_PLAN_URL + 'plans/discard',
			    	method: 'PUT'
			    },
			    
			    archivePlan: {
			    	url: API_PLAN_URL + 'plans/archive',
			    	method: 'PUT',
			    	transformResponse: transform
			    },
			    
			    classifyCustomer: {
			    	url: API_PLAN_URL + 'plans/:planId/classification',
			    	method: 'PUT',
			    	transformResponse: transform
			    },
			    
			    getMarketSegments: {
			    	url: API_PLAN_URL + 'v1/marketSegments',
			    	isArray: false,
			    	transformResponse: transform
			    },
			    
			    getMarketSegmentFilters: {
			    	url: API_PLAN_URL + 'v1/marketSegmentFilters',
			    	isArray: false,
			    	transformResponse: transform
			    },
			    
			    getParentBranchList: {
			    	url: API_PLAN_URL + 'parentBranches',
			    	isArray: false,
			    	transformResponse: transform
			    },
			    
			    getBranches: {
			    	url: API_PLAN_URL + 'branches',
			    	isArray: false,
			    	transformResponse: transform
			    },
			    
			    getEntityParentBranches: {
			    	url: API_PLAN_URL + 'branches/:entity',
			    	isArray: false,
			    	transformResponse: transform
				},
				
				getTopLevelUserPlanInfo: {
					url: API_PLAN_URL + 'plans/topLevelUserPlanInfo',
					method: 'GET',
					params: {userName: '@userName'},
					//isArray: false,
					//transformResponse: transform,
				}
			})
		}	
	}

export default planResource;