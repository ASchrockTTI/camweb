
import { environment } from './../../../environments/environment';
	
	OmsResource.$inject = [
		'$resource'
	]
	
	function OmsResource(
			$resource) {		
		
		var API_OMS_URL = environment.apiURL + 'plans/:planId/oms/';
		
		return {
			oms: $resource(API_OMS_URL, {}, {
				
				queryByPlan: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:type',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				query: {
					url: environment.apiURL + 'v1/oms/projects',
					method: 'POST',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				add: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/add',
					method: 'POST'
				},
				update: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:type/:projectId',
					method: 'PUT'
				},
				removeProject: {
					method: 'DELETE',
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId',
				},
				queryProject: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:type/:projectId',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				queryLineItems: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/details',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				addLineItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/add',
					method: 'POST'
				},
				updateLineItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/details/:detailId',
					method: 'PUT'
				},
				removeLineItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/details/:detailId/delete',
					params: {userName: '@userName'},
					method: 'DELETE'
				},
				queryBusinessPartners: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/partners',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				addBusinessPartner: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/partners/:partnerPlanId',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				removeBusinessPartner: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/partners/:projectCustomerId',
					method: 'DELETE',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				attachPlanToPartner: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/partners/:projectCustomerId/:customerPlanId',
					method: 'PUT',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				addActionItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/action/add',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				updateActionItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/projects/:projectId/action/:actionId',
					method: 'PUT',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				deleteActionItem: {
					url: environment.apiURL + 'v1/plans/:planId/oms/actions/:actionId',
					method: 'DELETE'
				},
				getOmsSummary: {
					url: environment.apiURL + 'v1/oms/summary/:username',
					method: 'GET'
				},
				getProjectOwnersByBranch: {
					url: environment.apiURL + 'v1/oms/projectOwners/:branch',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
		}
	}

export default OmsResource;