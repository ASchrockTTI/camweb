import { environment } from './../../../environments/environment';

ActionItemResource.$inject = ["$resource"];

function ActionItemResource($resource) {
  var API_ACTION_ITEM_URL = environment.apiURL + "action-item/";

  return {
    actionItem: $resource(
      API_ACTION_ITEM_URL,
      {},
      {
        queryByUserName: {
          method: "GET",
          url: API_ACTION_ITEM_URL + ":userName/allActionItems",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryByBranchList: {
          method: "GET",
          url:
            API_ACTION_ITEM_URL +
            ":userName/allActionItems/branchLevel/:branchList",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryByCriteria: {
          method: "POST",
          url: API_ACTION_ITEM_URL + "summary",
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryActionItemCounts: {
          method: "GET",
          url: API_ACTION_ITEM_URL + "dashboard/:userName",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        updateCallReportActionItem: {
          method: "PUT",
          url: API_ACTION_ITEM_URL + "call-report/:actionItemId",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        completeActionItem: {
          method: "PUT",
          url: API_ACTION_ITEM_URL + "complete",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        deleteCallReportActionItem: {
          method: "DELETE",
          url: API_ACTION_ITEM_URL + "call-report/:actionItemId",
        },
        deleteActionItem: {
          method: "DELETE",
          url: API_ACTION_ITEM_URL + ":actionItemType/:actionItemId",
        },
      }
    ),
  };
}
export default ActionItemResource;
