
import { environment } from './../../../environments/environment';
	
	VbsResource.$inject = [
		'$resource',
		'utilService',
		'vbsForm'
	];
	
	function VbsResource(
		$resource,
		vbsForm) {		
		
		var API_VBS_URL = environment.apiURL + 'plans/:planId/vbs';
		var API_VBS_URL_PRODUCTION_SUMMARY 		= API_VBS_URL + '/' + vbsForm.PRODUCTION_SUMMARY;
		var API_VBS_URL_CM_MATRIX 	= API_VBS_URL + '/' + vbsForm.CM_MATRIX;
		
		return {
			forms: $resource(API_VBS_URL, {}, {
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				findCM: {
					method: 'GET',
					url: environment.apiURL + 'plans/vbs/findCM',
					params: {search: '@search'},
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				}
			}),
			project: $resource(API_VBS_URL, {}, {
				queryByStatus: {
					url: environment.apiURL + 'plans/:planId/vbs-projects/:status',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				create: {
					url: environment.apiURL + 'plans/:planId/vbs-projects',
					method: 'POST'
				},
				migrate: {
					url: environment.apiURL + 'plans/:planId/vbs-projects/migrate',
					method: 'POST'
				},
				update: {
					url: environment.apiURL + 'plans/:planId/vbs-projects/:projectId',
					method: 'PUT',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				delete: {
					url: environment.apiURL + 'plans/:planId/vbs-projects/:projectId',
					method: 'DELETE',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				}
				
			}),
			
			customerInfluencer: $resource(API_VBS_URL, {}, {
				query: {
					url: API_VBS_URL + '/:vbsFormTypeId',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				}
			}),
			
			productionSummary: $resource(API_VBS_URL, {}, {
				query: {
					url: API_VBS_URL + '/:vbsFormTypeId',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						var items = JSON.parse(data);
						
						for (var i = 0; i <= items.length - 1; i++) {
							
							if (items[i].productionSummaryPlanModification) {
								items[i].selectedPlannedModification = {
										value: items[i].productionSummaryPlanModification.planModID,
										display: items[i].productionSummaryPlanModification.planMod,
									}
							} else {
								items[i].selectedPlannedModification = {};
							}
							
														
						}
						
						return {						
							response: items
						}
					}
				},
				save: {
					url: API_VBS_URL_PRODUCTION_SUMMARY,
					method: 'POST'
				},
				updateP: {
					url: API_VBS_URL + '/:vbsFormTypeId',
					method: 'PUT'
				},
				delete: {
					method: 'DELETE',
					url: environment.apiURL + 'plans/vbs/:vbsFormTypeId/:id'
				}
			}),
			
			cmMatrix: $resource(API_VBS_URL, {}, {
				query: {
					url: API_VBS_URL + '/:vbsFormTypeId',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				save: {
					url: API_VBS_URL_CM_MATRIX,
					method: 'POST'
				},
				updateP: {
					url: API_VBS_URL + '/:vbsFormTypeId',
					method: 'PUT'
				},
				delete: {
					method: 'DELETE',
					url: environment.apiURL + 'plans/vbs/:vbsFormTypeId/:id'
				}
			})
			
		}
		
	}

export default VbsResource;