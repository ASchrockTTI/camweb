
import { environment } from './../../../environments/environment';
	
	OverviewResource.$inject = [
		'$resource',
		'utilService'
	];
	
	function OverviewResource(
		$resource,
		utilService
		) {		
		
		var API_OVERVIEW_URL = environment.apiURL + 'dreg/';
		
		return {
			summary: $resource(API_OVERVIEW_URL, {}, {
				queryByEntity: {
					url: API_OVERVIEW_URL + 'counts/',
					params: {userName: '@userName'},
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						if (!data) {
							return {
								response: []
							}
						}
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
			
		}
		
	}

export default OverviewResource;