
import { environment } from './../../../environments/environment';
	
	TmwResource.$inject = [
		'$resource'
	]
	
	function TmwResource(
			$resource) {		
		
		var API_TMW_URL = environment.apiURL + 'tmw/';
		
		return {
			tmw: $resource(API_TMW_URL, {}, {
				
				query: {
					method: 'POST',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getQuarterRange: {
					method: 'GET',
					url: API_TMW_URL + 'quarter',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				queryFsr: {
					url: API_TMW_URL + 'lookup/fsr',
					params: {branch: '@branch', corpId: '@corpId'},
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				add: {
					method: 'POST'
				},
				update: {
					method: 'PUT'
				},
				updateNotes: {
					url: API_TMW_URL + 'notes/',
					method: 'PUT'
				},
				createNotes: {
					url: API_TMW_URL + 'notes/',
					method: 'POST'
				}
			})
		}
	}

export default TmwResource;