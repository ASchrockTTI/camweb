
import { environment } from './../../../environments/environment';
	
	TacticDelegateResource.$inject = [
		'$resource'
	]
	
	function TacticDelegateResource(
			$resource) {
		
		var API_TACTIC_DELEGATE_URL = environment.apiURL + 'plans/:planId/strategies/:planStrategyId/tactics/:planTacticId/tacticDelegates'
		
		return {
			
			delegate: $resource(API_TACTIC_DELEGATE_URL, {}, {
				
				update: {
					method: 'PUT'
				},
				
				remove: {
					url: API_TACTIC_DELEGATE_URL + '/:planTacticDelegateId',
					method: 'DELETE'
				}
			})
			
		}
	}

export default TacticDelegateResource;