
import { environment } from './../../../environments/environment';
	
	PendingCustomerResource.$inject=[
		'$resource'
	]
	
	function PendingCustomerResource(
		$resource) {
		
		var API_PENDING_CUSTOMER_URL = environment.apiURL + 'pending-customers/';
		
		var transform = function(data, header, status) {
			
			var response = {};
			
			if (data != '') {
				response = angular.fromJson(data);
			}
			
			return {
				response: response
			}
		}
		
		return {
			pendingCustomer: $resource(API_PENDING_CUSTOMER_URL, {}, {
				create: {
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				get: {
					method: 'GET',
					isArray: false,
					transformResponse: transform
				},
				getByUser: {
					url: API_PENDING_CUSTOMER_URL + ':userName',
					method: 'GET',
					isArray: false,
					transformResponse: transform
				},
				getCountByUser: {
					url: API_PENDING_CUSTOMER_URL + 'summary/:userName',
					method: 'GET',
					isArray: false,
					transformResponse: transform
				},
				postCountByUser: {
					url: API_PENDING_CUSTOMER_URL + 'summary',
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				delete: {
					url: API_PENDING_CUSTOMER_URL + ':pendingCustomerId/delete',
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				getByPlanId: {
					url: API_PENDING_CUSTOMER_URL + 'plan/:planId',
					method: 'GET',
					isArray: false,
					transformResponse: transform
				},
				reject: {
					url: API_PENDING_CUSTOMER_URL + ':pendingCustomerId/reject',
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				discard: {
					url: API_PENDING_CUSTOMER_URL + ':pendingCustomerId/discard',
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				wrongBranch: {
					url: API_PENDING_CUSTOMER_URL + ':pendingCustomerId/wrong-branch',
					method: 'POST',
					isArray: false,
					transformResponse: transform
				},
				qualify: {
					url: API_PENDING_CUSTOMER_URL + ':pendingCustomerId',
					method: 'POST',
					transformResponse: transform
				}
			})
		}
		
	}

export default PendingCustomerResource;