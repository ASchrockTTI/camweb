
import { environment } from './../../../environments/environment';
	
	NuggetsResource.$inject = [
		'$resource'
	]
	
	function NuggetsResource(
			$resource) {		
		
		var API_NUGGETS_URL = environment.apiURL + 'nugget/';
		
		return {
			nuggets: $resource(API_NUGGETS_URL, {}, {
				
				getNuggetCommodities: {
					url: environment.apiURL + 'nugget/commodities',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getNuggetsByPlanId: {
					url: API_NUGGETS_URL + 'plans/:planId',
					method: 'GET',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				queryByCriteria: {
					url: API_NUGGETS_URL + 'summary',
					method: 'POST',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				getNuggetCommpetitors: {
					url: environment.apiURL + 'nugget/competitors',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				queryManufacturers: {
					method: 'GET',
					url: environment.apiURL + 'nugget/manufacturers',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				addNuggetPart: {
					url: API_NUGGETS_URL + ':nuggetId/nuggetPart/create/:userName/userName',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				getNuggetStatuses: {
					url: environment.apiURL + 'nugget/statuses',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
					
				queryOwnersByBranches: {
					url: API_NUGGETS_URL + 'owners',
					method: 'POST',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				createCompetitor: {
					url:environment.apiURL + 'nugget/competitor/create',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				createNugget: {
					url:environment.apiURL + 'nugget/create',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				updateNuggetPart: {
					url: API_NUGGETS_URL + ':nuggetId/nuggetPart/update/:userName/userName',
					method: 'PUT',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				deleteNuggetPart: {
					url: API_NUGGETS_URL + ':nuggetId/nuggetPart/:nuggetPartsID/delete/:userName/userName',
					method: 'DELETE',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				

				deleteNugget: {
					url: API_NUGGETS_URL + ':nuggetId/delete/:userName/userName',
					method: 'DELETE',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				updateNugget: {
					url:environment.apiURL + 'nugget/update/:userName/userName',
					method: 'PUT',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getNuggetById: {
					url: API_NUGGETS_URL + ':nuggetId',
					method: 'GET',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

				getNuggetsDashboardInfo: {
					url: environment.apiURL + 'nugget/dashboard/:userName',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
			}),
			comments: $resource(API_NUGGETS_URL, {}, {
				
				getNuggetCommentsByPlanId: {
					url: environment.apiURL + 'nugget/plans/:planId/comments',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				deleteComment: {
					url: environment.apiURL + 'nugget/comment/:commentId',
					method: 'DELETE',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				save: {
					url: environment.apiURL + 'nugget/:nuggetId/comment',
					method: 'PUT',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},

			})
		}
	}

export default NuggetsResource;