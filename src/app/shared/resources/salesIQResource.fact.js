
import { environment } from './../../../environments/environment';
	
	SalesIQResource.$inject = [
		'$resource'
	]
	
	function SalesIQResource(
			$resource) {		
		
		var API_SALES_IQ_URL = environment.apiURL + 'salesIQ/';
		
		return {
			salesIQ: $resource(API_SALES_IQ_URL, {}, {
				getOpportunitiesByPlanId: {
					url: environment.apiURL + 'salesIQ/:planId',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getDashboardInfo: {
					url: environment.apiURL + 'salesIQ/dashboard/:username',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getVisualization: {
					url: environment.apiURL + 'salesIQ/:planId/visualization/:customerInsightKey',
					method: 'GET',
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				actionOpportunity: {
					url: environment.apiURL + 'salesIQ/actionOpportunity',
					method: 'POST',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				getOpportunityActionReasons: {
					url: environment.apiURL + 'salesIQ/opportunityActionReasons',
					method: 'GET',
					isArray: false,
					transformResponse: function (data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			}),
			customerInsight: $resource(API_SALES_IQ_URL, {}, {
				
				queryByCriteria: {
					url: API_SALES_IQ_URL + 'summary',
					method: 'POST',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				queryOwnersByCriteria: {
					url: API_SALES_IQ_URL + 'owners',
					method: 'POST',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
		}
	}

export default SalesIQResource;