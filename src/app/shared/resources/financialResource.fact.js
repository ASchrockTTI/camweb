
import { environment } from './../../../environments/environment';
	
	FinancialResource.$inject = ['$resource'];
	
	function FinancialResource(
			$resource) {
		
		var API_PLAN_LOCATION_URL = environment.apiURL + 'plans/:planId/financials';
		var API_PLANID = '@planId';
		
		return {
			financials: $resource(API_PLAN_LOCATION_URL, {}, {
				// This method is used to return the business plan data
				query: {
					params: {entity: '@entity', type: '@type', account: '@account', mfr: '@mfr'},
			    	method: 'GET',
					isArray: false,
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
				},
				export: {
					method: 'GET',
					hasBody: true,
					url: API_PLAN_LOCATION_URL + '/export',
					params: {fileName: '@fileName', userName: '@userName'},
					responseType: 'arraybuffer',
					transformResponse: function(data, headers) {
					 	 return {
					   		 response: {
					   			 data: data,
					   			 headers: headers
					   		 }
					   	 }
					}
				}
				
			}),	
			dsam: $resource(API_PLAN_LOCATION_URL, {}, {
				update: {
			    	method: 'PUT',
			    	url: environment.apiURL + 'financials/dsam',
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
			}})
		}
	}

export default FinancialResource;