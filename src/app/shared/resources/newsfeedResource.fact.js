
import { environment } from './../../../environments/environment';
	
	NewsFeedResource.$inject = ['$resource', 'utilService'];
	
	function NewsFeedResource($resource, utilService) {
		
		var API_NEWS_FEED_URL = environment.apiURL + 'plans/:planId/newsfeed';
		
		var headers = {};
		
		return {
			newsFeed: $resource(API_NEWS_FEED_URL, {}, {
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				save: {
					method: 'POST'
				},
				uploadDocument: {
					url: API_NEWS_FEED_URL + '/:newsFeedId/uploadDocument',
					method: 'POST',
					headers: {'Content-Type': headers.contentType, 'x-filename': headers.fileName, 'userName': headers.userName},
					transformRequest: function(data) {						
//						var fd = new FormData();
//						fd.append('x-filename', data.fullName);
//						fd.append('userName', data.userName);	
						test(data);
						return data;
					}
				},
				downloadDocument: {
					url: API_NEWS_FEED_URL + '/getDocument/:documentID',
					method: 'GET',
					responseType: 'arraybuffer',
					transformResponse: function(data, headers, status) {
						
						return {
							response: {
								data: data,
								headers: headers()
							}
						}
					}
				},
				deleteDocument: {
					url: API_NEWS_FEED_URL + '/document/:documentID',
					method: 'DELETE',
					hasBody: true
				},
				deleteNewsFeed: {
					url: API_NEWS_FEED_URL + '/:newsFeedId',
					method: 'DELETE',
					hasBody: true
				}
			})
		}
		
		function test(data) {
			headers = {
				contentType: undefined,
				fileName: data.fullName,
				userName: data.userName
			}
		}
	}

export default NewsFeedResource;