
import { environment } from './../../../environments/environment';
	
	ContactResource.$inject = [
		'$resource'
	]
	
	function ContactResource(
			$resource) {		
		
		var API_CONTACTS_URL = environment.apiURL + 'plans/:planId/delegates/';
		
		return {
			contact: $resource(API_CONTACTS_URL, {}, {
				
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				add: {
					method: 'POST'
				},
				update: {
					method: 'PUT'
				},
				remove: {
					method: 'DELETE',
					url: API_CONTACTS_URL + ':delegateId',
					hasBody: true
				},
				queryManufacturers: {
					method: 'GET',
					url: environment.apiURL + 'manufacturers',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				}
			})
		}
	}

export default ContactResource;