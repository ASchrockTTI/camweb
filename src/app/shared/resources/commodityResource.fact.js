
import { environment } from './../../../environments/environment';
	
	commodityResource.$inject = ['$resource'];
	
	function commodityResource($resource) {
		
		var API_PLAN_COMMODITY_URL = environment.apiURL + 'plans/:planId/commodities';
		
		return {
			commodities: $resource(API_PLAN_COMMODITY_URL, {}, {
				// This method is used to return the business plan data
				query: {
					params: {entity: '@entity'},
			    	method: 'GET',
					isArray: false,
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
				},
				
				update: {
					method: 'PUT'
				}
			}),	
		}
	}

export default commodityResource;