
import { environment } from './../../../environments/environment';
	
	locationResource.$inject = ['$resource'];
	
	function locationResource($resource) {
		
		var API_PLAN_LOCATION_URL = environment.apiURL + 'plans/:planId/locations';
		var API_PLANID = '@planId';
		
		return {
			locations: $resource(API_PLAN_LOCATION_URL, {}, {
				// This method is used to return the business plan data
				query: {
					params: {entity: '@entity'},
			    	method: 'GET',
					isArray: false,
			    	transformResponse: function(data,header) {
			    		return {
				    		response: JSON.parse(data)
				    	}
			    	}
				},
				
				update: {
					method: 'PUT'
				},
				
				add: {
					method: 'POST'
				},
				
				delete: {
					url: API_PLAN_LOCATION_URL + '/:planLocationSpendId',
					method: 'POST'
				}
				
			}),	
		}
	}

export default locationResource;