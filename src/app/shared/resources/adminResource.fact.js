
import { environment } from './../../../environments/environment';
	
	AdminResource.$inject = [
		'$resource',
		'utilService'
	]
	
	function AdminResource(
		$resource,
		utilService) {
		
		utilService.getLogger().info('Entering Admin Resource...');
		
		var API_ADMIN_URL = environment.apiURL + 'admin/';
		
		return {
			users: $resource(API_ADMIN_URL + 'users', {}, {
				findAll: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				updateUserTable: {
					url: API_ADMIN_URL + 'users/populate-users-table',
					method: 'POST',
				},
				add: {
					method: 'POST'
				},
				
				update: {
					method: 'PUT'
				},
				
				delete: {
					url: API_ADMIN_URL + 'users/:userId',
					params: {userId: '@userId'},
					method: 'DELETE',
					hasBody: true
				}
			}),
			
			titles: $resource(API_ADMIN_URL + 'titles', {}, {
				findAll: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				
				add: {
					method: 'POST'
				},
				
				update: {
					method: 'PUT'
				},
				
				delete: {
					url: API_ADMIN_URL + 'titles/:jobTitleId',
					params: {jobTitleId: '@jobTitleId'},
					method: 'DELETE',
					hasBody: true
				}
			}),
			
			releaseNotes: $resource(API_ADMIN_URL + 'release-notes', {}, {
				getLatest: {
					url: API_ADMIN_URL + 'latest-release',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				getAll: {
					method: 'GET',
					url: environment.apiURL + 'release-notes',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				delete: {
					url: environment.apiURL + 'release-notes/:releaseNoteId',
					method: 'DELETE'
				},
				upload: {
					url: environment.apiURL + 'release-notes',
					method: 'POST',
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				downloadDocument: {
					url: environment.apiURL + 'release-notes/document/:documentID',
					method: 'GET',
					responseType: 'arraybuffer',
					transformResponse: function(data, headers, status) {
						
						return {
							response: {
								data: data,
								headers: headers()
							}
						}
					}
				},
				deleteDocument: {
					url: environment.apiURL + 'release-notes/document/:documentID',
					method: 'DELETE'
				}
			}),
			
			exceptions: $resource(API_ADMIN_URL + 'exception-rule', {}, {
				findAll: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				add: {
					method: 'POST',					
				},
				delete: {
					method: 'DELETE',
					url: API_ADMIN_URL + 'exception-rule/:exceptionRuleId',
					params: {exceptionRuleId: '@exceptionRuleId'},
					hasBody: true
				}
			}),
			
			plans: $resource(API_ADMIN_URL + 'port', {}, {
				transfer: {
					method: 'POST'
				}
			}),
			
			accounts: $resource(API_ADMIN_URL + 'accounts', {}, {
				query: {
					method: 'GET',
					params: {dateToday: '@dateToday', dateTomorrow: '@dateTomorrow', account: '@account', entities: '@entities'},
					isArray: false,
					transformResponse: function(data, header) {
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				queryDeleted: {
					method: 'GET',
					url: API_ADMIN_URL + 'accounts/deleted',
					isArray: false,
					transformResponse: function(data, header) {
						if (!data) {
							return {
								response: []
							}
						}
						
						return {
							response: JSON.parse(data)
						}
					}
				},
				delete: {
					method: 'PUT',
					url: API_ADMIN_URL + 'accounts/delete/:accountId',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
				export: {
					method: 'GET',
					url: API_ADMIN_URL + 'accounts/export',
					params: {dateToday: '@dateToday', dateTomorrow: '@dateTomorrow', userName: '@userName'},
					responseType: 'arraybuffer',
					transformResponse: function(data, headers) {
					 	 return {
					   		 response: {
					   			 data: data,
					   			 headers: headers
					   		 }
					   	 }
					}
				}
			})

		}
	}

export default AdminResource;