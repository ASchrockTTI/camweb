CallReportResource.$inject = ["$resource"];
import { environment } from './../../../environments/environment';

function CallReportResource($resource) {
  var API_CALL_REPORT_URL = environment.apiURL + "plans/:planId/call-report/";

  return {
    callReport: $resource(
      API_CALL_REPORT_URL,
      {},
      {
        query: {
          method: "GET",
          url: API_CALL_REPORT_URL + ":callReportId",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAll: {
          method: "GET",
          url: API_CALL_REPORT_URL + "all",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllByReporter: {
          method: "GET",
          url: environment.apiURL + "users/:reporter/call-reports",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllDraftsByReporter: {
          method: "POST",
          url: environment.apiURL + "call-report/drafts",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllSubmittedByReporter: {
          method: "POST",
          url: environment.apiURL + "call-report/submitted",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllDraftsByReporterByQuarter: {
          method: "POST",
          url: environment.apiURL + "call-report/draftsByQuarter",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllSubmittedByReporterByQuarter: {
          method: "POST",
          url: environment.apiURL + "call-report/submittedByQuarter",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllDraftsBySearchCriteria: {
          method: "POST",
          url: environment.apiURL + "call-report/draftsBySearchCriteria",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryAllSubmittedBySearchCriteria: {
          method: "POST",
          url: environment.apiURL + "call-report/submittedBySearchCriteria",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryDrafts: {
          method: "POST",
          url: API_CALL_REPORT_URL + "drafts",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        querySubmitted: {
          method: "GET",
          url: API_CALL_REPORT_URL + "submitted",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        update: {
          method: "PUT",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        deleteCallReport: {
          method: "DELETE",
          url: API_CALL_REPORT_URL + ":callReportId",
        },
        create: {
          method: "POST",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
        queryDashboard: {
          method: "GET",
          url: environment.apiURL + "call-report/dashboard/:username",
          isArray: false,
          transformResponse: function (data, header) {
            return {
              response: JSON.parse(data),
            };
          },
        },
      }
    ),
  };
}
export default CallReportResource;
