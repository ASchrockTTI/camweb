
import { environment } from './../../../environments/environment';
	
	TacticResource.$inject = ['$resource'];
	
	function TacticResource(
			$resource) {
		
		var API_TACTIC_URL = environment.apiURL + 'plans/:planId/strategies/:planStrategyId/tactics/';
		
		return {
			tactic: $resource(API_TACTIC_URL, {}, {
				query: {
					method: 'GET',
					isArray: false,
					transformResponse: function(data, header) {
						return {
							response: JSON.parse(data)
						}
					}
				},
			    
			    updateTactic: {
			    	method: 'PUT'
			    },			    
				
			    addTactic: {
			    	method: 'POST'
			    },
			    
				remove: {
					method: 'DELETE',
					url: API_TACTIC_URL + ':planTacticId',
					hasBody: true
				}
			    
			})
		}
	}

export default TacticResource;