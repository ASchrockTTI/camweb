
import { environment } from './../../../environments/environment';
	
	EmailResource.$inject = ['$resource'];
	
	function EmailResource($resource) {
		
		var emailNotification = environment.apiURL + 'emailNotification';
		
		return {
			emailNotification: $resource(emailNotification, {}, {
				send: {url: emailNotification, method: 'POST'
				}
			})
			
		};
	}
	

export default EmailResource;