const app = {
	NAME: 'Customer Account Management', 
	VERSION: '1.65', 
	SPRINT: '68'
};

const server =  {
	DEV: 'txjbsd01', 
	UAT: 'txjbst01', 
	QA1: 'txjbsq01', 
	QA2: 'txjbsq02',
	QA3: 'qakam.ttiinc.com',
	QA4: 'qacam.ttiinc.com',
	LOCAL_HOST: 'localhost'
};

const environment = {
	DEV: 'DEV',
	QA: 'QA',
	UAT: 'UAT',
	LOCAL_HOST: 'localhost'
};

const roles = {
	ADMIN: 'ADMIN',
	EXEC: 'EXECUTIVE',
	RVP: 'RVP',
	RBOM: 'RBOM',
	GM: 'GM',
	ISM: 'ISM',
	FSM: 'FSM',
	FSR: 'FSR',
	ISR: 'ISR',
	OWNER: 'OWNER',
	DELEGATE: 'DELEGATE',
	CUSTOMER_SERVICE: 'CUSTOMER_SERVICE',
	PRODUCT: 'PRODUCT',
	HEAD_HONCHO: 'HEAD_HONCHO',
	RSE: 'RSE',
	CPM: 'CPM',
	BPM: 'BPM',
	SAM_GAM: 'SAM_GAM',
	SUPPORT: 'SUPPORT',
	FAE: 'FAE',
	BDM: 'BDM',
	PDM: 'PDM',
	SCM: 'SCM',
	BRANCH_MEMBER: 'BRANCH_MEMBER',
	BDG: 'BDG'
};


const entity = {
	NDC: 'NDC',
	EDC: 'EDC',
	ADC: 'ADC',
	GLOBAL: 'GLOBAL'
};

const session = {
	MAIN_PAGE_SORT_TYPE : 'MAIN_PAGE_SORT_TYPE',
	MAIN_PAGE_SORT_ORDER: 'MAIN_PAGE_SORT_ORDER'
};

const keys = {
	KAM: 'G4uyryGgqZh2zDwrrW1artZobWuYG90i'
};

const actionItemTypes = {
	OMS: 'OMS',
	CALL_REPORT: 'CALL_REPORT',
	TACTIC: 'TACTIC'
};

export {
  app,
  server,
  environment,
  roles,
  entity,
  session,
  keys,
  actionItemTypes
}