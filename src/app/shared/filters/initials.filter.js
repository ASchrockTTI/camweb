
	
	Initials.$inject = ['$filter'];
	
	function Initials($filter) {
		return function(input) {
		    var name = input;
		    var inls = name.match(/\b\w/g) || [];
		    inls = ((inls.shift() || '') + (inls.pop() || '')).toUpperCase();
		    return inls;
		  };
	}

export default Initials;