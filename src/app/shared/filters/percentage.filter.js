
	
	Percentage.$inject = ['$filter'];
	
	function Percentage($filter) {
		return function (input, decimals) {
			return $filter('number')(input * 100, decimals) + '%';
		}
	}

export default Percentage;