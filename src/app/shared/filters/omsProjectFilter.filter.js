function OmsProjectFilter() {
  return function (projects, properties) {
    var out = [];
    if (projects) {
      for (var i = 0; i < projects.length; i++) {
        if (
          projects[i].ownerName === properties.ownerName ||
          !properties.ownerName
        ) {
          if (
            !properties.masterProjectId ||
            projects[i].masterProjectId === properties.masterProjectId
          ) {
            projects[i].master = false;
            out.push(projects[i]);
          } else if (projects[i].projectId === properties.masterProjectId) {
            projects[i].master = true;
            out.push(projects[i]);
          }
        }
      }
    }
    return out;
  };
}
export default OmsProjectFilter;
