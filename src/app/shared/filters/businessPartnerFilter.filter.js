
	
	BusinessPartnerFilter.$inject = ['$filter'];
	
	function BusinessPartnerFilter($filter) {
		return function (list, openPlanId) {
			var out = [];
			for(var i = 0; i < list.length; i++) {
	            if(list[i].planType.planTypeId > 4 && list[i].planId !== openPlanId) {
	            	out.push(list[i]);
	            }
			}
			return out;
		}
	}

export default BusinessPartnerFilter;