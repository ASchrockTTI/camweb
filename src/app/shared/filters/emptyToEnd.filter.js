
	
	EmptyToEnd.$inject = ['$filter','$log', 'utilService'];
	
	function EmptyToEnd($filter,$log, utilService) {
		return function (array, key) {
            if (!angular.isArray(array) || array.length == 0) return;
            if (!array) return;
            
            // Move empty to the end only for the CqgFlag column
//            if (key === 'quoteHeader.cqgFlagSortOrderId') {
//            	key = 'quoteHeader.cqgFlag'            	
//            } else {
//            	return array;
//            }
            
            var split = key.split(".");
            var splitLength = split.length;
            
            var present = array.filter(function(item) {
            	
            	switch (splitLength) {
            	case 1:
                	return item[split[0]];
            		break;
            	case 2:
                	return item[split[0]][split[1]];
            		break;
            	}            	
            });
            
            var empty = array.filter(function(item) {
            	switch (splitLength) {
            	    case 1:
                	    return !item[split[0]];
            		    break;
            	    case 2:
                	    return !item[split[0]][split[1]];
            		    break;
            	}
            });
            
            if (utilService.isNotEmptyArray(empty)) {
            	empty = empty.reverse();
            }
            
            return present.concat(empty);
            
        };
	}

export default EmptyToEnd;