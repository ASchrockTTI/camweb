const commaDelimitedFilter = function ($filter) {
  return function (list, filterString) {
    if (!filterString) return list;

    var out = list;
    if (list) {
      var filterList = filterString.split(",");
      for (var i = 0; i < filterList.length; i++) {
        out = $filter("filter")(out, filterList[i].trim());
      }
    }
    return out;
  };
};
export default commaDelimitedFilter;
