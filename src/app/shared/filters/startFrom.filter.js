
	
	StartFrom.$inject = ['$filter'];
	
	function StartFrom($filter) {
		return function(input, start) {
	        if(input) {
	            start = +start; //parse to int
	            return input.slice(start);
	        }
	        return [];
	    }
	}

export default StartFrom;