
	
	TopTierFilter.$inject = ['$filter'];
	
	function TopTierFilter($filter) {
		return function (list) {
			var out = [];
			var families = [];
			var familyID = undefined;
			for(var i = 0; i < list.length; i++) {
	            if(families.indexOf(list[i].familyID) === -1) {
	            	out.push(list[i]);
	            	families.push(list[i].familyID);
	            }
			}
			return out;
		}
	}

export default TopTierFilter;