
	
	CommentFilter.$inject = ['$filter','$log'];
	
	function CommentFilter($filter,$log) {
		return function(comments, type) {
			
			var items = {
				showAll: type,
				out: []
			};
			
			angular.forEach(comments, function(value, key) {
				
				if (this.showAll) {
					switch(value.commentActionID) {
					case 1:					
						break;
					default:
						this.out.push(value);
					}
					
				} else {
					switch(value.commentActionID) {
					case 1:									
						this.out.push(value);
						break;
					default:
						
					}					
				}
			},items)
			
	        return items.out;
	    }
	}

export default CommentFilter;