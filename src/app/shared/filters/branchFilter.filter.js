
	
	BranchFilter.$inject = ['$filter'];
	
	function BranchFilter($filter) {
		return function (list, branches) {
			var out = [];
			for(var i = 0; i < list.length; i++) {
            if(branches.indexOf(list[i].branch) !== -1) 
	            {
	            	out.push(list[i]);
	            }
			}
			return out;
        };
	}

export default BranchFilter;