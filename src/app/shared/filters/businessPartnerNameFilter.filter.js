function BusinessPartnerNameFilter() {
  return function (projects, properties) {
    if (!properties.businessPartnerName) {
      return projects;
    }

    var out = [];
    if (!projects || projects.length == 0) {
      return out;
    }

    for (var i = 0; i < projects.length; i++) {
      if (!properties.businessPartnerName) {
        out.push(projects[i]);
      }

      var words = properties.businessPartnerName.split(" ");
      var firstWord = words[0];

      if (projects[i].businessPartnerName.startsWith(firstWord)) {
        out.push(projects[i]);
      }
    }

    return out;
  };
}
export default BusinessPartnerNameFilter;
