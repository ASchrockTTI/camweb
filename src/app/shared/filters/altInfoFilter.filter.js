
	
	AltInfoFilter.$inject = ['$filter','$log'];
	
	function AltInfoFilter($filter,
						   $log) {
		
		return function(lines, line ) {
			
			var items = {
					out: []
			};				
			
			angular.forEach(lines, function(value, key) {
				if (line.lineSeq !== value.lineSeq) {
					this.out.push(value);
				}
			},items)
			
			return items.out;
		}
	}

export default AltInfoFilter;