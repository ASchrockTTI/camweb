
	
	Titlecase.$inject = ['$filter'];
	
	function Titlecase($filter) {
		return function(input) {
		      input = input || '';
		      return input.replace(/\w*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		    };
	}

export default Titlecase;