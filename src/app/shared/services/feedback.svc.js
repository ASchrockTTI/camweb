
	
	FeedbackService.$inject = [
		'utilService',
		'userProfile',
		'feedbackResource',
		'$filter'
	];
	
	function FeedbackService(
			utilService,
			userProfile,
			feedbackResource,
			$filter) {
		
		utilService.getLogger().info('Entering FeedbackService...');
		
		var openIssues = [];
		var inProgressIssues = [];
		var releasedIssues = [];
		
		var features = [];
		
		function getOpenIssues() {
			return openIssues;
		}
		
		function getInProgressIssues() {
			return inProgressIssues;
		}
		
		function getReleasedIssues() {
			return releasedIssues;
		}
		
		function getFeatureList() {
			return features;
		}

		
		function retrieveFeatureList() {
			return feedbackResource.feedback.queryFeatures().$promise
			.then(function(response) {
				var features = response.response;
				var other = features.shift();
				features = $filter('orderBy')(features, 'feature');
				features.unshift(other);
				
				return features;
			},
			function(error) {				
				throw error;
			})
		}
		
		function retrieveAllIssuesByType(isBug) {
			
			var feedbackResourceInst = new feedbackResource.feedback;
			
			var type = 'suggestions';
			if(isBug) {
				type = 'bug-reports';
			}
			
			return feedbackResourceInst.$queryIssuesByType({type: type})
			.then(function(response) {
				
				var issues = response.response;
				
				openIssues = [];
				inProgressIssues = [];
				releasedIssues = [];
				
				angular.forEach(issues, function(value, key) {						
					
					if (value.released) {
						releasedIssues.push(value);
					} else if (value.inProgress) {
						inProgressIssues.push(value);
					} else {
						openIssues.push(value);
					}
					
				})
				
				return issues;
			},
			function(error) {				
				throw error;
			})
			
		}
		
		function addFeedback(feedbackContainer) {
			
			var feedbackResourceInst = new feedbackResource.feedback;
			
			feedbackResourceInst.feedback = {
				feedback: feedbackContainer.issue,
				feature: feedbackContainer.feature,
				isBug: feedbackContainer.isBug
			}
			
			feedbackResourceInst.securityProfile = {
				fullName: userProfile.getFullName(),
				sAMAccountName: userProfile.getSAMAccountName()
			}
			
			return feedbackResourceInst.$add()
			.then(function() {
				return;
			},
			function(error) {
				throw error;
			})
		}
		
		function updateFeedback(feedbackItem) {
			var feedbackResourceInst = new feedbackResource.feedback;
			
			feedbackResourceInst.feedback = feedbackItem;
			
			feedbackResourceInst.securityProfile = {
				fullName: userProfile.getFullName(),
				sAMAccountName: userProfile.getSAMAccountName()
			}
			
			return feedbackResourceInst.$update({feedbackId: feedbackItem.feedbackId})
			.then(function() {
				return;
			},
			function(error) {
				throw error;
			})
		}
		
		function upVote(feedbackId) {
			
			var feedbackResourceInst = new feedbackResource.feedback;
			
			feedbackResourceInst.feedbackVoter = {
				voterName: userProfile.getFullName(),
				voteUp: true,
			}
			
			return feedbackResourceInst.$addVote({feedbackId: feedbackId})
			.then(function(response) {
				return 
			},
			function(error) {
				throw error;
			})
		}
		
		function downVote(feedbackId) {

			var feedbackResourceInst = new feedbackResource.feedback;
			
			feedbackResourceInst.feedbackVoter = {
				voterName: userProfile.getFullName(),
				voteUp: false,
			}
			
			return feedbackResourceInst.$addVote({feedbackId: feedbackId})
			.then(function(response) {
				return 
			},
			function(error) {
				throw error;
			})
		}
		
		function markAsReleased(feedbackId) {
			var feedbackResourceInst = new feedbackResource.feedback;
			
			return feedbackResourceInst.$markAsReleased({feedbackId: feedbackId, markAsReleased: true})
			.then(function() {
				
			},
			function(error) {
				throw error;
			})
		}
		
		function markAsInProgress(feedbackId) {
			var feedbackResourceInst = new feedbackResource.feedback;
			
			return feedbackResourceInst.$markAsInProgress({feedbackId: feedbackId, markAsInProgress: true})
			.then(function() {
				
			},
			function(error) {
				throw error;
			})
		}
		
		function reply(feedbackId, reply) {
			
			var feedbackResourceInst = new feedbackResource.feedback;
			
			feedbackResourceInst.feedbackReply = reply;
			
			if(reply.feedbackReplyId == undefined) {
				feedbackResourceInst.feedbackReply.fullName = userProfile.getFullName();
				feedbackResourceInst.feedbackReply.userName = userProfile.getSAMAccountName();
			}
			
			return feedbackResourceInst.$reply({feedbackId: feedbackId})
			.then(function(response) {
				return 
			},
			function(error) {
				throw error;
			})
		}
		
		function deleteReply(feedbackId, feedbackReplyId) {
			var feedbackResourceInst = new feedbackResource.feedback;
			
			return feedbackResourceInst.$deleteReply({feedbackId: feedbackId, feedbackReplyId: feedbackReplyId})
			.then(function() {
				
			},
			function(error) {
				throw error;
			})

		}
		
		function discard(feedbackId) {
			var feedbackResourceInst = new feedbackResource.feedback;
			
			return feedbackResourceInst.$discard({feedbackId: feedbackId})
			.then(function() {
				
			},
			function(error) {
				throw error;
			})
		}
		
		return {
			getFeatureList : getFeatureList,
			retrieveFeatureList : retrieveFeatureList,
			retrieveAllIssuesByType : retrieveAllIssuesByType,
			addFeedback : addFeedback,
			updateFeedback : updateFeedback,
			getOpenIssues : getOpenIssues,
			getInProgressIssues: getInProgressIssues,
			getReleasedIssues : getReleasedIssues,
			upVote: upVote,
			downVote : downVote,
			markAsReleased : markAsReleased,
			markAsInProgress : markAsInProgress,
			reply : reply,
			deleteReply : deleteReply,
			discard : discard,
		}
	}

export default FeedbackService;