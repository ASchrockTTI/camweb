
import { environment } from './../../../environments/environment';
	
	NewsFeedService.$inject = [
		'utilService',
		'userProfile',
		'newsFeedResource'
	];
	
	function NewsFeedService(
		utilService,
		userProfile,
		newsFeedResource) {
		
		var newsFeed = [];
		var documents = [];
		var auditLog = [];
		
		function getNewsFeed() {
			return newsFeed;
		}
		
		function getDocuments() {
			return documents;
		}
		
		function getNewsFeedContainer() {
			return {
				planID: undefined, newsFeedID: undefined, parentNewsFeedID: undefined, newsFeed: undefined, author: undefined, dateTime: undefined,
				documentList: []
			}
		}
		
		function get(planId) {
			
			newsFeed = [];
			documents = [];
			
			return newsFeedResource.newsFeed.query({planId: planId}).$promise.then(
				function(response) {
					
					var feed = response.response;
					var temp = [];	
															
					for (var i = 0; i <= feed.length - 1; i++) {
						
						feed[i].dateTime = utilService.isoDateToShortDate(utilService.convertToISODate(feed[i].dateTime));
						feed[i].modifiedDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(feed[i].modifiedDateTime));
						
						angular.forEach(feed[i].documentList, function(value, key) {
							value.modifiedDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(value.modifiedDateTime));
							value.fullName = value.documentName + '.' + value.documentExt;
							documents.push(value);
						})
						
						feed[i].highlightMe = false;
												
						if (feed[i].parentNewsFeedID > 0) {
							
							var parentId = feed[i].parentNewsFeedID;
							
							var item = utilService.filterBy(temp, {newsFeedID: feed[i].parentNewsFeedID}, true);
							
							if (item.length === 0) {
								
								for (var j = 0; j <= temp.length - 1; j++) {
									item = utilService.filterBy(temp[j].replies, {newsFeedID: feed[i].parentNewsFeedID}, true);
									
									if (!item) continue;
									
									if (item.length > 0) {
										break;
									}
								}																
							}
							
							if (!item[0].replies) {
								item[0].replies = [];
							}
							
							item[0].replies.push(feed[i])
						} else {
							temp.push(feed[i]);
						}						
					}
					
					newsFeed = utilService.orderBy(temp,'newsFeedID',true);
					
					return temp;
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function setAuditLog(planId, action, feature) {
			
			auditLog = [];
			
			auditLog.push({
				planId: planId,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
		}		
		
		function saveNewsFeed(planId, container) {
			
			var newsFeedResourceInst = new newsFeedResource.newsFeed;
			
			var auditLogAction = container.newsFeedID > 0 ? 'UPDATE' : 'ADD';
			
			setAuditLog(container.planID, auditLogAction, 'NEWSFEED_COMMENT');
			
			newsFeedResourceInst.auditLog = auditLog;
			
			newsFeedResourceInst.planID = container.planID;
			newsFeedResourceInst.newsFeedID = container.newsFeedID;
			newsFeedResourceInst.parentNewsFeedID = container.parentNewsFeedID;
			newsFeedResourceInst.newsFeed = container.newsFeed;
			newsFeedResourceInst.author = container.author;
			newsFeedResourceInst.dateTime = container.dateTime;			
			
			return newsFeedResourceInst.$save({planId: planId}).then(
				function(response) {
					
					var item = response;
					item.dateTime = utilService.isoDateToShortDate(utilService.convertToISODate(response.dateTime))
					item.modifiedDateTime = utilService.isoDateToShortDate(utilService.convertToISODate(response.modifiedDateTime))
					return item;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function uploadDocument(planId, newsFeedId, document) {
		
			var xhr = new XMLHttpRequest();
			
			var method = 'POST';
			var url = environment.apiURL + 'plans/' + planId + '/newsfeed/' + newsFeedId + '/uploadDocument';
			var response = {};			
			
			try {
				xhr.open(method,url , false);
				xhr.setRequestHeader("x-filename", document.name + '.' + document.ext);
				xhr.setRequestHeader("userName", userProfile.getFullName());
				xhr.setRequestHeader("content-type", document.type);
				
				xhr.onreadystatechange = function() {//Call a function when the state changes.
				    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {				    	
				    	response = JSON.parse(xhr.response);
				    	return response;
				    }
				}
				
				xhr.send(document.file);
				
				return response;
				
			} catch(error) {
				throw error;
			}
			
		}
		
		function downloadDocument(planId, documentId) {
			
			return newsFeedResource.newsFeed.downloadDocument({planId: planId, documentID: documentId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function deleteDocument(planId, documentId) {
			
			var newsFeedResourceInst = new newsFeedResource.newsFeed;
			
			setAuditLog(planId, 'DELETE', 'NEWSFEED_DOCUMENT');
			newsFeedResourceInst.auditLog = auditLog;
			
			return newsFeedResourceInst.$deleteDocument({planId: planId, documentID: documentId}).then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function deleteNewsFeed(planId, newsFeedId) {
									
			var newsFeedResourceInst = new newsFeedResource.newsFeed;
			
			setAuditLog(planId, 'DELETE', 'NEWSFEED_COMMENT');
			newsFeedResourceInst.auditLog = auditLog;
			
			return newsFeedResourceInst.$deleteNewsFeed({planId: planId, newsFeedId: newsFeedId}).then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		return {
			get: get,
			getNewsFeedContainer : getNewsFeedContainer,
			getNewsFeed : getNewsFeed,
			saveNewsFeed : saveNewsFeed,
			getDocuments : getDocuments,
			uploadDocument : uploadDocument,
			downloadDocument : downloadDocument,
			deleteDocument : deleteDocument,
			deleteNewsFeed : deleteNewsFeed
		}
	}

export default NewsFeedService;