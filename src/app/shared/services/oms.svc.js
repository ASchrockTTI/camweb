
	
	OmsService.$inject = [
		'auditFactory',
		'omsResource',
		'utilService',
		'userProfile',
		'$rootScope'
	];
	
	function OmsService(
		auditFactory,
		omsResource,
		utilService,
		userProfile,
		$rootScope
		) {
		
		utilService.getLogger().info('Entering: omsService.svc.js');
		
		
		var projects = {
			active: [],
			archive: [],
			related: []
		}
		
		var loadedPlan = {
				active: undefined,
				archive: undefined,
				related: undefined
		}
		
		this.getProjects = function(type) {
			return projects[type];
		}	
		
		this.getLoadedPlan = function(type) {
				return loadedPlan[type];
		}
		
		
		this.retrieveProjectsByPlan = function(planId, type) {
			
			return omsResource.oms.queryByPlan({planId: planId, type: type}).$promise.then(
				function(response) {
					
					projects[type] = response.response;
					
					loadedPlan[type] = planId;
					
					return projects[type];
				},
				function(error) {
					utilService.getLogger().error('OmsService: retrieveProjectsByPlan() error...');
					throw error;
				}
			)	
		}
		
		this.retrieveProjects = function(criteria) {
			var omsResourceInst = new omsResource.oms();
			omsResourceInst.username = criteria.username;
			omsResourceInst.branch = criteria.branch;
			if(criteria.corpId) omsResourceInst.corpId = criteria.corpId.corpId;
			omsResourceInst.startDate = criteria.startDate;
			omsResourceInst.endDate = criteria.endDate;
			
			if(criteria.showMine) omsResourceInst.username = userProfile.getSAMAccountName();
			
			if(userProfile.getUserType().toUpperCase() === 'RSE') {
				omsResourceInst.isRSE = true;
			}
			
			return omsResourceInst.$query().then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('OmsService: retrieveProjects() error...');
					throw error;
				}
			)	
		}


		this.retrieveProject = function (planId, type, projectId) {

			return omsResource.oms.queryProject({ planId: planId, type: type, projectId: projectId }).$promise.then(
				function (response) {
					return response;
				},
				function (error) {
					utilService.getLogger().error('OmsService: retrieveProject() error...');
					throw error;
				}
			)
		}
		
		this.changeStatus = function(project) {
			if(project.headerStatus.statusId === 1){
				var prevStatus = 'archive';
				var newStatus = 'active';
			} else{
				var prevStatus = 'active';
				var newStatus = 'archive';
			}
			
			for(var i = 0 ; i < projects[prevStatus].length; i++) {
				if(projects[prevStatus][i].projectId === project.projectId) {
					projects[prevStatus].splice(i, 1);
					projects[newStatus].push(project);
				}
			}
		}
		
		this.saveActionItem = function(planId, projectId, actionItem) {
			sessionStorage.removeItem('omsProjects');
			
			var omsResourceInst = new omsResource.oms();
			omsResourceInst.actionId = actionItem.actionId;
			omsResourceInst.actionDescription = actionItem.actionDescription;
			omsResourceInst.complete = actionItem.complete;
			omsResourceInst.daysToRemind = actionItem.daysToRemind;
			omsResourceInst.dueDate = actionItem.dueDate;
			omsResourceInst.assigneeUsername = actionItem.assigneeUsername;
			omsResourceInst.assigneeFullName = actionItem.assigneeFullName;
			omsResourceInst.creatorUsername = userProfile.getSAMAccountName();
			omsResourceInst.creatorFullName = userProfile.getFullName();
			
			if(!planId) planId = 0;
			
			if(actionItem.actionId) {
				return omsResourceInst.$updateActionItem({planId: planId, projectId: projectId, actionId: actionItem.actionId}).then(
					function (response) {
						return response.response;
					},
					function (error) {
						utilService.getLogger().error('OmsService: updateActionItem() error...');
						throw error;
					}
				)
			} else {
				return omsResourceInst.$addActionItem({planId: planId, projectId: projectId}).then(
					function (response) {
						return response.response;
					},
					function (error) {
						utilService.getLogger().error('OmsService: addActionItem() error...');
						throw error;
					}
				)
			}
		}
		
		this.deleteActionItem = function(planId, projectId, actionId) {
			sessionStorage.removeItem('omsProjects');
			
			if(!planId) planId = 0;
			
			return omsResource.oms.deleteActionItem({ planId: planId, projectId: projectId, actionId: actionId }).$promise.then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('OmsService: deleteActionItem() error...');
					throw error;
				}
			)	
		}

		this.getDetails = function(planId, projectId) {

			return omsResource.oms.queryLineItems({ planId: planId, projectId: projectId }).$promise.then(
				function(response) {

					var details = response.response;

					var avlList = [];
					var avl = [];
					
					for(var i = 0; i < details.length; i++) {
						if (details[i].part.partNumber) {
							details[i].part.partNumber = details[i].part.partNumber.trim();
						}
						
						if(details[i].dregApprovalStatus) {
							details[i].dregSort = details[i].dregApprovalStatus;
						} else {
							details[i].dregSort = details[i].part.dregFlag;
						}
						
						if (details[i].registration) {
							details[i].registration = details[i].registration.trim();
						}
						if(details[i].updateDate === null) {
							details[i].updateDate = details[i].insertDate;
						}
						if(!details[i].primary && (avl.length === 0 || avl[0].primary || (details[i].resale > avl[0].resale))) {
							avl.push(details[i]);
						}
						else {
							avl.unshift(details[i]);
						}
						if(i+1 === details.length) {
							avlList.push(avl);
							break;
						}
						if(details[i].opportunity !== details[i+1].opportunity) {
							avl[0].primary = true;
							avlList.push(avl);
							avl = [];
						}
					}
					
					return {avlList: avlList, lineItemCount: details.length}
				},
				function(error) {
					utilService.getLogger().error('OmsService: getDetails() error...');
					throw error;
				}
			)	
		}
		
		this.addLineItem = function(planId, type, projectId, lineItem) {
			var omsResourceInst = new omsResource.oms();
			omsResourceInst.userName = userProfile.getSAMAccountName();
			omsResourceInst.createdBy = userProfile.getFullName();
			omsResourceInst.detailId = lineItem.detailId;
			omsResourceInst.designType = lineItem.designType;
			omsResourceInst.part = lineItem.part;
			omsResourceInst.part.entity = userProfile.getEntity();
			omsResourceInst.customerPartNumber = lineItem.customerPartNumber;
			omsResourceInst.opportunity = lineItem.opportunity;
			omsResourceInst.primary = lineItem.primary;
			omsResourceInst.resale = lineItem.resale;
			omsResourceInst.yr1Qty = lineItem.yr1Qty;
			omsResourceInst.yr2Qty = lineItem.yr2Qty;
			omsResourceInst.yr3Qty = lineItem.yr3Qty;
			omsResourceInst.phase = lineItem.phase;
			omsResourceInst.sampled = lineItem.sampled;
			omsResourceInst.notes = lineItem.notes;
			omsResourceInst.description = lineItem.description;
			omsResourceInst.lost = lineItem.lost;
			//omsResourceInst.detailStatus = {statusId: 1, statusName: "In Process", orderBy: 1};

			return omsResourceInst.$addLineItem({ planId: planId, projectId: projectId}).then(
				function (response) {
					$rootScope.$emit('refreshLineItems');
				},
				function (error) {
					utilService.getLogger().error('OmsService: addLineItem() error...');
					throw error;
				}
			)
		}

		this.updateLineItem = function(planId, type, projectId, lineItem) {
			var omsResourceInst = new omsResource.oms();

			omsResourceInst.userName = userProfile.getSAMAccountName();
			omsResourceInst.detailId = lineItem.detailId;
			omsResourceInst.designType = lineItem.designType;
			omsResourceInst.part = lineItem.part;
			omsResourceInst.part.entity = userProfile.getEntity();
			omsResourceInst.customerPartNumber = lineItem.customerPartNumber;
			omsResourceInst.opportunity = lineItem.opportunity;
			omsResourceInst.primary = lineItem.primary;
			omsResourceInst.resale = lineItem.resale;
			omsResourceInst.yr1Qty = lineItem.yr1Qty;
			omsResourceInst.yr2Qty = lineItem.yr2Qty;
			omsResourceInst.yr3Qty = lineItem.yr3Qty;
			omsResourceInst.phase = lineItem.phase;
			omsResourceInst.sampled = lineItem.sampled;
			omsResourceInst.notes = lineItem.notes;
			omsResourceInst.description = lineItem.description;
			omsResourceInst.lost = lineItem.lost;

			return omsResourceInst.$updateLineItem({ planId: planId, projectId: projectId, detailId: lineItem.detailId }).then(
				function (response) {
					
				},
				function (error) {
					utilService.getLogger().error('OmsService: updateLineItem() error...');
					throw error;
				}
			)
		}
		
		this.removeLineItem = function(planId, type, projectId, lineItem) {
			var omsResourceInst = new omsResource.oms();
			
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$removeLineItem({ planId: planId, projectId: projectId, detailId: lineItem.detailId }).then(
				function (response) {
					
				},
				function (error) {
					utilService.getLogger().error('OmsService: removeLineItem() error...');
					throw error;
				}
			)
		}
		
		
		this.addProject = function(planId, project) {
			sessionStorage.removeItem('omsProjects');

			var omsResourceInst = new omsResource.oms();

			omsResourceInst.projectName = project.projectName;
			omsResourceInst.masterProjectId =  project.masterProjectId;
			omsResourceInst.application = project.application;
			omsResourceInst.eau = project.eau;
			omsResourceInst.notes = project.notes;
			omsResourceInst.estimatedPrototypeDate = Date.parse(project.estimatedPrototypeDate);
			omsResourceInst.estimatedProductionDate = Date.parse(project.estimatedProductionDate);
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$add({ planId: planId}).then(
				function (response) {
					return response;
				},
				function (error) {
					utilService.getLogger().error('OmsService: addProject() error...');
					throw error;
				}
			)
		}
		
		
		this.updateProject = function(planId, type, project) {
			sessionStorage.removeItem('omsProjects');

			var omsResourceInst = new omsResource.oms();

			omsResourceInst.projectId = project.projectId;
			omsResourceInst.masterProjectId =  project.masterProjectId;
			omsResourceInst.projectName = project.projectName;
			omsResourceInst.application = project.application;
			omsResourceInst.eau = project.eau;
			omsResourceInst.notes = project.notes;
			omsResourceInst.estimatedPrototypeDate = Date.parse(project.estimatedPrototypeDate);
			omsResourceInst.estimatedProductionDate = Date.parse(project.estimatedProductionDate);
			omsResourceInst.headerStatus = project.headerStatus;

			return omsResourceInst.$update({ planId: planId, type: type, projectId: project.projectId }).then(
				function (response) {

				},
				function (error) {
					utilService.getLogger().error('OmsService: updateProject() error...');
					throw error;
				}
			)
		}
		
		this.removeProject = function(planId, projectId, status) {
			sessionStorage.removeItem('omsProjects');
			
			var omsResourceInst = new omsResource.oms();
			
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$removeProject({ planId: planId, projectId: projectId }).then(
				function (response) {
					for (var i = 0; i < (projects[status]).length; i++) {
            			if (projects[status][i].projectId === projectId) {
            		        projects[status].splice(i, 1);
            		    }
            		}
				},
				function (error) {
					utilService.getLogger().error('OmsService: removeProject() error...');
					throw error;
				}
			)
		}
		
		this.getBusinessPartners = function(planId, projectId) {

			return omsResource.oms.queryBusinessPartners({planId: planId, projectId: projectId}).$promise.then(
					function(response) {
						angular.forEach(response.response.omsBusinessPartners, function(partner){
							if(partner.partnerPlan) {
								partner.customerName = partner.partnerPlan.customerName;
							}
						})
						
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('OmsService: retrieveBusinessPartners() error...');
						throw error;
					}
				)	
		}
		
		this.addBusinessPartner = function(planId, projectId, partnerPlanId) {
			var omsResourceInst = new omsResource.oms();
			
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$addBusinessPartner({ planId: planId, projectId: projectId, partnerPlanId: partnerPlanId }).then(
				function (response) {
					angular.forEach(response.response, function(partner){
						if(partner.partnerPlan) {
							partner.customerName = partner.partnerPlan.customerName;
						}
					})
					return response.response;
				},
				function (error) {
					utilService.getLogger().error('OmsService: addBusinessPartner() error...');
					throw error;
				}
			)
		}

		this.removeBusinessPartner = function(planId, projectId, projectCustomerId) {
			var omsResourceInst = new omsResource.oms();
			
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$removeBusinessPartner({ planId: planId, projectId: projectId, projectCustomerId: projectCustomerId }).then(
				function (response) {
					angular.forEach(response.response, function(partner){
						if(partner.partnerPlan) {
							partner.customerName = partner.partnerPlan.customerName;
						}
					})
					return response.response;
				},
				function (error) {
					utilService.getLogger().error('OmsService: removeBusinessPartner() error...');
					throw error;
				}
			)
		}
		
		this.attachPlanToPartner = function(planId, projectId, projectCustomerId, customerPlanId) {
			var omsResourceInst = new omsResource.oms();
			
			omsResourceInst.userName = userProfile.getSAMAccountName();
			
			return omsResourceInst.$attachPlanToPartner({ planId: planId, projectId: projectId, projectCustomerId: projectCustomerId, customerPlanId: customerPlanId}).then(
				function (response) {
					angular.forEach(response.response, function(partner){
						if(partner.customerPlanId) {
							partner.customerName = partner.partnerPlan.customerName;
						}
					})
					return response.response;
				},
				function (error) {
					utilService.getLogger().error('OmsService: attachPlanToPartner() error...');
					throw error;
				}
			)
		}
		
		this.getOmsSummary = function() {
			
			var username = userProfile.getSAMAccountName();
			
			return omsResource.oms.getOmsSummary({ username: username }).$promise.then(
					function(response) {
						return response;
					},
					function(error) {
						utilService.getLogger().error('OmsService: getOmsSummary() error...');
						throw error;
					}
				)
		}
		
		this.getProjectOwnersByBranch = function(branch) {
			return omsResource.oms.getProjectOwnersByBranch({ branch: branch }).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('OmsService: getProjectOwnersByBranch() error...');
						throw error;
					}
				)
		}
		
	}

export default OmsService;