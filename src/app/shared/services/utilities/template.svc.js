
	
	TemplateService.$inject = ['$log'];
	
	function TemplateService(
		$log) {
		
		var pageHeaderTemplate = 'kamApp/shared/templates/pageHeader.html';
		var clientErrorTemplate = 'kamApp/shared/templates/clientError.html';
		var clientErrorModalTemplate = 'kamApp/shared/templates/clientError_modal.html';
		var oopsTemplate = 'kamApp/shared/templates/oops.html';
		var oopsModalTemplate = 'kamApp/shared/templates/oops_modal.html';
		var notFoundTemplate = 'kamApp/shared/templates/notfound.html';
		
		function getPageHeaderTemplate() {
			return pageHeaderTemplate;
		}
		
		function getClientErrorTemplate() {
			return clientErrorTemplate;
		}
		
		function getClientErrorModalTemplate() {
			return clientErrorModalTemplate;
		}
		
		function getOopsTemplate() {
			return oopsTemplate;
		}
		
		function getOopsModalTemplate() {
			return oopsModalTemplate;
		}
		
		function getNotFoundTemplate() {
			return notFoundTemplate;
		}
		
		function getAll() {
			return {
				pageHeaderTemplate : this.getPageHeaderTemplate(),
				clientErrorTemplate : this.getClientErrorTemplate(),
				clientErrorModalTemplate : this.getClientErrorModalTemplate(),
				oopsTemplate : this.getOopsTemplate(),
				oopsModalTemplate : this.getOopsModalTemplate(),
				notFoundTemplate : this.getNotFoundTemplate()
			}
		}
		
		return {
			getPageHeaderTemplate : getPageHeaderTemplate,
			getClientErrorTemplate : getClientErrorTemplate,
			getClientErrorModalTemplate : getClientErrorModalTemplate,
			getOopsTemplate : getOopsTemplate,
			getOopsModalTemplate : getOopsModalTemplate,
			getNotFoundTemplate : getNotFoundTemplate,
			getAll : getAll
		}
		
	}

export default TemplateService;