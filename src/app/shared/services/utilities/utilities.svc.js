
	
	utilService.$inject = [
		'$log',
		'$rootScope',
		'$filter',
		'$location',
		'templateService',
		'$uibModal'];
	
	function utilService(
			$log, 
			$rootScope,
			$filter,
			$location,
			templateService,
			$uibModal) {
		
		var eventName = 'displayPlanOptions';
		
		var mainPageEventName = 'refreshMainPageBlog';		
		
		var delayMilliseconds = 500;  
		
		// For percent label upper/lower bound		
		var successLowerBound =      100;
		var successUpperBound =  1000000;
		
		this.getLogger = function() {
			return $log;
		}
		
		this.getTemplates = function() {
			return templateService;
		}		

		this.percentToPlanLabel = function(percentNo) {
			
			var lbl = 'label label-';
			var defaultPercentLabel = lbl + 'danger';
			
			if (percentNo === null || percentNo === undefined) {
				lbl = defaultPercentLabel;
			} else {
				
				if (percentNo >= 100) {
					lbl += 'success';							
				} else { // everything other than success 
					lbl = defaultPercentLabel;
				}
			}
			return lbl;
		}
		
		this.updateSideBarOptions = function(plan, isOwnerType) {
			var sideBarOptions = {};

			if (plan !== undefined && isOwnerType !== undefined) {
				sideBarOptions.data = plan;
				
				var blogEnabled = false;
				
				switch(isOwnerType.toLowerCase()) {
				case 'owner':
				case 'admin':
					blogEnabled = true;
					break;
				default:
					blogEnabled = false;
				}
				
				sideBarOptions.blogEnable = blogEnabled;
			}
			
			$rootScope.$emit(eventName, sideBarOptions);
		}
		
		this.displayPlanOptionsEventName = function() {
			return eventName;
		}		
		
		this.isNotEmpty = function(value) {
			return value !== undefined && value !== null ? true : false;
		}
		
		this.objectHasKeys = function(value) {
			return Object.keys(value).length === 0 ? false : true;
		}
		
		this.isEmpty = function(value) {						
			return value == undefined || value == null || value.length == 0 ? true : false;
		}
		
		this.isNotEmptyArray = function(value) {
			return value !== undefined && value !== null  && value.length != 0 ? true : false;
		}
		
		this.isEmptyArray = function(value) {
			return value === undefined || value === null || value.length == 0 ? true : false;
		}
		
		this.getTodayISODate = function() {
			var dateValue = new Date().toISOString();
			// get yyyy-mm-dd
			dateValue = dateValue.substring(0, 10);
			return dateValue;
		}
		
		this.convertToISODate = function(date) {
			var dateValue = new Date(date).toISOString();
			// get yyyy-mm-dd
			dateValue = dateValue.substring(0, 10);
			return dateValue;
		}
		
		// input yyyy-mm-date, output mm/date/yyyy 
		this.isoDateToShortDate = function(dateValue, convertToISO) {
			
			var newDateValue = dateValue;
			
			if (convertToISO != undefined && convertToISO === true) {
				newDateValue = this.convertToISODate(dateValue);
			}
			
			if (this.isEmpty(dateValue)) {
				newDateValue = this.getTodayISODate();				
			}
			
			var fields = newDateValue.split('-');
			return fields[1] + '/' + fields[2] + '/' + fields[0];
		}
		
		// input mm/date/yyyy, output yyyy-mm-date  
		this.shortDateToIsoDate = function(dateValue) {
			if (isEmpty(dateValue)) {
				var dateValue = isoDateToShortDate(null);
			}
			var dateFields = dateValue.split('/');
			return dateFields[2] + '-' + dateFields[0] + '-' + dateFields[1];
		}	
		
		this.getClientErrorObject = function() {
			return {
				message: undefined,
				page: this.getTemplates().getOopsTemplate(),
				modal: this.getTemplates().getOopsModalTemplate()
			}
		}

		this.getDelayMilliseconds = function() {
			return delayMilliseconds;
		}				
		
		this.equalsIgnoreCaseSpace = function(value1, value2) {
			
			var value1 = value1 ? value1 : '';
			value1 = value1.trim();
			value1 = value1.toUpperCase();
			
			var value2 = value2 ? value2 : '';
			value2 = value2.trim();
			value2 = value2.toUpperCase();

			if (value1 === value2) {
				return true;
			}
			
			return false;
		}
		
		this.orderBy = function(listToSort, sortByColumn, isReverse) {
			var sortedList;
			
			if (isReverse == undefined) {
				sortedList = $filter('orderBy')(listToSort, sortByColumn);
			} else {
				sortedList = $filter('orderBy')(listToSort, sortByColumn, isReverse);
			}			
			
			return sortedList;
		}
		
		this.deepCopy = function(object) {
			return JSON.parse(JSON.stringify(object));
		}
		
		this.filterBy = function(object, filterBy, comparator) {
			var filtered = [];
			
			if (!comparator) {
				filtered = $filter('filter')(object, filterBy);
			} else {
				filtered = $filter('filter')(object, filterBy, comparator);
			}
						
			return filtered;
		}
		
		this.location = {
			path: function(uri) {
				$location.path(uri);
			},
			host: function() {
				return $location.$$host;
			},
			api: function() {
				return $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + '/kam/api/';
			},
			base: function() {
				return $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + '/';
			}
		}
		
		this.uibModal = function() {
			return $uibModal;
		}
		
		this.deleteConfirmation = function(headerText, bodyText, okButtonColor, isConstruction) {
			
			return this.uibModal().open({
				component: 'deleteConfirmModal',
				resolve: {
					data: function() {
						return {
							headerText: headerText,
							bodyText: bodyText,
							okButtonColor: okButtonColor,
							isConstruction: isConstruction
						}
					}
				}
				}
			);
		}
	}

export default utilService;