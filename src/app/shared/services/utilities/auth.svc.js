
	
	authService.$inject = [
		'utilService',
		'userProfile',
		'userInfoService',
		'$q'
		];
	
	function authService(
			utilService,
			userProfile,
			userInfoService,
			$q
			) {
		
		this.promise = undefined;
		
		this.isAdmin = function() {
			var deferred = $q.defer();
			setTimeout(function () { 
				deferred.resolve(userProfile.isAdmin());
			}, 200);
			return deferred.promise;
		}
		
		this.allowAdminConsole = function() {
			var deferred = $q.defer();
			setTimeout(function () { 
				deferred.resolve(userProfile.isAdmin() || (userProfile.getUserType() === 'rbom'));
			}, 200);
			return deferred.promise;
		}
		
	}

export default authService;