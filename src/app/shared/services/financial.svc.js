
	
	FinancialService.$inject = [		
		'financialResource',
		'mfrResource',
		'commodityResource',
		'utilService',
		'userProfile'
	];
	
	function FinancialService(
			financialResource,
			mfrResource,
			commodityResource,
			utilService,
			userProfile) {
		
		var entities;
		var locations;
		var mfrs;
		var commodities
		
		function getEntities() {
			return entities;
		}
		
		function setEntities(value) {
			entities = value;
		}
		
		function getLocations() {
			return locations;
		}
		
		function setLocations(value) {
			locations = value;
		}
		
		function getMfrs() {
			return mfrs;
		}
		
		function setMfrs(value) {
			mfrs = value;
		}
		
		function getCommodities() {
			return commodities;
		}
		
		function setCommodities(value) {
			commodities = value;
		}
		
		function getEntityFinancials(planId) {
			return financialResource.financials.query({planId:planId, type: 'REGION'}).$promise.then(
				function (response) {						
					entities = response.response;					
										
					return entities;
				},
				function (error) {
					utilService.getLogger().error('financialService: getEntityFinancials() Error' );
					throw error;
				}
			)
		}
				
		function getAccountFinancials(planId, entity) {
			return financialResource.financials.query({planId:planId, entity: entity, type: 'LOCATION'}).$promise.then(
				function (response) {
					locations = response.response;
					return locations;
				},
				function (error) {
					utilService.getLogger().error('financialService: getAccountFinancials() Error' );			
					throw error;
				}
			)
		}
		
		function getMfrFinancials(planId, entity, account) {
			return financialResource.financials.query({planId:planId, entity: entity, type: 'VENDOR', account: account}).$promise.then(
				function (response) {
					mfrs = response.response;
					return mfrs;
				},
				function (error) {
					utilService.getLogger().error('financialService: getMfrFinancials() Error' );			
					throw error;
				}
			)
		}		
		
		
		function getCommodityFinancials(planId, entity, account, mfr) {
			return financialResource.financials.query({planId:planId, entity: entity, type: 'COMMODITY', account: account, mfr: mfr}).$promise.then(
				function (response) {
					commodities = response.response;
					return commodities
				},
				function (error) {
					utilService.getLogger().error('financialService: retrieve Commodities Error' );			
					throw error;
				}
			)
		}
		
		
		function updateDsam(dsamObject) {
			 var dsamInst = new financialResource.dsam;
			 
			 dsamInst.dsam = dsamObject.dsam;
			 dsamInst.planId = dsamObject.planId;
			 dsamInst.entity = dsamObject.entity;
			 dsamInst.account = dsamObject.account;
			 dsamInst.manufacturer = dsamObject.manufacturer;
			 dsamInst.commodity = dsamObject.commodity;
			 dsamInst.modifiedBy = userProfile.getSAMAccountName();
			 debugger;
			 return dsamInst.$update();
		}
		
		
		function exportSites(planId) {
			var financialResourceInst =  new financialResource.financials;
			
			return financialResourceInst.$export({planId: planId, fileName: 'Financials', userName: userProfile.getFullName()}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		
		return {
			getEntityFinancials : getEntityFinancials,
			getAccountFinancials : getAccountFinancials,
			getMfrFinancials : getMfrFinancials,
			getCommodityFinancials : getCommodityFinancials,
			updateDsam : updateDsam,
			exportSites : exportSites
		}
	}

export default FinancialService;