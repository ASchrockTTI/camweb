
	
	AccountService.$inject = [
		'accountResource',
		'utilService'
	];
	
	function AccountService(
		accountResource,
		utilService) {
		
		this.findAccountsByCorpId = function(planId, corpId, account) {
			return accountResource.accounts.findAccountsByCorpId({corpId: corpId, account: account}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('Account Service: findAccountsByCorpId error...');
					throw error;
				});
		}
		
		this.findAccountCorpId = function(acctCorpId, acctType) {
			return accountResource.accounts.findAccountCorpId({acctCorpId: acctCorpId, acctType: acctType}).$promise.then(
				function(response) {
				
					var list = response.response;
					
					for(var i = 0; i < list.length; i++) {					
						list[i].display = (list[i].account === null ? list[i].corpId.trim() : list[i].account.trim()) + ' - ' + list[i].customerName + ' - ' + '(' + list[i].entity + ')';					
					}
					
					return list;
				},
				function(error) {
					utilService.getLogger().error('Account Service: findAccountCorpId error...');
					throw error;
				});
		}
		
		this.findPlans = function(value, activePlansOpt) {
			var activePlansValue = activePlansOpt !== undefined ? activePlansOpt : true;
			
			return accountResource.accounts.findPlans({search: value, activePlans: true}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('Account Service: findPlans error...');
					throw error;
				});
		}
		
		this.getAccountsByFullName = function(fullName) {
			return accountResource.accounts.getAccountsByFullName({fullName: fullName}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('Account Service: getAccountsByUser error...');
					throw error;
				});
		}
	}

export default AccountService;