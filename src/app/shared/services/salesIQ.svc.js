
	
	salesIQService.$inject = [
		'salesIQResource',
		'utilService',
		'userProfile'];
	
	function salesIQService(
			salesIQResource, 
			utilService, 
			userProfile) {
		
		
		function getOpportunities(planId) {
			return salesIQResource.salesIQ.getOpportunitiesByPlanId({planId:planId}).$promise.then(
				function(response) {					
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('salesIQService: getOpportunities Error');
					throw error;
				}
			)
		}
		
		function getOpportunityActionReasons() {
			return salesIQResource.salesIQ.getOpportunityActionReasons().$promise.then(
				function(response) {					
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('salesIQService: getOpportunityActionReasons Error');
					throw error;
				}
			)
		}

		function getVisualization(planId, customerInsightKey) {
			return salesIQResource.salesIQ.getVisualization({planId:planId,customerInsightKey:customerInsightKey}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('salesIQService: getVisualization Error');
					throw error;
				}
			)
		}
		
		function getCustomerInsightSummariesByCriteria(criteria) {
			var salesIQResourceInst = new salesIQResource.customerInsight();
			salesIQResourceInst.criteria = {
				branches: criteria.branches,
				statuses: criteria.statuses,
				email: criteria.email
			};
			if(criteria.corpId) {
				salesIQResourceInst.criteria.corpIdList = [criteria.corpId.corpId];
			}
			if(criteria.showMine) {
				salesIQResourceInst.criteria.email = userProfile.getMail();
			}
			
			return salesIQResourceInst.$queryByCriteria().then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('SalesIQService: getCustomerInsightsByCriteria() error...');
					throw error;
				}
			)	
		}
		
		function getOpportunityOwnersByBranches(branches) {
			var salesIQResourceInst = new salesIQResource.customerInsight();
			salesIQResourceInst.criteria = {
				branches: branches,
			};
			
			return salesIQResourceInst.$queryOwnersByCriteria().then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('SalesIQService: getOpportunityOwnersByBranches() error...');
					throw error;
				}
			)	
		}
		
		function getSalesIQDashboardInfo() {
			var username = userProfile.getSAMAccountName()
			
			return salesIQResource.salesIQ.getDashboardInfo({username: username}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('SalesIQService: getSalesIQDashboardInfo() error...');
					throw error;
				}
			)
		}
		
		function actionOpportunity(opportunity) {
			var salesIQResourceInst = new salesIQResource.salesIQ;
			salesIQResourceInst.activeOpportunitiyID = opportunity.activeOpportunitiyID;
			salesIQResourceInst.customerInsightKey = opportunity.salesIQResourceInst;
			salesIQResourceInst.amount = opportunity.amount;
			salesIQResourceInst.username = opportunity.username;
			salesIQResourceInst.planId = opportunity.planId;
			salesIQResourceInst.accountNumber = opportunity.accountNumber;
			salesIQResourceInst.clusterSegmentId = opportunity.clusterSegmentId;
			salesIQResourceInst.productGroupingKey = opportunity.productGroupingKey;
			salesIQResourceInst.productGroupingInsightState = opportunity.productGroupingInsightState;
			salesIQResourceInst.productGroupingInsightCreatedDate = opportunity.productGroupingInsightCreatedDate;
			salesIQResourceInst.productGroupingInsightLastModifiedDate = opportunity.productGroupingInsightLastModifiedDate;
			salesIQResourceInst.productGroupingOpportunityType = opportunity.productGroupingOpportunityType;
			salesIQResourceInst.productGroupingIdValue = opportunity.productGroupingIdValue;
			salesIQResourceInst.customerInsightKey = opportunity.customerInsightKey;
			salesIQResourceInst.customerInsightVersionKey = opportunity.customerInsightVersionKey;
			salesIQResourceInst.statusSummary = opportunity.statusSummary;
			
			salesIQResourceInst.opportunityActionReason = opportunity.selectedOpportunityActionReason;
			salesIQResourceInst.opportunityActionComments = opportunity.opportunityActionComments;
			salesIQResourceInst.productGroupingInsightActionState = opportunity.productGroupingInsightActionState;
			salesIQResourceInst.createdBy = opportunity.createdBy;
			salesIQResourceInst.modifiedBy = opportunity.modifiedBy;
			salesIQResourceInst.createdDateTime = opportunity.createdDateTime ;
			salesIQResourceInst.modifiedDateTime = opportunity.modifiedDateTime;

			return salesIQResourceInst.$actionOpportunity().then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}		
		
		return {
			getOpportunities : getOpportunities,
			getCustomerInsightSummariesByCriteria : getCustomerInsightSummariesByCriteria,
			getOpportunityOwnersByBranches : getOpportunityOwnersByBranches,
			getVisualization: getVisualization,
			getSalesIQDashboardInfo: getSalesIQDashboardInfo,
			actionOpportunity: actionOpportunity,
			getOpportunityActionReasons: getOpportunityActionReasons
		}
	}

export default salesIQService;