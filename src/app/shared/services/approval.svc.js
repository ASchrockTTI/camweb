
	
	ApprovalService.$inject = [
		'utilService',
		'planResource'
	];
	
	function ApprovalService(
			utilService,
			planResource) {
		
		utilService.getLogger().info('Entering Approval Service...');		
		
		function approve(planId, userName) {
			
			var approvalResource = new planResource.plan;
			
			approvalResource.sAMAccountName = userName;
			
//			approvalResource.securityProfile = {
//					sAMAccountName: userName
//			}
			
			return approvalResource.$approval({planId: planId, approvalStatus: 'APPROVED'}).then(
				function() {
					utilService.getLogger().info('Approval Successful...');
				},
				function(error) {
					utilService.getLogger().info('approvalService: approve failed...');
					throw error;
				}
			)
		}
		
		return {
			approve : approve
		}
	}

export default ApprovalService;