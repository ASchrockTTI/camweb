
	
	StrategyService.$inject = [
		'$filter',
		'strategyResource',
		'utilService',
		'userProfile'
	]
	
	function StrategyService(
			$filter,
			strategyResource,
			utilService,
			userProfile) {
		
		var strategies = [];
		var selectedStrategy = {};
		var planId = undefined;
		var strategyIndex = undefined;
		var addStrategyAction = false;
		var strategyData = {};
		var strategy = {};
		
		function createStrategyResource() {
			var addStrategyResource = new strategyResource.strategy;
			addStrategyResource.planStrategy = getStrategy();
			addStrategyResource.securityProfile = userProfile.getUserProfile();
			addStrategyResource.auditLog = [];
			return addStrategyResource;
		}
		
		function setAuditLog(planId, action, feature) {
			
			var auditLog = [];
			
			auditLog.push({
				planId: planId,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
			
			return auditLog;
		}	

		function addStrategy(planId) {
			var addStrategyResource = createStrategyResource();
			
			addStrategyResource.auditLog = setAuditLog(planId, 'ADD', 'STRATEGY')
			
			addStrategyResource.planStrategy.planStrategyId = null;
			addStrategyResource.planStrategy.planTactics    = null; 
			return addStrategyResource.$addStrategy({planId: planId}).then(
				function(response) {
				},
				function(error) {
					utilService.getLogger().error('strategyService: addStrategy Error');
					throw error;
				});
		}
		
		function updateStrategy(planId) {
			var updateStrategy = createStrategyResource();
			updateStrategy.auditLog = setAuditLog(planId, 'UPDATE', 'STRATEGY')
			return updateStrategy.$updateStrategy({planId: planId, planStrategyId: getStrategy().planStrategyId}).then(
				function(response) {
				},
				function(error) {
					utilService.getLogger().error('strategyService: updateStrategy Error');
					throw error;
				});
		}
		
		function updateSortOrders(planId, container) {
			
			var strategyResourceInst = new strategyResource.strategy;
			
			strategyResourceInst.sortOrders = container.sortOrders;
			strategyResourceInst.securityProfile = container.securityProfile;			
			
			return strategyResourceInst.$updateSortOrders({planId: planId}).then(
				function(response) {				
				},
				function(error) {
					utilService.getLogger().error('strategyService: updateStrategySortOrders Error');
					throw error;
				});
		}

		
		function removeStrategy(planId, planStrategyId) {
			
			var strategyResourceInst = new strategyResource.strategy;
			strategyResourceInst.auditLog = setAuditLog(planId, 'DELETE', 'STRATEGY')
			
			return strategyResourceInst.$removeStrategy({planId:planId, planStrategyId: planStrategyId}).then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('strategyService: removeStrategy Error');
					throw error;
				}
			)
		}
		
		function retrieveStrategies(planId) {
			return strategyResource.strategy.query({planId: planId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('strategyService: retrieveStrategies Error');
					throw error;
				}
			)
		}
		
		function getStrategyData() {
			return strategyData;
		}
				
		function setStrategyData(data) {
			strategyData = data;;
		}
		
		function setStrategy(data) {
			strategy = data;;
		}
		
		function getStrategy() {
			return strategy;
		}
		
		function isAddStrategyAction() {
			return addStrategyAction;
		}
				
		function setAddStrategyAction(addStrategyB) {
			addStrategyAction = addStrategyB;
		}

		function getStrategyIndex() {
			return strategyIndex;
		}
				
		function setStrategyIndex(idx) {
			strategyIndex = idx;;
		}
		
		function getPlanId() {
			return planId;
		}
		
		function setPlanId(pId) {
			planId = pId;
		}
		
		function getStrategies() {
			return strategies;
		}
		
		function setStrategies(inStrategies) {
			strategies = inStrategies;
		}
		
		function findPlanStrategyIndexByPlanStrategyId(paramPlanStrategyId) {
			var foundStrategy = false;
			var pStrategyIndex = undefined;
			var strategyObject = undefined;
			
			if (utilService.isNotEmpty(paramPlanStrategyId) && 
				utilService.isNotEmptyArray(strategies) 				) {
				strategyObject = $filter('filter')(strategies, {planStrategyId: paramPlanStrategyId});
				
				if (utilService.isNotEmpty(strategyObject)) {
					var i;
					for (i = 0; i < strategies.length; i++) {
						if (strategies[i].planStrategyId ==  paramPlanStrategyId) {				
							setStrategy(strategies[i]);
							setStrategyIndex(i);
							pStrategyIndex = i;
							foundStrategy = true;
							break;
						}
					}
				}
			}
			
			return foundStrategy;
		}

		return {
			addStrategy : addStrategy,
			removeStrategy : removeStrategy,
			updateStrategy : updateStrategy,
			setStrategy : setStrategy,
			getStrategy : getStrategy,
			getStrategyData :  getStrategyData,
			setStrategyData :  setStrategyData,
			retrieveStrategies : retrieveStrategies,
			updateSortOrders : updateSortOrders,
			isAddStrategyAction : isAddStrategyAction,
			setAddStrategyAction : setAddStrategyAction,
			getStrategyIndex : getStrategyIndex,
			setStrategyIndex : setStrategyIndex,
			getPlanId : getPlanId,
			setPlanId : setPlanId,
			getStrategies : getStrategies,
			setStrategies : setStrategies,
			findPlanStrategyIndexByPlanStrategyId : findPlanStrategyIndexByPlanStrategyId
		}
	}

export default StrategyService;