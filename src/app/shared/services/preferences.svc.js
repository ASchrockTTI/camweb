
	
	PreferencesService.$inject = [
		'utilService',
		'userProfile',
		'preferencesResource',
		'$rootScope',
		'app'
	];
	
	function PreferencesService(
		utilService,
		userProfile,
		preferencesResource,
		$rootScope,
		app) {
		
		this.getContainer = function() {
			return {viewID: undefined, 
				viewName: "Default View", 
				userName: undefined,
				selected: true,
				preferenceList: [
					{
						preference:	{
							preferenceID: 1,
							preference: "Filter By Dollar"
						},						
						preferenceValue: undefined,
						userPreferenceID: undefined,
						userView: undefined
					},
					{
						preference:	{
							preferenceID: 2,
							preference: "Filter By FSR/ISR Name"
						},
						preferenceValue: undefined,
						userPreferenceID: undefined,
						userView: undefined
					},
					{
						preference:	{
							preferenceID: 3,
							preference: "Filter By Market Segment"
						},
						preferenceValue: undefined,
						userPreferenceID: undefined,
						userView: undefined
					},
					{
						preference:	{
							preferenceID: 4,
							preference: "Filter By Branch"
						},						
						preferenceValue: undefined,
						userPreferenceID: undefined,
						userView: undefined
					}
				]}
		}
		
		this.getNewPreference = function(id) {
			var preference = this.getContainer().preferenceList.filter(function(item) {
				return item.preference.preferenceID == id;
			})
			
			return preference[0];
		}
		
		this.getAll = function(userName) {
			
			return preferencesResource.preferences.query({userName: userName}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
			
		}
		
		this.save = function(container) {
			var preferencesResourceInst = new preferencesResource.preferences;
			
			preferencesResourceInst
			preferencesResourceInst.viewID = container.viewID;
			preferencesResourceInst.userName = container.userName;
			preferencesResourceInst.selected = container.selected;
			preferencesResourceInst.viewName = container.viewName;
			preferencesResourceInst.preferenceList = container.preferenceList;
						
			return preferencesResourceInst.$update().then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		this.checkLastVersion = function(userName) {
        	return preferencesResource.version.checkLastVersion({userName: userName}).$promise.then(
				function(response) {
					$rootScope.$broadcast('showReleaseNoteAlert', response.response);
				},
				function(error) {
					utilService.getLogger().error('PreferencesService: checkLastVersion() error...');
					throw error;
				}
			)	
        }
		
		this.updateLastVersion = function(version) {
			return preferencesResource.version.updateLastVersion({userName: userProfile.getSAMAccountName(), version: version}).$promise.then(
				function(response) {
					$rootScope.$broadcast('showReleaseNoteAlert', false);
				},
				function(error) {
					utilService.getLogger().error('PreferencesService: updateLastVersion() error...');
					throw error;
				}
			)
		}
		
	}

export default PreferencesService;