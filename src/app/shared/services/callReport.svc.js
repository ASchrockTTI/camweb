CallReportService.$inject = [
  "$interval",
  "$q",
  "utilService",
  "userProfile",
  "callReportResource",
  "tmwService",
];

function CallReportService(
  $interval,
  $q,
  utilService,
  userProfile,
  callReportResource,
  tmwService
) {
  var callReports = [];

  this.saveCallReport = function (planId, callReportContainer, submit) {
    var callReportResourceInst = new callReportResource.callReport();

    callReportResourceInst.callReport = callReportContainer.callReport;
    callReportResourceInst.participantContactIds =
      callReportContainer.participantContactIds;
    callReportResourceInst.participants = callReportContainer.newContacts;
    callReportResourceInst.actionItems = callReportContainer.actionItems;
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.submit = submit;
    if (callReportContainer.callReport.callReportId === undefined) {
      return callReportResourceInst.$create({ planId: planId });
    } else {
      return callReportResourceInst.$update({ planId: planId });
    }
  };

  this.retrieveCallReport = function (planId, callReportId) {
    var callReportResourceInst = new callReportResource.callReport();
    return callReportResourceInst.$query({
      planId: planId,
      callReportId: callReportId,
    });
  };

  this.retrieveCallReportDrafts = function (planId) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.fullName = userProfile.getFullName();
    return callReportResourceInst.$queryDrafts({ planId: planId });
  };

  this.retrieveSubmittedCallReports = function (planId) {
    var callReportResourceInst = new callReportResource.callReport();
    return callReportResourceInst.$querySubmitted({ planId: planId });
  };

  this.retrieveCallReportsByReporter = function (reporter, year, quarter) {
    var callReportResourceInst = new callReportResource.callReport();
    callReports = callReportResourceInst.$queryAllByReporter({
      reporter: reporter,
    });
    return callReports;
  };

  this.retrieveAllCallReportDraftsByReporterByQuarter = function (
    reporter,
    year,
    quarter
  ) {
    if (!year) {
      var currentDate = new Date();
      year = currentDate.getUTCFullYear();
      quarter = Math.floor((currentDate.getMonth() + 3) / 3);
    }

    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName = reporter;
    callReportResourceInst.year = year;
    callReportResourceInst.quarter = quarter;

    callReports = callReportResourceInst.queryAllDraftsBySearchCriteria();
    return callReports;
  };

  this.retrieveAllSubmittedCallReportsByReporterByQuarter = function (
    reporter,
    year,
    quarter
  ) {
    if (!year) {
      var currentDate = new Date();
      year = currentDate.getUTCFullYear();
      quarter = Math.floor((currentDate.getMonth() + 3) / 3);
    }

    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName = reporter;
    callReports = callReportResourceInst.$queryAllSubmittedByReporterByQuarter();
    callReportResourceInst.year = year;
    callReportResourceInst.quarter = quarter;

    return callReports;
  };

  this.retrieveAllCallReportDraftsByReporter = function (reporter) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName = reporter;
    callReports = callReportResourceInst.$queryAllDraftsByReporter();
    return callReports;
  };

  this.retrieveAllSubmittedCallReportsByReporter = function (reporter) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName = reporter;
    callReports = callReportResourceInst.$queryAllSubmittedByReporter();
    return callReports;
  };

  this.retrieveCallReports = function (planId) {
    var callReportResourceInst = new callReportResource.callReport();
    return callReportResourceInst.$queryAll({ planId: planId });
  };

  this.retrieveAllCallReportDraftsByCriteria = function (
    callReportSearchCriteriaContainer
  ) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName =
      callReportSearchCriteriaContainer.fullName;
    callReportResourceInst.customerName =
      callReportSearchCriteriaContainer.customerName;

    if (callReportSearchCriteriaContainer.showMine)
      callReportResourceInst.fullName = userProfile.getFullName();

    if (callReportSearchCriteriaContainer.selectedCorpId) {
      callReportResourceInst.selectedCorpIds = [
        callReportSearchCriteriaContainer.selectedCorpId.corpId,
      ];
    } else {
      callReportResourceInst.selectedBranches =
        callReportSearchCriteriaContainer.selectedBranches;
    }

    callReportResourceInst.planId = callReportSearchCriteriaContainer.planId;
    callReportResourceInst.startDate =
      callReportSearchCriteriaContainer.startDate;
    callReportResourceInst.endDate = callReportSearchCriteriaContainer.endDate;

    callReports = callReportResourceInst.$queryAllDraftsBySearchCriteria();
    return callReports;
  };

  this.retrieveAllSubmittedCallReportsByCriteria = function (
    callReportSearchCriteriaContainer
  ) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst.securityProfile = userProfile.getUserProfile();
    callReportResourceInst.fullName =
      callReportSearchCriteriaContainer.fullName;
    callReportResourceInst.customerName =
      callReportSearchCriteriaContainer.customerName;

    if (callReportSearchCriteriaContainer.showMine)
      callReportResourceInst.fullName = userProfile.getFullName();

    if (callReportSearchCriteriaContainer.selectedCorpId) {
      callReportResourceInst.selectedCorpIds = [
        callReportSearchCriteriaContainer.selectedCorpId.corpId,
      ];
    } else {
      callReportResourceInst.selectedBranches =
        callReportSearchCriteriaContainer.selectedBranches;
    }

    callReportResourceInst.planId = callReportSearchCriteriaContainer.planId;
    callReportResourceInst.startDate =
      callReportSearchCriteriaContainer.startDate;
    callReportResourceInst.endDate = callReportSearchCriteriaContainer.endDate;

    callReports = callReportResourceInst.$queryAllSubmittedBySearchCriteria();
    return callReports;
  };

  this.deleteCallReport = function (planId, callReportId) {
    var callReportResourceInst = new callReportResource.callReport();
    callReportResourceInst
      .$deleteCallReport({ planId: planId, callReportId: callReportId })
      .then(
        function (response) {
          return response;
        },
        function (error) {
          utilService
            .getLogger()
            .error("CallReportService: deleteCallReport() error...");
          throw error;
        }
      );
  };

  this.uploadDocument = function (planId, callReportId, document) {
    return $q(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      var method = "POST";
      var url =
        utilService.location.base() +
        "cam/api/plans/" +
        planId +
        "/call-report/" +
        callReportId +
        "/uploadDocument";
      var response = {};

      try {
        xhr.open(method, url, true);
        xhr.setRequestHeader("x-filename", document.name);
        xhr.setRequestHeader("userName", userProfile.getFullName());
        xhr.setRequestHeader("content-type", document.type);

        xhr.onreadystatechange = function () {
          //Call a function when the state changes.
          if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            response = JSON.parse(xhr.response);
            resolve(response);
          }
        };

        xhr.send(document);
      } catch (error) {
        throw error;
      }
    });
  };

  this.getCallReportDashboardInfo = function () {
    var username = userProfile.getSAMAccountName();
    return callReportResource.callReport
      .queryDashboard({ username: username })
      .$promise.then(
        function (response) {
          return response;
        },
        function (error) {
          utilService
            .getLogger()
            .error("CallReportService: getCallReportDashboardInfo() error...");
          throw error;
        }
      );
  };
}
export default CallReportService;