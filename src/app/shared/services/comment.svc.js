
	
	CommentService.$inject = [		
		'commentResource',
		'utilService',
		'userProfile'
		];
	
	function CommentService(			
			commentResource,
			utilService,
			userProfile) {
		
		var comments = [];
		var selectedComment = {};
		var commentIndex = 0;
		
		var delegateIndex = undefined;
		
		function getNewPlanTacticCommentContainer() {
			return {
				planTacticComment: {},
				securityProfile: {}
			}
		}
		
		function retrieveComments(planId, strategyId, tacticId) {
			return commentResource.comment.query({planId:planId, planStrategyId:strategyId, planTacticId:tacticId}).$promise.then(
				function(response) {
					
					var comments = response.response;
					
					for (var i = 0; i < comments.length; i++) {
						comments[i].commentDateTime = utilService.isoDateToShortDate(comments[i].commentDateTime,true);
						comments[i].modifyDateTime = utilService.isoDateToShortDate(comments[i].modifyDateTime,true);
					}
					
					return comments;
				},
				function(error) {
					utilService.getLogger().error('commentService: retrievingComments Error.');
					throw error;
				}
			)
		}
		
		function setAuditLog(planId, action, feature) {
			
			var auditLog = [];
			
			auditLog.push({
				planId: planId,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
			
			return auditLog;
		}
		
		function addComment(planId, strategyId, tacticId, container) {
			
			var commentResourceInst = new commentResource.comment;
			
			commentResourceInst.planTacticComment = container.planTacticComment;
			commentResourceInst.securityProfile = container.securityProfile;
			commentResourceInst.auditLog = setAuditLog(planId, 'ADD', 'COMMENT');
			
			return commentResourceInst.$add({planId:planId, planStrategyId:strategyId, planTacticId:tacticId})
			.then(function() {
				return;
			},
			function(error) {
				utilService.getLogger().error('CommentService: addComment error...');
				throw error;
			});
		}
		
		function deleteComment(planId, strategyId, tacticId, commentId) {
			var commentResourceInst = new commentResource.comment;
			commentResourceInst.auditLog = setAuditLog(planId, 'DELETE', 'COMMENT');
			
			return commentResourceInst.$delete({planId:planId, planStrategyId:strategyId, planTacticId:tacticId, planTacticCommentId: commentId})
			.then(function() {
				return;
			},
			function(error) {
				utilService.getLogger().error('CommentService: deleteComment error...');
				throw error;
			});
		}
		
		function updateComment(planId, strategyId, tacticId, container) {
			
			var commentResourceInst = new commentResource.comment;
			
			commentResourceInst.planTacticComment = container.planTacticComment;
			commentResourceInst.securityProfile = container.securityProfile;
			commentResourceInst.auditLog = setAuditLog(planId, 'UPDATE', 'COMMENT');
			
			return commentResourceInst.$update({planId:planId, planStrategyId:strategyId, planTacticId:tacticId})
			.then(function() {
				return;
			},
			function(error) {
				utilService.getLogger().error('CommentService: updateComment error...');
				throw error;
			});
		}
		
		function getDelegateIndex() {
			return delegateIndex;
		}
				
		function setDelegateIndex(idx) {
			delegateIndex = idx;
		}
		
		return {
			getNewPlanTacticCommentContainer : getNewPlanTacticCommentContainer,
			addComment : addComment,
			retrieveComments : retrieveComments,
			deleteComment : deleteComment,
			updateComment : updateComment,
			getDelegateIndex : getDelegateIndex,
			setDelegateIndex : setDelegateIndex
		}
	}

export default CommentService;