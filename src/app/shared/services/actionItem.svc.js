  ActionItemService.$inject = [
    "$q",
    "utilService",
    "userProfile",
    "actionItemResource"
  ];

  function ActionItemService(
	  $q,
	  utilService,
	  userProfile,
	  actionItemResource
  ) {

      this.saveActionItem = function (actionItem) {
          var actionItemResourceInst = new actionItemResource.actionItem();

          actionItemResourceInst.actionItem = actionItem;
          actionItemResourceInst.securityProfile = userProfile.getUserProfile();
    	  return actionItemResourceInst.$updateCallReportActionItem({actionItemId: actionItem.actionItemId});
      }
      
      this.completeActionItem = function (actionItem) {
          var actionItemResourceInst = new actionItemResource.actionItem();

          actionItemResourceInst.actionItem = actionItem;
          actionItemResourceInst.actionItemType = actionItem.actionItemType;
          actionItemResourceInst.securityProfile = userProfile.getUserProfile();
    	  return actionItemResourceInst.$completeActionItem();
      }
      
			

      this.retrieveActionItemsByCriteria = function(criteria) {
    	  var actionItemResourceInst = new actionItemResource.actionItem();
    	  actionItemResourceInst.profile = userProfile.getUserProfile();
    	  actionItemResourceInst.criteria = {
    			  branchList: criteria.branches,
    			  username: criteria.username,
    			  startDate: criteria.startDate,
    			  endDate: criteria.endDate
    	  }
    	  
    	  if(criteria.corpId) actionItemResourceInst.criteria.corpIdList = [criteria.corpId.corpId];
    	  if(criteria.showMine) actionItemResourceInst.criteria.username = userProfile.getSAMAccountName();
    	  
    	  return actionItemResourceInst.$queryByCriteria().then(
  				function(response) {
						var allActionItems = response.response;

						if(allActionItems.assignedActionItems) {
							var assignedActionItems          = cleanActionItems(allActionItems.assignedActionItems);
							var assignedActionItemsFlattened = assignedActionItems.planTacticActionItems.concat(assignedActionItems.callReportActionItems);
							assignedActionItemsFlattened     = assignedActionItemsFlattened.concat(assignedActionItems.omsActionItems);
						}
						//var assignedActionItemsFlattened = [...assignedActionItems.planTacticActionItems, ...assignedActionItems.callReportActionItems, ...assignedActionItems.omsActionItems];

						if(allActionItems.myActionItems) {
							var myActionItems          = cleanActionItems(allActionItems.myActionItems);
							var myActionItemsFlattened = myActionItems.planTacticActionItems.concat(myActionItems.callReportActionItems);
							myActionItemsFlattened     = myActionItemsFlattened.concat(myActionItems.omsActionItems);
						}
						// var myActionItemsFlattened = [...myActionItems.planTacticActionItems, ...myActionItems.callReportActionItems, ...myActionItems.omsActionItems]
						
						if(allActionItems.branchActionItems) {
							var branchActionItems          = cleanActionItems(allActionItems.branchActionItems);
							var branchActionItemsFlattened = branchActionItems.planTacticActionItems.concat(branchActionItems.callReportActionItems);
							branchActionItemsFlattened     = branchActionItemsFlattened.concat(branchActionItems.omsActionItems);
						}

						var allActionItemsFlattened = {assignedActionItems: assignedActionItemsFlattened, myActionItems: myActionItemsFlattened, branchActionItems: branchActionItemsFlattened};
						console.log("Retrieved Action Items");
						return ({allActionItemsFlattened: allActionItemsFlattened, allActionItems: allActionItems});
  				},
  				function(error) {
  					utilService.getLogger().error('ActionItemService: retrieveActionItemsByCriteria() error...');
  					throw error;
  				}
  			)	
			}
			
      function cleanActionItems(actionItemArray) {
    	  if(!actionItemArray) return [];
    	  
		angular.forEach(actionItemArray.planTacticActionItems, function(actionItem, key) {
			if(actionItem.isComplete === false) {
				actionItem.completeDate = null;
			}
		});

		angular.forEach(actionItemArray.omsActionItems, function(actionItem, key) {
			if(actionItem.isComplete === false) {
				actionItem.completeDate = null;
			}
		});

		return actionItemArray;
	  }
      
      this.getActionItemCounts = function() {
    	  return actionItemResource.actionItem.queryActionItemCounts({ userName: userProfile.getSAMAccountName() }).$promise.then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('ActionItemService: retrieveActionItemCounts() error...');
					throw error;
				}
			)	
      }
      
      this.deleteCallReportActionItem = function(actionItemId) {
    	  return actionItemResource.actionItem.deleteCallReportActionItem({ actionItemId: actionItemId }).$promise.then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('ActionItemService: deleteCallReportActionItem() error...');
					throw error;
				}
			)	
      }
      
      this.deleteActionItem = function(actionItemType, actionItemId) {
    	  return actionItemResource.actionItem.deleteActionItem({actionItemType: actionItemType, actionItemId: actionItemId }).$promise.then(
  				function(response) {
  					return response;
  				},
  				function(error) {
  					utilService.getLogger().error('ActionItemService: deleteActionItem() error...');
  					throw error;
  				}
  			)	
      }
      
  }
export default ActionItemService;