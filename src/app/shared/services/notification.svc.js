
    NotificationService.$inject = [
        'utilService',
        '$rootScope',
        'notificationResource'
    ];

    function NotificationService(
        utilService,
        $rootScope,
        notificationResource) {
    	
    	var notifications = [];
        
        //initially called from header component
        this.retrieveNotifications = function(userName) {
        	return notificationResource.notifications.query({userName: userName}).$promise.then(
				function(response) {
					
					var _notifications = response.response;
					angular.forEach(_notifications, function(notification) {
						notification.params = JSON.parse(notification.params);
					})
					
					notifications = _notifications;
					$rootScope.$broadcast('notifications', notifications.length);
					
				},
				function(error) {
					utilService.getLogger().error('NotificationService: retrieveNotifications() error...');
					throw error;
				}
			)	
        }
        
        this.getNotifications = function() {
        	return notifications;
        }
        
        this.getNotificationCount = function() {
        	var count = 0;
        	angular.forEach(notifications, function(notification) {
        		if(!notification.markedRead) {
        			count++;
        		}
        	})
        	
        	return count;
        }
        
        this.updateCount = function() {
        	$rootScope.$broadcast('notifications', notifications.length);
        }
        
        
        this.readToggle = function(notification) {
        	var notificationResourceInst = new notificationResource.notifications();
        	
        	notificationResourceInst.body = "bodyRequired"
        	
        	return notificationResourceInst.$setNotificationReadStatus({notificationId: notification.notificationId, markedRead:notification.markedRead}).then(
    				function(response) {
    					
    				},
    				function(error) {
    					utilService.getLogger().error('NotificationService: setNotificationReadStatus() error...');
    					throw error;
    				}
    			)	
        }


        this.dismiss = function (notificationId) {
        	
        	deleteNotification(notificationId);
        	
        	for(var index = 0; index < notifications.length; index++) {
        		if(notifications[index].notificationId === notificationId) {
        			notifications.splice(index, 1);
        			break;
        		}
        	}
        	
            $rootScope.$broadcast('notifications', notifications.length);
        }

        this.dismissAll = function () {
        	angular.forEach(notifications, function(notification) {
        		deleteNotification(notification.notificationId);
        	})
        	
            notifications.splice(0);
            $rootScope.$broadcast("notifications", notifications.length);
        }
        
        this.dismissRead = function() {
        	for(var i = 0; i < notifications.length; i++){
        		if(notifications[i].markedRead) {
        			deleteNotification(notifications[i].notificationId);
        			notifications.splice(i, 1);
        			i--;
        		}
        	}
        	$rootScope.$broadcast("notifications", notifications.length);
        }
        
        function deleteNotification(notificationId) {
        	return notificationResource.notifications.deleteNotification({notificationId: notificationId}).$promise.then(
    				function(response) {
    					
    				},
    				function(error) {
    					utilService.getLogger().error('NotificationService: deleteNotification() error...');
    					throw error;
    				}
    			)	
        }        

    }

export default NotificationService;