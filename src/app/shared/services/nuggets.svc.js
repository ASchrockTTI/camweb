
	
	NuggetsService.$inject = [
		'nuggetsResource',
		'userProfile',
		'utilService'
	];
	
	function NuggetsService(
		nuggetsResource,
		userProfile,
		utilService
		) {
		
		var NDC = 'NDC';
		var EDC = 'EDC';

		utilService.getLogger().info('Entering: nuggetsService.svc.js');
				
		this.getNuggetCommodities = function() {
		
			return nuggetsResource.nuggets.getNuggetCommodities().$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetCommodities() error...');
						throw error;
					}
				)
		}
		
		this.getNuggetStatuses = function() {
		
			return nuggetsResource.nuggets.getNuggetStatuses().$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetStatuses() error...');
						throw error;
					}
				)
		}
		
		this.getNuggetCommentsByPlanId = function(planId) {
			return nuggetsResource.comments.getNuggetCommentsByPlanId({planId: planId}).$promise.then(
					function(response) {
						console.log('comments: ', response.response)
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetCommentsByPlanId() error...');
						throw error;
					}
				)
		}
		
		this.saveNuggetComment = function(comment, nuggetId) {
			var nuggetsResourceInst = new nuggetsResource.comments();
			nuggetsResourceInst.profile = userProfile.getUserProfile();
			nuggetsResourceInst.comment = comment;
			
			return nuggetsResourceInst.$save({nuggetId: nuggetId}).then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: saveNuggetComment() error...');
						throw error;
					}
				)	
		}
		
		this.deleteNuggetComment = function(commentId) {
			return nuggetsResource.comments.deleteComment({commentId: commentId}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: deleteComment() error...');
						throw error;
					}
				)
		}

		this.getNuggetsByPlanId = function(planId) {
		
			return nuggetsResource.nuggets.getNuggetsByPlanId({planId: planId}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetsByPlanId() error...');
						throw error;
					}
				)
		}
		
		this.getNuggetsByCriteria = function(criteria) {
			var nuggetsResourceInst = new nuggetsResource.nuggets();
			nuggetsResourceInst.criteria = {
				branches: criteria.branches,
				ownerUsername: criteria.ownerUsername,
				startDate: criteria.startDate,
				endDate: criteria.endDate
			};
			
			if(criteria.corpId) nuggetsResourceInst.criteria.corpIdList = [criteria.corpId.corpId];
			
			if(criteria.showMine) nuggetsResourceInst.criteria.ownerUsername = userProfile.getSAMAccountName();
			
			return nuggetsResourceInst.$queryByCriteria().then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetsByCriteria() error...');
						throw error;
					}
				)	
		}		

		this.getNuggetCompetitors = function() {
			
			return nuggetsResource.nuggets.getNuggetCommpetitors().$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetCompetitors() error...');
						throw error;
					}
				)
		}

		this.retrieveNuggetManufacturers = function() {
			var entity = userProfile.getUserProfile().entity;
    		if(!entity || !entity === '') return [];

			return nuggetsResource.nuggets.queryManufacturers().$promise.then(
				function(response) {
					var mfrList = response.response;

		        	var filtered = [];
		        	angular.forEach(mfrList, function(item) {
		        		if(!item.entity || !item.entity === '') return;
		        	    if( item.entity.toUpperCase().indexOf(entity.toUpperCase()) >= 0 ) filtered.push(item);
		        	});

					return filtered;
					
				},
				function(error) {
					utilService.getLogger().error('Nugget Service: retrieveNuggetManufacturers Error...');
					throw error;
				}
			)
			
		}

		this.createCompetitor = function(competitorStruct) {
				var nuggetsResourceInst = new nuggetsResource.nuggets;
				nuggetsResourceInst.createdBy = userProfile.getSAMAccountName();
				nuggetsResourceInst.competitor = competitorStruct.competitor;

				return nuggetsResourceInst.$createCompetitor().then(
					function(response) {
						return response;
					},
					function(error) {
						throw error;
					}
				)
		}

		this.createNugget = function(createNugget) {
			var nuggetsResourceInst = new nuggetsResource.nuggets;
			nuggetsResourceInst.createdBy = userProfile.getSAMAccountName();
			nuggetsResourceInst.owner = userProfile.getSAMAccountName();
			nuggetsResourceInst.ownerFullName = userProfile.getFullName();
			nuggetsResourceInst.planID = createNugget.planID;
			nuggetsResourceInst.value = createNugget.value;
			nuggetsResourceInst.manufacturer = createNugget.selectedMfr;
			nuggetsResourceInst.commodity = createNugget.selectedCommodity;
			nuggetsResourceInst.nuggetCompetitor = createNugget.selectedCompetitor.competitor;

			return nuggetsResourceInst.$createNugget().then(
				function(response) {
					return response;
				},
				function(error) {
					throw error;
				}
			)
		}

		this.addNuggetPart = function(nuggetPart) {

			console.log('nuggetPart: @NuggetService', nuggetPart);
			var nuggetsResourceInst = new nuggetsResource.nuggets();
			var userName = userProfile.getSAMAccountName();

			nuggetsResourceInst.part          = nuggetPart.part;

			return nuggetsResourceInst.$addNuggetPart({userName: userName, nuggetId: nuggetPart.nuggetID}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('NuggetsService: addNuggetPart() error...');
					throw error;
				}
			)

		}

		this.updateNuggetPart = function(nuggetPart) {

			console.log('nuggetPart: @NuggetService', nuggetPart);

			var nuggetsResourceInst = new nuggetsResource.nuggets();
			var userName = userProfile.getSAMAccountName();

			nuggetsResourceInst.part          = nuggetPart.part;
			nuggetsResourceInst.nuggetPartsID = nuggetPart.nuggetPartsID;

			return nuggetsResourceInst.$updateNuggetPart({userName: userName, nuggetId: nuggetPart.nuggetID}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('NuggetsService: updateNuggetPart() error...');
					throw error;
				}
			)

		}

		this.deleteNuggetPart = function(nuggetPart) {

			console.log('nuggetPartDelete: @NuggetService', nuggetPart);

			var userName = userProfile.getSAMAccountName();

			return nuggetsResource.nuggets.deleteNuggetPart({userName: userName, nuggetId: nuggetPart.nuggetID, nuggetPartsID: nuggetPart.nuggetPartsID}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('NuggetsService: deleteNuggetPart() error...');
					throw error;
				}
			)

		}

		this.deleteNugget = function(nuggetId) {
			console.log('nuggetDelete: @NuggetService', nuggetId);

			var userName = userProfile.getSAMAccountName();

			return nuggetsResource.nuggets.deleteNugget({userName: userName, nuggetId: nuggetId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('NuggetsService: deleteNuggetPart() error...');
					throw error;
				}
			)
		}

		this.updateNugget = function(updateNugget) {
			
			var nuggetsResourceInst = new nuggetsResource.nuggets;
			nuggetsResourceInst.createdBy = updateNugget.createdBy;
			nuggetsResourceInst.createdDateTime = updateNugget.createdDateTime;
			nuggetsResourceInst.owner = updateNugget.owner ;
			nuggetsResourceInst.ownerFullName = updateNugget.ownerFullName;
			nuggetsResourceInst.modifiedBy = userProfile.getSAMAccountName();

			nuggetsResourceInst.planID = updateNugget.planID;
			nuggetsResourceInst.value = updateNugget.value;
			nuggetsResourceInst.manufacturer = updateNugget.selectedMfr;
			nuggetsResourceInst.commodity = updateNugget.selectedCommodity;
			nuggetsResourceInst.nuggetID = updateNugget.nuggetID;
			nuggetsResourceInst.nuggetStatus = updateNugget.selectedStatus;
			
			var nuggetCompetitor = updateNugget.selectedCompetitor.competitor && updateNugget.selectedCompetitor.competitor.nuggetCompetitorID ?
					updateNugget.selectedCompetitor.competitor : updateNugget.selectedCompetitor;
			nuggetsResourceInst.nuggetCompetitor = nuggetCompetitor;

			return nuggetsResourceInst.$updateNugget({userName: updateNugget.owner}).then(
				function(response) {
					updateNugget.manufacturer = updateNugget.selectedMfr;
					updateNugget.commodity = updateNugget.selectedCommodity;
					updateNugget.nuggetStatus = updateNugget.selectedStatus;
					updateNugget.nuggetCompetitor = updateNugget.selectedCompetitor.competitor && updateNugget.selectedCompetitor.competitor.nuggetCompetitorID ?
							updateNugget.selectedCompetitor.competitor : updateNugget.selectedCompetitor;

					return updateNugget;
				},
				function(error) {
					throw error;
				}
			)
		}

		this.getNuggetById = function(nuggetId) {
			return nuggetsResource.nuggets.getNuggetById({nuggetId: nuggetId}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetCommentsByPlanId() error...');
						throw error;
					}
				)
		}

		this.getNuggetOwnersByBranches = function(branches) {
			var nuggetsResourceInst = new nuggetsResource.nuggets();
			nuggetsResourceInst.criteria = {
				branches: branches,
				ownerUsername: undefined,
				startDate: undefined,
				endDate: undefined
			};
			
			return nuggetsResourceInst.$queryOwnersByBranches().then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetOwnersByBranches() error...');
						throw error;
					}
				)	
		}

		this.getNuggetsDashboardInfo = function() {
			var userName = userProfile.getSAMAccountName();

			return nuggetsResource.nuggets.getNuggetsDashboardInfo({userName: userName}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('NuggetsService: getNuggetsDashboardInfo() error...');
						throw error;
					}
				)
		}


		this.getEntityTypeFromUserInfo = function() {
			return userProfile.getUserProfile().getEntity();
		}

		this.getEntityTypeFromNugget = function(nugget) {
			return getEntityTypeFromNugget(nugget);
		}
		
		function getEntityTypeFromNugget(nugget) {
			
			if(!nugget || !nugget.manufacturer) {
				return undefined;
			}

			var entity = nugget.manufacturer.entity;
			
			if(entity === NDC) {
				return NDC;
			}
			
			if(entity === EDC) {
				return EDC;
			}

			return undefined;
		}

		this.getEntityTypeFromNuggetList = function(nuggetList) {
			
			if(!Array.isArray(nuggetList)) {
				return undefined;
			}

			var edcExists = false;
			var ndcExists = false;
			var entity    = undefined;
			
			angular.forEach(nuggetList, function(nugget) {
				entity = getEntityTypeFromNugget(nugget);

				if(entity === NDC) {
				ndcExists = true;
				} else if(entity === EDC) {
				edcExists = true;
				} else {
				//do nothing
				}
			});


			if(ndcExists && edcExists) {
				return undefined;
			}
			if(ndcExists && !edcExists) {
				return NDC;
			}
			if(!ndcExists && edcExists) {
				return EDC;
			}
			if(!ndcExists && !edcExists) {
				return undefined;
			}

			return undefined;
		}

	}

export default NuggetsService;