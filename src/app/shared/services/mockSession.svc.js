
	
	MockSessionService.$inject = [
		'utilService',
		'userProfile',
		'$rootScope'
	];
	
	function MockSessionService(
			utilService,
			userProfile,
			$rootScope) {
		
		
		this.createSession = function(mockUser, trueMock) {
			utilService.getLogger().info('Mock Session Created...');
			userProfile.setSelectedUserTypeName(undefined);
			sessionStorage.setItem('originalProfile', userProfile.toString());
			sessionStorage.setItem('mockSession', JSON.stringify({active: true, userName: mockUser, trueMock: trueMock}));
		}
		
		this.getOriginalUser = function() {
			return JSON.parse(sessionStorage.getItem('originalProfile'));
		}
		
		this.getMockSession = function() {
			return JSON.parse(sessionStorage.getItem('mockSession'));
		}
		
		this.clearSession = function() {			
			utilService.getLogger().info('Mock Session Cleared...');
			sessionStorage.removeItem('mockSession');
			sessionStorage.removeItem('originalProfile');			
		}
		
		this.hasSession = function() {
			
			var mockSession = JSON.parse(sessionStorage.getItem('mockSession'));
			
			if (!mockSession) {
				return false;
			}
			
			return mockSession.active;
		}
		
		this.isMocking = function() {
			var mockSession = JSON.parse(sessionStorage.getItem('mockSession'));
			
			if (!mockSession) {
				return false;
			}
			
			return !mockSession.trueMock;
		}
		
	}

export default MockSessionService;