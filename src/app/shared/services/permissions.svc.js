
	
	PermissionService.$inject = ['roles', 'userProfile', 'utilService'];
	
	function PermissionService(roles, userProfile, utilService) {
		
		var _addRoles;
		var _editRoles;
		var _deleteRoles;
		
		var _permissions = [];
		
		var _plan;
					
		this.setAddRoles = function(addRoles) {
			_addRoles = addRoles;
			this.setCustomPermission('add', _addRoles, undefined);
		}
		
		this.setEditRoles = function(editRoles) {
			_editRoles = editRoles;
			this.setCustomPermission('edit', _editRoles, undefined);
		}
		
		this.setDeleteRoles = function(deleteRoles) {
			_deleteRoles = deleteRoles;
			this.setCustomPermission('delete', _deleteRoles, undefined);
		}
		
		this.setCustomPermission = function(action, roles, callBack, preventClear) {
			var permission = {};
			
			permission.action = action;
			permission.roles = roles;
			permission.callBack = callBack ? callBack : undefined;
			permission.preventClear = preventClear ? preventClear : undefined;
			
			_permissions.push(permission);
			
		}
		
		this.clearPermissions = function() {
			var permissions = _permissions.filter(function(obj) {
				return obj.preventClear === true;
			});
			
			_permissions = permissions;
		}
		
		this.hasPermission = function(action, plan) {
			
			if(_permissions.length === 0) {
				throw 'Permission Service Error: _permissions must be greater than 0.';
			}
			
			if (!plan) {
				return false;
			}
			
			_plan = plan;
			
			return _hasPermission(_permissions, action);
		}
		
		this.runCustom = function(action) {
			var allow = false;
			
			if(_permissions.length === 0) {
				throw 'Permission Service Error: _permissions must be greater than 0.';
			}
			
			angular.forEach(_permissions, function(value, key) {
				
				if (value.action === action.toLowerCase()) {
					if (value.callBack) {
						allow = value.callBack();
					}
				}
			});
			
			return allow;
		}
		
		function _hasPermission(permissions, action) {
			
			var allow = false;
			
			if (userProfile.getUserType() === undefined) {
				return false;
			}
			
			angular.forEach(permissions, function(value, key) {
				
				if (value.action === action.toLowerCase()) {
					
				}
				if (value.callBack) {
					allow = value.callBack();
				} else {
//					if (value.action === action.toLowerCase()) {
						
						for(var i = 0; i < value.roles.length; i++) {

							switch(value.roles[i]) {						
							case roles.ADMIN:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {									
									allow = true;
								}	
								break;
							case roles.RSE:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {									
									if(utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.rseParticipant)) {
										allow = true;
									}
								}	
								break;				
							case roles.EXEC:
							case roles.GM:
							case roles.ISM:
							case roles.FSM:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {								
									if (userProfile.getBranchList().length > 0) {
										if (userProfile.getBranchList().indexOf(_plan.branch) >= 0) {
											allow = true;
										}
									}
									
									if (utilService.equalsIgnoreCaseSpace(userProfile.getBranch(),_plan.branch)) {
										allow = true;
									}
								}
								break;
							case roles.OWNER:
							case roles.FSR:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.acctManager)) {
									allow = true;
									break;
								}
								
								if (_plan.insideSalesPerson != null) {
									var isrList = _plan.insideSalesPerson.substring(0, _plan.insideSalesPerson.length).toLowerCase().trim().split(' ,');									
									var found = isrList.indexOf(userProfile.getFullName().toLowerCase());
									
									if (found !== -1) {
										allow = true;
									}
									
									break;
									
								}
								break;
							default:							
								allow = false;
							}
						}					
//					}
				}
			})
						
			return allow;
		}
		
	}

export default PermissionService;