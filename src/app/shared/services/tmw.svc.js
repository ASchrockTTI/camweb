
	
	TmwService.$inject = [
		'auditFactory',
		'tmwResource',
		'utilService',
		'userProfile',
		'planService'
	];
	
	function TmwService(
		auditFactory,
		tmwResource,
		utilService,
		userProfile,
		planService
		) {
		
		utilService.getLogger().info('Entering: tmw.svc.js');
		
		var _tmwItems = [];
		var _tmwNotes = undefined;
		var auditLog = [];
		var _fsrList = [];
		var _currentQuarter = {
			quarter: undefined,
			year: undefined
		};
		var _oldestQuarter = {
			quarter: undefined,
			year: undefined
		};

		
		this.resetService = function() {
			_tmwItems = [];
			_tmwNotes = undefined;
			auditLog = [];
			_fsrList = [];
		}
		
		this.getQuarterRange = function(container) {
			var tmwResourceInst = new tmwResource.tmw();
			
			return tmwResourceInst.$getQuarterRange()
			.then(
				function (response) {
					_currentQuarter.quarter = response.response[0].quarter;
					_currentQuarter.year = response.response[0].year;
					_oldestQuarter.quarter = response.response[1].quarter;
					_oldestQuarter.year = response.response[1].year;
					return [_currentQuarter, _oldestQuarter];
				},
				function (error) {
					utilService.getLogger().error('TMW Service: getCurrentQuarter() error...');
					throw error;
				})
		}
		
		this.getTmwItems = function() {
			return _tmwItems;
		}
		
		this.getTmwNotes = function() {
			return _tmwNotes;
		}

		this.getFsrList = function() {
			return _fsrList;
		}
		
		
		this.updateYTD = function(item) {
			item.visitsYTD = item.janVisits + item.febVisits + item.marVisits;
		}
		
		this.updateTmw = function(container) {
			var tmwResourceInst = new tmwResource.tmw;
			
			for(var prop in container) {
				tmwResourceInst[prop]=container[prop];
			}
			
			return tmwResourceInst.$update();
		}

		this.updateTmwNotes = function (container) {
			var tmwResourceInst = new tmwResource.tmw;

			//setAuditLog(container.tmwNotes.acctManager, container.tmwNotes.quarter, "CREATE", "TMW_NOTES");

      		//tmwResourceInst.auditLog = auditLog;

			for (var prop in container) {
				tmwResourceInst[prop] = container[prop];
			}

			return tmwResourceInst.$updateNotes();
		}

		this.createTmwNotes = function(container) {
			var tmwResourceInst = new tmwResource.tmw;

			//setAuditLog(container.tmwNotes.acctManager, container.tmwNotes.quarter, 'CREATE', 'TMW_NOTES');

			//tmwResourceInst.auditLog = auditLog;

			for (var prop in container) {
				tmwResourceInst[prop] = container[prop];
			}

			return tmwResourceInst.$createNotes();
		};

		function setAuditLog(owner, quarter, action, feature) {

			auditLog = [];

			auditLog.push({
				tmwOwner: owner,
				tmwQuarter: quarter,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
		}

		this.retrieveFsrList = function(branch, corpId) {
			var tmwResourceInst = new tmwResource.tmw();

			return tmwResourceInst.$queryFsr({branch: branch, corpId: corpId}).then(
				function (response) {
					_fsrList = response.response.outsideSalesPersons;
				},
				function (error) {
					utilService.getLogger().error('TMW Service: retrieveFsrList() error...');
					throw error;
				})
		}
		
		
		this.retrieveTmw = function(container) {
			
			var tmwResourceInst = new tmwResource.tmw;
			
			tmwResourceInst.securityProfile = container.securityProfile;
			
//			//if current quarter selected, set quarter to null to retrieve current quarter
//			if(container.quarter === _currentQuarter.quarter) {
//				tmwResourceInst.quarter = null;
//				tmwResourceInst.year = null;
//			} else {
//				
//			}
			
			tmwResourceInst.quarter = container.quarter;
			tmwResourceInst.year = container.year;
			
			return tmwResourceInst.$query().then(
				function(response) {
					
					var tmwModel = response.response;
					var tmw = tmwModel.tmwTransformers;

					_tmwNotes = tmwModel.tmwNotes;

					_tmwItems = [];

					
					angular.forEach(tmw, function(value, key) {
						if(value.archivedRunRate != null) {
							value.totalRunRate = value.archivedRunRate;
						}
						if(value.archivedDSAM != null) {
							value.dsam = value.archivedDSAM;
						}
						if(value.dsam !== 0) {
							value.marketShare = 100 * (value.totalRunRate / value.dsam);
						}
						if(isNaN(value.marketShare)) {
							value.marketShare = 0.0;
						}
						if(value.monthOneVisits == null) {
							value.monthOneVisits = 0;
						}
						if(value.monthTwoVisits == null) {
							value.monthTwoVisits = 0;
						}
						if(value.monthThreeVisits == null) {
							value.monthThreeVisits = 0;
						}
						if(value.totalVisits == null) {
							value.totalVisits = 0;
						}
						value.insideSalesPersonList = value.insideSalesPerson.substring(0,value.insideSalesPerson.length -1).trim().split(',');
						if(value.insideSalesPersonList.length > 1) {
							value.insideSalesPerson = "Multiple ISR"
						}
						
						if(value.dregCount === null) {
							value.dregCount = 0;
						}
						
						_tmwItems.push(value);						
					})
					
					return tmw;
				},
				function(error) {
					utilService.getLogger().error('TMW Service: retrieveTmw() error...');
					throw error;
				}
			)
			
		}
	}

export default TmwService;