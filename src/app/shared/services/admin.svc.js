
	
	AdminService.$inject = [
		'$q',
		'utilService',
		'userProfile',
		'adminResource',
		'auditFactory',
	]
	
	function AdminService(
			$q,
			utilService,
			userProfile,
			adminResource,
			auditFactory) {
		
		utilService.getLogger().info('Entering Admin Service...');
		
		var users = {};
		var titles = {};
		
		users.findAll = function() {
			return adminResource.users.findAll().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		};
		
		users.updateUserTable = function() {
			return adminResource.users.updateUserTable().$promise;
		}
			
		users.add = function(container) {
			var adminResourceInst = new adminResource.users;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'ADD', 'ADMIN_USERS')
			
			adminResourceInst.user = {
				userName: container.userName,
				fullName: container.fullName,
				jobTitle: container.jobTitle,
				branches: container.branches
			}
			
			adminResourceInst.securityProfile = {
				fullName: userProfile.getFullName()
			}
			
			return adminResourceInst.$add().then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		};
			
		users.update = function(container) {
			var adminResourceInst = new adminResource.users;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'UPDATE', 'ADMIN_USERS')
			
			adminResourceInst.user = {
				userId: container.userId,
				userName: container.userName,
				fullName: container.fullName,
				jobTitle: container.jobTitle,
				branches: container.branches
			}
			
			adminResourceInst.securityProfile = {
				fullName: userProfile.getFullName()
			}
			
			return adminResourceInst.$update().then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		};
			
		users.delete = function(userId) {
			var adminResourceInst = new adminResource.users;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'DELETE', 'ADMIN_USERS')
			
			return adminResourceInst.$delete({userId: userId}).then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		};
		
		titles.findAll = function() {
			
			return adminResource.titles.findAll().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			) 
		}
		
		titles.add = function(container) {
			
			var adminResourceInst = new adminResource.titles;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'ADD', 'ADMIN_TITLES')
			
			adminResourceInst.jobTitle = {
				adTitle: container.adTitle,
				kamTitle: container.kamTitle
			}
			
			adminResourceInst.securityProfile = {
				fullName: userProfile.getFullName()
			}
			
			return adminResourceInst.$add().then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		titles.update = function(container) {
			
			var adminResourceInst = new adminResource.titles;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'UPDATE', 'ADMIN_TITLES')
			
			adminResourceInst.jobTitle = {
				jobTitleId: container.jobTitleId,
				adTitle: container.adTitle,
				kamTitle: container.kamTitle
			}
			
			adminResourceInst.securityProfile = {
				fullName: userProfile.getFullName()
			}
			
			return adminResourceInst.$update().then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		titles.delete = function(jobTitleId) {
			
			var adminResourceInst = new adminResource.titles;
			
			adminResourceInst.auditLog = auditFactory.createLog(0, 'DELETE', 'ADMIN_TITLES')
			
			return adminResourceInst.$delete({jobTitleId: jobTitleId}).then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		} 
		
		
		function uploadReleaseNote(releaseNote, fileList) {
			var adminResourceInst = new adminResource.releaseNotes;
			
			adminResourceInst.releaseNote= releaseNote;
			
			return adminResourceInst.$upload().then(
				function(response) {
					var releaseNoteId = response.response;
					
					var promises = [];
					for(var i = 0; i < fileList.length; i++) {
						promises.push(uploadReleaseNoteDocument(fileList[i], releaseNoteId));
					}
					return $q.all(promises);
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function deleteReleaseNote(releaseNoteId) {
			return adminResource.releaseNotes.delete({releaseNoteId: releaseNoteId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function uploadReleaseNoteDocument(document, releaseNoteId) {
			 return $q(function (resolve, reject) {
				var xhr = new XMLHttpRequest();
				
				var method = 'POST';
				var url = utilService.location.base() + 'cam/api/release-notes/' + releaseNoteId + '/document';
				var response = {};	
				
				
				try {
					xhr.open(method, url, true);
					xhr.setRequestHeader("x-filename", document.name);
					xhr.setRequestHeader("userName", userProfile.getFullName());
					xhr.setRequestHeader("content-type", document.type);
					
					xhr.onreadystatechange = function() {//Call a function when the state changes.
					    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {				    	
					    	response = JSON.parse(xhr.response);
					    	resolve(response);
					    }
					}
					
					xhr.send(document);
					
					
				} catch(error) {
					throw error;
				}
			});
		}
		
		
		function downloadReleaseNoteDocument(documentId) {
			return adminResource.releaseNotes.downloadDocument({documentID: documentId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function deleteReleaseNoteDocument(documentId) {
			return adminResource.releaseNotes.deleteDocument({documentID: documentId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function getAllReleaseNotes() {
			return adminResource.releaseNotes.getAll().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			) 
		}
		
		
		function getLatestReleaseNote() {
			
			return adminResource.releaseNotes.getLatest().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		var exceptions = {
			findAll: function() {
				return adminResource.exceptions.findAll().$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						throw error;
					}
				)
			},
			add: function(container) {
				
				var adminResourceInst = new adminResource.exceptions();
				
				adminResourceInst.auditLog = auditFactory.createLog(0, 'ADD', 'ADMIN_BR_EXCEPTION')
				
				adminResourceInst.exceptionValue = container.exceptionValue;
				adminResourceInst.exceptionType = container.exceptionType;
				adminResourceInst.exceptionAction = container.exceptionAction;
				adminResourceInst.exceptionColumn = container.exceptionColumn;
				
				return adminResourceInst.$add().then(
					function(response) {
						
					},
					function(error) {
						throw error;
					}
				)
			},
			delete: function(exceptionRuleId) {
				var adminResourceInst = new adminResource.exceptions();
				
				adminResourceInst.auditLog = auditFactory.createLog(0, 'DELETE', 'ADMIN_BR_EXCEPTION')
				
				return adminResourceInst.$delete({exceptionRuleId: exceptionRuleId}).then(
					function(response) {
						
					},
					function(error) {
						throw error;
					}
				)
			}
		}
		
		var plans = {
			getContainer: function() {
				return {userName: undefined, fromPlanId: undefined, toPlanId: undefined, portOptionList: []};
			},
			transfer: function(container) {
				
				var adminResourceInst = new adminResource.plans;
				
				adminResourceInst.auditLog = auditFactory.createLog(container.fromPlanId, 'UPDATE', 'ADMIN_PORT')
				
				adminResourceInst.userName = container.userName;
				adminResourceInst.fromPlanId = container.fromPlanId;
				adminResourceInst.toPlanId = container.toPlanId;
				adminResourceInst.portOptionList = container.portOptionList;											
				
				return adminResourceInst.$transfer().then(
					function(response) {
						
					},
					function(error) {
						throw error;
					}
				)
			}
		}
		
		var accounts = {
			getAccounts: function(dateToday, dateTomorrow) {
				return adminResource.accounts.query({dateToday: dateToday, dateTomorrow: dateTomorrow}).$promise.then(
					function(response) {
						
						var accounts = utilService.filterBy(response.response, {account: 'AFO001'});
						
						utilService.getLogger().info(accounts);
						
						return utilService.orderBy(response.response, ['account','planId','planTypeId']);
					},
					function(error) {
						throw error;
					}
				)
			},
			getAccountsByAccountAndEntity: function(account, entities) {
				return adminResource.accounts.query({account: account, entities: entities}).$promise.then(
						function(response) {
							
							utilService.getLogger().info(accounts);
							
							return utilService.orderBy(response.response, ['account','planId','planTypeId']);
						},
						function(error) {
							throw error;
						}
					)
			},
			getDeletedAccounts: function() {
				return adminResource.accounts.queryDeleted({}).$promise.then(
						function(response) {
							return utilService.orderBy(response.response, ['modifyDateTime']);
						},
						function(error) {
							throw error;
						}
					)
			},
			deleteAccount: function(accountId) {
				var adminResourceInst = new adminResource.accounts;
				adminResourceInst.securityProfile = userProfile.getUserProfile();
				
				return adminResourceInst.$delete({accountId: accountId}).then(
						function(response) {
							return accountId;
						},
						function(error) {
							throw error;
						}
					)
			},
			export: function(dateToday, dateTomorrow) {
				
				var adminResourceInst =  new adminResource.accounts;
				
				return adminResourceInst.$export({dateToday: dateToday, dateTomorrow: dateTomorrow, userName: userProfile.getFullName()}).then(
					function(response) {
						return response.response
					},
					function(error) {
						throw error;
					}
				)
			}
		}
		
		return {
			users: users,
			titles: titles,
			uploadReleaseNote : uploadReleaseNote,
			deleteReleaseNote : deleteReleaseNote,
			uploadReleaseNoteDocument : uploadReleaseNoteDocument,
			downloadReleaseNoteDocument: downloadReleaseNoteDocument,
			deleteReleaseNoteDocument : deleteReleaseNoteDocument,
			getAllReleaseNotes : getAllReleaseNotes,
			getLatestReleaseNote : getLatestReleaseNote,
			exceptions : exceptions,
			plans : plans,
			accounts : accounts
		}
	}

export default AdminService;