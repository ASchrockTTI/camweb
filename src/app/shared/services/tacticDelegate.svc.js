
	
	TacticDelegateService.$inject = [
		'utilService',
		'tacticDelegateResource'
	];
	
	function TacticDelegateService(
			utilService,
			tacticDelegateResource) {
		
		function getNewPlanTacticDelegateContainer() {
			return {
				planTacticDelegate: {},
				securityProfile: {}
			}
		}
		
		function remove(planId, planStrategyId, planTacticId, planTacticDelegateId) {
			
			if ( !window.confirm('Are you sure you want to delete this delegate?') ) {
				return;
			}
			
			var tacticDelegateResourceInst = new tacticDelegateResource.delegate;
			
			return tacticDelegateResourceInst.$remove({planId: planId, planStrategyId: planStrategyId, planTacticId: planTacticId, planTacticDelegateId: planTacticDelegateId})
			.then(function(response) {
				utilService.getLogger().info('TacticDelegateService: delete successful...');
				return;
			},
			function(error) {
				utilService.getLogger().error('TacticDelegateService: delete error...');
				throw error;
			});
		}
		
		function update(planId, planStrategyId, planTacticId, container) {
			
			var tacticDelegateResourceInst = new tacticDelegateResource.delegate;
			
			tacticDelegateResourceInst.securityProfile = container.securityProfile;
			tacticDelegateResourceInst.planTacticDelegate = container.planTacticDelegate;
			
			return tacticDelegateResourceInst.$update({planId: planId, planStrategyId: planStrategyId, planTacticId: planTacticId})
			.then(function(response) {
				utilService.getLogger().info('TacticDelegateService: update successful...');
				return;
			},
			function(error) {
				utilService.getLogger().error('TacticDelegateService: update error...');
				throw error;
			});
			
		}
		
		return {
			getNewPlanTacticDelegateContainer : getNewPlanTacticDelegateContainer,
			update : update,
			remove : remove
		}
		
	}

export default TacticDelegateService;