const vbsForm = {
  PRODUCTION_SUMMARY: "productionSummary",
  DESIGN_PROJECT: "designSummaryProject",
  DESIGN_PRODUCT: "designSummaryProduct",
  CUSTOMER_INFLUENCER: "influenceMap",
  CM_MATRIX: "cmMatrix",
};

VbsService.$inject = ["userProfile", "utilService", "vbsResource", "vbsForm"];

function VbsService(userProfile, utilService, vbsResource, vbsForm) {
  utilService.getLogger().info("VbsService: Entering...");

  var forms = {
    getList: function (planId) {
      return vbsResource.forms.query({ planId: planId }).$promise.then(
        function (response) {
          return response.response;
        },
        function (error) {
          utilService.getLogger().error("VbsService: vbs.getList error...");
          throw error;
        }
      );
    },
    findCM: function (value) {
      return vbsResource.forms.findCM({ search: value }).$promise.then(
        function (response) {
          return response.response;
        },
        function (error) {
          throw error;
        }
      );
    },
  };

  var project = {
    queryByStatus: function (planId, status) {
      return vbsResource.project
        .queryByStatus({ planId: planId, status: status })
        .$promise.then(
          function (response) {
            return response.response;
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: project.queryAll() error...");
            throw error;
          }
        );
    },
    save: function (project) {
      var vbsResourceInst = new vbsResource.project();
      vbsResourceInst.project = project;
      vbsResourceInst.securityProfile = userProfile.getUserProfile();

      if (project.vbsProjectId) {
        return vbsResourceInst
          .$update({ planId: project.planId, projectId: project.vbsProjectId })
          .then(
            function (response) {
              return response.response;
            },
            function (error) {
              utilService
                .getLogger()
                .error("VbsService: project update error...");
              throw error;
            }
          );
      } else if (project.migrate) {
        return vbsResourceInst.$migrate({ planId: project.planId }).then(
          function (response) {
            return response;
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: project migration error...");
            throw error;
          }
        );
      } else {
        return vbsResourceInst.$create({ planId: project.planId }).then(
          function (response) {
            return response;
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: project creation error...");
            throw error;
          }
        );
      }
    },
    delete: function (project) {
      var vbsResourceInst = new vbsResource.project();

      return vbsResourceInst
        .$delete({ planId: project.planId, projectId: project.vbsProjectId })
        .then(
          function (response) {
            return response.response;
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: project.delete() error...");
            throw error;
          }
        );
    },
  };

  var customerInfluencer = {
    roles: [
      { value: "1", name: "Materials/Buyers", data: [] },
      { value: "2", name: "Engineering/Design", data: [] },
      { value: "3", name: "Supply Chain/Inventory", data: [] },
      { value: "4", name: "Quality", data: [] },
      { value: "5", name: "Finance", data: [] },
      { value: "6", name: "OEM Outsource(CM) Mgr", data: [] },
      { value: "7", name: "CM Program Mgr", data: [] },
      { value: "8", name: "Manufacturing/Production", data: [] },
      { value: "9", name: "Executive", data: [] },
    ],
    getRole: function (roleId) {
      return customerInfluencer.roles[roleId];
    },
    clearRoleData: function () {
      for (var i = 0; i <= customerInfluencer.roles.length - 1; i++) {
        customerInfluencer.roles[i].data = [];
      }
    },
    getList: function (planId) {
      return vbsResource.customerInfluencer
        .query({ planId: planId, vbsFormTypeId: vbsForm.CUSTOMER_INFLUENCER })
        .$promise.then(
          function (response) {
            var data = response.response;

            utilService.getLogger().info(data);

            for (var i = 0; i <= data.front.length - 1; i++) {
              switch (data.front[i].vbsRole.vbsRoleID) {
                case 5:
                  customerInfluencer.roles[4].data = data.front[i];
                  break;
                case 9:
                  customerInfluencer.roles[8].data = data.front[i];
                  break;
              }
            }

            for (var i = 0; i <= data.middle.length - 1; i++) {
              switch (data.middle[i].vbsRole.vbsRoleID) {
                case 1:
                  customerInfluencer.roles[0].data = data.middle[i];
                  break;
                case 2:
                  customerInfluencer.roles[1].data = data.middle[i];
                  break;
                case 3:
                  customerInfluencer.roles[2].data = data.middle[i];
                  break;
                case 4:
                  customerInfluencer.roles[3].data = data.middle[i];
                  break;
                case 6:
                  customerInfluencer.roles[5].data = data.middle[i];
                  break;
                case 7:
                  customerInfluencer.roles[6].data = data.middle[i];
                  break;
              }
            }

            for (var i = 0; i <= data.back.length - 1; i++) {
              switch (data.back[i].vbsRole.vbsRoleID) {
                case 8:
                  customerInfluencer.roles[7].data = data.back[i];
                  break;
              }
            }

            utilService.getLogger().info(customerInfluencer.roles);
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: customerInfluencer.getList() error...");
            throw error;
          }
        );
    },
  };

  var productionSummary = {
    getContainer: function () {
      return {
        productName: undefined,
        firstProduction: undefined,
        unitProduction: undefined,
        unitForecast: undefined,
        costReduction: "",
        reDesign: "",
        contractManufacturer: undefined,
        contractManufacturerPlanID: undefined,
        modifiedBy: undefined,
        modifiedDateTime: undefined,
        selectedPlannedModification: undefined,
        plannedModicationDate: undefined,
        calendarFirstProductionOpen: false,
        calendarPlannedModificationOpen: false,
      };
    },
    getList: function (planId) {
      return vbsResource.productionSummary
        .query({ planId: planId, vbsFormTypeId: vbsForm.PRODUCTION_SUMMARY })
        .$promise.then(
          function (response) {
            return response.response;
          },
          function (error) {
            utilService
              .getLogger()
              .error("VbsService: getProductionSummary error...");
            throw error;
          }
        );
    },
    save: function (planId, container) {
      var productionSummaryResource = new vbsResource.productionSummary();

      productionSummaryResource.map = {};
      productionSummaryResource.map = container;

      utilService.getLogger().info(container);

      return productionSummaryResource.$save({ planId: planId }).then(
        function (response) {
          return response;
        },
        function (error) {
          utilService
            .getLogger()
            .info("VbsService: productionSummary.save Error...");
          throw error;
        }
      );
    },
    update: function (planId, container) {
      var productionSummaryResource = new vbsResource.productionSummary();

      switch (container.costReduction) {
        case "Yes":
          container.costReduction = true;
          break;
        case "No":
          container.costReduction = false;
          break;
        default:
          container.costReduction = undefined;
      }

      switch (container.reDesign) {
        case "Yes":
          container.reDesign = true;
          break;
        case "No":
          container.reDesign = false;
          break;
        default:
          container.reDesign = undefined;
      }

      productionSummaryResource.map = {};
      productionSummaryResource.map = container;
      return productionSummaryResource
        .$updateP({ planId: planId, vbsFormTypeId: vbsForm.PRODUCTION_SUMMARY })
        .then(
          function (response) {},
          function (error) {
            utilService
              .getLogger()
              .info("VbsService: productionSummary.update Error...");
            throw error;
          }
        );
    },
    delete: function (productionSummaryID) {
      var productionSummaryResource = new vbsResource.productionSummary();
      return productionSummaryResource
        .$delete({
          id: productionSummaryID,
          vbsFormTypeId: vbsForm.PRODUCTION_SUMMARY,
        })
        .then(
          function (response) {},
          function (error) {
            throw error;
          }
        );
    },
  };

  var cmMatrix = {
    getContainer: function () {
      return {
        matrixID: undefined,
        customer: undefined,
        locationName: undefined,
        projectName: undefined,
        estAnnualRev: undefined,
        customerContact: undefined,
        cmMgr: undefined,
        cmLocation: undefined,
        vbsID: undefined,
        modifiedBy: undefined,
        modifyDateTime: undefined,
      };
    },
    getList: function (planId) {
      return vbsResource.cmMatrix
        .query({ planId: planId, vbsFormTypeId: vbsForm.CM_MATRIX })
        .$promise.then(
          function (response) {
            return response.response;
          },
          function (error) {
            throw error;
          }
        );
    },
    save: function (planId, container) {
      var cmMatrixResource = new vbsResource.cmMatrix();

      cmMatrixResource.map = {};
      cmMatrixResource.map = container;

      return cmMatrixResource.$save({ planId: planId }).then(
        function (response) {
          return response;
        },
        function (error) {
          utilService
            .getLogger()
            .info("VbsService: cmMatrixResource.save Error...");
          throw error;
        }
      );
    },
    update: function (planId, container) {
      var cmMatrixResource = new vbsResource.cmMatrix();

      cmMatrixResource.map = {};
      cmMatrixResource.map = container;
      return cmMatrixResource
        .$updateP({ planId: planId, vbsFormTypeId: vbsForm.CM_MATRIX })
        .then(
          function (response) {},
          function (error) {
            utilService
              .getLogger()
              .info("VbsService: cmMatrixResource.update Error...");
            throw error;
          }
        );
    },
    delete: function (cmMatrixID) {
      var cmMatrixResource = new vbsResource.cmMatrix();
      return cmMatrixResource
        .$delete({ id: cmMatrixID, vbsFormTypeId: vbsForm.CM_MATRIX })
        .then(
          function (response) {},
          function (error) {
            throw error;
          }
        );
    },
  };

  return {
    forms: forms,
    project: project,
    customerInfluencer: customerInfluencer,
    productionSummary: productionSummary,
    cmMatrix: cmMatrix,
  };
}
export { vbsForm, VbsService };
