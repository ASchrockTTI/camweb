
	
	UserProfile.$inject = ['utilService', '$rootScope'];
	
	function UserProfile(utilService, $rootScope) {
						
		var givenName;
		var branchLocation;
		var sn;
		var title;
		var sAMAccountName;
		var mail;
		var distinguishedName;
		var physicalDeliveryOfficeName;
		var groups;
		var isMock;
		var entities = ["NDC", "EDC", "ADC"];
		var entity;
		var selectedEntity;
		var userType;
		var selectedUserTypeName;
		var userProfile;		
		var userTypes = ["admin","head_honcho", "executive", "delegate","owner", "none"];		
		var adminIdx = 0;
		var executiveIdx = 1;
		var delegateIdx = 2;
		var ownerIdx = 3;
		var noneIdx  = 4;
		var branchList;
		var userId;
		
		var getSelectedEntity = function() {
			return selectedEntity;
		}
		
		var setSelectedEntity = function(value) {
			selectedEntity = value;
		}
		
		var getBranchLocation = function() {
			return branchLocation;
		}
		
		var setBranchLocation = function(value) {
			branchLocation = value;
		}
		
		var getEntity = function() {
			return entity;
		}
	
		var getEntities = function() {
			return lpars;
		}
		
		var setIsMock = function(value) {
			isMock = value;
		}
		
		var getIsMock = function() {
			
			var mockSession = sessionStorage.getItem('mockSession');
			isMock = false;
			
			if (mockSession) {
				isMock = true;
				return true;
			}
						
			return false;
		}
		
		var getMockUser = function() {
			
			if (isMock) {
				return JSON.parse(sessionStorage.getItem('originalProfile'));
			} 
			
			return null;
		}
		
		var getGivenName = function() {
			return givenName;
		}
		
		var getSn = function() {
			return sn;
		}
		
		var getTitle = function() {
			return title
		}
		
		var getSAMAccountName = function() {
			return sAMAccountName;
		}
		
		var getMail = function() {
			return mail;
		}
		
		var getDistinguishedName = function() {
			return distinguishedName;
		}
		
		var getPhysicalDeliveryOfficeName = function() {
			return physicalDeliveryOfficeName;
		}
		
		var getBranch = function() {
			if (physicalDeliveryOfficeName !== undefined && physicalDeliveryOfficeName !== null) {
				return physicalDeliveryOfficeName.slice(0,2);
			}			
		}
		
		var getFullName = function() { 
			return givenName + ' ' + sn;
		}
		
		var getGroups = function() {
			return groups;
		}
		
		var getUserType = function() {
			
			switch (userType) {
				case 'BPM':
					var type = 'FSM';
					break;
				case 'CPM':
					var type = 'EXECUTIVE';
					break;
				case 'SUPPORT':
					var type = 'ADMIN';
					break;
				case 'FAE':
				case 'SCM':
				case 'BDM':
				case 'PDM':
					var type = 'RSE';
					break;
				case 'RVP':
					var type = 'GM';
					break;
				default:
					var type = userType;
			}

			if (userType !== undefined) {
				return type.toLowerCase();
			}
			
			return userType;			
		}
		
		var getUserTypeName = function() {
			return userType;
		}
		
		var setUserType = function(selectedUserType) {
			userType = selectedUserType;
		}
		
		var getSelectedUserTypeName = function() {
			return selectedUserTypeName;
		}
		
		var setSelectedUserTypeName = function(selectedTypeName) {
			selectedUserTypeName = selectedTypeName;
		}
		
		var getBranchList = function() {
			if(!branchList) return [];
			return branchList.sort(function(a, b){
			    if(a < b) { return -1; }
			    if(a > b) { return 1; }
			    return 0;
			});
		}
		
		var getUserId = function() {
			return userId;
		}
		
		var setUserProfile = function(value) {
			givenName = value.givenName;
			sn = value.sn;
			sAMAccountName = value.sAMAccountName;
			branchLocation = value.branchLocation;
			mail = value.mail;
			title = value.title;
			distinguishedName = value.distinguishedName;
			physicalDeliveryOfficeName = value.physicalDeliveryOfficeName;
			groups = value.groups;
			entity = value.entity;
			selectedEntity = value.selectedEntity;
			selectedUserTypeName = givenName + ' ' + sn;
			userType = utilService.isEmpty(value.userType) ? '' : value.userType;
			branchList = value.branchList;
			userId = value.userId;
			userProfile = value;
			
			$rootScope.$broadcast('userProfileAvailable');
		}
		
		var isAdmin = function() {
			return hasGroup('APPS_TTI_TX_KAM Admins') || userTypes[0] === getUserType();			
		}
		
		function hasGroup(name) {
			if (groups != null && groups.indexOf(name) !== -1) {
				return true;
			}
			
			return false;
		}
		
		function hasBranch(branch) {
			if(getBranch() === branch) return true;
			var branchList = getBranchList();
			if(branchList.length > 0) {
				return branchList.indexOf(branch) > -1
			}
			return false;
		}
		
		function getUserProfile() {
			return userProfile;
		}
		
		// Return true if user type is equal to none
		function isPlanOwnerType(type) {
			var retBool = false;
			if ( utilService.isNotEmpty(type) && 
				 type === userTypes[noneIdx] ) {
				
				retBool = true;
			}
			
			return retBool;
		}
		
		function canEdit() {
			
			switch(userType) {
			case 'owner':
			case 'executive':
				return true;
			default:
				return false;
			}
		}
		
		function isBeta() {
			var betaList = ['AL','FW','AU','AZ','DE','EP','KC','HU','MX'];
			return hasBetaBranch(betaList);
		}
		
		function hasBetaBranch(betaBranchList) {
			if(betaBranchList.indexOf(getBranch()) !== -1) return true;
			var branchList = getBranchList();
			
			branchList = branchList.filter(function(branch) {
			    return betaBranchList.indexOf(branch) !== -1;
			});
			if(branchList.length > 0) return true;
			return false;
		}
		
		var toString = function() {
			return '{'
			       + '"givenName" : "' + this.getGivenName() + '",'			       
			       + '"sn" : "' + this.getSn() + '",'
			       + '"title" : "' + this.getTitle() + '",'
			       + '"sAMAccountName" : "' + this.getSAMAccountName() + '",'
			       + '"mail" : "' + this.getMail() + '",'			       
			       + '"branch" : "' + this.getBranch() + '",'
			       + '"fullName" : "' + this.getFullName() + '",'
			       + '"groups" : "' + this.getGroups() + '"'
			       + '}';
		}
		
		return {
			getEntities : getEntities,
			getBranchLocation : getBranchLocation,
			setBranchLocation : setBranchLocation,
			getGivenName : getGivenName,
			getSn : getSn,
			getTitle : getTitle,
			getSAMAccountName : getSAMAccountName,
			getMail : getMail,
			getDistinguishedName : getDistinguishedName,
			getFullName : getFullName,
			getBranch : getBranch,
			getPhysicalDeliveryOfficeName : getPhysicalDeliveryOfficeName,
			getGroups : getGroups,
			getUserId : getUserId,
			isAdmin : isAdmin,
			isPlanOwnerType : isPlanOwnerType,
			setUserProfile : setUserProfile,
			getUserType : getUserType,
			getUserTypeName : getUserTypeName,
			setUserType : setUserType,
			getSelectedUserTypeName : getSelectedUserTypeName,
			setSelectedUserTypeName : setSelectedUserTypeName,
			setIsMock : setIsMock,
			getIsMock : getIsMock,			
			toString : toString,
			getEntity : getEntity,
			getUserProfile : getUserProfile,
			getSelectedEntity : getSelectedEntity,
			setSelectedEntity : setSelectedEntity,
			canEdit : canEdit,
			getMockUser : getMockUser,
			getBranchList : getBranchList,
			hasBranch : hasBranch,
			isBeta : isBeta
		};
	}

export default UserProfile;