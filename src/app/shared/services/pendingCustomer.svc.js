
	
	PendingCustomerService.$inject = [
		'pendingCustomerResource',
		'utilService',
		'userProfile'
	]
	
	function PendingCustomerService(
		pendingCustomerResource,
		utilService,
		userProfile) {
		
		var vm = this;
		
		vm.submitCustomer = function(newCustomer) {
			
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.pendingCustomer = newCustomer;
			pendingCustomerResourceInst.pendingCustomer.submitterUserName = userProfile.getSAMAccountName();
			pendingCustomerResourceInst.pendingCustomer.submitterName = userProfile.getFullName();
			pendingCustomerResourceInst.branch = newCustomer.branch ? newCustomer.branch : undefined;
			pendingCustomerResourceInst.marketSegment = newCustomer.marketSegment ? newCustomer.marketSegment : undefined;
			
			return pendingCustomerResourceInst.$create().then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.deletePendingCustomer = function(pendingCustomerId) {
			
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
				
			return pendingCustomerResourceInst.$delete({pendingCustomerId: pendingCustomerId}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.rejectPendingCustomer = function(pendingCustomerId, reason) {
			
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.pendingCustomer = { reason: reason }
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
				
			return pendingCustomerResourceInst.$reject({pendingCustomerId: pendingCustomerId}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.notifyWrongBranch = function(pendingCustomerId) {
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
			return pendingCustomerResourceInst.$wrongBranch({pendingCustomerId: pendingCustomerId}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.getPendingCustomers = function() {
			return pendingCustomerResource.pendingCustomer.get().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.getPendingCustomersByCurrentUser = function() {
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
			return pendingCustomerResourceInst.$getByUser({userName: userProfile.getSAMAccountName()}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}

		vm.getPendingCustomerCountByCurrentUser = function() {
			
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();

			return pendingCustomerResourceInst.$postCountByUser().then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}

		vm.getPendingCustomerByPlanId = function(planId) {
			return pendingCustomerResource.pendingCustomer.getByPlanId({planId: planId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		vm.qualifyCustomer = function(container, pendingCustomerId) {
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			
			pendingCustomerResourceInst.plan = container.plan;
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
			return pendingCustomerResourceInst.$qualify({ pendingCustomerId: pendingCustomerId })
			.then(function(response) {
				return response;
			},
			function(error) {				
				throw error;
			});
		}
		
		
		vm.discardPendingCustomer = function(pendingCustomerId, reason) {
			
			var pendingCustomerResourceInst = new pendingCustomerResource.pendingCustomer;
			pendingCustomerResourceInst.pendingCustomer = { reason: reason }
			pendingCustomerResourceInst.securityProfile = userProfile.getUserProfile();
			
				
			return pendingCustomerResourceInst.$discard({pendingCustomerId: pendingCustomerId}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}

	}

export default PendingCustomerService;