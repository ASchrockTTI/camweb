
	
	ContactService.$inject = [
		'auditFactory',
		'contactResource',
		'utilService',
		'userProfile'
	];
	
	function ContactService(
		auditFactory,
		contactResource,
		utilService,
		userProfile
		) {
		
		utilService.getLogger().info('Entering: contactService.svc.js');
		
		var _customers = [];
		var _suppliers = [];
		var _delegates = [];
		var _owners = [];
		
		this.getCustomers = function() {
			return _customers;
		}
		
		this.getSuppliers = function() {
			return _suppliers;
		}
		
		this.getDelegates = function() {
			return _delegates;
		}		
		
		this.getOwners = function() {
			return _owners;
		}
		
		this.retrieveContactsWithOwnerDelegate = function(planId) {
			return contactResource.contact.query({planId: planId}).$promise.then(
				function(response) {
					
					var contacts = response.response;
					
					_customers = [];
					_delegates = [];
					
					angular.forEach(contacts, function(value, key) {						
						
						if (value.delegateType.delegateTypeId === 3) {
							_customers.push(value)
						} else {
							_delegates.push(value);
						}
						
					})
					
					return contacts;
				},
				function(error) {
					utilService.getLogger().error('Contact Service: retrieveContactsWithOwnerDelegate Error...');
					throw error;
				}
			)
			
		}
		
		this.retrieveContacts = function(planId) {
			return contactResource.contact.query({planId: planId}).$promise.then(
				function(response) {
					
					var contacts = response.response;
					
					_customers = [];
					_suppliers = [];
					_delegates = [];
					_owners = [];
										
					angular.forEach(contacts, function(value, key) {
						
						var spaceIdx = value.delegateName.indexOf(" ");
						
						if (spaceIdx > 0) {							
							value.delegateFirstName = value.delegateName.substring(0, spaceIdx);
							value.delegateLastName = value.delegateName.substring(spaceIdx + 1, value.delegateName.length);							
						} else {
							value.delegateFirstName = value.delegateName;
						}
						
						switch(value.delegateType.delegateTypeId) {
						case 7:
							_suppliers.push(value);
							break;
						case 3:							
							_customers.push(value)
							break;
						case 2:
							_delegates.push(value);
							break;
						default:
							_owners.push(value);
						}
						
					})					
					
					return contacts;
				},
				function(error) {
					utilService.getLogger().error('Contact Service: retrieveContacts Error...');
					throw error;
				}
			)
			
		}
		
		this.addContact = function(planId, contactContainer) {
			
			var contactResourceInst = new contactResource.contact();
			
			switch(contactContainer.delegateTypeEnum) {
			case 'Customer':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_CUSTOMER', 'ADD');
				break;
			default :
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_TTI', 'ADD');
			}			
			
			contactResourceInst.delegateTypeEnum = contactContainer.delegateTypeEnum;
			contactResourceInst.planDelegate = contactContainer.planDelegate;
			contactResourceInst.securityProfile = contactContainer.securityProfile;
			
			if (contactContainer.auditLog) {
				contactResourceInst.auditLog = contactContainer.auditLog;
			}
			
			return contactResourceInst.$add({planId: planId}).then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('Contact Service: addContact Error...');
					throw error;
				});
				
		}
		
		this.updateContact = function(planId, contactContainer) {
			var contactResourceInst = new contactResource.contact();
			
			switch(contactContainer.delegateTypeEnum) {
			case 'Customer':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_CUSTOMER', 'UPDATE' );
				break;
			default :
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_TTI', 'UPDATE');				
			}	
			
			contactResourceInst.delegateTypeEnum = contactContainer.delegateTypeEnum;
			contactResourceInst.planDelegate = contactContainer.planDelegate;
			contactResourceInst.securityProfile = contactContainer.securityProfile;
		
			return contactResourceInst.$update({planId: planId}).then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('Contact Service: editContact Error...');
					throw error;
				});
		}
		
		this.removeContact = function(planId, delegateId, delegateTypeEnum) {
			
			var contactResourceInst = new contactResource.contact();
			
			switch(delegateTypeEnum) {
			case 'Customer':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_CUSTOMER', 'DELETE');
				break;
			case 'Supplier':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_SUPPLIER', 'DELETE');
				break;
			case 'tti':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_TTI', 'DELETE');
				break;
			case 'RSE':
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'RSE', 'DELETE');
				break;
			default :
				contactResourceInst.auditLog = auditFactory.createLog(planId, 'CONTACT_TTI', 'DELETE');				
			}
			
			return contactResourceInst.$remove({planId: parseInt(planId), delegateId: delegateId}).then(
				function(response) {
					
				},
				function(error) {
					utilService.getLogger().error('Contact Service: removeContact Error...');
					throw error;
				});
			
		}
		
		
		this.retrieveManufacturers = function() {
			return contactResource.contact.queryManufacturers().$promise.then(
				function(response) {
					
					return response.response;
					
				},
				function(error) {
					utilService.getLogger().error('Contact Service: retrieveManufacturers Error...');
					throw error;
				}
			)
			
		}
	}

export default ContactService;