
	
	OverviewService.$inject = [
		'userProfile',
		'utilService',
		'overviewResource',
	];
	
	function OverviewService(
		userProfile,
		utilService,
		overviewResource
		) {
		
		var chance = new Chance();
		
		var charPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var digitPool = '0123456789';
		
		
		
		this.getSummaries =  function(){
			var summaries = [];
			
			this.prevYearTotal = 0;
			this.currentYearTotal = 0;
			this.goal = 0;
			this.currentYearRR = 0;
			
			for(var i = 0; i < 15; i++) {
				
				var regionSummary = {
					name: chance.string({length: 3, pool: charPool}),
					prevYearTotal: 0,
					goal: 0,
					currentYearTotal: 0,
					currentYearRR: 0,  
					summaries: []
				}
				
				for(var j = 0; j < chance.integer({min: 2, max: 5}); j++) {
					var branchSummary = {
						name: chance.string({length: 2, pool: charPool}),
						prevYearTotal: 0,
						goal: 0,
						currentYearTotal: 0,
						currentYearRR: 0,  
						summaries: []
					}
					
					for(var k = 0; k < chance.integer({min: 1, max: 5}); k++) {
						var osrSummary = {
							name: chance.name({nationality: 'it'}),
							prevYearTotal: 0,
							goal: 0,
							currentYearTotal: 0,
							currentYearRR: 0,  
							summaries: []
						}
						
						for(var l = 0; l < chance.integer({min: 1, max: 5}); l++) {
							var accountSummary = {
								name: chance.string({length: 3, pool: charPool}) + chance.string({length: 3, pool: digitPool}),
								prevYearTotal: 0,
								goal: 0,
								currentYearTotal: 0,
								currentYearRR: 0,  
								dRegs: []
							}
							
							
							var dRegCount = chance.integer({min: 1, max: 3});
							for(var m = 0; m < dRegCount; m++) {
								var dReg = {
									dRegNumber: chance.string({ length: 10, pool: (charPool + digitPool)})
								}
								accountSummary.dRegs.push(dReg);
							}
								
							accountSummary.currentYearTotal = dRegCount;
							accountSummary.currentYearRR = Math.ceil(dRegCount * 200/90);
							accountSummary.prevYearTotal = chance.integer({min: 0, max: 6});
							accountSummary.goal = Math.ceil(accountSummary.prevYearTotal * 1.5);
							
							osrSummary.summaries.push(accountSummary);
							osrSummary.prevYearTotal += accountSummary.prevYearTotal;
							osrSummary.goal += accountSummary.goal;
							osrSummary.currentYearTotal += accountSummary.currentYearTotal;
							osrSummary.currentYearRR += accountSummary.currentYearRR;
						}
						branchSummary.summaries.push(osrSummary);
						branchSummary.prevYearTotal += osrSummary.prevYearTotal;
						branchSummary.goal += osrSummary.goal;
						branchSummary.currentYearTotal += osrSummary.currentYearTotal;
						branchSummary.currentYearRR += osrSummary.currentYearRR;
					}
					regionSummary.summaries.push(branchSummary);
					regionSummary.prevYearTotal += branchSummary.prevYearTotal;
					regionSummary.goal += branchSummary.goal;
					regionSummary.currentYearTotal += branchSummary.currentYearTotal;
					regionSummary.currentYearRR += branchSummary.currentYearRR;
				}
				summaries.push(regionSummary);
				this.prevYearTotal += regionSummary.prevYearTotal;
				this.goal += regionSummary.goal;
				this.currentYearTotal += regionSummary.currentYearTotal;
				this.currentYearRR += regionSummary.currentYearRR;
			}
			return summaries;
		}
		
		this.getPrevYearTotal = function(){
			return this.prevYearTotal;
		};
		this.getCurrentYearTotal = function(){
			return this.currentYearTotal;
		};
		this.getGoal = function(){
			return this.goal;
		};
		this.getCurrentYearRR = function(){
			return this.currentYearRR;
		};
		
		this.summary = {
			queryByEntity: function() {
				return overviewResource.summary.queryByEntity({entity: userProfile.getEntity(), userName: userProfile.getFullName()}).$promise.then(
					function(response) {					
						return summaries;
					}, function(error) {
						utilService.getLogger().error('OverviewService: summary.queryByEntity() error...');
						throw error
					}
				)
			}
		}
		
		
	}

export default OverviewService;