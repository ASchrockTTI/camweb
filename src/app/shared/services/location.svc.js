
	
	LocationService.$inject = [
		'utilService',
		'locationResource',
		'userProfile'
	];
	
	function LocationService(
		utilService,
		locationResource,
		userProfile) {
		
		var planLocationSpendContainer = {};
		
		function getNewPlanLocationSpendContainer() {
			
			return {
				planLocationSpend: {},
				securityProfile: {},
				addedAccounts: [],
				removedAccounts: [],
				eventLog: {}
			}
			
		}
		
		function addLocation(planId, container) {
			var locationResourceInst = new locationResource.locations;
			
			locationResourceInst.planLocationSpend = container.planLocationSpend;
			locationResourceInst.securityProfile = container.securityProfile;
			locationResourceInst.addedAccounts = container.addedAccounts;
			locationResourceInst.removedAccounts = container.removedAccounts;
			locationResourceInst.eventLog = container.eventLog;
			locationResourceInst.eventTypeEnum = container.eventTypeEnum;
			
			return locationResourceInst.$add({planId: planId})
			.then(function(response) {
				utilService.getLogger().info('Location Service: Location added successfully...');
				return;
			},function(error) {
				utilService.getLogger().error('Location Service: addLocation error...');
				throw error;
			});
		}
		
		function mergeLocation(planId, container) {
			
			return addLocation(planId, container)
			.then(function(response) {				
				
			})
			.catch(function(error) {
				throw error;
			})			
			
		}
		
		function updateLocation(planId, container) {
			var locationResourceInst = new locationResource.locations;
			
			locationResourceInst.planLocationSpend = container.planLocationSpend;
			locationResourceInst.securityProfile = container.securityProfile;
			locationResourceInst.addedAccounts = container.addedAccounts;
			locationResourceInst.removedAccounts = container.removedAccounts;
			locationResourceInst.eventLog = container.eventLog;
			locationResourceInst.eventTypeEnum = container.eventTypeEnum;
			
			return locationResourceInst.$update({planId: planId})
			.then(function(response) {
				utilService.getLogger().info('Location Service: Location updated successfully...');
				return;
			},
			function(error) {
				utilService.getLogger().error('Location Service: updateLocation error...');
				throw error;
			});
		}
		
		function deleteLocation(planId, planLocationSpendId, container) {
			
			if ( !window.confirm('Are you sure you want to delete this site location?') ) {
				return;
			}
			
			return deleteLocationNoConfirmation(planId, planLocationSpendId, container)
			.then(function(response) {
				utilService.getLogger().info('Location Service: Location deleted successfully...');
				return;
			},
			function(error) {
				utilService.getLogger().error('Location Service: deleteLocation error...');
				throw error;
			});
		}
		
		function deleteLocationNoConfirmation(planId, planLocationSpendId, container) {
						
			var locationResourceInst = new locationResource.locations;			
			locationResourceInst.eventLog = container.eventLog;
			locationResourceInst.eventTypeEnum = container.eventTypeEnum;
			
			return locationResourceInst.$delete({planId: planId, planLocationSpendId: planLocationSpendId})
			.then(function(response) {
				utilService.getLogger().info('Location Service: Location deleted successfully...');
				return;
			},
			function(error) {
				utilService.getLogger().error('Location Service: deleteLocation error...');
				throw error;
			});
		}
		
		return {
			getNewPlanLocationSpendContainer : getNewPlanLocationSpendContainer,		
			addLocation : addLocation,
			updateLocation : updateLocation,
			deleteLocation: deleteLocation,
			mergeLocation : mergeLocation,
			deleteLocationNoConfirmation : deleteLocationNoConfirmation
		}
		
		
	}

export default LocationService;