
	
	planService.$inject = [
		'planResource',
		'utilService',
		'userProfile'];
	
	function planService(
			planResource, 
			utilService, 
			userProfile) {
		
		var plans 		 = []; // default empty array
		var nonTTIPlans = [];
		var seelctedPlan = {}; // default empty object
		var planIndex 	 = 0;  // default to first element
		
		var planStatusEnum = {
			PENDING: 1,
			APPROVED: 2,
			DENIED: 3,
			ARCHIVED: 4
		}
		
		var retrieveYearlyArchiveB = false;
		var selectedYear = new Date().getFullYear();
		var selectedIndex = undefined;
		var planArchiveYears = [];
		var planYears = [];
		var planIds = [];
		
		var sessionArchivePlanIds 		= 'sessionArchivePlanIds';
		var sessionArchivePlanYears 	= 'sessionArchivePlanYears';
		var sessionArchiveSelectedIndex = 'sessionArchiveSelectedIndex';
		
		function getRetrieveYearlyArchiveB() {
			return retrieveYearlyArchiveB;
		}
		
		function setRetrieveYearlyArchiveB(bRetrieveYearlyArchiveB) {
			retrieveYearlyArchiveB = bRetrieveYearlyArchiveB;
		}
		
		// function isNotCurrentYear() {
		// 	return !(this.isCurrentYear());
		// }
		
		// function isCurrentYear() {
		// 	return(new Date().getFullYear() === this.getSelectedYear() ? true : false);
		// }
		
		function getSelectedPlanId() {
			if (utilService.isEmpty(selectedIndex) || utilService.isEmptyArray(planIds)) {
				planIds       = JSON.parse(sessionStorage.getItem(sessionArchivePlanIds));
				selectedIndex = JSON.parse(sessionStorage.getItem(sessionArchiveSelectedIndex));
			}
			
			return planIds[selectedIndex];
		}
		
		function getPlanStatusPending() {
			return planStatusEnum.PENDING;
		}
		
		function getPlanStatusApproved() {
			return planStatusEnum.APPROVED;
		}
		
		function getPlanStatusDenied() {
			return planStatusEnum.DENIED;
		}
		
		function getPlanStatusArchived() {
			return planStatusEnum.ARCHIVED;
		}
		
		function getNewPlanContainer() {
			return {
				plan: {},
				securityProfile: {},
				mockingProfile: {}
			}
		}
		
		function getBlog() {
			return utilService.isEmpty(getPlan().blogEntry) ? '' : getPlan().blogEntry;
		}
		
		function getAccountManager() {
			return utilService.isEmpty(getPlan().acctManager) ? '' : getPlan().acctManager;
		}
		
		function getSpendByEntities() {
			return utilService.isEmptyArray(getPlan().spendByEntities) ? [] : getPlan().spendByEntities;
		}
		
		function getTotalEntity() {
			return utilService.isEmpty(getPlan().totalEntity) ? {} : getPlan().totalEntity;
		}
				
		function getBlogEntryDate() {
			return getPlan().blogDateTime;
		}
		
		function setPlanIndex(index) {
			planIndex = index;
		}
		
		function getPlanIndex() {
			return planIndex;
		}
		
		function getPlanId() {
			return getPlan().planId;
		}
		
		function setPlan(plan) {
			seelctedPlan = plan;
			var i;
			if (utilService.isNotEmptyArray(seelctedPlan.planYears)) {
				selectedIndex = 0;
				planArchiveYears = [];
				planYears = [];
				planIds = [];
				for (i = 0; i < seelctedPlan.planYears.length; i++) {
					planArchiveYears.push(seelctedPlan.planYears[i]);
					planYears.push(seelctedPlan.planYears[i].planYear);
					planIds.push(seelctedPlan.planYears[i].planId);
				}
				
				if (!retrieveYearlyArchiveB) {
					// remove from session storage
					sessionStorage.removeItem(sessionArchivePlanIds);
					sessionStorage.removeItem(sessionArchivePlanYears);
					sessionStorage.removeItem(sessionArchiveSelectedIndex);
					// set to session storage
					sessionStorage.setItem(sessionArchivePlanIds, JSON.stringify(planIds));
					sessionStorage.setItem(sessionArchivePlanYears, JSON.stringify(planYears));
					sessionStorage.setItem(sessionArchiveSelectedIndex, planYears.length - 1);
				}
				
				retrieveYearlyArchiveB = false;
				
				// testing purpose
				var theArchivedPlans = this.getPlanArchiveYears();
				var thePlanIds = this.getPlanIds();
				var thePlanYears = this.getPlanYears();
				var theSelectedIndex = this.getSelectedIndex();
				var testingPlanIds   = JSON.parse(sessionStorage.getItem(sessionArchivePlanIds));
				var testingPlanYears = JSON.parse(sessionStorage.getItem(sessionArchivePlanYears));
				var testingSelectedIndex = JSON.parse(sessionStorage.getItem(sessionArchiveSelectedIndex));
			}
		}
		
		function getSelectedIndex() {
			return selectedIndex;
		}
		
		function setSelectedIndex(sIndex) {
			selectedIndex = sIndex;
			sessionStorage.removeItem(sessionArchiveSelectedIndex);
			sessionStorage.setItem(sessionArchiveSelectedIndex, selectedIndex);
		}
		
		function getPlan() {
			return seelctedPlan;
		}
		
		function getPlanYears() {			
			
			if (utilService.isEmptyArray(planYears)) {
				planYears = JSON.parse(sessionStorage.getItem(sessionArchivePlanYears));
			}
			
			return planYears;
		}
		
		function setPlanYears(pYears) {
			planYears = pYears;
		}
		
		function getPlanIds() {
			if (utilService.isEmptyArray(planIds)) {
				planIds = JSON.parse(sessionStorage.getItem(sessionArchivePlanIds));
			}
			return planIds;
		}
		
		function setPlanIds(PIds) {
			planIds = pIds;
		}
		
		function setPlanArchiveYears(thePlans) {
			planArchiveYears = thePlans;
		} 
		
		function getPlanArchiveYears() {
			return planArchiveYears;
		}
				
		function getSelectedYear() {
//			if (utilService.isEmpty(selectedIndex) || utilService.isEmptyArray(planYears)) {
				planYears       = JSON.parse(sessionStorage.getItem(sessionArchivePlanYears));
				selectedIndex = JSON.parse(sessionStorage.getItem(sessionArchiveSelectedIndex));
//			}
				
			if (planYears) {
				return planYears[selectedIndex];
			}
			
		}
		
		function setSelectedYear(sYear) {
			selectedYear = sYear;
		}
		
		function setPlans(thePlans) {
			plans = thePlans;
		}
		
		function getPlans() {
			return plans;
		}

		function setNonTTIPlans(theNonTTIPlans) {
			nonTTIPlans = theNonTTIPlans;
		}
		
		function getNonTTIPlans() {
			return nonTTIPlans;
		}
		
		function addPlan(container) {
			
			var planResourceInst = new planResource.plan;
			
			planResourceInst.plan = container.plan;
			planResourceInst.securityProfile = container.securityProfile;
			planResourceInst.mockingProfile = container.mockingProfile;
			
			return planResourceInst.$savePlan()
			.then(function(response) {
				return response;
			},
			function(error) {				
				throw error;
			});
			
		}
		
		function updatePlanBlog(planId, container) {
			
			var planResourceInst = new planResource.plan;
			
			planResourceInst.mainPageContainer = {
				plan: container.plan,
				securityProfile: container.securityProfile,
				mockingProfile: container.mockingProfile,
				auditLog: container.auditLog
			};
			
//			planResourceInst.plan = container.plan;
//			planResourceInst.securityProfile = container.securityProfile;
//			planResourceInst.mockingProfile = container.mockingProfile;
			
			return planResourceInst.$updateBlog({planId: planId})
			.then(function(response) {
				utilService.getLogger().info('planService: updatePlanBlog successful...');
				return;
			},
			function(error) {				
				throw error;
			});		
		}
		
		function lookupPlan(planId) {
			return planResource.plan.getPlanByPlanId({planId:planId}).$promise.then(
				function(response) {					
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('planService: lookupPlan Error');
					throw error;
				}
			)
		}
		
		function getBusinessPlan(planId) {
			return planResource.plan.getBusinessPlanByPlanId({planId:planId}).$promise.then(
					function(response) {					
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('planService: getBusinessPlan Error');
						throw error;
					}
				)
			}
		
		function findRelatedPlans(planId) {
			return planResource.plan.findRelatedPlans({planId:planId}).$promise.then(
				function(response) {					
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('planService: findRelatedPlans Error');
					throw error;
				}
			)			
		}
		
		function retrievePlans(userName, type) {
			//TODO: Remove deprecated allPlans
			return planResource.plan.query({userName:userName, type:type, allPlans: false}).$promise.then(
				function (response) {
					
					var plans = response.response.plans;
					var filteredPlans = [];

					if(plans && plans.length > 0) {

						filteredPlans = plans.filter( function(plan)  {
							return plan.planType == undefined || plan.planType.planTypeId != 6;
						});

						plans = filteredPlans;
					}

					if(plans && plans.length == 0) {
						response.response.plans = undefined;
					} else {
						response.response.plans = plans;
					}
					
					setPlans(plans);
					return response.response;

				},
				function (error) {
					throw error;
				}
			)
		}

		function retrieveNonTTICustomers(userName, type) {
			//TODO: Remove deprecated allPlans
			return planResource.plan.nonTTI({userName:userName, type:type, allPlans: false}).$promise.then(
				function (response) {
					
					if(response.response.plans && response.response.plans.length == 0) {
						response.response.plans = undefined;
					}

					setNonTTIPlans(response.response.plans);
					//console.log('settingNonTTI Plans: ', response.response.plans);
					return response.response;
				},
				function (error) {
					throw error;
				}
			)
		}
		
		function getTopLevelUserPlanInfo(userName) {
			return planResource.plan.getTopLevelUserPlanInfo({userName:userName}).$promise.then(
				function (response) {
					console.log('response @ plan Level: ', response);
					return response;
				},
				function (error) {
					throw error;
				}
			)
		}
		
		function updatePlan(planId, container) {
			
			var planResourceInst = new planResource.plan;			
			
			planResourceInst.plan = container.plan;
			planResourceInst.securityProfile = container.securityProfile;
			planResourceInst.mockingProfile = container.mockingProfile;
			planResourceInst.auditLog = container.auditLog;
			
			return planResourceInst.$updatePlan({planId:planId})
			.then(function() {
				if (sessionStorage.getItem('sessionPlans')) {
					var plans = JSON.parse(sessionStorage.getItem('sessionPlans'));
					var lv=-1;
					for(lv=0; lv<plans.length; lv++) {
						if(plans[lv].planId === container.plan.planId) {
							break;
						}
					}
					if(lv>-1) {
						if(container.plan.acctManager && container.plan.acctManager.lastIndexOf(" (") > 0) {
						    var n = container.plan.acctManager.lastIndexOf(" (");
							container.plan.acctManager = container.plan.acctManager.substring(0, n)
						}
					    
						plans[lv] = container.plan;
					}
					sessionStorage.setItem('sessionPlans', JSON.stringify(plans));
				}

				utilService.getLogger().info('planService: updatePlan successful...');
				return;
			},
			function(error) {
				utilService.getLogger().error('planService: updatePlan error...');
				utilService.getLogger().error(error);
				throw error;
			});
		}
		
		function updateStatus(planId, fullName, status) {
			
			var planResourceInst = new planResource.plan;
			
			planResourceInst.fullName = fullName;
			
			return planResourceInst.$updateStatus({planId: planId, planStatus: status}).then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('planService: updateStatus error...');
					throw error;
				}
			)
		}
		
		function deletePlan(planId, isDeletePlan, fullName) {
			
			var planResourceInst = new planResource.plan;
			
			return planResourceInst.$deletePlan({planId: planId, isDeletePlan: isDeletePlan, fullName: fullName}).then(
				function(response) {
					
				},
				function(error) {
					utilService.getLogger().error('planService: deletePlan error...');
					throw error;
				}
			)
		}
		
		function discardPlan(container) {
			var planResourceInst = new planResource.plan;
			
			planResourceInst.plan = container.plan;
			planResourceInst.securityProfile = container.securityProfile;
			
			return planResourceInst.$discardPlan()
			.then(function(response) {
				return response;
			},
			function(error) {				
				throw error;
			});
		}
		
		function getMarketSegments() {
			
			return planResource.plan.getMarketSegments().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			);
		}
		
		function getMarketSegmentFilters() {
			
			return planResource.plan.getMarketSegmentFilters().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			);
		}
		
		function getParentBranchList() {
			return planResource.plan.getParentBranchList().$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						throw error;
					}
				)
		}
		
		function getBranches() {
			return planResource.plan.getBranches().$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function getEntityParentBranches(entity) {
			return planResource.plan.getEntityParentBranches({entity: entity}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function classifyCustomer(planId, classification) {
			var planResourceInst = new planResource.plan;
			planResourceInst.planId = planId;
			planResourceInst.administrative = classification.administrative;
			planResourceInst.engineering = classification.engineering;
			planResourceInst.purchasing = classification.purchasing;
			planResourceInst.manufacturing = classification.manufacturing;
			planResourceInst.closed = classification.closed;
			
			return planResourceInst.$classifyCustomer({planId: planId}).then(
					function(response) {
						return response.response;
					},
					function(error) {
						throw error;
					}
				)
		}
		
		function archivePlan(planId, reason) {
			var planResourceInst = new planResource.plan;
			planResourceInst.planId = planId;
			planResourceInst.userId = userProfile.getUserId();
			planResourceInst.reason = reason;
			
			return planResourceInst.$archivePlan().then(
					function(response) {
						return response.response;
					},
					function(error) {
						throw error;
					}
				)
		}
		
		
		return {
			setPlan  	  		: setPlan,
			getPlan  	  	 	: getPlan,
			getBusinessPlan     : getBusinessPlan,
			findRelatedPlans    : findRelatedPlans,
			getPlanId			: getPlanId,
			setPlans	  	 	: setPlans,
			getPlans	  	 	: getPlans,
			getNonTTIPlans      : getNonTTIPlans,
			setNonTTIPlans      : setNonTTIPlans,
			addPlan 			: addPlan,
			retrievePlans 	 	: retrievePlans,
			retrieveNonTTICustomers: retrieveNonTTICustomers,
			getTopLevelUserPlanInfo : getTopLevelUserPlanInfo,
			getBlog		  	 	: getBlog,
			getBlogEntryDate	: getBlogEntryDate,
			getAccountManager	: getAccountManager,
			getSpendByEntities	: getSpendByEntities,
			getTotalEntity      : getTotalEntity,
			setPlanIndex 		: setPlanIndex,
			getPlanIndex 		: getPlanIndex,
			updatePlanBlog		: updatePlanBlog,
			lookupPlan 			: lookupPlan,
			getNewPlanContainer : getNewPlanContainer,
			updatePlan          : updatePlan,
			updateStatus        : updateStatus,
			getPlanStatusPending : getPlanStatusPending,
			getPlanStatusApproved : getPlanStatusApproved,
			getPlanStatusDenied : getPlanStatusDenied,
			getPlanStatusArchived : getPlanStatusArchived,
			deletePlan : deletePlan,
			discardPlan : discardPlan,
			getPlanArchiveYears : getPlanArchiveYears,
			setPlanArchiveYears : setPlanArchiveYears,
			getSelectedYear : getSelectedYear,
			setSelectedYear : setSelectedYear,
			getPlanYears : getPlanYears,
			setPlanYears : setPlanYears,
			getPlanIds : getPlanIds,
			setPlanIds : setPlanIds,
			getSelectedIndex : getSelectedIndex,
			setSelectedIndex : setSelectedIndex,
			getSelectedPlanId : getSelectedPlanId,
			// isCurrentYear : isCurrentYear,
			// isNotCurrentYear : isNotCurrentYear,
			getRetrieveYearlyArchiveB : getRetrieveYearlyArchiveB,
			setRetrieveYearlyArchiveB : setRetrieveYearlyArchiveB,
			getMarketSegments : getMarketSegments,
			getMarketSegmentFilters : getMarketSegmentFilters,
			getParentBranchList: getParentBranchList,
			getBranches : getBranches,
			getEntityParentBranches : getEntityParentBranches,
			classifyCustomer : classifyCustomer,
			archivePlan : archivePlan
		}
	}

export default planService;