
	
	TacticService.$inject = [
		'tacticResource',
		'utilService',
		'userProfile'
	]
	
	function TacticService(
			tacticResource,
			utilService,
			userProfile) {

		var planId = 1;
		var planStrategyId = 0;
		
		var tactics = [];
		var selectedTactic = {};
		var tacticIndex = 0;
		
		var removeDelegateId = 0;
		var assignDelegateId = 1;
		
		var assignDelegates = [];
		var removeDelegates = [];
		
		var removedTacticDelegates = [];
		
		var planTactic = {};
		var planTacticDescription = {};
		var dueDate = undefined;
		
		var planTacticContainer = {};

		var tacticResourceInstance = {};
		
		var tacticsIndex = undefined;
		var updateTacticsAction = false;
		var addTacticsAction = false;
		
		var isRetrieveTacticsDone = false;
		
		var Actions = [
			{id: removeDelegateId, show: true, toolTip: "User marked for removal",    classContent: 'text-danger fa fa-user-times'},
			{id: assignDelegateId, show: true, toolTip: "User marked for assignment", classContent: 'text-primary fa fa-user-plus'}
			];
		
		function getActions() {
			return Actions;
		}
		
		function getRemoveDelegateId() {
			return removeDelegateId;
		}
		
		function getAssignDelegateId() {
			return assignDelegateId;
		}
		
		function retrieveTactics(planId, strategyId) {
			return tacticResource.tactic.query({planId:planId, planStrategyId:strategyId}).$promise.then(
				function(response) {
					var tactics = response.response;
					
					for (var i = 0; i < tactics.length; i++) {
						
						var tacticDelegates = tactics[i].planTacticDelegates
						var count = 0;
						
						for (var j = 0; j < tacticDelegates.length; j++) {
							if (tacticDelegates[j].planTacticComplete == true) {
								count++;
							}
						}
						
						if (count === tacticDelegates.length) {
							tactics[i].planStatus = 'complete';
						}
						
						if (count > 0 && count < tacticDelegates.length) {
							tactics[i].planStatus = 'in progress';
						} else if (count === 0) {
							tactics[i].planStatus = undefined;
						}
						
					}
					
					return tactics;
					
				},
				function(error) {
					utilService.getLogger().error('tacticService: retrievingTactics Error.');
					throw error;
				}
			)
		}
		
		function setAssignDelegates(asgnDelegates) {
			assignDelegates = asgnDelegates;
		}
		
		function getAssignDelegates() {
			return assignDelegates;
		}
		
		function setRemoveDelegates(rmvDelegates) {
			removeDelegates = rmvDelegates;
		}
		
		function getRemoveDelegates() {
			return removeDelegates;
		}
		
		function setPlanTactic(pTactic) {
			planTactic = pTactic;
		}
		
		function getPlanTactic() {
			return planTactic;
		}
		
		function setDueDate(dDate) {
			dueDate = dDate;
		}
		
		function getDueDate() {
			return dueDate;
		}
		
		function setPlanTacticContainer(theTactic) {
			planTacticContainer = theTactic;
		}
		
		function getPlanTacticContainer() {
			return planTacticContainer;
		}
		
		function setUpRemovedTacticDelegates() {
			removedTacticDelegates = [];
			if (utilService.isNotEmptyArray(removeDelegates)) {
				var i;
				for (i = 0; i < removeDelegates.length; i++) {
				    var participant = {};
				    participant.fullName = removeDelegates[i].fullName;
				    //PlanTacticDelegate.planTacticDelegateId = removeDelegates[i].planTacticDelegateId;
				    removedTacticDelegates.push(participant);
				}
			}
		}
		
		function setUpPlanTacticContainer() {
			planTacticContainer = {};
			planTacticContainer.planTactic 				= planTactic;
			planTacticContainer.planTactic.dueDate 		= dueDate;
			planTacticContainer.planTactic.planTactic	= planTacticDescription;
			planTacticContainer.securityProfile 		= userProfile.getUserProfile();
			planTacticContainer.addedTacticDelegates	= assignDelegates;
			setUpRemovedTacticDelegates();
			planTacticContainer.removedTacticDelegates = removedTacticDelegates;
		}
		
		function createUpdateTacticsResource() {
			tacticResourceInstance = new tacticResource.tactic;
			tacticResourceInstance.planTactic 				= planTacticContainer.planTactic;
			tacticResourceInstance.securityProfile			= planTacticContainer.securityProfile;
			tacticResourceInstance.removedTacticDelegates	= planTacticContainer.removedTacticDelegates;
			tacticResourceInstance.addedTacticDelegates   	= planTacticContainer.addedTacticDelegates;
			tacticResourceInstance.auditLog = [];
			return tacticResourceInstance;
		}
		
		function setPlanId(id) {
			planId = id;
		}
		
		function getPlanId() {
			return planId;
		}
		
		function setPlanStrategyId(id) {
			planStrategyId = id;
		}
		
		function getPlanStrategyId() {
			return planStrategyId;
		}
		
		function setAuditLog(planId, action, feature) {
			
			var auditLog = [];
			
			auditLog.push({
				planId: planId,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
			
			return auditLog;
		}
		
		function addPlanTactic() {
			
			tacticResourceInstance.auditLog = setAuditLog(planId, 'ADD', 'TACTIC');
			
			return tacticResourceInstance.$addTactic({planId: planId, planStrategyId: planStrategyId})
			.then(function(response) {
			},
			function(error) {
				utilService.getLogger().error('tacticService: addTactic Error');
				utilService.getLogger().error(error);
			});

		}
		
		function updatePlanTactic() {
			
			tacticResourceInstance.auditLog = setAuditLog(planId, 'UPDATE', 'TACTIC');
			
			return tacticResourceInstance.$updateTactic({planId: planId, planStrategyId: planStrategyId})
			.then(function(response) {
			},
			function(error) {
				utilService.getLogger().error('tacticService: updateTactic Error');
				utilService.getLogger().error(error);
			});
		}
		
		function updatePlanTactics() {
			if (utilService.isEmpty(tacticResourceInstance.planTactic.planTacticId)) {
				addPlanTactic();				
			} else {
				updatePlanTactic();
			}
		}
		
		function updateSortOrders(planId, planStrategyId, container) {
			var tacticResourceInst = new tacticResource.tactic;
			
			tacticResourceInst.sortOrders = container.sortOrders;
			tacticResourceInst.securityProfile = container.securityProfile;			
			
			return tacticResourceInst.$updateTactic({planId: planId, planStrategyId: planStrategyId})
			.then(function(response) {				
			},
			function(error) {
				utilService.getLogger().error('tacticService: updateSortOrders Error');
				utilService.getLogger().error(error);
			});
		}

		function setPlanTacticDescription(pTacticDescription) {
			planTacticDescription = pTacticDescription;
		}
		
		function getPlanTacticDescription() {
			return planTacticDescription;
		}
		
		function isAddPlanTactics() {
			return utilService.isEmpty(tacticResourceInstance.planTactic.planTacticId);
		}
		
		function isUpdatePlanTactics() {
			return utilService.isNotEmpty(tacticResourceInstance.planTactic.planTacticId);
		}
		
		function removeTactic(planId, planStrategyId, planTacticId) {
			
			var tacticResourceInst = new tacticResource.tactic;
			
			tacticResourceInst.auditLog = setAuditLog(planId, 'DELETE', 'TACTIC');
			return tacticResourceInst.$remove({planId:planId, planStrategyId: planStrategyId, planTacticId: planTacticId}).then(
				function(response) {
					return response;
				},
				function(error) {
					utilService.getLogger().error('tacticService: removeTactic Error');
					throw error;
				}
			)
		}
		
		function isAddTacticsAction() {
			return addTacticsAction;
		}
				
		function setAddTacticsAction(addTacticsB) {
			addTacticsAction = addTacticsB;
		}

		function getTacticsIndex() {
			return tacticsIndex;
		}
				
		function setTacticsIndex(idx) {
			tacticsIndex = idx;;
		}
		
		
		function isUpdateTacticsAction() {
			return updateTacticsAction;
		}
				
		function setUpdateTacticsAction(updateTacticsB) {
			updateTacticsAction = updateTacticsB;
		}
		
		function findPlanTacticIndexByPlanTacticId(paramPlanId, paramPlanStrategyId, paramPlanTacticId) {
			isRetrieveTacticsDone = false;
			var foundTactic = false;
			if (utilService.isNotEmpty(paramPlanId) &&
				utilService.isNotEmpty(paramPlanStrategyId) &&	
				utilService.isNotEmpty(paramPlanTacticId)    ) {
				
				return retrieveTactics(paramPlanId, paramPlanStrategyId)
				.then(function(response) {
					
					if (utilService.isNotEmptyArray(response) ) {
						var i;
						
						angular.forEach(response, function(value, key) {
							
							if (value.planTacticId == parseInt(paramPlanTacticId)) {
								
								setTacticsIndex(key);
								
								foundTactic = true;								
							}
						})
						
						return foundTactic;
					}	

				},
				function(error) {
					utilService.getLogger().error('tacticService: findPlanTacticIndexByPlanTacticId Error');
					throw error;
				});
			}
		}
				
		function getRetrieveTacticsDone() {
			return isRetrieveTacticsDone;
		}
		
		
		function setRetrieveTacticsDone(setDone) {
			isRetrieveTacticsDone = setDone;
		}

		
		return {
			retrieveTactics : retrieveTactics,
			getActions		: getActions,
			getRemoveDelegateId : getRemoveDelegateId,
			getAssignDelegateId : getAssignDelegateId,
			setAssignDelegates : setAssignDelegates,
			getAssignDelegates : getAssignDelegates,
			setRemoveDelegates : setRemoveDelegates,			
			getRemoveDelegates : getRemoveDelegates,
			setPlanTactic : setPlanTactic,
			getPlanTactic : getPlanTactic,
			setDueDate : setDueDate,
			getDueDate : getDueDate,
			setPlanTacticContainer : setPlanTacticContainer,
			getPlanTacticContainer : getPlanTacticContainer,
			setUpPlanTacticContainer : setUpPlanTacticContainer,
			createUpdateTacticsResource : createUpdateTacticsResource,
			setPlanId : setPlanId,
			getPlanId : getPlanId,
			setPlanStrategyId : setPlanStrategyId,
			getPlanStrategyId : getPlanStrategyId,
			updatePlanTactic : updatePlanTactic,
			addPlanTactic : addPlanTactic,
			setPlanTacticDescription : setPlanTacticDescription,
			getPlanTacticDescription : getPlanTacticDescription,
			updatePlanTactics : updatePlanTactics,
			isAddPlanTactics : isAddPlanTactics,
			isUpdatePlanTactics : isUpdatePlanTactics,
			removeTactic : removeTactic,
			updateSortOrders : updateSortOrders,
			isAddTacticsAction : isAddTacticsAction,
			setAddTacticsAction : setAddTacticsAction,
			getTacticsIndex : getTacticsIndex,
			setTacticsIndex : setTacticsIndex,
			isUpdateTacticsAction : isUpdateTacticsAction,
			setUpdateTacticsAction : setUpdateTacticsAction,
			findPlanTacticIndexByPlanTacticId : findPlanTacticIndexByPlanTacticId,
			getRetrieveTacticsDone : getRetrieveTacticsDone,
			setRetrieveTacticsDone : setRetrieveTacticsDone
		}
	}

export default TacticService;