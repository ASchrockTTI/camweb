
	
	FamilyService.$inject = [
		'familyResource',
		'utilService'
	]
	
	function FamilyService(
		familyResource,
		utilService) {
		
		this.findFamilyByPlanId = function(planId) {
			
			return familyResource.family.query({planId: planId}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					throw error;
				}
			)
		}

	}

export default FamilyService;