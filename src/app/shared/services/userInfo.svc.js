
	
	userInfoService.$inject = [
		'utilService',
		'userInfoResource'
	];
	
	function userInfoService(
			utilService,
			userInfoResource) {
		
		function checkUserInfo() {
			return userInfoResource.userInfo.checkUserInfo().$promise.then(
				function(response) {
					
				},
				function(error) {
					throw error;
				}
			)
		}
		
		function retrieveUserInfo(userName) {			
			return userInfoResource.userInfo.getByUserName({userName:userName}).$promise.then(
				function (response) {
					return response;
				},
				function (error) {
					utilService.getLogger().error('userInfoService: retrievePlans Error' );
					throw error;
				}
			)
		}
		
		function retrieveUserInfoByLastName(lastName) {
			return userInfoResource.userInfo.getByLastName({lastName: lastName}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('userInfoService: retrieveUserInfoByLastName error...');
					throw error;
				}
			)
		}
		
		function retrieveOsrInfoByLastName(lastName) {
			return userInfoResource.userInfo.getOsrByLastName({lastName: lastName}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('userInfoService: retrieveOsrInfoByLastName error...');
					throw error;
				}
			)
		}
		
		function lookupCallReportCreatorsByLastName(lastName) {
			return userInfoResource.userInfo.lookupCallReportCreatorsByLastName({lastName: lastName}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('userInfoService: lookupCallReportCreatorsByLastName error...');
						throw error;
					}
				)
			}
		
		function getCallReportCreatorsByBranch(branch) {
			return userInfoResource.userInfo.getCallReportCreatorsByBranch({branch: branch}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('userInfoService: getCallReportCreatorsByBranch error...');
					throw error;
				}
			)
		}
		
		function retrieveTitleInfo(title) {
			return userInfoResource.userInfo.getByTitle({title: title}).$promise.then(
				function(response) {
					return response.response;
				},
				function(error) {
					utilService.getLogger().error('userInfoService: retrieveTitleInfo error...');
					throw error;
				}
			)
		}
		
		function getUserCorpIdList(username) {
			return userInfoResource.userInfo.getUserCorpIdList({username: username}).$promise.then(
					function(response) {
						return response.response;
					},
					function(error) {
						utilService.getLogger().error('userInfoService: getUserCorpIdList error...');
						throw error;
					}
				)
		}
		
		return {
			retrieveUserInfo : retrieveUserInfo,
			retrieveUserInfoByLastName : retrieveUserInfoByLastName,
			retrieveOsrInfoByLastName : retrieveOsrInfoByLastName,
			retrieveTitleInfo : retrieveTitleInfo,
			checkUserInfo: checkUserInfo,
			lookupCallReportCreatorsByLastName : lookupCallReportCreatorsByLastName,
			getCallReportCreatorsByBranch : getCallReportCreatorsByBranch,
			getUserCorpIdList : getUserCorpIdList
		}
	}

export default userInfoService;