
	
	PermissionsFactory.$inject = ['roles', 'userProfile', 'utilService'];
	
	function PermissionsFactory(roles, userProfile, utilService) {
		
		function permissionsValidator() {
			this.addRoles;
			this.editRoles;
			this.deleteRoles;
			this.permissions = [];
			this.plan;
		}
		
		var _plan;
		
		function setAddRoles(addRoles) {
			this.addRoles = addRoles;
			this.setCustomPermission('add', this.addRoles, undefined);
		}
		
		function setEditRoles(editRoles) {
			this.editRoles = editRoles;
			this.setCustomPermission('edit', this.editRoles, undefined);
		}
		
		function setDeleteRoles(deleteRoles) {
			this.deleteRoles = deleteRoles;
			this.setCustomPermission('delete', this.deleteRoles, undefined);
		}
		
		function setCustomPermission(action, roles, callBack, preventClear) {
			var permission = {};
			
			permission.action = action;
			permission.roles = roles;
			permission.callBack = callBack ? callBack : undefined;
			permission.preventClear = preventClear ? preventClear : undefined;
			
			this.permissions.push(permission);
			
		}
		
		function clearPermissions() {
			var permissions = this.permissions.filter(function(obj) {
				return obj.preventClear === true;
			});
			
			this.permissions = permissions;
		}
		
		function hasPermission(action, plan) {
			
			if(this.permissions.length === 0) {
				throw 'Permission Service Error: permissions must be greater than 0.';
			}
			
			if (!plan) {
				return false;
			}
			
			_plan = plan;
			
			return _hasPermission(this.permissions, action);
		}
		
		function runCustom(action) {
			var allow = false;
			
			if(this.permissions.length === 0) {
				throw 'Permission Service Error: permissions must be greater than 0.';
			}
			
			angular.forEach(this.permissions, function(value, key) {
				
				if (value.action === action.toLowerCase()) {
					if (value.callBack) {
						allow = value.callBack();
					}
				}
			});
			
			return allow;
		}
		
		function userTypeHasPermission(permission) {
			var roles = [];
			
			if (userProfile.getUserType() === undefined) {
				return false;
			}

			for(var i = 0; i < this.permissions.length; i++) {
				if(this.permissions[i].action === permission) {
					roles = this.permissions[i].roles;
					break;
				}
			}
			return roles.indexOf(userProfile.getUserType().toUpperCase()) > -1;
		}
		
		function _hasPermission(permissions, action) {
			
			var allow = false;
			
			if (userProfile.getUserType() === undefined) {
				return false;
			}
			
			angular.forEach(permissions, function(value, key) {
				if(allow === true) return true;
				
				if (value.action.toLowerCase() === action.toLowerCase()) {
					
					if (value.callBack) {
						allow = value.callBack();
					} else {
							
						for(var i = 0; i < value.roles.length; i++) {
							if(allow === true) return true;
							switch(value.roles[i]) {						
							case roles.ADMIN:
							case roles.EXEC:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {									
									allow = true;
								}	
								break;
							case roles.RSE:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {									
									if(utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.rseParticipant)
										|| 	utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.faeParticipant)
										|| 	utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.bdmParticipant)
										|| 	utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.pdmParticipant)
										|| 	utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.scmParticipant)) {
										allow = true;
									}
								}	
								break;	
							case roles.GM:
							case roles.ISM:
							case roles.FSM:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {								
									if (userProfile.getBranchList().length > 0) {
										if (userProfile.getBranchList().indexOf(_plan.branch) >= 0) {
											allow = true;
										}
									}
									
									if (utilService.equalsIgnoreCaseSpace(userProfile.getBranch(),_plan.branch)) {
										allow = true;
									}
								}
								break;
							case roles.OWNER:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getFullName(), _plan.acctManager)) {
									allow = true;
									break;
								}
								
								if (_plan.insideSalesPerson != null) {
									var isrList = _plan.insideSalesPerson.substring(0, _plan.insideSalesPerson.length).toLowerCase().trim().split(' ,');									
									var found = isrList.indexOf(userProfile.getFullName().toLowerCase());
									
									if (found >= 0) {
										allow = true;
									}
									
									break;
									
								}
								break;
							case roles.BRANCH_MEMBER:
								if (userProfile.getBranchList().length > 0) {
									if (userProfile.getBranchList().indexOf(_plan.branch) >= 0) {
										allow = true;
									}
								}
								
								if(utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), roles.EXEC)) allow = true;
								
								if (utilService.equalsIgnoreCaseSpace(userProfile.getBranch(),_plan.branch)) {
									allow = true;
								}
								break;
							default:
								if (utilService.equalsIgnoreCaseSpace(userProfile.getUserType(), value.roles[i])) {									
									allow = true;
								}
								else allow = false;
							}
						}					
					}
				}
			})
						
			return allow;
		}
		
		permissionsValidator.prototype.setAddRoles = setAddRoles;
		permissionsValidator.prototype.setEditRoles = setEditRoles;
		permissionsValidator.prototype.setDeleteRoles = setDeleteRoles
		permissionsValidator.prototype.setCustomPermission = setCustomPermission;
		permissionsValidator.prototype.clearPermissions = clearPermissions;
		permissionsValidator.prototype.hasPermission = hasPermission;
		permissionsValidator.prototype.runCustom = runCustom;
		permissionsValidator.prototype.userTypeHasPermission = userTypeHasPermission;
		permissionsValidator.prototype._hasPermission = _hasPermission;
		
		return {
			newInstance: function() {
				return new permissionsValidator();
			}
		}
		
	}

export default PermissionsFactory;