
	
	AuditFactory.$inject = ['userProfile'];
	
	function AuditFactory(userProfile) {
		
		var service = {};
		
		service.createLog = function(planId, action, feature) {
			
			var auditLog = [];
			
			auditLog.push({
				planId: planId,
				auditActionEnum: action,
				auditFeatureEnum: feature,
				modifiedBy: userProfile.getFullName()
			})
			
			return auditLog;
		}
		
		return service;
		
	}
	

export default AuditFactory;