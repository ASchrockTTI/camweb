import re
import os

componentsPath = './src/app/shared'


def modularize(filepath, filename, registrationFile):
    file = open(filepath, "r+")

    content = file.read()

    regexResult = re.search('\.module\(\'shared\'\)\s*(.+?);', content)
    

    content = re.sub('\A\s*\(function\(\)\s*\{\s*.*\s*angular\.module\(\'shared\'\)\s*.*;', '', content)
    content = re.sub('(\}\)\(\);)\s*\Z', '', content)
    content = re.sub('(\))\s*\Z', '', content)

    if regexResult:
        registration = regexResult.group(1) + '\n'
        moduleName = re.search('\s*(.*)\.', content)

        print(registration)
        registrationFile.write(registration)

        content = content + '\nexport default ' + moduleName.group(1) + ';'

        file.seek(0)
        file.write(content)
        file.truncate()
        file.close()


registrationFile = open('./sharedRegistrations.txt', "r+")

for subdir, dirs, files in os.walk(componentsPath):
    for filename in files:
        filepath = subdir + os.sep + filename

        if filepath.endswith(".js"):
            modularize(filepath, filename, registrationFile)
