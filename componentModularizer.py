import re
import os

componentsPath = './src/app/components'


def modularize(filepath, filename, registrationFile):
    file = open(filepath, "r+")

    content = file.read()

    regexResult = re.search('\.module\(\'scmApp\'\)\s*(.+?),', content)

    content = re.sub('\A\s*\(function\(\)\s*\{\s*.*\s*angular\.module\(\'scmApp\'\)\s*.*,', '', content)
    content = re.sub('(\}\)\(\);)\s*\Z', '', content)
    content = re.sub('(\))\s*\Z', '', content)

    moduleName = re.search('(.[^\.]*)\.', filename).group(1)

    if regexResult:
        registration = regexResult.group(1) + ', ' + moduleName + ')\n'
        print(registration)
        registrationFile.write(registration)

        content = 'const ' + moduleName + ' = ' + content
        content = content + '\nexport default ' + moduleName + ';'

        file.seek(0)
        file.write(content)
        file.truncate()
        file.close()


registrationFile = open('./componentRegistrations.txt', "r+")

for subdir, dirs, files in os.walk(componentsPath):
    for filename in files:
        filepath = subdir + os.sep + filename

        if filepath.endswith(".js"):
            modularize(filepath, filename, registrationFile)
