import re
import os

componentsPath = './src/app/components'


def modularize(filepath, filename):
    file = open(filepath, "r+")

    content = file.read()

    regexResult = re.search('templateUrl:\s*.*/(.+?)\.html', content)
    

    if regexResult:
        templateName = regexResult.group(1)
        importStatement = 'import ' + templateName + 'Template' + ' from \'./' + templateName + '.html\';\n\n'
        
        content = re.sub('templateUrl:[^,]*,', 'template: ' + templateName + 'Template,', content)

        content = importStatement + content

        file.seek(0)
        file.write(content)
        file.truncate()
        file.close()


for subdir, dirs, files in os.walk(componentsPath):
    for filename in files:
        filepath = subdir + os.sep + filename

        if filepath.endswith(".js"):
            modularize(filepath, filename)
